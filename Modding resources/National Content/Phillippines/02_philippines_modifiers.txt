PHI_islamism_dynamic_modifier = {
	icon = "GFX_idea_muslim1"
	enable = { always = yes }
	custom_modifier_tooltip = PHI_islamism_dynamic_modifier_tooltip

	fascism_drift = PHI_fascism_drift_effect
	stability_factor = PHI_stability_factor_effect
	political_power_factor = PHI_political_power_factor_effect
}

PHI_religious_dynamic_modifier = {
	icon = "GFX_idea_filipino_syncretism"
	enable = { always = yes }

	political_power_factor = PHI_syncretism_power_factor_effect
	stability_factor = PHI_syncretism_stability_factor_effect
	consumer_goods_factor = PHI_syncretism_consumer_goods_factor_effect
}