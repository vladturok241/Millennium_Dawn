GER_gdr_proxy_conflict_decisions_category = {
	icon = GFX_decision_category_power_struggle
	priority = 250
	picture = GFX_belarus_weapon_export
	allowed = {
			OR = {
				has_idea = SOV_warsaw_pact_idea
				has_idea = NATO_member
			}
		}
	visible = {
		GER = {
			has_country_flag = raskol
		}
		OR = {
			has_idea = SOV_warsaw_pact_idea
			has_idea = NATO_member
		}
	}
}

