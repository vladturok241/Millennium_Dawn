country_event = {
	id = sov_warsaw_pact.15
	title = Sov_warsaw_pact.15.t
	desc = Sov_warsaw_pact.15.d
	picture = GFX_warsaw_event1
	is_triggered_only = yes
	option = {
		name = Sov_warsaw_pact.15.a
		log = "[GetDateText]: [This.GetName]: Sov_warsaw_pact.15.a"
		leave_faction = yes
		SOV = {
			end_puppet = ROOT
		}
		add_popularity = {
			ideology = communism
			popularity = -0.25
		}
		clr_country_flag = warsaw_member_check
		set_country_flag = WP_normal_puppet
		clr_country_flag = target_wp_pl
		clr_country_flag = Bulgaria_target_wp
		clr_country_flag = Romania_target_wp
		clr_country_flag = Hungary_target_wp
		clr_country_flag = Albania_target_wp
		clr_country_flag = Czechia_target_wp
		clr_country_flag = Slovakia_target_wp
		clr_country_flag = sov_southern_front
		clr_country_flag = sov_northern_front
		ai_chance = {
			base = 40
		}

	}

	option = {
		name = Sov_warsaw_pact.15.b
		log = "[GetDateText]: [This.GetName]: Sov_warsaw_pact.15.b"
		SOV = {
			set_autonomy = {
				target = ROOT
				autonomy_state = autonomy_associated_state
				end_wars = no
				end_civil_wars = no
			}
		}
		clr_country_flag = warsaw_member_check
		clr_country_flag = warsaw_pact_reintegrate
		clr_country_flag = sov_southern_front
		clr_country_flag = sov_northern_front
		clr_country_flag = target_wp_pl
		clr_country_flag = Bulgaria_target_wp
		clr_country_flag = Romania_target_wp
		clr_country_flag = Hungary_target_wp
		clr_country_flag = Albania_target_wp
		clr_country_flag = Czechia_target_wp
		clr_country_flag = Slovakia_target_wp
		set_country_flag = WP_escaped
		ai_chance = {
			base = 60
		}

	}
}