focus_tree = {

	id = uganda_focus

	country = {
		factor = 0
		
		modifier = {
			add = 20
			tag = UGA
		}
	}

	shared_focus = EAC_east_african_community

	continuous_focus_position = { x = 25 y = 600 }

	focus = {
		id = UGA_ugandan_defense_force
		icon = forces_uga


		x = 4
		y = 0
		
		cost = 5
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_ugandan_defense_force executed"
			add_war_support = 0.05
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_the_airforce
		icon = air_force


		x = 2
		y = 1
		
		cost = 2.9
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_the_airforce executed"
			air_experience = 10
		}

		prerequisite = {
			focus = UGA_ugandan_defense_force
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_modernize_the_airforce
		icon = fighter_plane


		x = 0
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_modernize_the_airforce executed"
			add_tech_bonus = {
				name = multirole_bonus
				bonus = 0.30
				uses = 1
				category = Cat_FIGHTER
			}
		}

		prerequisite = {
			focus = UGA_the_airforce
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_mr_fighter_jets
		icon = modern_fighter


		x = 2
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_mr_fighter_jets executed"
			add_tech_bonus = {
				name = multirole_bonus
				bonus = 0.30
				uses = 1
				category = Cat_MR_FIGHTER
			}
		}

		prerequisite = {
			focus = UGA_the_airforce
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_strong_airforce
		icon = close_air_support


		x = 1
		y = 3
		
		cost = 5

		available = {
			has_deployed_air_force_size = {
    				size > 29
			}
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_strong_airforce executed"
			air_experience = 50
			add_war_support = 0.03
			remove_ideas = {
				UGA_bad_pilots
			}
		}

		prerequisite = {
			focus = UGA_modernize_the_airforce
		}

		prerequisite = {
			focus = UGA_mr_fighter_jets
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_post_war_evaluations
		icon = army_reform


		x = 4
		y = 1
		
		cost = 10

		available = {
			has_war = no
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_post_war_evaluations executed"
			add_tech_bonus = {
				bonus = 0.40
				uses = 2
				category = land_doctrine
			}
		}

		prerequisite = {
			focus = UGA_ugandan_defense_force
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_new_doctorines
		icon = army_doctrine


		x = 4
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_new_doctorines executed"
			add_tech_bonus = {
				bonus = 0.30
				uses = 1
				category = land_doctrine
			}
			add_tech_bonus = {
				bonus = 0.30
				uses = 1
				category = air_doctrine
			}
		}

		prerequisite = {
			focus = UGA_post_war_evaluations
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_land_force
		icon = combined_arms


		x = 6
		y = 1
		
		cost = 2.9
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_land_force executed"
			army_experience = 25
		}

		prerequisite = {
			focus = UGA_ugandan_defense_force
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_small_arms
		icon = small_arms_east


		x = 6
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_small_arms executed"
			add_tech_bonus = {
				name = inf_wep_bonus
				bonus = 0.35
				uses = 1
				category = Cat_INF_WEP
			}
		}

		prerequisite = {
			focus = UGA_land_force
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_tanks
		icon = tanks5


		x = 8
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_tanks executed"
			add_tech_bonus = {
				name = MBT_bonus
				bonus = 0.0
				uses = 1
				category = Cat_MBT
			}
		}

		prerequisite = {
			focus = UGA_land_force
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_field_equipment
		icon = grenade


		x = 7
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_field_equipment executed"
			add_tech_bonus = {
				name = inf_wep_bonus
				bonus = 0.20
				uses = 2
				category = Cat_INF
			}
		}

		prerequisite = {
			focus = UGA_small_arms
		}

		prerequisite = {
			focus = UGA_tanks
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_tackle_the_problems
		icon = concessions2


		x = 13
		y = 0
		
		cost = 5
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_tackle_the_problems executed"
			country_event = UGA.1
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_makerere_university
		icon = research


		x = 11
		y = 1
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_makerere_university executed"
			add_research_slot = 1
			add_stability = 0.01
			subtract_from_variable = { treasury = 0.5 }
			custom_effect_tooltip = 500_million_expense_tt
		}

		prerequisite = {
			focus = UGA_tackle_the_problems
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_kiira_motors
		icon = kiira_motors

		x = 10
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_kiira_motors executed"
			subtract_from_variable = { treasury = 0.5 }
			custom_effect_tooltip = 500_million_expense_tt
			set_technology = {
				util_vehicle_0 = 1
				util_vehicle_1 = 1
			}
		}

		prerequisite = {
			focus = UGA_makerere_university
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_vehicles
		icon = motorized


		x = 9
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_vehicles executed"
			add_tech_bonus = {
				name = util_bonus
				bonus = 0.50
				uses = 1
				category = Cat_UTIL
			}
			add_tech_bonus = {
				name = util_bonus
				bonus = 0.35
				uses = 1
				category = Cat_AFV
			}
			add_equipment_to_stockpile = {
				type = util_vehicle_1
				amount = 150
				producer = UGA
			}
		}

		prerequisite = {
			focus = UGA_kiira_motors
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_army_tech
		icon = soldier


		x = 8
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_army_tech executed"
			add_research_slot = 1
			increase_military_spending = yes
			subtract_from_variable = { treasury = 4 }
			custom_effect_tooltip = 4_billion_expense_tt
		}

		prerequisite = {
			focus = UGA_new_doctorines
		}

		prerequisite = {
			focus = UGA_vehicles
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_public_internet
		icon = radar


		x = 11
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_public_internet executed"
			add_stability = 0.005
			246 = {
				add_building_construction = {
					type = internet_station
					level = 1
					instant_build = yes
				}
			}
			247 = {
				add_building_construction = {
					type = internet_station
					level = 1
					instant_build = yes
				}
			}
			248 = {
				add_building_construction = {
					type = internet_station
					level = 1
					instant_build = yes
				}
			}
			249 = {
				add_building_construction = {
					type = internet_station
					level = 1
					instant_build = yes
				}
			}
			subtract_from_variable = { treasury = 1 }
			custom_effect_tooltip = 1_billion_expense_tt
		}

		prerequisite = {
			focus = UGA_makerere_university
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_continue_privatisation
		icon = industry_democratic


		x = 15
		y = 0
		
		cost = 5
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_continue_privatisation executed"
			add_political_power = -25
			add_stability = 0.03
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_kickstart_economy
		icon = better_money


		x = 14
		y = 1
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_kickstart_economy executed"
			increase_economic_growth = yes
		}

		prerequisite = {
			focus = UGA_tackle_the_problems
		}

		prerequisite = {
			focus = UGA_continue_privatisation
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_university_expansions
		icon = research2


		x = 12
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_university_expansions executed"
			add_research_slot = 1
			custom_effect_tooltip = 3_billion_expense_tt
			subtract_from_variable = { treasury = 3 }
		}

		prerequisite = {
			focus = UGA_makerere_university
		}

		prerequisite = {
			focus = UGA_kickstart_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_infrastructure_efforts
		icon = infrastructure


		x = 14
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_infrastructure_efforts executed"
			random_owned_state = { add_building_construction = { type = infrastructure level = 1 instant_build = yes } }
			add_timed_idea = { idea = UGA_infrastructure days = 720 }
			custom_effect_tooltip = 6_billion_expense_tt
			subtract_from_variable = { treasury = 6 }
		}

		prerequisite = {
			focus = UGA_kickstart_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_mobilizing_economy
		icon = industry_communist2


		x = 13
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_mobilizing_economy executed"
			increase_economic_growth = yes
		}

		prerequisite = {
			focus = UGA_infrastructure_efforts
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_civilian_industry
		icon = industry_civilian


		x = 11
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_civilian_industry executed"
			random_owned_state = { 
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
			custom_effect_tooltip = 8_billion_expense_tt
			add_to_variable = { treasury = -8.0 }
		}

		prerequisite = {
			focus = UGA_mobilizing_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_industrial_resources
		icon = mining_truck


		x = 12
		y = 5
		
		cost = 5
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_industrial_resources executed"
			add_resource = { 
				type = steel
				amount = 4
				state = 249
			}
			custom_effect_tooltip = 1_billion_expense_tt
			add_to_variable = { treasury = -1.0 }
		}

		prerequisite = {
			focus = UGA_civilian_industry
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_military_industry
		icon = industry_military


		x = 10
		y = 5
		
		cost = 10

		available = {
			num_of_factories > 5
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_military_industry executed"
			random_owned_state = { 
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
			custom_effect_tooltip = 8_billion_expense_tt
			add_to_variable = { treasury = -8.0 }
		}

		prerequisite = {
			focus = UGA_vehicles
		}

		prerequisite = {
			focus = UGA_civilian_industry
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_military_resources
		icon = GFX_focus_generic_aluminum


		x = 10
		y = 6
		
		cost = 5
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_military_resources executed"
			add_resource = { 
				type = tungsten
				amount = 2
				state = 248
			}
			custom_effect_tooltip = 1_billion_expense_tt
			add_to_variable = { treasury = -1.0 }
		}

		prerequisite = {
			focus = UGA_military_industry focus = UGA_industrial_resources
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_lake_albert_oil
		icon = oil_production


		x = 12
		y = 6
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_lake_albert_oil executed"
			country_event = UGA.2
		}

		prerequisite = {
			focus = UGA_industrial_resources
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_promising_economy
		icon = economic_prosperity


		x = 13
		y = 4
		
		cost = 10

		available = {
			has_idea = fast_growth
			has_idea = economic_boom
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_promising_economy executed"
			add_political_power = 200
			add_stability = 0.05
		}

		prerequisite = {
			focus = UGA_mobilizing_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_lion_economy
		icon = lion_economy


		x = 14
		y = 6
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_lion_economy executed"
			add_ideas = KEN_a_lion_economy
		}

		prerequisite = {
			focus = UGA_promising_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_pipelines
		icon = oil_trade


		x = 15
		y = 5
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_pipelines executed"
			country_event = UGA.20
			subtract_from_variable = { treasury = 4 }
			248 = { add_building_construction = { type = infrastructure level = 1 instant_build = yes } }
			custom_effect_tooltip = 1_billion_expense_tt
		}

		prerequisite = {
			focus = UGA_promising_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_public_finance_act
		icon = union_negotiations


		x = 16
		y = 6
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_public_finance_act executed"
			decrease_corruption = yes
			custom_effect_tooltip = 500_million_expense_tt
			add_to_variable = { treasury = -0.5 }
		}

		prerequisite = {
			focus = UGA_promising_economy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_agriculture
		icon = agriculture


		x = 18
		y = 0
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_agriculture executed"
			add_timed_idea = {
				idea = agricultural_investments
				days = 1800
			}
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_imf_relief
		icon = imf


		x = 16
		y = 1
		
		cost = 5
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_imf_relief executed"
			if = {
				limit = {
					check_variable = { debt > 0.9 }
				}
				custom_effect_tooltip = UGA_3_billion_debt_tt
				add_to_variable = { debt = -3 }
			}
			else_if = {
				custom_effect_tooltip = UGA_1_billion_increase_tt
				add_to_variable = { treasury = 1 }
			}
		}

		prerequisite = {
			focus = UGA_continue_privatisation
		}

		prerequisite = {
			focus = UGA_agriculture
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_foreign_aid
		icon = improve_relations_green


		x = 16
		y = 3
		
		cost = 5
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_foreign_aid executed"
			add_to_variable = { influence_array_val^0 = 5 }
			add_to_variable = { influence_array_val^1 = 5 }
			add_to_variable = { influence_array_val^2 = 5 }
			recalculate_influence = yes
			custom_effect_tooltip = UGA_3_billion_increase_tt
			add_to_variable = { treasury = 3 }
		}

		prerequisite = {
			focus = UGA_kickstart_economy
		}

		prerequisite = {
			focus = UGA_imf_relief
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_foreign_investments
		icon = generic_banks


		x = 15
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_foreign_investments executed"
			country_event = UGA.3
		}

		prerequisite = {
			focus = UGA_infrastructure_efforts	focus = UGA_foreign_aid
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_future_of_usaid
		icon = trade_with_america


		x = 17
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_future_of_usaid executed"
			country_event = UGA.4
		}

		prerequisite = {
			focus = UGA_foreign_aid
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_exterminate_poverty
		icon = generic_healthcare


		x = 18
		y = 1
		
		cost = 8
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_exterminate_poverty executed"
			increase_social_spending = yes
			add_stability = 0.03
			add_political_power = -100
		}

		prerequisite = {
			focus = UGA_tackle_the_problems
		}

		prerequisite = {
			focus = UGA_agriculture
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_business_enviroment
		icon = race_relations2


		x = 17
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_business_enviroment executed"
			if = {
				limit = { NOT = { has_country_flag = enthusiastic_industrial_conglomerates } }
				if = {
					limit = { has_idea = industrial_conglomerates }
					visually_display_opinion_rise_industrial_conglomerates = yes
					set_country_flag = current_industrial_conglomerates
					increase_internal_faction_opinion = yes
				}
			}
			if = {
				limit = { NOT = { has_country_flag = enthusiastic_international_bankers } }
				if = {
					limit = { has_idea = international_bankers }
					visually_display_opinion_rise_international_bankers = yes
					set_country_flag = current_international_bankers
					increase_internal_faction_opinion = yes
				}
			}
			if = {
				limit = { NOT = { has_country_flag = enthusiastic_small_medium_business_owners } }
				if = {
					limit = { has_idea = small_medium_business_owners }
					visually_display_opinion_rise_small_medium_business_owners = yes
					set_country_flag = current_small_medium_business_owners
					increase_internal_faction_opinion = yes
				}
			}
			if = {
				limit = { NOT = { has_country_flag = hostile_farmers } }
				if = {
					limit = { has_idea = farmers }
					visually_display_opinion_fall_farmers = yes
					set_country_flag = current_farmers
					decrease_internal_faction_opinion = yes
				}
			}
			if = {
				limit = { NOT = { has_country_flag = hostile_landowners } }
				if = {
					limit = { has_idea = landowners }
					visually_display_opinion_fall_landowners = yes
					set_country_flag = current_landowners
					decrease_internal_faction_opinion = yes
				}
			}
		}

		prerequisite = {
			focus = UGA_imf_relief
		}

		prerequisite = {
			focus = UGA_exterminate_poverty
		}

		mutually_exclusive = {
			focus = UGA_protecting_farmers
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_protecting_farmers
		icon = agriculture2


		x = 19
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_protecting_farmers executed"
			if = {
				limit = { NOT = { has_country_flag = enthusiastic_farmers } }
				if = {
					limit = { has_idea = farmers }
					visually_display_opinion_rise_farmers = yes
					set_country_flag = current_farmers
					increase_internal_faction_opinion = yes
				}
			}
			if = {
				limit = { NOT = { has_country_flag = enthusiastic_landowners } }
				if = {
					limit = { has_idea = landowners }
					visually_display_opinion_rise_landowners = yes
					set_country_flag = current_landowners
					increase_internal_faction_opinion = yes
				}
			}
			if = {
				limit = { NOT = { has_country_flag = hostile_industrial_conglomerates } }
				if = {
					limit = { has_idea = industrial_conglomerates }
					visually_display_opinion_fall_industrial_conglomerates = yes
					set_country_flag = current_industrial_conglomerates
					decrease_internal_faction_opinion = yes
				}
			}
			if = {
				limit = { NOT = { has_country_flag = hostile_international_bankers } }
				if = {
					limit = { has_idea = international_bankers }
					visually_display_opinion_fall_international_bankers = yes
					set_country_flag = current_international_bankers
					decrease_internal_faction_opinion = yes
				}
			}
			if = {
				limit = { NOT = { has_country_flag = hostile_small_medium_business_owners } }
				if = {
					limit = { has_idea = small_medium_business_owners }
					visually_display_opinion_fall_small_medium_business_owners = yes
					set_country_flag = current_small_medium_business_owners
					decrease_internal_faction_opinion = yes
				}
			}
		}

		prerequisite = {
			focus = UGA_exterminate_poverty
		}

		mutually_exclusive = {
			focus = UGA_business_enviroment
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_great_african_war
		icon = african_conflict


		x = 21
		y = 0
		
		cost = 2.9

		bypass = {
			has_war = no
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_great_african_war executed"
			add_war_support = 0.05
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_end_hostilities
		icon = align_to_congo_kinshasa


		x = 20
		y = 1
		
		cost = 10

		bypass = {
			has_war = no
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_end_hostilities executed"
			country_event = UGA.5
		}

		prerequisite = {
			focus = UGA_great_african_war
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_funding_the_insurections
		icon = aggressive_diplomacy


		x = 22
		y = 1
		
		cost = 8

		bypass = {
			has_war = no
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_funding_the_insurections executed"
			MLC = {
				add_stability = 0.02
				add_war_support = 0.05
			}
			RCD = {
				add_stability = 0.02
				add_war_support = 0.05
			}
			custom_effect_tooltip = 500_million_expense_tt
			add_to_variable = { treasury = -0.5 }
		}

		prerequisite = {
			focus = UGA_great_african_war
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_carve_congo
		icon = forceful_treaty


		x = 21
		y = 2

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_carve_congo executed"
			country_event = UGA.10
		}

		prerequisite = {
			focus = UGA_funding_the_insurections
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_integrating_western_states
		icon = political_pressure_dark_green


		x = 21
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_integrating_western_states executed"
		}

		prerequisite = {
			focus = UGA_carve_congo
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_puppet_involvements
		icon = become_puppet


		x = 23
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_puppet_involvements executed"
		}

		prerequisite = {
			focus = UGA_funding_the_insurections
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_regional_security
		icon = military_sphere


		x = 25
		y = 0
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_regional_security executed"
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_support_south_sudan
		icon = align_to_south_sudan


		x = 24
		y = 1
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_support_south_sudan executed"
		}

		prerequisite = {
			focus = UGA_regional_security
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_countering_sudanese_influence
		icon = attack_sudan


		x = 24
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_countering_sudanese_influence executed"
		}

		prerequisite = {
			focus = UGA_support_south_sudan
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_lord_resistance
		icon = strike_lra


		x = 20
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_lord_resistance executed"
		}

		prerequisite = {
			focus = UGA_end_hostilities focus = UGA_countering_sudanese_influence
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_operation_iron_fist
		icon = ultimatum2


		x = 20
		y = 5
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_operation_iron_fist executed"
		}

		prerequisite = {
			focus = UGA_lord_resistance
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_taking_out_ssu
		icon = caudillo_armies


		x = 20
		y = 6
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_taking_out_ssu executed"
		}

		prerequisite = {
			focus = UGA_operation_iron_fist
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_diplomacy
		icon = volunteer_alliance


		x = 28
		y = 0
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_diplomacy executed"
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_fighting_terrorism
		icon = anti_fascist_diplomacy


		x = 26
		y = 1
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_fighting_terrorism executed"
		}

		prerequisite = {
			focus = UGA_regional_security
		}

		prerequisite = {
			focus = UGA_diplomacy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_au_somalia
		icon = align_to_africa


		x = 25
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_au_somalia executed"
		}

		prerequisite = {
			focus = UGA_fighting_terrorism
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_east_african_friendship
		icon = align_to_kenya


		x = 28
		y = 1
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_east_african_friendship executed"
		}

		prerequisite = {
			focus = UGA_diplomacy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_global_order
		icon = global_community


		x = 28
		y = 2
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_global_order executed"
		}

		prerequisite = {
			focus = UGA_east_african_friendship
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_embracing_china
		icon = align_to_china


		x = 27
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_embracing_china executed"
		}

		prerequisite = {
			focus = UGA_global_order
		}

		mutually_exclusive = {
			focus = UGA_denounce_china
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_chinese_investment_funds
		icon = trade_with_china


		x = 26
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_chinese_investment_funds executed"
		}

		prerequisite = {
			focus = UGA_embracing_china
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_denounce_china
		icon = align_to_taiwan


		x = 29
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_denounce_china executed"
		}

		prerequisite = {
			focus = UGA_global_order
		}

		mutually_exclusive = {
			focus = UGA_embracing_china
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_writing_of_debt
		icon = money


		x = 28
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_writing_of_debt executed"
		}

		prerequisite = {
			focus = UGA_embracing_china focus = UGA_denounce_china
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_non_nato_ally_status
		icon = nato


		x = 30
		y = 4
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_non_nato_ally_status executed"
		}

		prerequisite = {
			focus = UGA_denounce_china
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_community_contributions
		icon = eac


		x = 31
		y = 1
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_community_contributions executed"
		}

		prerequisite = {
			focus = UGA_diplomacy
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_call_for_integration
		icon = align_to_east_africa


		x = 30
		y = 2

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_call_for_integration executed"
			set_country_flag = eac_integration_priority
			add_country_leader_trait = pro_east_africa
		}

		prerequisite = {
			focus = UGA_community_contributions
		}

		mutually_exclusive = {
			focus = UGA_drawing_line
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_drawing_line
		icon = eac_red_line


		x = 32
		y = 2

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_drawing_line executed"
		}

		prerequisite = {
			focus = UGA_community_contributions
		}

		mutually_exclusive = {
			focus = UGA_call_for_integration
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_multiparty_referendum
		icon = Election


		x = 38
		y = 0

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_multiparty_referendum executed"
			set_politics = {
				ruling_party = neutrality
				elections_allowed = yes
				election_frequency = 60
			}
			remove_ideas = UGA_military_despot
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_museveni
		icon = museveni


		x = 35
		y = 1

		cost = 10

		available = {
			has_country_leader = { name = "Yoweri Museveni" ruling_only = yes }
		}

		bypass = {
			custom_trigger_tooltip = {
				is_in_array = { ruling_party = 14 }
				tooltip = UGA_nrm_in_power_tt
			}
			NOT = {
				has_country_leader = { name = "Yoweri Museveni" ruling_only = yes }
			}
		}

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_museveni executed"
			add_political_power = 100
		}

		prerequisite = {
			focus = UGA_multiparty_referendum
		}

		mutually_exclusive = {
			focus = UGA_besigye
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_rigging_elections
		icon = propaganda


		x = 34
		y = 2

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_rigging_elections executed"
			if = {
				limit = {
					has_idea = UGA_dictator
				}
				swap_ideas = { remove_idea = UGA_dictator add_idea = UGA_dictator_1 }
			}
			if = {
				limit = {
					NOT = {
						has_idea = UGA_dictator
					}
				}
				add_ideas = UGA_dictator
			}
			add_political_power = 50
		}

		prerequisite = {
			focus = UGA_museveni
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_harassing_opposition
		icon = generic_controlled_press


		x = 36
		y = 2

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_harassing_opposition executed"
			if = {
				limit = {
					has_idea = UGA_dictator
				}
				swap_ideas = { remove_idea = UGA_dictator add_idea = UGA_dictator_1 }
			}
			if = {
				limit = {
					NOT = {
						has_idea = UGA_dictator
					}
				}
				add_ideas = UGA_dictator
			}
			add_political_power = 50
		}

		prerequisite = {
			focus = UGA_museveni
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_dictator_for_life
		icon = strike_at_democracy1


		x = 35
		y = 3

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_dictator_for_life executed"
			swap_ideas = { remove_idea = UGA_dictator_1 add_idea = UGA_dictator_2 }
			add_country_leader_trait = dictator
			add_political_power = 100
		}

		prerequisite = {
			focus = UGA_rigging_elections
		}

		prerequisite = {
			focus = UGA_harassing_opposition
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_besigye
		icon = besigye


		x = 41
		y = 1

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_besigye executed"
			if = {
				limit = { NOT = { is_in_array = { ruling_party = 2 } } }
				hidden_effect = {
					clear_array = ruling_party
					clear_array = gov_coalition_array
					add_to_array = { ruling_party = 2 }
					update_government_coalition_strength = yes
					update_party_name = yes
					set_coalition_drift = yes
					meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
				}
				set_politics = {
					ruling_party = democratic
					elections_allowed = yes
				}
				add_popularity = {
					ideology = democratic
					popularity = 0.2
				}
				add_to_variable = { party_pop_array^2 = 0.15 }
				recalculate_party = yes
				create_country_leader = {
					name = "Kizza Besigye"
					picture = "Kizza_Besigye.dds"
					ideology = liberalism
					traits = {
						doctor
						western_liberalism
					}
				}
			}
			else_if = {
				limit = { is_in_array = { ruling_party = 2 } }
				add_popularity = {
					ideology = democratic
					popularity = 0.1
				}
				add_to_variable = { party_pop_array^2 = 0.1 }
				recalculate_party = yes
			}
		}

		prerequisite = {
			focus = UGA_multiparty_referendum
		}

		mutually_exclusive = {
			focus = UGA_museveni
		}

		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_rooting_corruption
		icon = anti_establishment


		x = 40
		y = 2

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_rooting_corruption executed"
			decrease_corruption = yes
			add_popularity = {
				ideology = democratic
				popularity = 0.05
			}
			add_stability = 0.02
			add_political_power = -50
		}

		prerequisite = {
			focus = UGA_besigye
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_popular_reforms
		icon = support_the_left_right


		x = 42
		y = 2

		cost = 10

		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_popular_reforms executed"
			random_list = {
				50 = {
					if = {
						limit = { NOT = { has_country_flag = enthusiastic_farmers } }
						if = {
							limit = { has_idea = farmers }
							visually_display_opinion_rise_farmers = yes
							set_country_flag = current_farmers
							increase_internal_faction_opinion = yes
						}
					}
				}
				50 = {
					if = {
						limit = { NOT = { has_country_flag = enthusiastic_small_medium_business_owners } }
						if = {
							limit = { has_idea = small_medium_business_owners }
							visually_display_opinion_rise_small_medium_business_owners = yes
							set_country_flag = current_small_medium_business_owners
							increase_internal_faction_opinion = yes
						}
					}
				}
			}
			add_popularity = {
				ideology = democratic
				popularity = 0.05
			}
			add_stability = 0.03
			add_political_power = -50
		}

		prerequisite = {
			focus = UGA_besigye
		}


		ai_will_do = {
			base = 10
		}
	}

	focus = {
		id = UGA_democratic_uganda
		icon = democracy


		x = 41
		y = 3
		
		cost = 10
		
		completion_reward = {
			log = "[GetDateText]: [This.GetName]: focus UGA_democratic_uganda executed"
			add_ideas = UGA_democracy
			add_popularity = {
				ideology = democratic
				popularity = 0.05
			}
			add_stability = 0.01
			add_political_power = -80
		}

		prerequisite = {
			focus = UGA_rooting_corruption
		}

		prerequisite = {
			focus = UGA_popular_reforms
		}


		ai_will_do = {
			base = 10
		}
	}
}

