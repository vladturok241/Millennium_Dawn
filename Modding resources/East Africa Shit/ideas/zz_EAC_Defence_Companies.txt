ideas = {

	Infantry_Weapon_Company = {

		designer = yes

		EAC_burundi_infantry_weapon_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EAC_burundi_infantry_weapon_company" }

			picture = KMDB_UKR


			available = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
				has_global_flag = eac_fund_merge_bills_won
			}
			visible = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_INF_WEP = 0.217
			}

			traits = {
				Cat_INF_WEP_7

			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare

				modifier = {
					num_of_military_factories > 5 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_INF_WEP_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_infantry_weapons = yes
					factor = 4000
				}
			}

		}
	}

	Vehicle_Company = {

		designer = yes

		EAC_kiira_defense_technology_vehicle_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea UAE_emirates_defense_technology_vehicle_company" }

			picture = KMDB_UKR


			available = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
				has_global_flag = eac_fund_merge_bills_won
			}
			visible = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_ARMOR = 0.217
			}

			traits = {
				Cat_ARMOR_7

			}
			ai_will_do = {
				factor = 0.7 #All countries need a land army, vehicles are part of modern warfare

				modifier = {
					num_of_military_factories > 10 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_ARMOR_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_armor = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_afv = yes
						has_AFV_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_tanks = yes
						has_TANKS_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_artillery = yes
						has_ARTILLERY_designer = yes
					}
					factor = 0
				}
			}

		}
	}

	Aircraft_Company = {

		designer = yes

		EAC_furukombe_aircraft_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EAC_furukombe_aircraft_company" }

			picture = Alenia_Aeronautica_ITA


			available = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
				has_global_flag = eac_fund_merge_bills_won
			}
			visible = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_FIXED_WING = 0.217
			}

			traits = {
				Cat_L_Fighter_7

			}
			ai_will_do = {
				factor = 0.7 #Most countries don't have decent airforces

				modifier = {
					or = {
						has_tech = AS_Fighter2 #has semi-modern tech
						has_tech = MR_Fighter2
						has_tech = Strike_fighter2
						has_tech = L_Strike_fighter2
						has_tech = Air_UAV1
					}
					factor = 1
				}
				modifier = {
					or = {
						has_tech = strategic_bomber3 #has semi-modern tech, most countries dont have it
						has_tech = transport_plane2
						has_tech = naval_plane3
						has_tech = cas2
					}
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					has_better_than_L_Fighter_7 = yes
					factor = 0
				}
				modifier = {
					is_researching_light_fighter = yes
					factor = 4000
				}
			}

		}
	}

	Helicopter_Company = {

		designer = yes

		EAC_furukombe_helicopter_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EAC_furukombe_helicopter_company" }

			picture = Airbus_Helicopters_ITA


			available = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
				has_global_flag = eac_fund_merge_bills_won
			}
			visible = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_HELI = 0.248
			}

			traits = {
				Cat_HELI_8

			}
			ai_will_do = {
				factor = 0.8 #Most countries don't have decent airforces

				modifier = {
					has_tech = attack_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					has_tech = transport_helicopter2 #has semi-modern tech, most countries dont have it
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					original_tag = ITA
					factor = 0 # To force them to use their domestic AgustaWestland designer
				}
				modifier = {
					has_better_than_HELI_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_helicopters = yes
					factor = 4000
				}
			}

		}
	}

	Ship_Company = {

		designer = yes

		EAC_kigamboni_ship_building_ship_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EAC_kigamboni_ship_building_ship_company" }

			picture = Nikolayev_Shipyard_UKR


			available = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
				has_global_flag = eac_fund_merge_bills_won
			}
			visible = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_NAVAL_EQP = 0.246
			}

			traits = {
				Cat_NAVAL_EQP_8
			}
			ai_will_do = {
				factor = 0.6

				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
					factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
							is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_NAVAL_EQP_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_naval_equipment = yes
					factor = 1000
				}
				modifier = {
					AND = {
						is_researching_carrier = yes
						has_CARRIER_designer = yes
					}
					factor = 0
				}
				modifier = {
					AND = {
						is_researching_surface_ship = yes
						has_SURFACE_SHIP_designer = yes
					}
					factor = 0
				}
			}
		}
	}

	Submarine_Company = {

		designer = yes

		EAC_kigamboni_submarine_company = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EAC_kigamboni_submarine_company" }

			picture = Fincantieri_ITA


			available = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
				has_global_flag = eac_fund_merge_bills_won
			}
			visible = {
				OR = {
					tag = TNZ
					tag = KEN
					tag = UGA
					tag = RWA
					tag = BUR
					tag = SSU
					tag = COM
					tag = ZAN
				}
			}
			cost = 150

			removal_cost = 10

			research_bonus = {
				Cat_D_SUB = 0.248
			}

			traits = {
				Cat_D_SUB_8

			}
			ai_will_do = {
				factor = 0.8

				modifier = {
					has_navy_size = { size > 25 } #has a large navy
					factor = 1
				}
				modifier = {
					num_of_naval_factories > 3 #has the industry to take advantage of the company
					factor = 1
				}
				modifier = {
					is_major = yes #Majors project power
						factor = 1
				}
				modifier = {
					NOT = { #need to have ports
						any_owned_state = {
						is_coastal = yes
						}
					}
					factor = 10
				}
				modifier = {
					has_better_than_D_SUB_8 = yes
					factor = 0
				}
				modifier = {
					is_researching_diesel_electric_sub = yes
					factor = 4000
				}
			}

		}
	}
}