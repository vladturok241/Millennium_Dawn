ideas = {
	country = {
		hutu_tutsi_tensions = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea hutu_tutsi_tensions" }
			picture = hutu_tutsi_tensions
			
			modifier = {
				stability_factor = -0.1
			}
			
		}

		great_lakes_aftermath = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea great_lakes_aftermath" }
			picture = war_to_end_all_wars

			removal_cost = -1
			
			modifier = {
				MONTHLY_POPULATION = 0.10
				production_speed_buildings_factor = -0.30
				research_speed_factor = -0.30
				stability_factor = -0.05
			}
		}

		BUR_countryside_terrorists = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUR_countryside_terrorists" }
			picture = army_of_aggression

			removal_cost = -1
			
			modifier = {
				stability_factor = -0.1
				political_power_gain = -0.1
			}
		}

		BUR_nation_building = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUR_nation_building" }

			picture = propaganda

			removal_cost = -1
			
			modifier = {

				stability_factor = 0.05
				neutrality_drift = 0.01
				democratic_drift = 0.01
				political_power_gain = -0.05 
			}
		}

		BUR_democracy_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUR_democracy_1" }

			picture = national_unity

			removal_cost = -1
			
			modifier = {

				stability_factor = 0.02
				stability_weekly = 0.0010
				democratic_drift = 0.01
				political_power_gain = -0.05
				
			}
		}

		BUR_democracy_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUR_democracy_2" }
			picture = national_unity

			removal_cost = -1
			
			modifier = {

				stability_weekly = 0.0020
				democratic_drift = 0.02
				nationalist_drift = -0.01
				political_power_gain = -0.15
			}
		}

		BUR_autocratic_1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUR_autocratic_1" }
			picture = scw_intervention_rep

			removal_cost = -1
			
			modifier = {
				neutrality_drift = 0.02
				political_power_gain = 0.05 
			}
		}

		BUR_autocratic_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BUR_autocratic_2" }
			picture = scw_intervention_rep

			removal_cost = -1
			
			modifier = {
				stability_factor = -0.03
				democratic_drift = -0.01
				neutrality_drift = 0.03
				political_power_gain = 0.20
			}
		}
	}
}