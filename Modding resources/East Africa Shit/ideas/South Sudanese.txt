ideas = {

	country = {

		SSU_to_survive = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SSU_to_survive" }

			allowed = {
				tag = SSU
			}

			picture = foreign_capital

			modifier = {
				army_core_attack_factor = 0.15
				army_core_defence_factor = 0.25
				dig_in_speed_factor = 0.35
				surrender_limit = 1
			}

		}
	}

}