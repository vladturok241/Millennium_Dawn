##Somalian Splinter tags and Somalian Nation focuses
PUN_add_1_pirate_morale = {
	add_to_variable = {
		var = pirates_morale
		value = 1
	}
	clamp_variable = {
		var = pirates_morale
		min = 0
		max = 100
	}
	custom_effect_tooltip = PUN_morale_increases_1_tt
}
PUN_add_5_pirate_morale = {
	add_to_variable = {
		var = pirates_morale
		value = 5
	}
	clamp_variable = {
		var = pirates_morale
		min = 0
		max = 100
	}
	custom_effect_tooltip = PUN_morale_increases_5_tt
}
PUN_add_10_pirate_morale = {
	add_to_variable = {
		var = pirates_morale
		value = 10
	}
	clamp_variable = {
		var = pirates_morale
		min = 0
		max = 100
	}
	custom_effect_tooltip = PUN_morale_increases_10_tt
}