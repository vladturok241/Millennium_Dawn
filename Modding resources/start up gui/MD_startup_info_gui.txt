scripted_gui = {
	MD_info_event_gui_window = {
		window_name = "MD_info_event_gui_window"
		context_type = player_context

		visible = { NOT = { has_country_flag = MD_event_info_closed } }
		effects = {
			close_button_click = { set_country_flag = MD_event_info_closed }
		}
	}
}