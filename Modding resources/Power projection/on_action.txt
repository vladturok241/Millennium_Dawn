on_military_access

FROM = {
				country_event = { id = projection.1 }
			}
			add_to_variable = { power_projection = 1 }
			if = {
				limit = {
					NOT = {
						has_idea = limited_power_projection
						has_idea = significant_power_projection
						has_idea = massive_power_projection
					}
				}
				add_ideas = limited_power_projection
			}
			if = {
				limit = {
					check_variable = { power_projection > 9 }
					has_idea = limited_power_projection
				}
				swap_ideas = {
					remove_idea = limited_power_projection
					add_idea = significant_power_projection
				}
			}
			else_if = {
				limit = {
					check_variable = { power_projection > 29 }
					has_idea = significant_power_projection
				}
				swap_ideas = {
					remove_idea = significant_power_projection
					add_idea = massive_power_projection
				}
			}