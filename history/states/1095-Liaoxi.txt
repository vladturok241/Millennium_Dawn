state = {
	id = 1095
	name = "STATE_1095"
	manpower = 14582026
	state_category = state_15

	resources = {
		oil = 4
		aluminium = 13
		steel = 26
		chromium = 15
	}

	history = {
		owner = CHI
		victory_points = {
			9788 5
		}
		victory_points = {
			9835 5
		}
		victory_points = {
			846 1
		}
		victory_points = {
			934 1
		}
		victory_points = {
			3881 1
		}
		victory_points = {
			6837 1
		}
		victory_points = {
			11815 1
		}

		buildings = {
			infrastructure = 2
			internet_station = 1
			industrial_complex = 1
			offices = 1
			arms_factory = 1
			dockyard = 1
			air_base = 10
			nuclear_reactor = 1
			6837 = {
				naval_base = 4
			}
		}
		add_core_of = CHI
		add_core_of = TAI
		2017.1.1 = {
			add_manpower = 2000000
			add_manpower = 2000000
			add_manpower = 26540
			buildings = {
				industrial_complex = 4
				offices = 2
				arms_factory = 3
				dockyard = 2
				infrastructure = 3
				internet_station = 4
			}
		}
	}

	provinces = {
		814 846 934 3814 3881 3934 6837 6852 6893 6898 6951 7633 9788 9835 9927 11815 11886 14019 14304 14305
	}
}