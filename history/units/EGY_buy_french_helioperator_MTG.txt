units = {
	fleet = {
		name = "French Helioperator"
		naval_base = 4076
		task_force = {
			name = "French Helioperator"
			location = 4076
			ship = { name = "French Helioperator" definition = carrier start_experience_factor = 0.65 equipment = { helicopter_operator_hull_2 = { amount = 1 owner = FRA creator = FRA version_name = "Charles de Gaulle Class 2" } } }
		}
	}
}