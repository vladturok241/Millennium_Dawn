instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 4
		variant_name = "A-37 Dragonfly"
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		amount = 6
		variant_name = "Pilatus PC-7"
		producer = SWI
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1	#in place of all other smaller variants they actually use and we do not have
		amount = 6
		variant_name = "C-130 Hercules"
		producer = USA
	}

}