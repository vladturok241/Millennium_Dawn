﻿division_template = {
	name = "Brigade d'Infanterie"
	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Commando Bataillon"
	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}

	priority = 2
}

units = {
	division = {
		name = "1ère Brigade d'Infanterie"
		location = 10919		#Porto Novo
		division_template = "Brigade d'Infanterie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "Commando Bataillon"
		location = 10919		#Porto Novo
		division_template = "Commando Bataillon"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 20
		producer = SOV
		variant_name = "PT-76"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		amount = 14
		producer = SOV
		variant_name = "BRDM-2"
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_3		#Panhard VBL
		amount = 10
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons			#Type 56
		amount = 2000
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 100
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 50
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 50
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 50
		producer = SOV
	}
}