﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #F-16A
		amount = 57
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #Saab MFI-17 Supporter
		amount = 16
		producer = SWE
		#version_name = "Saab MFI-17 Supporter"
	}
	add_equipment_to_stockpile = {
		type = naval_plane2 #P-3C Orion
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sea King HU5
		amount = 12
		producer = USA
		#version_name = "Sea King HU5"
	}
	add_equipment_to_stockpile = {
		type = transport_plane3 #C-130J Super Hercules
		amount = 4
		producer = USA
	}
}