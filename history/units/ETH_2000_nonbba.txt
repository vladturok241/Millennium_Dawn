﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1		#MiG-21
		amount = 24
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter1		#MiG-23
		amount = 17
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = cas1				#Su-25
		amount = 4
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2		#Su-27
		amount = 8
		producer = SOV
	}
}