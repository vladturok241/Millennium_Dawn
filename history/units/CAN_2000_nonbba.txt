﻿###Canada Is intentionally low on equipment due to having reserves deployed###
#Author: Kanthier/Hiddengearz
instant_effect = {
	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #Boeing F/A-18 Hornet
		amount = 122
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #Boeing F/A-18 Hornet
		amount = 122
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = naval_plane2 #CP-140 Aurora
		amount = 21
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 32
	}
	add_equipment_to_stockpile = {
		type = transport_plane3 #A310 MRTT
		amount = 5
	}
	add_equipment_to_stockpile = {
		type = transport_plane3 #A310 MRTT
		amount = 5
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #CC-109 Cosmopolitan
		amount = 7
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #CH-46D
		amount = 45
		#version_name = "CH-46D"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 107
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #Canadair CT-114 Tutor
		amount = 130
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell 206
		amount = 9
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #Beechcraft T-6 Texan II
		amount = 12
		producer = USA
		#version_name = "Beechcraft T-6 Texan II"
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #BAE Systems Hawk
		amount = 1
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = awacs_equipment_1 #E-3 Sentry
		amount = 1
		producer = USA
	}
}
