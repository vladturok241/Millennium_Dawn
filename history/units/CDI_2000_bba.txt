﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 5
		variant_name = "Dassault Alpha Jet"
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		amount = 4
		variant_name = "Dassault Falcon 20"			#3x extra to cover for Fokker 100's
		producer = FRA
	}
}