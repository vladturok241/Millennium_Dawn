﻿
instant_effect = {
	## AIRFORCE
	# Planes
	add_equipment_to_stockpile = { 
		type = small_plane_strike_airframe_2
		amount = 37
		variant_name = "AMX A-1"
		producer = BRA
	}

	add_equipment_to_stockpile = { 
		type = small_plane_strike_airframe_1
		variant_name = "Embraer EMB-312 Tucano"
		amount = 61
		producer = BRA
	}
	add_equipment_to_stockpile = { 
		type = small_plane_strike_airframe_1
		variant_name = "Embraer EMB-326"
		amount = 53
		producer = BRA
	}

	add_equipment_to_stockpile = { 
		type = 	cv_small_plane_strike_airframe_1
		amount = 22
		variant_name = "A-4 Skyhawk"
		producer = USA
	}

	add_equipment_to_stockpile = { 
		type = small_plane_airframe_1
		producer = USA
		variant_name = "F-5E Tiger II"
		amount = 47
	}

	add_equipment_to_stockpile = { 
		type = small_plane_airframe_1
		producer = FRA
		variant_name = "Mirage F1"
		amount = 18
	}

	add_equipment_to_stockpile = { 
		type = small_plane_airframe_1
		producer = FRA
		variant_name = "Mirage III"
		amount = 14
	}

	add_equipment_to_stockpile = { 
		type = large_plane_air_transport_airframe_1			#extra to account for no variants on C-95
		variant_name = "C-130 Hercules"
		producer = USA
		amount = 42
	}

	add_equipment_to_stockpile = { 
		type = large_plane_air_transport_airframe_1			#in place of C-115
		variant_name = "CC-109 Cosmopolitan"
		producer = CAN
		amount = 24
	}

	add_equipment_to_stockpile = { 
		type = large_plane_awacs_airframe_2					#covers RC-95, RC-130E
		variant_name = "Boeing EL/M-2075 Phalcon"
		producer = USA
		amount = 7
	}

	add_equipment_to_stockpile = { 
		type = medium_plane_maritime_patrol_airframe_1			
		variant_name = "EMB 111A Patrulha"
		producer = BRA
		amount = 30
	}
}