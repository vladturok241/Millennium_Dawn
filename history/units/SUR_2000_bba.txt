instant_effect = {
	
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-212 Aviocar"
		amount = 2
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = medium_plane_maritime_patrol_airframe_1
		variant_name = "BN-2 Defender"
		amount = 4
		producer = ENG
	}

}