﻿division_template = {
	name = "Taxiarchía Pezikoú"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Thorakisméni Taxiarchía"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
units = {
	division = {
		name = "Thorakisméni Taxiarchía"
		location = 11984
		division_template = "Thorakisméni Taxiarchía"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "I Taxiarchía Pezikoú"
		location = 11984
		division_template = "Taxiarchía Pezikoú"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "II Taxiarchía Pezikoú"
		location = 11984
		division_template = "Taxiarchía Pezikoú"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons1		#Zastava M77
		amount = 500
		producer = SER
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons		#Vz 58
		amount = 500
		producer = CZE
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1		#AK-74
		amount = 500
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons		#AK-47
		amount = 500
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons		#G3
		amount = 500
		producer = GER
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 280
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1 			#MILAN
		amount = 45
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0 	#EE-3 Jararaca
		amount = 15
		producer = BRA
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1 	#HOT
		amount = 22
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0 	#
		amount = 50
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-80"
		amount = 41
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "AMX-30B2"
		amount = 104
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 124
		producer = BRA
		variant_name = "EE-9 Cascavel"
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "VAB Late"
		amount = 27
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "Leonidas 2"
		amount = 268
		producer = GRE
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "AMX-VCI"
		amount = 16
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "BMP-3"
		amount = 43
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "Mk F3"
		amount = 12
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "M-63"
		amount = 18
		producer = SER
	}

	add_equipment_to_stockpile = {
		type = artillery_0		#Mod 56
		amount = 54
		producer = ITA
	}

	add_equipment_to_stockpile = {
		type = artillery_1		#TRF1
		amount = 12
		producer = FRA
	}
}