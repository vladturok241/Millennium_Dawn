﻿division_template = {
	name = "Brigade d'Infanterie"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		armor_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

units = {
	division = {
		name = "1 Brigade d'Infanterie"
		location = 9152		#
		division_template = "Brigade d'Infanterie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "2 Brigade d'Infanterie"
		location = 9152		#
		division_template = "Brigade d'Infanterie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
	division = {
		name = "3 Brigade d'Infanterie"
		location = 9152		#
		division_template = "Brigade d'Infanterie"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons 		#AKM
		amount = 1000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons 		#Type 56
		amount = 250
		producer = CHI
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1			#Zastava M70
		amount = 250
		producer = SER
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1			#FAMAS
		amount = 250
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons			#G3
		amount = 250
		producer = GER
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons			#FN FAL
		amount = 250
		producer = BEL
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1			#M16
		amount = 250
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 200
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1 		#ERYX
		amount = 50
		producer = CAN
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1 		#MILAN
		amount = 70
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0 		#TOW
		amount = 55
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0 		#Strela
		amount = 8
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0 		#Redeye
		amount = 130
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_1 		#Stinger
		amount = 30
		producer = USA
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 60
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 4
		producer = FRA
		variant_name = "Panhard ERC 90"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 50
		producer = FRA
		variant_name = "Panhard AML 90"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 10
		producer = BRA
		variant_name = "EE-9 Cascavel"
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0			#Some shitty technicals
		amount = 600
		producer = CHA
	}
}