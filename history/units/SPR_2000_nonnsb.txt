﻿division_template = {
	name = "Mechanized Brigade"

	division_names_group = SPR_ARMY_DIVISIONS

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 2 y = 0 }
		SP_Arty_Bat = { x = 3 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Recon Brigade"

	division_names_group = SPR_ARMY_BRIGADES

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 0 }
		Arty_Bat = { x = 3 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Parachute Brigade"

	division_names_group = SPR_PARATROOPERS_BRIGADES

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		Mech_Air_Inf_Bat = { x = 1 y = 0 }
		Mech_Air_Inf_Bat = { x = 1 y = 1 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		Mot_Recce_Comp = { x = 0 y = 1 }
		armor_Recce_Comp = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Mechanized Brigade 2"

	division_names_group = SPR_ARMY_BRIGADES

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 2 y = 0 }
		Arty_Bat = { x = 3 y = 0 }
	}
	support = {
		Mot_Recce_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Cavalry Regiment"

	division_names_group = SPR_CAVALRY_REGIMENT

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
	}
	support = {
		armor_Comp = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Cavalry Regiment 2"

	division_names_group = SPR_CAVALRY_REGIMENT

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Comp = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Special Operations Group"

	division_names_group = SPR_SPEC_FORCES_BRIGADES

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}

	priority = 2
}
division_template = {
	name = "Light Infantry Brigade"

	division_names_group = SPR_ARMY_BRIGADES

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		Arty_Battery = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Marine Infantry Brigade"

	division_names_group = SPR_MAR_BRIGADES

	regiments = {
		L_Marine_Bat = { x = 0 y = 0 }
		L_Marine_Bat = { x = 0 y = 1 }
		Mech_Marine_Bat = { x = 1 y = 0 }
	}
	support = {
		Mot_Recce_Comp = { x = 0 y = 0 }
		armor_Comp = { x = 0 y = 1 }
		Arty_Battery = { x = 0 y = 2 }
		SP_Arty_Battery = { x = 0 y = 3 }
		SP_AA_Battery = { x = 0 y = 4 }
	}
}

units = {
	#Division "San Marcial"
	division = {
		name = "I Brigade Aragón"
		location = 3816
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "X Brigade Guzmán el Bueno"
		location = 875
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "XI Brigade Extremadura"
		location = 6902
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "XII Brigade Guadarrama"
		location = 9767
		division_template = "Mechanized Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	#Division Castillejos
	division = {
		name = "Spanish Legion Brigade Rey Alfonso XIII"
		location = 12038
		division_template = "Recon Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "VI Parachute Brigade Almogávares"
		location = 3938
		division_template = "Parachute Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "VII Brigade Galicia"
		location = 6734
		division_template = "Mechanized Brigade 2"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "VIII Brigade Galicia"
		location = 6734
		division_template = "Mechanized Brigade 2"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "Mountain Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "Light Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "Light Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "Light Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "Light Infantry Brigade"
		location = 6734
		division_template = "Light Infantry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	#Balearic General Command

	#Ceuta General Command
	division = {
		name = "3rd Cavalry Regiment Montesa"
		location = 3799
		division_template = "Cavalry Regiment"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	#Melilla General Command
	division = {
		name = "10th Cavalry Regiment Alcántara"
		location = 9877
		division_template = "Cavalry Regiment"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "11th Cavalry Regiment Alcántara"
		location = 9877
		division_template = "Cavalry Regiment"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	#Special Operations Command
	division = {
		name = "2nd Special Operations Group Granada"
		location = 1176
		division_template = "Special Operations Group"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Special Operations Group Valencia"
		location = 6906
		division_template = "Special Operations Group"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division = {
		name = "4th Special Operations Group Tercio del Ampurdán"
		location = 3938
		division_template = "Special Operations Group"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}
	division = {
		name = "XVI Brigade Canarias"
		location = 962
		division_template = "Marine Infantry Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	#2 Hem&n Cortes (US Newport) 1ST, capadty: 400 rps, 500t vehides, 3 LCVPs, 1LCPL
	#2 Galicia LPD, capadty 620 rps, 6 LCVP Plus 13 craft: 3 LCT, 2 LCU, 8 LCM
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons3
		amount = 15000
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment2
		amount = 1800
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1 #C90-CR
		amount = 750
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1 #TOW-2
		amount = 200
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1
		amount = 525
		producer = FRA
	}

	add_equipment_to_stockpile = {
		type = MBT_2 #AMX-30
		amount = 209
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = MBT_1 #M48 Patton
		amount = 164
		producer = USA
		#version_name = "M48 Patton"
	}
	add_equipment_to_stockpile = {
		type = MBT_1 #M60A3
		amount = 184
		producer = USA
		#version_name = "M60A3"
	}

	add_equipment_to_stockpile = {
		type = MBT_3 #Leopard 2A4
		amount = 108
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_2
		amount = 80 #added for balance
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = Rec_tank_1 #VEC-M1
		amount = 340
	}
	add_equipment_to_stockpile = {
		type = IFV_4 #ASCOD Pizarro
		amount = 195
	}
	add_equipment_to_stockpile = {
		type = APC_1 #M113 APC
		amount = 1311
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = APC_2
		amount = 600
	}
	add_equipment_to_stockpile = {
		type = APC_3
		amount = 48
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #OTO-Melara Mod 56
		amount = 158 #+12 from marines
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #L118
		amount = 56
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #M114
		amount = 20
		producer = USA
		#version_name = "M114"
	}
	add_equipment_to_stockpile = {
		type = SP_arty_0 #M109
		amount = 90
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = SP_arty_0 #M110
		amount = 64
		producer = USA
		#version_name = "M110"
	}
	add_equipment_to_stockpile = {
		type = SP_Anti_Air_0 #MIM-23 Hawk
		amount = 24
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = SP_Anti_Air_1 #Mistral
		amount = 108
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Eurocopter AS532 Cougar
		amount = 23
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 93
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sea King HU5
		amount = 2
		producer = USA
		#version_name = "Sea King HU5"
	}
	add_equipment_to_stockpile = {
		type = MBT_1 #M60A3
		amount = 16
		producer = USA
		#version_name = "M60A3"
	}
	add_equipment_to_stockpile = {
		type = Rec_tank_1 #FV101 Scorpion
		amount = 17
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #OTO-Melara Mod 56
		amount = 12 #+12 from marines
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = SP_arty_0 #M109
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = SP_Anti_Air_1 #Mistral
		amount = 12
		producer = FRA
	}
}