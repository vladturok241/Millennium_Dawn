﻿division_template = {
	name = "Gebirgsbrigade"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Panzerbrigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Panzergrenadierbrigade"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }

	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Panzergrenadierdivision"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		Arm_Inf_Bat = { x = 1 y = 0 }
		Arm_Inf_Bat = { x = 1 y = 1 }
		armor_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 1 y = 3 }
		armor_Bat = { x = 2 y = 0 }
		armor_Bat = { x = 2 y = 1 }
		Arm_Inf_Bat = { x = 2 y = 2 }
		SP_Arty_Bat = { x = 2 y = 3 }

	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Panzerdivision"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 1 y = 0 }
		armor_Bat = { x = 1 y = 1 }
		Arm_Inf_Bat = { x = 1 y = 2 }
		SP_Arty_Bat = { x = 1 y = 3 }
		armor_Bat = { x = 2 y = 0 }
		Arm_Inf_Bat = { x = 2 y = 1 }
		Arm_Inf_Bat = { x = 2 y = 2 }
		SP_Arty_Bat = { x = 2 y = 3 }

	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
	}
}

division_template = {
	name = "PanzergrenadierbrigadeNA"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }

	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
	}
	priority = 0
}

division_template = {
	name = "PanzerbrigadeNA"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
	}

	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
		SP_AA_Battery = { x = 0 y = 2 }
	}
	priority = 0
}

division_template = {
	name = "Panzeraufklärungsbatallion"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
	}

	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
		armor_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Luftlandebrigade"

	regiments = {
		Mech_Air_Inf_Bat = { x = 0 y = 0 }
		Mech_Air_Inf_Bat = { x = 0 y = 1 }
	}

	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		L_Recce_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Kommandobataillon"
	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
	}

	priority = 2
}

division_template = {
	name = "Marinekommandobattalion"

	regiments = {
		L_Marine_Bat = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Luftbewegliche Brigade"

	regiments = {
		L_Air_assault_Bat = { x = 0 y = 0 }
	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Deutsch-Französische Brigade"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		SP_Arty_Bat = { x = 0 y = 2 }
	}

	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		mech_Recce_Comp = { x = 0 y = 1 }
	}
}

units = {
	division = {
		name = "Panzerbrigade 21"
		location = 3355
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerbrigade 8"
		location = 6263
		division_template = "PanzerbrigadeNA"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzergrenadierbrigade 7"
		location = 9347
		division_template = "PanzergrenadierbrigadeNA"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerbrigade 39"
		location = 3561
		division_template = "PanzerbrigadeNA"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerbrigade 34"
		location = 3423
		division_template = "PanzerbrigadeNA"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzergrenadierbrigade 38"
		location = 3561
		division_template = "PanzergrenadierbrigadeNA"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzergrenadierbrigade 40"
		location = 321
		division_template = "PanzergrenadierbrigadeNA"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzergrenadierbrigade 19"
		location = 6535
		division_template = "PanzergrenadierbrigadeNA"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzergrenadierbrigade 1"
		location = 6377
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerlehrbrigade 9"
		location = 6263
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerbrigade 42"
		location = 3499
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerbrigade 18"
		location = 11331
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Luftlandebrigade 26"
		location = 11531
		division_template = "Luftlandebrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzergrenadierbrigade 41"
		location = 349
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerbrigade 12"
		location = 11544
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzerbrigade 14"
		location = 6488
		division_template = "Panzerbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Gebirgsjägerbrigade 23"
		location = 707
		division_template = "Gebirgsbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzergrenadierbrigade 30"
		location = 11499
		division_template = "Panzergrenadierbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Jägerbrigade 37"
		location = 11481 #Frankenburg, Saxony
		division_template = "Gebirgsbrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Luftlandebgrigade 31"
		location = 6325
		division_template = "Luftlandebrigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Dt.-Frz. Brigade"
		location = 11640
		division_template = "Deutsch-Französische Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Kommando Spezialkräfte"
		location = 11486 #Calw, Baden-Württemberg
		division_template = "Kommandobataillon"
		start_experience_factor = 0.9
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzeraufklärungslehrbatallion 3"
		location = 3271
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzeraufklärungsbatallion 5"
		location = 564
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzeraufklärungsbatallion 6"
		location = 11331
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzeraufklärungsbatallion 7"
		location = 6535
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Gebirgspanzeraufklärungsbatallion 8"
		location = 9681
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzeraufklärungsbatallion 12"
		location = 11417
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}

	division = {
		name = "Panzeraufklärungsbatallion 13"
		location = 6524
		division_template = "Panzeraufklärungsbatallion"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons3 #Heckler & Koch G36
		amount = 16100
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2
		amount = 12000
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment3
		amount = 2750
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1 #Panzerfaust 3
		amount = 1300
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_2 #Milan
		amount = 385
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 300
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1 #Stinger
		amount = 900
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1	 #Mercedes-Benz G-Class
		amount = 500
		producer = GER
	}

	add_equipment_to_stockpile = {
		type = MBT_2 #Leopard 1
		amount = 670
	}
	add_equipment_to_stockpile = {
		type = MBT_4 #Leopard 2A4
		amount = 1728
	}
	add_equipment_to_stockpile = {
		type = Rec_tank_0 #SPz 11-2 Kurz
		amount = 409
		#version_name = "SPz 11-2 Kurz"
	}
	add_equipment_to_stockpile = {
		type = Rec_tank_0 #Tpz-1 Fuchs
		amount = 114
		#version_name = "TPz-1"
	}
	add_equipment_to_stockpile = {
		type = IFV_4 #Marder 1 A3
		amount = 2122
	}
	add_equipment_to_stockpile = {
		type = APC_3 #Tpz-1 Fuchs
		amount = 909
	}
	add_equipment_to_stockpile = {
		type = APC_1 #M113 APC
		amount = 2167
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #M101
		amount = 118
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = artillery_1 #FH-70
		amount = 196
	}
	add_equipment_to_stockpile = {
		type = SP_arty_1 #M109A3
		amount = 499
		#version_name = "M109A3"
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = SP_arty_2 #Panzerhaubitze 2000
		amount = 165
	}
	add_equipment_to_stockpile = {
		type = SP_R_arty_0 #LARS 110
		amount = 50
	}
	add_equipment_to_stockpile = {
		type = SP_R_arty_1 #M270
		amount = 150
	}
	add_equipment_to_stockpile = {
		type = SP_Anti_Air_0 #Flakpanzer Gepard
		amount = 143
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter1 #MBB Bo 105
		amount = 199
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 188
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sikorsky CH-53 Sea Stallion
		amount = 107
		producer = USA
		#version_name = "Sikorsky CH-53 Sea Stallion"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bo 105
		amount = 60
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aérospatiale SA-316
		amount = 28
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter3
		amount = 13
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Westland WG-13 Lynx
		amount = 22
		producer = ENG
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sea King HU5
		amount = 21
		#version_name = "Sea King HU5"
		producer = USA
	}
}