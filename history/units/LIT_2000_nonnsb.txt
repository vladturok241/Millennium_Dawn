﻿division_template = {
	name = "Lietuvos Specialiųjų Operacijų Pajėgos"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Mechanizuotoji Pėstininkų Brigada"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mech_Inf_Bat = { x = 1 y = 0 }
		Mech_Inf_Bat = { x = 1 y = 1 }
	}
	support = {
		L_Recce_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Motorizuotoji Pėstininkų Brigada"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Mech_Recce_Comp = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

units = {

	division = {
		name = "Lietuvos Specialiųjų Operacijų Pajėgos"
		location = 3320		#Vilnius
		division_template = "Lietuvos Specialiųjų Operacijų Pajėgos"
		start_experience_factor = 0.5
		start_equipment_factor = 1.0
		force_equipment_variants = { infantry_weapons1 = { owner = "SOV" } }
		force_equipment_variants = { util_vehicle_1 = { owner = "SOV" } }
		force_equipment_variants = { Anti_tank_0 = { owner = "SOV" } }
		force_equipment_variants = { Anti_Air_0 = { owner = "SOV" } }
	}

	division = {
		name = "Mechanizuotoji Pėstininkų Brigada 'Geležinis Vilkas'"
		location = 6296		#Rukla
		division_template = "Mechanizuotoji Pėstininkų Brigada"
		start_experience_factor = 0.3
		start_equipment_factor = 1
		force_equipment_variants = { infantry_weapons1 = { owner = "SOV" } }
		force_equipment_variants = { command_control_equipment1 = { owner = "USA" } }
		force_equipment_variants = { util_vehicle_1 = { owner = "SOV" } }
		force_equipment_variants = { Anti_tank_0 = { owner = "SOV" } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { owner = "SOV" } }
		force_equipment_variants = { Anti_Air_0 = { owner = "SOV" } }
		force_equipment_variants = { APC_1 = { owner = "SOV" } }
	}

	division = {
		name = "Motorizuotoji Pėstininkų Brigada 'Vakarai'"
		location = 3288		#Klaipeda
		division_template = "Motorizuotoji Pėstininkų Brigada"
		start_experience_factor = 0.3
		start_equipment_factor = 1
		force_equipment_variants = { infantry_weapons1 = { owner = "SOV" } }
		force_equipment_variants = { command_control_equipment1 = { owner = "USA" } }
		force_equipment_variants = { util_vehicle_1 = { owner = "SOV" } }
		force_equipment_variants = { Anti_tank_0 = { owner = "SOV" } }
		force_equipment_variants = { Heavy_Anti_tank_0 = { owner = "SOV" } }
		force_equipment_variants = { Anti_Air_0 = { owner = "SOV" } }
		force_equipment_variants = { APC_1 = { owner = "SWE" } }
	}
}
instant_effect = {
	add_equipment_to_stockpile = {
		type = transport_helicopter1 		#Mi-8
		amount = 4
		producer = SOV
	}

}