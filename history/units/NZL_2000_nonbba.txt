﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = naval_plane1 # "P-3 Orion"
		amount = 6
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 # "3rd Gen MR" A-4K & T NEED FIXING FOR PROPER VERSION.
		amount = 19
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 # C-130H and Boeing 727
		amount = 7
		producer = USA
	}
}