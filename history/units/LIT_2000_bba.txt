﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Aero L-39"
		type = small_plane_strike_airframe_1
		amount = 6
		producer = CZE
	}
	add_equipment_to_stockpile = {
		variant_name = "An-26"
		type = large_plane_air_transport_airframe_1
		amount = 25
		producer = SOV
	}
}