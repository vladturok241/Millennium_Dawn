﻿units = {

	### Naval OOB ###
	fleet = {
		name = "Libyan Navy"
		naval_base = 1149
		task_force = {
			name = "Libyan Navy"
			location = 1149
			ship = { name = "Al Hani" definition = corvette start_experience_factor = 0.30 equipment = { corvette_2 = { amount = 1 owner = LBA creator = SOV version_name = "Koni-Class" } } }
			ship = { name = "Al Ghardabia" definition = corvette start_experience_factor = 0.30 equipment = { corvette_2 = { amount = 1 owner = LBA creator = SOV version_name = "Koni-Class" } } }
			ship = { name = "Tariq Ibn Ziyad" definition = corvette start_experience_factor = 0.30 equipment = { corvette_2 = { amount = 1 owner = LBA creator = SOV } } }
			ship = { name = "Ain Al Gazala" definition = corvette start_experience_factor = 0.30 equipment = { corvette_2 = { amount = 1 owner = LBA creator = SOV } } }
			ship = { name = "Ain Zaara" definition = corvette start_experience_factor = 0.30 equipment = { corvette_2 = { amount = 1 owner = LBA creator = SOV } } }
			ship = { name = "Al Badr" definition = attack_submarine start_experience_factor = 0.30 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = LBA creator = SOV version_name = "Foxtrot-Class" } } }
			ship = { name = "Al Fatah" definition = attack_submarine start_experience_factor = 0.30 equipment = { diesel_attack_submarine_1 = { amount = 1 owner = LBA creator = SOV version_name = "Foxtrot-Class" } } }
		}
	}
}
