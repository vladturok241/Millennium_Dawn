﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		variant_name = "JAS 39 Gripen"
		amount = 90
		producer = SWE
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "JA-37 Viggen"
		amount = 130
		producer = SWE
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "SH-37 Viggen"
		amount = 14
		producer = SWE
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Sk-60"
		amount = 100
		producer = SWE
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130 Hercules"
		amount = 12
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_awacs_airframe_2
		variant_name = "S-100B Argus"
		amount = 6
		producer = SWE
	}

	#Helicopters

	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bo 105
		amount = 20
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1
		amount = 5
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1
		amount = 14
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1
		amount = 11
		producer = FRA
	}
}