﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #MiG-21 Fishbed
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #MiG-29 Fulcrum
		amount = 5
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter2 #J-22 Orao
		amount = 63
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #G-4 Super Galeb
		amount = 34
	}
}