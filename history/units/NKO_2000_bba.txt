﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "MiG-21s Bis"
		type = small_plane_strike_airframe_1
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "J-6"
		type = small_plane_strike_airframe_1
		amount = 159
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "J-7"
		type = small_plane_strike_airframe_1
		amount = 130
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-29 Fulcrum"
		type = small_plane_airframe_2
		amount = 46
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-23"
		type = small_plane_strike_airframe_1
		amount = 46
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Harbin H-5"
		type = large_plane_airframe_1
		amount = 80
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-7"
		type = small_plane_strike_airframe_1
		amount = 18
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-25"
		type = medium_plane_cas_airframe_2
		amount = 35
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "An-26"					#in place of An-24 & Il-18 & Il-62M
		type = large_plane_air_transport_airframe_1
		amount = 8
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Tu-154M"
		type = large_plane_air_transport_airframe_1
		amount = 10
		producer = SOV
	}
}