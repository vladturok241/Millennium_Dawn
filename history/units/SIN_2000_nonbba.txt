﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #A-4SU Super Skyhawk
		amount = 68
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5S Tiger II
		amount = 45
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter3 #F-16C Fighting Falcon
		amount = 49
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 9
		producer = USA
	}
}