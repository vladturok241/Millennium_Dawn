﻿division_template = {
	name = "Motastralkovaya Brigada" #Armored Inf

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		armor_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 0 y = 4 }
		SP_Arty_Bat = { x = 1 y = 0 }
		SP_Arty_Bat = { x = 1 y = 1 }
	}
	support = {
		L_Engi_Comp = { x = 0 y = 0 }
		SP_AA_Battery = { x = 0 y = 1 }
		combat_service_support_company = { x = 0 y = 2 }
	}
}

division_template = {
	name = "Brigada VDV"

	regiments = {
		Mech_Air_Inf_Bat = { x = 0 y = 0 }
		Mech_Air_Inf_Bat = { x = 0 y = 1 }
		Mech_Air_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Brigada Spetsnaza"					#Spetsnaz Brigade

	regiments = {
		Special_Forces = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
		Special_Forces = { x = 0 y = 3 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}

	priority = 2
}

units = {
	division = {
		name = "120-ya Asobnaya Mekhanizavanaya Brigada"
		location = 11370		#Minsk
		division_template = "Motastralkovaya Brigada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	division = {
		name = "19-ya Asobnaya Mekhanizavanaya Brigada"
		location = 699
		division_template = "Motastralkovaya Brigada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	division = {
		name = "6-ya Asobnaya Mekhanizavanaya Brigada"
		location = 3393			#Grodno
		division_template = "Motastralkovaya Brigada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	division = {
		name = "11-ya Asobnaya Mekhanizavanaya Brigada"
		location = 6359			#Slonim
		division_template = "Motastralkovaya Brigada"
		start_experience_factor = 0.1
		start_equipment_factor = 0.01
	}

	division = {
		name = "38-ya Asobnaya Mabilnaya Brigada"
		location = 3392		#Brest
		division_template = "Brigada VDV"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "11-ya Asobnaya Mabilnaya Brigada"
		location = 11241		#Polotsk
		division_template = "Brigada VDV"
		start_experience_factor = 0.2
		start_equipment_factor = 0.01
	}

	division = {
		name = "5-ya Brigada Spetsnaza"	#ok
		location = 11370		#Maryina Horka
		division_template = "Brigada Spetsnaza"
		start_experience_factor = 0.3
		start_equipment_factor = 0.01
	}
}

instant_effect = {

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72B"
		amount = 646
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-80"
		amount = 95
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-2"
		amount = 975
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 81
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMD"
		amount = 154
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 193
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-70"
		amount = 445
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MT-LB"
		amount = 92
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_1
		producer = SOV
		variant_name = "BM-30 Smerch"
		amount = 36
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 126
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		producer = SOV
		variant_name = "BM-27 Uragan"
		amount = 72
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S19 Msta"
		amount = 12
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S3 Akatsiya"
		amount = 108
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 198
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S5 Giatsynt"
		amount = 116
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S9 Nona"
		amount = 48
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = artillery_0	#Various Soviet artillery pieces
		amount = 228
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_0		#SA-8 Osa
		amount = 250
		producer = SOV
		variant_name = "9K33 Osa"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 40
		producer = SOV
		variant_name = "SA-13 Strela-10"
	}

	add_equipment_to_stockpile = {
		type = spaa_hull_1
		amount = 8
		producer = SOV
		variant_name = "SA-15 Tor-M"
	}

	add_equipment_to_stockpile = {
		type = command_control_equipment1
		amount = 1000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 500
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 1000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Anti_tank_1
		amount = 1000
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1
		amount = 400
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = infantry_weapons1 #AK74
		amount = 10000
		producer = SOV
	}

	#HELOS
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0
		amount = 69
		producer = SOV
		variant_name = "Mil Mi-24"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1		#Mi-8
		amount = 160
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2		#Mi-26
		amount = 14
		producer = SOV
	}
}