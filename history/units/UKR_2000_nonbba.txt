﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = strategic_bomber2 #Tu-22M Blinder
		amount = 32
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = strategic_bomber2 #Tu-22M Blinder
		amount = 32
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter2 #Su-24
		amount = 104
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = cas2 #SU-25
		amount = 63
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = CV_MR_Fighter2 #MiG-29
		amount = 219
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2 #SU-27
		amount = 292
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #Il-76 Candid
		amount = 60
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = transport_plane3 #An-70
		amount = 45
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #Aero L-39 Albatros
		amount = 345
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2	 #Aero L-39
		amount = 37
		producer = CZE
	}
}