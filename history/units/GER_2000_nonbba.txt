﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = Strike_fighter2 #Tornado
		amount = 311
	}
	add_equipment_to_stockpile = {
		type = naval_plane2 #Atlantique 2
		amount = 16
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter1 #F-4 Phantom II
		amount = 152
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter3 #EF-2000 Typhoon
		amount = 8
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #MiG-21 Fishbed
		amount = 1
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter1 #MiG-23 Flogger
		amount = 2
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter1 #Su-22 Fitter
		amount = 1
		producer = SOV
		#version_name = "Su-22 Fitter"
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-160R Transall
		amount = 83
		producer = FRA
	}
}