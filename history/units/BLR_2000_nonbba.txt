﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2		#L-39
		amount = 10
		producer = CZE
	}

	#Aircraft
	add_equipment_to_stockpile = {
		type = MR_Fighter2		#MiG-29
		amount = 50
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = AS_Fighter1		#MiG-23
		amount = 36
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = AS_Fighter2		#MiG-27
		amount = 34
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = AS_Fighter1		#MiG-23
		amount = 42
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = cas2				#Su-25
		amount = 80
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_plane2			#Il-76 + An-26 so they can form a sqdrn
		amount = 24
		producer = SOV
	}
}