units = {
	fleet = {
		name = "Italian Frigates"
		naval_base = 4076
		task_force = {
			name = "Italian Frigates"
			location = 4076
			ship = { name = "Scirocco (F773)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_2 = { amount = 1 owner = ITA creator = ITA } } }
			ship = { name = "Aliseo (F774)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_2 = { amount = 1 owner = ITA creator = ITA } } }
			ship = { name = "Euro (F775)" definition = frigate start_experience_factor = 0.65 equipment = { frigate_2 = { amount = 1 owner = ITA creator = ITA } } }
		}
	}
}