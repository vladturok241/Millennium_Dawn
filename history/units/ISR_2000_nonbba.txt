﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = AS_Fighter1 #F-4 Phantom II
		amount = 70
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2 #F-15A Eagle
		amount = 46
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter3 #F-15C Eagle
		amount = 27
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter3 #F-15E Strike Eagle
		amount = 25
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #F-16A Fighting Falcon
		amount = 109
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter3 #F-16C Fighting Falcon
		amount = 128
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 22
		producer = USA
	}
}