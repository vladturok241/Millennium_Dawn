instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Pilatus PC-9"
		amount = 12
		producer = SWI
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Let L-410"
		amount = 7
		producer = CZE
	}

}