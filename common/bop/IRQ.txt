IRQ_clergy_balance_of_power_category = {
	initial_value = 0.25
	left_side = IRQ_secularists_bop_left_side
	right_side = IRQ_clergy_right_side
	decision_category = IRQ_clergy_balance_of_power_category
	range = {
		id = IRQ_secularists_bop_mid_range
		min = -0.1
		max = 0.1
		modifier = {
			stability_factor = 0.10
		}
	}

	side = {
		id = IRQ_secularists_bop_left_side
		icon = GFX_bop_GER_Bundeswehr
		range = {
			id = IRQ_secularists_bop_left_win_range
			min = -1
			max = -0.9

			on_activate = {
				remove_power_balance = {
					id = IRQ_clergy_balance_of_power_category
				}
				newline = yes
				swap_ideas = {
					remove_idea = sunni
					add_idea = pluralist
				}
				newline = yes
				add_ideas = IRQ_secular_society
			}
		}

		range = {
			id = IRQ_secularists_bop_left_advanced_range
			min = -0.9
			max = -0.6
			modifier = {
				production_speed_offices_factor = 0.25
				corporate_tax_income_multiplier_modifier = 0.10
				econ_cycle_upg_cost_multiplier_modifier = -0.10
				projects_cost_modifier = -0.20
				production_speed_buildings_factor = 0.15
			}
		}

		range = {
			id = IRQ_secularists_bop_left_medium_range
			min = -0.6
			max = -0.4
			modifier = {
				production_speed_offices_factor = 0.20
				corporate_tax_income_multiplier_modifier = 0.05
				econ_cycle_upg_cost_multiplier_modifier = -0.10
				production_speed_buildings_factor = 0.10
				projects_cost_modifier = -0.15
			}
		}

		range = {
			id = IRQ_secularists_bop_left_low_range
			min = -0.4
			max = -0.1
			modifier = {
				production_speed_offices_factor = 0.10
				production_speed_buildings_factor = 0.05
				corporate_tax_income_multiplier_modifier = 0.03
				econ_cycle_upg_cost_multiplier_modifier = -0.05
			}
		}
	}

	side = {
		id = IRQ_clergy_right_side
		icon = GFX_bop_GER_Bundeswehr
		range = {
			id = IRQ_clergy_bop_right_win_range
			min = 0.9
			max = 1

			on_activate = {
				remove_power_balance = {
					id = IRQ_clergy_balance_of_power_category
				}
				newline = yes
				add_ideas = IRQ_sharia_society
			}
		}

		range = {
			id = IRQ_clergy_bop_right_advanced_range
			min = 0.6
			max = 0.9
			modifier = {
				conscription = 0.07
				drift_defence_factor = 0.25
				army_attack_factor = 0.10
				army_personnel_cost_multiplier_modifier = -0.20
				training_time_factor = -0.15
				terrain_penalty_reduction = 0.10
				war_support_factor = 0.10
			}
		}

		range = {
			id = IRQ_clergy_bop_right_medium_range
			min = 0.4
			max = 0.6
			modifier = {
				conscription = 0.05
				drift_defence_factor = 0.15
				army_attack_factor = 0.05
				army_personnel_cost_multiplier_modifier = -0.15
				war_support_factor = 0.10
			}
		}

		range = {
			id = IRQ_clergy_bop_right_low_range
			min = 0.1
			max = 0.4
			modifier = {
				conscription = 0.03
				drift_defence_factor = 0.10
				army_personnel_cost_multiplier_modifier = -0.10
				war_support_factor = 0.05
			}
		}
	}
}

# Post - War Iraq

IRQ_debaath_balance_of_power_category = {
	initial_value = 0.25
	left_side = IRQ_antibaathists_bop_left_side
	right_side = IRQ_baathists_right_side
	decision_category = IRQ_debaath_balance_of_power_category
	range = {
		id = IRQ_antibaathists_bop_mid_range
		min = -0.1
		max = 0.1
		modifier = {
			stability_factor = -0.10
			drift_defence_factor = -0.30
		}
	}

	side = {
		id = IRQ_antibaathists_bop_left_side
		icon = GFX_bop_GER_Bundeswehr
		range = {
			id = IRQ_antibaathists_bop_left_win_range
			min = -1
			max = -0.9

			on_activate = {
				remove_power_balance = {
					id = IRQ_debaath_balance_of_power_category
				}
				remove_ideas = IRQ_baath_equilibrium
			}
		}

		range = {
			id = IRQ_antibaathists_bop_left_advanced_range
			min = -0.9
			max = -0.6
			modifier = {
				nationalist_drift = -0.05
				nationalist_acceptance = -20
				fascism_drift = -0.03
				fascism_acceptance = -15
				neutrality_drift = 0.03
				neutrality_acceptance = 15
				democratic_drift = 0.05
				democratic_acceptance = 20
			}
		}

		range = {
			id = IRQ_antibaathists_bop_left_medium_range
			min = -0.6
			max = -0.4
			modifier = {
				nationalist_drift = -0.03
				nationalist_acceptance = -15
				fascism_drift = -0.01
				fascism_acceptance = -10
				neutrality_drift = 0.01
				neutrality_acceptance = 10
				democratic_drift = 0.03
				democratic_acceptance = 15
			}
		}

		range = {
			id = IRQ_antibaathists_bop_left_low_range
			min = -0.4
			max = -0.1
			modifier = {
				nationalist_drift = -0.01
				fascism_drift = -0.01
				democratic_drift = 0.01
			}
		}
	}

	side = {
		id = IRQ_baathists_right_side
		icon = GFX_bop_GER_Bundeswehr
		range = {
			id = IRQ_baathists_bop_right_win_range
			min = 0.9
			max = 1

			on_activate = {
				remove_power_balance = {
					id = IRQ_debaath_balance_of_power_category
				}
				country_event = {
					id = iraq_civil_war.204
				}
			}
		}

		range = {
			id = IRQ_baathists_bop_right_advanced_range
			min = 0.6
			max = 0.9
			modifier = {
				nationalist_drift = 0.05
				communism_drift = 0.03
				democratic_drift = -0.03
				fascism_drift = 0.02
				fascism_acceptance = 10
				democratic_acceptance = -15
				communism_acceptance = 15
				nationalist_acceptance = 20
				stability_factor = -0.10
				war_support_factor = 0.15
			}
		}

		range = {
			id = IRQ_baathists_bop_right_medium_range
			min = 0.4
			max = 0.6
			modifier = {
				nationalist_drift = 0.03
				communism_drift = 0.02
				democratic_drift = -0.02
				fascism_drift = 0.01
				fascism_acceptance = 10
				democratic_acceptance = -10
				communism_acceptance = 10
				nationalist_acceptance = 10
				stability_factor = -0.05
				war_support_factor = 0.10
			}

		}

		range = {
			id = IRQ_baathists_bop_right_low_range
			min = 0.1
			max = 0.4
			modifier = {
				nationalist_drift = 0.02
				communism_drift = 0.01
				democratic_drift = -0.01
				stability_factor = -0.03
				war_support_factor = 0.05
			}

		}
	}
}