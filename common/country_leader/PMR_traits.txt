leader_traits = {

	PMR_general_monarchist_christian = {
		random = no
		army_attack_factor = 0.03
		army_speed_factor = 0.03
		offensive_war_stability_factor = 0.03
		nationalist_drift = 0.01
		social_cost_multiplier_modifier = -0.05
		ai_will_do = {
			factor = 0
		}
	}
}