# List of names and logos for intelligence agencies
# available trigger will determine if the logo is in the list of available logos or not
# names is a list of names. Can be a localization entry, or the name directly. A random name will be picked from them.

################
#North America #
################

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_usa
	names = { "Central Intelligence Agency (CIA)" }

	default = { tag = USA }
	available = { original_tag = USA }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_can
	names = { "Canadian Security Intelligence Service (CSIS)" }
	available = { original_tag = CAN }
	default = { tag = CAN }
}

################
#South America #
################

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_bra
	names = { "Brazilian Intelligence Agency (ABIN)" }
	default = { tag = BRA }
	available = { original_tag = BRA }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_arg
	names = { "The Federal Intelligence Agency (AFI)" }
	default = { tag = ARG }
	available = { original_tag = ARG }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_col
	names = { "National Intelligence Directorate" }
	default = { tag = COL }
	available = { original_tag = COL }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_col
	names = { "National Intelligence Directorate" }
	default = { tag = COL }
	available = { original_tag = COL }
}


intelligence_agency = {
	picture = GFX_intelligence_agency_logo_chl
	names = { "Agencia Nacional de Inteligencia" }
	default = { tag = CHL }
	available = { original_tag = CHL }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ven
	names = { "SEBIN" "Bolivarian National Intelligence Service" "Dirección Nacional de los Servicios de Inteligencia y Prevención (DISIP)" }
	default = { tag = VEN }
	available = { original_tag = VEN }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_pru
	names = { "Dirección Nacional de Inteligencia (DINI)" }
	default = { tag = PRU }
	available = { original_tag = PRU }
}


#########
#Europe #
#########

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_alb
	names = { "State Intelligence Service (SHISH)" }
	default = { tag = ALB }
	available = { original_tag = ALB }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_eng
	names = { "Secret Intelligence Service MI6 (SIS)" }
	default = { tag = ENG }
	available = { original_tag = ENG }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_sov
	names = { "Federal Security Service (FSB)" }
	default = { tag = SOV }
	available = { original_tag = SOV }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_fra
	names = { "Directorate-General for External Security (DGSE)" }
	default = { tag = FRA }
	available = { original_tag = FRA }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ger
	names = { "Federal Intelligence Service (BND)" }
	default = { tag = GER }
	available = { original_tag = GER }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ita
	names = { "Department of Information Security (AISE) (AISI)" }
	default = { tag = ITA }
	available = { original_tag = ITA }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_swe
	names = { "Swedish Security Service (SÄPO)" }
	default = { tag = SWE }
	available = { original_tag = SWE }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_nor
	names = { "Norwegian Intelligence Service (NIS)" }
	default = { tag = NOR }
	available = { original_tag = NOR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_bel
	names = { "State Security Service (VSSE)" }
	default = { tag = BEL }
	available = { original_tag = BEL }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_hol
	names = { "General Intelligence and Security Service (AIVD)" }
	default = { tag = HOL }
	available = { original_tag = HOL }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_rom
	names = { "Romanian Intelligence Service (SRI)" }
	default = { tag = ROM }
	available = { original_tag = ROM }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_mlv
	names = { "Information and Security Service of the Republic of Moldova (SIS)" }
	default = { tag = MLV }
	available = { original_tag = MLV }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_pmr
	names = { "Ministry of State Security (MGB)" }
	default = { tag = PMR }
	available = { original_tag = PMR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_spr
	names = { "National Intelligence Centre (CNI)" }
	default = { tag = SPR }
	available = { original_tag = SPR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_pol
	names = { "Agencja Wywiadu (AW)" }
	default = { tag = POL }
	available = { original_tag = POL }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_por
	names = { "Sistema de Informações da República Portuguesa (SIRP)" }
	default = { tag = POR }
	available = { original_tag = POR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_fin
	names = { "Finnish Security Intelligence Service (Supo)" }
	default = { tag = FIN }
	available = { original_tag = FIN }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_cro
	names = { "Security and Intelligence Agency" }
	default = { tag = CRO }
	available = { original_tag = CRO }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_geo
	names = { "State Security Service" }
	default = { tag = GEO }
	available = { original_tag = GEO }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_gre
	names = { "National Intelligence Service (NIS)" }
	default = { tag = GRE }
	available = { original_tag = GRE }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ukr
	names = { "Security Service of Ukraine (SSU)" }
	default = { tag = UKR }
	available = { original_tag = UKR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_blr
	names = { "Committee for State Security (KGB)" }
	default = { tag = BLR }
	available = { original_tag = BLR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_lat
	names = { "State Security Service (VDD)" }
	default = { tag = LAT }
	available = { original_tag = LAT }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_lit
	names = { "State Security Department (VSD)" }
	default = { tag = LIT }
	available = { original_tag = LIT }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_pol
	names = { "Agencja Wywiadu" }
	default = { tag = POL }
	available = { original_tag = POL }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ser
	names = { "Bezbednosno-informativna agencija (BIA)" }
	default = { tag = SER }
	available = { original_tag = SER }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_bul
	names = { "Dohržavna Agencija (DAR)" }
	default = { tag = BUL }
	available = { original_tag = BUL }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_nor
	names = { "Etterretningstjenesten" }
	default = { tag = NOR }
	available = { original_tag = NOR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_cze
	names = { "Bezpecnostní Informacní Sluzba (BIS)" }
	default = { tag = CZE }
	available = { original_tag = CZE }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_slo
	names = { "Vojenské Spravodajstvo (VS)" }
	default = { tag = SLO }
	available = { original_tag = SLO }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_cyp
	names = { "Cyprus Intelligence Service (CIS)" }
	default = { tag = CYP }
	available = { original_tag = CYP }
}


#######
#Asia #
#######

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ARM
	names = { "National Security Service of Armenia" }
	default = { tag = ARM }
	available = { original_tag = ARM }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_aze
	names = { "State Security Service" }
	default = { tag = AZE }
	available = { original_tag = AZE }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_jap
	names = { "Public Security Intelligence Agency" }
	default = { tag = JAP }
	available = { original_tag = JAP }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_nko
	names = { "Reconnaissance General Bureau" }
	default = { tag = NKO }
	available = { original_tag = NKO }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_pak
	names = { "Inter-Services Intelligence (ISI)" }
	default = { tag = PAK }
	available = { original_tag = PAK }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_prc
	names = { "Ministry of State Security (MSS)" }
	default = { tag = CHI }
	available = { original_tag = CHI }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ast
	names = { "Australian Secret Intelligence Service (ASIS)" }
	default = { tag = AST }
	available = { original_tag = AST }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_raj
	names = { "Research and Analysis Wing (RAW)" }
	default = { tag = RAJ }
	available = { original_tag = RAJ }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_chi
	names = { "National Intelligence Agency (NIA)" }
	default = { tag = TAI }
	available = { original_tag = TAI }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_brm
	names = { "Bureau of Special Investigation (BSI)" }
	default = { tag = BRM }
	available = { original_tag = BRM }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_kor
	names = { "National Intelligence Service (NIS)" }
	default = { tag = KOR }
	available = { original_tag = KOR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_may
	names = { "Royal Intelligence Corps" }
	default = { tag = MAY }
	available = { original_tag = MAY }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_phi
	names = { "National Intelligence Coordinating Agency" }
	default = { tag = PHI }
	available = { original_tag = PHI }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_taj
	names = { "State Committee for National Security" }
	default = { tag = TAJ }
	available = { original_tag = TAJ }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_kaz
	names = { "National Security Committee" }
	default = { tag = KAZ }
	available = { original_tag = KAZ }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_nzl
	names = { "Government Communications Security Bureau" }
	default = { tag = NZL }
	available = { original_tag = NZL }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_trk
	names = { "Ministry for National Security" }
	default = { tag = TRK }
	available = { original_tag = TRK }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_uzb
	names = { "State Security Service" }
	default = { tag = UZB }
	available = { original_tag = UZB }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_nep
	names = { "National Investigation Department (NID)" }
	default = { tag = NEP }
	available = { original_tag = NEP }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_sin
	names = { "Security and Intelligence Division (SID)" }
	default = { tag = SIN }
	available = { original_tag = SIN }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ind
	names = { "Indonesian State Intelligence Agency" }
	default = { tag = IND }
	available = { original_tag = IND }
}


##############
#Middle East #
##############

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_afg
	names = { "National Directorate of Security (NDS)" }
	default = { tag = AFG }
	available = { original_tag = AFG NOT = { country_exists = TAL } }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_isr
	names = { "Mossad" }
	default = { tag = ISR }
	available = { original_tag = ISR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_tur
	names = { "National Intelligence Organization (MİT)" }
	default = { tag = TUR }
	available = { original_tag = TUR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_per
	names = { "Ministry of Intelligence (VAJA)" }
	default = { tag = PER }
	available = {
		original_tag = PER
		has_government = communism
	}
}
#
intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_5
	names = { "Ministry of Intelligence" }
	default = { tag = PER }
	available = {
		original_tag = PER
		NOT = {
			OR = {
				emerging_hardline_shiite_are_in_power = yes
				emerging_moderate_shiite_are_in_power = yes
			}
		}
	}
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_SAU
	names = { "General Intelligence Presidency (GIP)" }
	default = { tag = SAU }
	available = { original_tag = SAU }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_SYR
	names = { "Military Intelligence Directorate" }
	default = { tag = SYR }
	available = { original_tag = SYR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_egy
	names = { "General Intelligence Service Mukhabarat (GIS)" }
	default = { tag = EGY }
	available = { original_tag = EGY }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_irq
	names = { "Iraqi National Intelligence Service (INIS)" }
	default = { tag = IRQ }
	available = { original_tag = IRQ }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_irq1
	names = { "Iraqi Falcons Intelligence Cell" }
	default = { tag = IRQ }
	available = { original_tag = IRQ }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_KUR
	names = { "Asayîş" }
	default = { tag = KUR }
	available = { original_tag = KUR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ROJ
	names = { "Intelligence Directorate" }
	default = { tag = ROJ }
	available = { original_tag = ROJ }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_leb
	names = { "General Security Directorate" }
	default = { tag = LEB }
	available = { original_tag = LEB }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_oma
	names = { "Qatar State Security" }
	default = { tag = OMA }
	available = { original_tag = OMA }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_uae
	names = { "Signals Intelligence Agency (SIA)" }
	default = { tag = UAE }
	available = { original_tag = UAE }
}

#########
#Africa #
#########

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ALG
	names = { "Department of Intelligence Security (DRS)" }
	default = { tag = ALG }
	available = { original_tag = ALG }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_eth
	names = { "National Intelligence and Security Service (NISS)" }
	default = { tag = ETH }
	available = { original_tag = ETH }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_sud
	names = { "National Intelligence & Security Service (NISS)" }
	default = { tag = SUD }
	available = { original_tag = SUD }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_drc
	names = { "Agence Nationale de Renseignements (ANR)" }
	default = { tag = DRC }
	available = { original_tag = DRC }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_lba
	names = { "Mukhabarat el-Jamahiriya" }
	default = { tag = LBA }
	available = { original_tag = LBA }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_lib
	names = { "National Security Agency" }
	default = { tag = LIB }
	available = { original_tag = LIB }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_mor
	names = { "General Directorate for Territorial Surveillance (DGST)" }
	default = { tag = MOR }
	available = { original_tag = MOR }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_som
	names = { "National Intelligence and Security Agency (NISA)" }
	default = { tag = SOM }
	available = { original_tag = SOM }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_bot
	names = { "Directorate of Intelligence and Security (DIS)" }
	default = { tag = BOT }
	available = { original_tag = BOT }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_saf
	names = { "National Intelligence Agency (NIA)" }
	default = { tag = SAF }
	available = { original_tag = SAF }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_nig
	names = { "National Intelligence Agency (NIA)" }
	default = { tag = NIG }
	available = { original_tag = NIG }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_gah
	names = { "Bureau of National Investigation (BNI)" }
	default = { tag = GAH }
	available = { original_tag = GAH }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_cha
	names = { "L'Agence Nationale de Sécurité (ANS)" }
	default = { tag = CHA }
	available = { original_tag = CHA }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_cam
	names = { "Brigade Mixte Mobile" }
	default = { tag = CAM }
	available = { original_tag = CAM }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_uga
	names = { "External Security Organisation (ESO)" }
	default = { tag = UGA }
	available = { original_tag = UGA }
}

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_ken
	names = { "National Intelligence Service (NIS)" }
	default = { tag = KEN }
	available = { original_tag = KEN }
}

##########
#Generic #
##########

intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_1
	names = { "Rooke" }
	available = { always = yes }
	default = { always = yes }
}
intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_2
	names = { "Tower" }
	available = { always = yes }
	default = { always = yes }
}
intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_3
	names = { "Snake" }
	available = { always = yes }
	default = { always = yes }
}
intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_4
	names = { "Stag" }
	available = { always = yes }
	default = { always = yes }
}
intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_5
	names = { "Golden Lion" }
	available = { always = yes }
	default = { always = yes }
}
intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_6
	names = { "Arrow" }
	available = { always = yes }
	default = { always = yes }
}
intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_7
	names = { "Lion Head" }
	available = { always = yes }
	default = { always = yes }
}
intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_8
	names = { "Golden Eagle" }
	available = { always = yes }
	default = { always = yes }
}
intelligence_agency = {
	picture = GFX_intelligence_agency_logo_generic_9
	names = { "Hammer and Sickle" }
	available = {
		has_government = communism
	}
	default = { always = yes }
}