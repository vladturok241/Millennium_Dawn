GEO_cis_eu_influence_decision = {
#Certain focuses unlock due to this shit, kek
	GEO_expand_cis_influence = {
		icon = GFX_russia_star
		visible = { always = yes }
		available = {
			NOT = { has_country_flag = GEO_flag_eu_incr_stuff }
		}
		cost = 200

		days_remove = 25
		fire_only_once = no

		complete_effect = {
			custom_effect_tooltip = GEO_cis_trade_influence_incr_TT
			custom_effect_tooltip = GEO_EU_trade_influence_decr_TT
			hidden_effect = {
				add_to_variable = { GEO_cis_trade_influence = 5 }
				add_to_variable = { GEO_eu_trade_influence = -5 }
				clamp_variable = { var = GEO_cis_trade_influence max = 100 }
				clamp_variable = { var = GEO_eu_trade_influence min = 0 }
				set_country_flag = GEO_flag_cis_incr_stuff
			}
		}
		remove_effect = { clr_country_flag = GEO_flag_cis_incr_stuff }

		ai_will_do = {
			factor = 0
			modifier = {
				add = 5
				date < 2006.1.1
				has_global_flag = GEO_PRO_RUS_PATH
			}
			modifier = {
				add = 5
				date > 2010.1.1
				OR = {
					has_global_flag = GEO_NATIONALIST_2013_PATH
					has_global_flag = GEO_UNITED_GEORGIA_PATH
				}
			}
		}
	}

	GEO_expand_eu_influence = {
		icon = GFX_european_union_texticon
		visible = { always = yes }
		available = {
			NOT = { has_country_flag = GEO_flag_cis_incr_stuff }
		}
		cost = 200

		days_remove = 25
		fire_only_once = no

		complete_effect = {
			custom_effect_tooltip = GEO_EU_trade_influence_incr_TT
			custom_effect_tooltip = GEO_cis_trade_influence_decr_TT
			hidden_effect = {
				add_to_variable = { GEO_eu_trade_influence = 5 }
				add_to_variable = { GEO_cis_trade_influence = -5 }
				clamp_variable = { var = GEO_eu_trade_influence max = 100 }
				clamp_variable = { var = GEO_cis_trade_influence min = 0 }
				set_country_flag = GEO_flag_eu_incr_stuff
			}
		}
		remove_effect = { clr_country_flag = GEO_flag_eu_incr_stuff }

		ai_will_do = {
			factor = 0
			modifier = {
				add = 1
				check_variable = { from.GEO_eu_trade_influence < 80 }
				has_global_flag = GEO_PRO_WEST_SHEVA_PATH
			}
			modifier = {
				add = 5
				date < 2004.1.1
				has_global_flag = GEO_HELP_CHE
			}
			modifier = {
				add = 5
				date < 2006.1.1
				has_global_flag = GEO_PRO_WEST_SHEVA_PATH
			}
			modifier = {
				add = 5
				date > 2010.1.1
				has_global_flag = GEO_UNITED_NATIONAL_MOVEMENT_PATH
			}
		}
	}
}