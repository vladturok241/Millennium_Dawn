EST_baltictram_category = {
	# Opt-In/Opt-Out
	EST_bt_new_beginnings = {
		icon = GFX_decision_generic_research

		visible = {
			NOT = {
				has_idea = EST_baltictram_project
			}
		}

		custom_cost_trigger = {
			check_variable = {
				treasury > 9.99
			}
		}

		custom_cost_text = cost_10_0

		days_remove = 100


		ai_will_do = {
			factor = 30
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_new_beginnings"
			set_temp_variable = { treasury_change = -10 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_new_beginnings"
			add_ideas = EST_baltictram_project
		}
	}
	EST_bt_opt_out = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 25

		ai_will_do = {
			factor = 0
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_opt_out"
			remove_ideas = EST_baltictram_project
		}
	}


	# Ideas to Expand the Concept
	EST_bt_developing_ideas = {
		icon = GFX_decision_generic_research

		visible = { has_idea = EST_baltictram_project }

		cost = 90

		days_remove = 75

		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_developing_ideas"
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_developing_ideas"
			add_tech_bonus = {
				name = EST_bt_developing_ideas
				category = CAT_industry
				bonus = 0.15
				uses = 1
			}
			add_tech_bonus = {
				name = EST_bt_developing_ideas
				category = CAT_construction_tech
				bonus = 0.25
				uses = 1
			}
		}

		ai_will_do = {
			factor = 45
		}
	}

	EST_bt_getting_there = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 75

		days_remove = 50

		fire_only_once = yes

		ai_will_do = {
			factor = 45
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_getting_there"
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_getting_there"
			add_tech_bonus = {
				name = EST_bt_getting_there
				category = CAT_internet_tech
				bonus = 0.1
				uses = 1
			}
			add_tech_bonus = {
				name = EST_bt_getting_there
				category = CAT_computing_tech
				bonus = 0.1
				uses = 1
			}
		}
	}
	EST_bt_ai_breakthrough = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 120

		days_remove = 80

		fire_only_once = yes

		ai_will_do = {
			factor = 45
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_ai_breakthrough"
			set_temp_variable = { treasury_change = -4 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_ai_breakthrough"
			add_tech_bonus = {
				name = EST_bt_ai_breakthrough
				category = CAT_ai
				bonus = 0.15
				uses = 1
			}
		}
	}

	# Non-LAR Upgrade
	EST_bt_cybersec_upgrade = {
		allowed = { NOT = { has_dlc = "La Resistance" } }
		icon = GFX_decision_generic_research

		visible = { has_idea = EST_baltictram_project }

		cost = 60

		days_remove = 60

		fire_only_once = yes

		ai_will_do = {
			factor = 45
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_cybersec_upgrade"
			set_temp_variable = { treasury_change = -2.5 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_cybersec_upgrade"
			add_tech_bonus = {
				name = EST_bt_cybersec_upgrade
				category = CAT_encryption_tech
				bonus = 0.1
				uses = 1
			}
			add_tech_bonus = {
				name = EST_bt_cybersec_upgrade
				category = CAT_decryption_tech
				bonus = 0.2
				uses = 1
			}
		}
	}

	EST_bt_3d_printers = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 75

		days_remove = 50

		fire_only_once = yes

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_3d_printers"
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_3d_printers"
			add_tech_bonus = {
				name = EST_bt_3d_printers
				category = CAT_3d
				bonus = 0.2
				uses = 1
			}
		}

		ai_will_do = {
			factor = 45
		}
	}


	EST_bt_excavation = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 80

		days_remove = 75

		fire_only_once = yes

		ai_will_do = {
			factor = 45
		}

		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_excavation"
			add_tech_bonus = {
				name = EST_bt_excavation
				category = CAT_excavation_tech
				bonus = 0.5
				uses = 1
			}
		}
	}
	EST_bt_internet_speedup = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 55

		days_remove = 45

		fire_only_once = yes

		ai_will_do = {
			factor = 45
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_internet_speedup"
			set_temp_variable = { treasury_change = -3.5 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_internet_speedup"
			add_tech_bonus = {
				name = EST_bt_internet_speedup
				category = CAT_internet_tech
				bonus = 0.15
				uses = 2
			}
		}
	}
	EST_bt_nanofibres = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 110

		days_remove = 90

		fire_only_once = yes

		ai_will_do = {
			factor = 45
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_nanofibres"
			set_temp_variable = { treasury_change = -2.5 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_nanofibres"
			add_tech_bonus = {
				name = EST_bt_nanofibres
				category = CAT_nfibers
				bonus = 0.35
				uses = 2
			}
		}
	}
	EST_bt_radars = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 100

		days_remove = 100

		fire_only_once = yes

		ai_will_do = {
			factor = 45
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_radars"
			set_temp_variable = { treasury_change = -4 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_radars"
			add_tech_bonus = {
				name = EST_bt_radars
				category = CAT_internet_tech
				bonus = 0.25
				uses = 1
			}
		}
	}
	EST_bt_genetic_editing = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 125

		days_remove = 125

		fire_only_once = yes

		ai_will_do = {
			factor = 45
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_genetic_editing"
			set_temp_variable = { treasury_change = -4 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_genetic_editing"
			add_tech_bonus = {
				name = EST_bt_genetic_editing
				category = CAT_genes
				bonus = 0.5
				uses = 1
			}
		}
	}
	EST_bt_fuel_researching = {
		icon = GFX_decision_generic_research

		visible = {
			has_idea = EST_baltictram_project
		}

		cost = 100

		days_remove = 75

		fire_only_once = yes

		ai_will_do = {
			factor = 45
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision EST_bt_fuel_researching"
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision timeout EST_bt_fuel_researching"
			add_tech_bonus = {
				name = EST_bt_fuel_researching
				category = CAT_fuel_oil
				bonus = 0.5
				uses = 2
			}
		}
	}
}