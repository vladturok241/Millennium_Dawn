NOR_norge_mining_decision_cat = {
	icon = GFX_decision_category_generic_propaganda

	visible = {
		original_tag = NOR
		has_country_flag = NOR_norge_mining_decisions
	}
	priority = 250
}