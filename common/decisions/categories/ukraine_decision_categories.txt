################
##### UKR ######
################

UKR_stability_of_ukraine_decision_category = {
	icon = GFX_decision_ukr_stability_dec_group

	priority = 100

	allowed = {
		original_tag = UKR
	}

	# Should Hide After Initial War
	visible = {
		original_tag = UKR
		NOT = { has_country_flag = UKR_civil_war_in_the_donbass_flag }
	}
}

UKR_state_of_the_army_category = {
	icon = GFX_decision_ukr_stability_dec_group
	priority = 100

	allowed = {
		original_tag = UKR
	}

	visible = {
		original_tag = UKR
		OR = {
			has_country_flag = UKR_civil_war_in_the_donbass_flag
			has_war = yes
		}
	}
}

UKR_russian_bootlickers = {
	priority = 100

	allowed = {
		original_tag = UKR
	}

	visible = {
		original_tag = UKR
		has_idea = UKR_russian_dependent_military
	}
}
UKR_Interlegion_decision_category = {
	icon = GFX_decisions_category_inter_legion
		picture = GFX_decisions_interlegion
	priority = 100

	allowed = {
		original_tag = UKR
	}

	visible = {
		original_tag = UKR
		has_completed_focus = UKR_inter_legion
	}
}
UKR_UDA_decision_category = {
	icon = GFX_decisions_category_uda
		picture = GFX_decisions_uda
	priority = 100

	allowed = {
		original_tag = UKR
	}

	visible = {
		original_tag = UKR
		has_completed_focus = UKR_UDA
	}
}
UKR_uop_decision_category = {
	icon = GFX_decisions_category_ukrprom
		picture = GFX_decisions_ukrprom
	priority = 100

	allowed = {
		original_tag = UKR
	}

	visible = {
		original_tag = UKR
		has_completed_focus = UKR_found_the_uop
	}
}
UKR_reorganize_the_national_guard_ukraine_decision_category = {
	icon = GFX_decisions_category_natguard
		picture = GFX_decisions_natguard
	priority = 100

	allowed = {
		original_tag = UKR
	}

	visible = {
		original_tag = UKR
		has_completed_focus = UKR_reorganize_the_national_guard
	}
}
UKR_tro_brigades_ukraine_decision_category = {
	icon = GFX_decisions_category_tro
		picture = GFX_decisions_tro
	priority = 100

	allowed = {
		original_tag = UKR
	}

	visible = {
		original_tag = UKR
		has_completed_focus = UKR_ter_oborona
	}
}