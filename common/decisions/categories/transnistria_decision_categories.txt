# Author(s): Tasos, Kor, LordBogdanoff
# Last Modified Feb 07/02/2024 - LordBogdanoff
# File Was Bug-Fixed

PMR_eastern_europe_operations = {
	allowed = { original_tag = PMR }
	priority = 155
	icon = GFX_decisions_category_cobasna
	visible = {
		original_tag = PMR 
	}
}
PMR_russia_moldovian_way = {
	allowed = { original_tag = PMR }
	priority = 155 
	icon = GFX_decisions_category_rusmlv
	visible = {
		original_tag = PMR 
	}
}
PMR_sheriff_domination = {
	allowed = { original_tag = PMR }
	priority = 155 
	icon = GFX_decisions_category_sheriff
	visible = {
		has_completed_focus = PMR_smirnov_era
		NOT = { has_completed_focus = PMR_sheriff_start_bad }
	}
}
PMR_kazaks_prepare = {
	allowed = { original_tag = PMR }
	priority = 155 
	icon = GFX_decisions_category_kazaki
	visible = {
		original_tag = PMR
	}
}
PMR_Federation_category = {
	allowed = { original_tag = PMR }
	priority = 155 
	icon = GFX_decisions_category_federation
	visible = {
		original_tag = PMR
	}
}
