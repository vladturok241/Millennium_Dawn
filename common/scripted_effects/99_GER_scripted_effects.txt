
GER_update_commanders = {
### 2003
if = {
	limit = { AND = { date > 2002.1.2 date < 2003.1.2 } }
	create_corps_commander = {
		name = "Hans-Werner Fritz"
		desc = "GER_HANS_WERNER_FRITZ_DESC"
		picture = "Portrait_Hans_Werner_Fritz.dds"
		id = 80014
		traits = { panzer_leader }
		skill = 3
		attack_skill = 4
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 3
	}
}
### 2007
if = {
	limit = { AND = { date > 2006.1.2 date < 2007.1.2 } }
	create_corps_commander = {
		name = "Jörg Vollmer"
		desc = "GER_JORG_VOLLMER_DESC"
		picture = "Portrait_Jörg_Vollmer.dds"
		id = 80007
		traits = { infantry_leader }
		skill = 3
		attack_skill = 3
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 3
	}
}
### 2011
if = {
	limit = { AND = { date > 2010.1.2 date < 2011.1.2 } }
	create_corps_commander = {
			name = "Alfons Mais"
			desc = "GER_ALFONS_MAIS_DESC"
			picture = "Portrait_Alfons_Mais.dds"
			id = 80008
			traits = { commando paratrooper }
			skill = 3
			attack_skill = 4
			defense_skill = 2
			planning_skill = 3
			logistics_skill = 3
		}
	}
}

# HEALTHCARE COST
	update_GER_healthcare_cost = {
	custom_effect_tooltip = GER_healthcare_cost_tt
	add_to_variable = { GER_health_cost = GER_health_cost_change }
}
# WELFARE COST
	update_GER_welfare_cost = {
	custom_effect_tooltip = GER_welfare_cost_tt
	add_to_variable = { GER_welfare_cost = GER_welfare_cost_change }
}

# Centralization COST
	update_GER_centralization_cost = {
	custom_effect_tooltip = GER_centralization_cost_tt
	add_to_variable = { GER_centralization_cost = GER_centralization_cost_change }
}

# EDUCATION COST
	update_GER_education_cost = {
	custom_effect_tooltip = GER_education_cost_tt
	add_to_variable = { GER_education_cost = GER_education_cost_change }
}
# INTERNAL SECURITY COST
	update_GER_police_cost = {
	custom_effect_tooltip = GER_police_cost_tt
	add_to_variable = { GER_police_cost = GER_police_cost_change }
}
# INTEREST RATE COST
	update_GER_interest_cost = {
	custom_effect_tooltip = GER_interest_cost_tt
	add_to_variable = { GER_interest_cost = GER_interest_cost_change }
}
# MILITARY COST
	update_GER_military_cost = {
	custom_effect_tooltip = GER_military_cost_tt
	add_to_variable = { GER_military_cost = GER_military_cost_change }
}

# ENERGY EFFECTS
#modify_energy_gain_effect = {
#	custom_effect_tooltip = modify_energy_gain_TT
#	add_to_variable = { GER_energy_gain_var = GER_energy_gain_change }
#}
GER_die_linke_events = {
	random_list = {
		70 = {
			add_to_variable = { GER_event_counter_1_dielinke = 1 }
		}
		30 = {
		}
	}
	if = {
		limit = { check_variable = { GER_event_counter_1_dielinke > 5 } }
		set_variable = { GER_event_counter_1_dielinke = 0 }
		random_list = {
			10 = {
				country_event = germany_divide.1
			}
			10 = {
				country_event = germany_divide.2
			}
			10 = {
				country_event = germany_divide.3
			}
			10 = {
				country_event = germany_divide.4
			}
			10 = {
				country_event = germany_divide.5
			}
			10 = {
				country_event = germany_divide.6
			}
			10 = {
				country_event = germany_divide.7
			}
			10 = {
				country_event = germany_divide.8
			}
			10 = {
				country_event = germany_divide.9
			}
			10 = {
				country_event = germany_divide.10
			}
		}
	}
}
#
GER_east_german_events = {
	random_list = {
		70 = {
			add_to_variable = { GER_event_counter_1_egerman = 1 }
		}
		30 = {
		}
	}
	if = {
		limit = { check_variable = { GER_event_counter_1_egerman > 7 } }
		set_variable = { GER_event_counter_1_egerman = 0 }
		random_list = {
			10 = {
				country_event = germany_divide.11
			}
			10 = {
				country_event = germany_divide.12
			}
			10 = {
				country_event = germany_divide.13
			}
			10 = {
				country_event = germany_divide.14
			}
			10 = {
				country_event = germany_divide.15
			}
			10 = {
				country_event = germany_divide.16
			}
			10 = {
				country_event = germany_divide.17
			}
			10 = {
				country_event = germany_divide.18
			}
			10 = {
				country_event = germany_divide.19
			}
			10 = {
				country_event = germany_divide.20
			}
		}
	}
}
# BAVARIA MECHANIC #
GER_update_bavaria_standing = {
	if = {
		limit = {
			OR = {
				has_idea = stagnation
				has_idea = recession
				has_idea = depression
			}
		}
		random_list = {
			50 = {
			}
			40 = {
				add_to_variable = { GER_bavarian_nationalism_var = 1 }
			}
		}
	}
	if = {
		limit = {
			OR = {
				has_idea = fast_growth
				has_idea = economic_boom
			}
			check_variable = { GER_bavarian_nationalism_var > 0 }
		}
		random_list = {
			50 = {
			}
			40 = {
				add_to_variable = { GER_bavarian_nationalism_var = -1 }
			}
		}
	}
}
# WoT system
GER_wot_random_events = {
	random_list = {
		50 = {
		add_to_variable = { GER_event_counter_1_wot = 1 }
		}
		50 = {
		}
	}
	if = {
		limit = { check_variable = { GER_event_counter_1_wot > 6 } }
		random_list = {
			70 = {
				add_to_variable = { GER_event_counter_1_wot = 1 }
			}
			30 = {
			}
		}
		set_variable = { GER_event_counter_1_wot = 0 }
		random_list = {
			50 = {
				country_event = Germany.406
			}
			50 = {
				country_event = Germany.405
			}
		}
	}
}
#
GER_bfv_events = {
	random_list = {
		80 = {
			add_to_variable = { GER_event_counter_1_bfv = 1 }
		}
		20 = {
		}
	}
	if = {
		limit = { check_variable = { GER_event_counter_1_bfv > 2 } }
		set_variable = { GER_event_counter_1_bfv = 0 }
		random_list = {
			25 = {
				country_event = Germany.415
			}
			25 = {
				country_event = Germany.416
			}
			25 = {
				country_event = Germany.417
			}
			25 = {
				country_event = Germany.418
			}
		}
	}
}
#
GER_government_change = {
	if = {
		limit = {
			original_tag = GER
			has_cosmetic_tag = GER_franco_german_union
			emerging_anarchist_communism_are_in_power = no
		}
		drop_cosmetic_tag = yes
	}
	if = {
		limit = {
			original_tag = GER
			has_cosmetic_tag = GER_fourth_reich
			nationalist_fascist_are_in_power = no
		}
		drop_cosmetic_tag = yes
	}
	if = {
		limit = {
			original_tag = GER
			OR = {
				has_cosmetic_tag = GER_german_empire
				has_cosmetic_tag = GER_holy_german_empire
			}
			nationalist_monarchists_are_in_power = no
		}
		drop_cosmetic_tag = yes
	}
}

# new ones
update_GER_military_cost2 = {
	custom_effect_tooltip = GER_military_cost2_tt
	add_to_variable = { GER_military_cost2 = GER_military_cost2_change }
}
update_GER_Army_attack = {
	custom_effect_tooltip = GER_Army_attack_tt
	add_to_variable = { GER_Army_attack = GER_Army_attack_change }
}
update_GER_recon_factor = {
	custom_effect_tooltip = GER_recon_factor_tt
	add_to_variable = { GER_recon_factor = GER_recon_factor_change }
}
update_GER_Army_defence = {
	custom_effect_tooltip = GER_Army_defence_tt
	add_to_variable = { GER_Army_defence = GER_Army_defence_change }
}
update_GER_breakthrough_factor = {
	custom_effect_tooltip = GER_breakthrough_factor_tt
	add_to_variable = { GER_breakthrough_factor = GER_breakthrough_factor_change }
}
update_Ger_army_artillery_attack_factor = {
	custom_effect_tooltip = Ger_army_artillery_attack_factor_tt
	add_to_variable = { Ger_army_artillery_attack_factor = Ger_army_artillery_attack_factor_change }
}
update_GER_leader_xp_gain = {
	custom_effect_tooltip = GER_leader_xp_gain_tt
	add_to_variable = { GER_leader_xp_gain = GER_leader_xp_gain_change }
}
update_GER_org_loss_when_moving = {
	custom_effect_tooltip = GER_org_loss_when_moving_tt
	add_to_variable = { GER_org_loss_when_moving = GER_org_loss_when_moving_change }
}
update_GER_experience_gain_army = {
	custom_effect_tooltip = GER_experience_gain_army_tt
	add_to_variable = { GER_experience_gain_army = GER_experience_gain_army_change }
}
update_GER_army_org_regain = {
	custom_effect_tooltip = GER_army_org_regain_tt
	add_to_variable = { GER_army_org_regain = GER_army_org_regain_change }
}
update_GER_terrain_penalty_reduction = {
	custom_effect_tooltip = GER_terrain_penalty_reduction_tt
	add_to_variable = { GER_terrain_penalty_reduction = GER_terrain_penalty_reduction_change }
}
update_GER_air_attack = {
	custom_effect_tooltip = GER_air_attack_tt
	add_to_variable = { GER_air_attack = GER_air_attack_change }
}
update_GER_air_defence = {
	custom_effect_tooltip = GER_air_defence_tt
	add_to_variable = { GER_air_defence = GER_air_defence_change }
}
update_GER_air_mission_efficiency = {
	custom_effect_tooltip = GER_air_mission_efficiency_tt
	add_to_variable = { GER_air_mission_efficiency = GER_air_mission_efficiency_change }
}
update_GER_navy_personnel_cost_multiplier_modifier = {
	custom_effect_tooltip = GER_navy_personnel_cost_multiplier_modifier_tt
	add_to_variable = { GER_navy_personnel_cost_multiplier_modifier = GER_navy_personnel_cost_multiplier_modifier_change }
}
update_GER_positioning = {
	custom_effect_tooltip = GER_positioning_tt
	add_to_variable = { GER_positioning = GER_positioning_change }
}
update_GER_navy_max_range_factor = {
	custom_effect_tooltip = GER_navy_max_range_factor_tt
	add_to_variable = { GER_navy_max_range_factor = GER_navy_max_range_factor_change }
}
update_GER_navy_fuel_consumption_factor = {
	custom_effect_tooltip = GER_navy_fuel_consumption_factor_tt
	add_to_variable = { GER_navy_fuel_consumption_factor = GER_navy_fuel_consumption_factor_change }
}
update_GER_equipment_cost_multiplier_modifier = {
	custom_effect_tooltip = GER_equipment_cost_multiplier_modifier_tt
	add_to_variable = { GER_equipment_cost_multiplier_modifier = GER_equipment_cost_multiplier_modifier_change }
}
update_GER_bureaucracy_cost_multiplier_modifier = {
	custom_effect_tooltip = GER_bureaucracy_cost_multiplier_modifier_tt
	add_to_variable = { GER_bureaucracy_cost_multiplier_modifier = GER_bureaucracy_cost_multiplier_modifier_change }
}
update_GER_dockyard_productivity = {
	custom_effect_tooltip = GER_dockyard_productivity_tt
	add_to_variable = { GER_dockyard_productivity = GER_dockyard_productivity_change }
}

#deutsche bahn
update_GER_offices_contruction_speed = {
	custom_effect_tooltip = GER_offices_contruction_speed_tt
	add_to_variable = { GER_offices_contruction_speed = GER_offices_contruction_speed_change }
}
update_GER_arms_factory_contruction_speed = {
	custom_effect_tooltip = GER_arms_factory_contruction_speed_tt
	add_to_variable = { GER_arms_factory_contruction_speed = GER_arms_factory_contruction_speed_change }
}
update_GER_civ_construction_speed = {
	custom_effect_tooltip = GER_civ_construction_speed_tt
	add_to_variable = { GER_civ_construction_speed = GER_civ_construction_speed_change }
}
update_GER_population_energy_consumption = {
	custom_effect_tooltip = GER_population_energy_consumption_tt
	add_to_variable = { GER_population_energy_consumption = GER_population_energy_consumption_change }
}
update_GER_mil_factory_workforce = {
	custom_effect_tooltip = GER_mil_factory_workforce_tt
	add_to_variable = { GER_mil_factory_workforce = GER_mil_factory_workforce_change }
}
update_GER_office_workforce = {
	custom_effect_tooltip = GER_office_workforce_tt
	add_to_variable = { GER_office_workforce = GER_office_workforce_change }
}
update_GER_supply_consumption_factor = {
	custom_effect_tooltip = GER_supply_consumption_factor_tt
	add_to_variable = { GER_supply_consumption_factor = GER_supply_consumption_factor_change }
}