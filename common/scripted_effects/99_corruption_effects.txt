roll_corruption_events = {
	set_temp_variable = { roll_chance = 5 }
	set_temp_variable = { not_roll_chance = 100 }

	set_roll_chance = yes
	random_list = {
		roll_chance = {
			roll_corruption_event = yes
		}
		not_roll_chance = {
			# nothing
		}
	}
}

set_roll_chance = {
	if = {
		limit = { has_idea = negligible_corruption }
		set_temp_variable = { roll_chance = 6.5 }
	}
	else_if = {
		limit = { has_idea = slight_corruption }
		set_temp_variable = { roll_chance = 8 }
	}
	else_if = {
		limit = { has_idea = modest_corruption }
		set_temp_variable = { roll_chance = 9.5 }
	}
	else_if = {
		limit = { has_idea = medium_corruption }
		set_temp_variable = { roll_chance = 11 }
	}
	else_if = {
		limit = { has_idea = widespread_corruption }
		set_temp_variable = { roll_chance = 12.5 }
	}
	else_if = {
		limit = { has_idea = systematic_corruption }
		set_temp_variable = { roll_chance = 14 }
	}
	else_if = {
		limit = { has_idea = unrestrained_corruption }
		set_temp_variable = { roll_chance = 15.5 }
	}
	else_if = {
		limit = { has_idea = rampant_corruption }
		set_temp_variable = { roll_chance = 17 }
	}
	else_if = {
		limit = { has_idea = crippling_corruption }
		set_temp_variable = { roll_chance = 18.5 }
	}
	else = {
		set_temp_variable = { roll_chance = 20 }
	}

	if = {
		limit = {
			OR = {
				has_idea = oligarchs
				neutrality_oligarchism_in_power_or_coalition = yes
			}
		}

		multiply_temp_variable = { roll_chance = 1.3 }
	}

	subtract_from_temp_variable = { not_roll_chance = roll_chance }
}

roll_corruption_event = {
	random_list = {
		10 = {
			country_event = { id = corruption.1 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.2 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.3 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.4 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.5 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.6 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.7 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.8 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.9 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.10 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.11 random_days = 30 }
		}
		10 = {
			country_event = { id = corruption.12 random_days = 30 }
		}
		5 = {
			modifier = {
				factor = 2
				has_idea = international_bankers
			}
			country_event = { id = corruption.13 random_days = 30 }
		}
		10 = {
			modifier = {
				factor = 0
				NOT = { has_idea = oligarchs }
			}
			country_event = { id = corruption.14 random_days = 30 }
		}
	}
}