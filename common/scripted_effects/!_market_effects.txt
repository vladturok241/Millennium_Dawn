cic_contract_wipe = {
	every_purchase_contract = {
		limit = { deal_completion < 1 }
		ROOT = {
			add_cic = -150
		}
	}
}

market_set_starting_values = {
	#sets the base prices for the market
	#since these are global variables, if we wanna mess with them, you can via events
	set_variable = { global.base_unit_cost = 2.95 }
	set_variable = { global.inf_cost_mult = 0.003 }
	set_variable = { global.cnc_cost_mult = 0.004 }
	set_variable = { global.aa_cost_mult = 0.009 }
	set_variable = { global.at_cost_mult = 0.008 }
	set_variable = { global.hat_cost_mult = 0.027 }
	set_variable = { global.art_cost_mult = 0.007 }
	set_variable = { global.atk_helo_cost_mult = 0.075 }
	set_variable = { global.trans_helo_cost_mult = 0.168 }
	set_variable = { global.util_cost_mult = 0.021 }
	set_variable = { global.mbt_cost_mult = 0.022 }
	set_variable = { global.light_tank_cost_mult = 0.027 }
	set_variable = { global.ifv_cost_mult = 0.012 }
	set_variable = { global.apc_cost_mult = 0.009 }
	set_variable = { global.spart_cost_mult = 0.018 }
	set_variable = { global.spaa_cost_mult = 0.072 }
	set_variable = { global.train_cost_mult = 0.135 }
	set_variable = { global.convoy_cost_mult = 0.125 }
	set_variable = { global.sml_pln_cost_mult = 0.135 }
	set_variable = { global.med_pln_cost_mult = 0.315 }
	set_variable = { global.lrg_pln_cost_mult = 0.413 }
}