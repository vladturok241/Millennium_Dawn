set_leader_USB = {

	###########################
	#### Western Autocrats ####
	###########################
	if = { limit = { has_country_flag = set_Western_Autocracy }
		if = { limit = { check_variable = { Western_Autocracy_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Western_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Bob Wise"
				picture = "Bob_Wise.dds"
				ideology = Western_Autocracy
				traits = {
					western_liberalism
					career_politician
					lawyer
					sly
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Western_Autocracy_leader = 1 } }
		}
	}

	###########################
	#### Western Liberalism ####
	###########################
	else_if = { limit = { has_country_flag = set_liberalism }
		if = { limit = { check_variable = { liberalism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Edward Rendell"
				picture = "Ed_Rendell.dds"
				ideology = liberalism
				traits = {
					western_liberalism
					lawyer
					rational
					career_politician
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
		}
	}

	##################################
	#### Emerging Communist State ####
	##################################
	else_if = { limit = { has_country_flag = set_Communist-State }
		if = { limit = { check_variable = { Communist-State_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Babette Josephs"
				picture = "Babette_Josephs.dds"
				ideology = Communist-State
				traits = {
					western_socialism
					lawyer
					rational
					emotional
					cleric
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
		}
	}

	################################
	######### Oligarchism ##########
	################################
	else_if = { limit = { has_country_flag = set_oligarchism }
		if = { limit = { check_variable = { oligarchism_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Robert \"Bob\" Ehrlich Jr."
				picture = "Robert_L_Ehrlich_Jr.dds"
				ideology = oligarchism
				traits = {
					neutrality_Neutral_conservatism
					lawyer
					likeable
					capable
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
		}
	}

	################################
	#### Neutral Libertarianism ####
	################################
	else_if = { limit = { has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Mark Warner"
				picture = "Mark_Warner.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian
					businessman
					compassionate_gentleman
					likeable
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
		}
	}

	else_if = { limit = { has_country_flag = set_Autocracy }
		if = { limit = { check_variable = { Autocracy_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Elijah F. Sommers"
				picture = ""
				ideology = Autocracy
				traits = {
					emerging_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }

		}
	}

}