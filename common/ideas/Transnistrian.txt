###Created by Lord Bogdanoff
ideas = {
	country = {
		PMR_confederation = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add PMR_confederation" }
			picture = pmr_moldovian_fed
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.08
				tax_gain_multiplier_modifier = -0.15
				police_cost_multiplier_modifier = -0.10
				education_cost_multiplier_modifier = -0.10
				health_cost_multiplier_modifier = -0.10
				social_cost_multiplier_modifier = -0.10
				bureaucracy_cost_multiplier_modifier = -0.10
				personnel_cost_multiplier_modifier = -0.10
				production_speed_buildings_factor = 0.10
				industry_free_repair_factor = 0.10
			}
		}
		PMR_republic_federation = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_republic_federation" }
			picture = pmr_moldovian_fed
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				Military_Spending_cost_factor = 2
				Conscription_Law_cost_factor = 2
				Foreign_Intervention_Law_cost_factor = 2
				opinion_gain_monthly_factor = -0.50
				trade_laws_cost_factor = 0.15
				trade_opinion_factor = -0.4
				production_speed_buildings_factor = -0.10
				expected_mil_modifier = -4.00
				expected_police_modifier = -3.00
				resource_export_multiplier_modifier = 0.20
			}
		}
		PMR_political_nation_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_political_nation_idea" }
			allowed = { always = no }
			picture = pmr_political_nation
			modifier = {
				political_power_gain = 0.02
			}
		}
		PMR_political_nation_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_political_nation_idea1" }
			allowed = { always = no }
			picture = pmr_political_nation
			name = PMR_political_nation_idea
			modifier = {
				political_power_gain = 0.05
				foreign_influence_defense_modifier = 0.15
			}
		}
		PMR_political_nation_idea2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_political_nation_idea2" }
			allowed = { always = no }
			name = PMR_political_nation_idea
			picture = pmr_political_nation
			modifier = {
				political_power_gain = 0.06
				foreign_influence_defense_modifier = 0.18
				army_core_defence_factor = 0.05
			}
		}
		PMR_kazachestvo = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_kazachestvo" }
			picture = pmr_kazaki
			allowed = {	original_tag = PMR }
			modifier = {
				drift_defence_factor = 0.05
				army_core_defence_factor = 0.03
				army_attack_factor = 0.05
			}
		}
		PMR_kazachestvo1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_kazachestvo1" }
			picture = pmr_kazaki
			name = PMR_kazachestvo
			allowed = {	original_tag = OPR }
			modifier = {
				drift_defence_factor = 0.05
				army_core_defence_factor = 0.03
				army_attack_factor = 0.05
			}
		}
		PMR_develop_turism = {
			picture = tourism
			modifier = {
				tax_gain_multiplier_modifier = 0.02
			}
		}
		PMR_political_nation_idea3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_political_nation_idea3" }
			allowed = { always = no }
			picture = pmr_political_nation
			name = PMR_political_nation_idea
			modifier = {
				political_power_gain = 0.08
				foreign_influence_defense_modifier = 0.20
				army_core_defence_factor = 0.10
			}
		}
		PMR_political_nation_idea4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_political_nation_idea3" }
			allowed = { always = no }
			picture = sov_governorate_idea
			modifier = {
				political_power_gain = 0.10
				foreign_influence_defense_modifier = 0.25
				army_core_defence_factor = 0.15
			}
		}
		PMR_trans_libelaralism = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_trans_libelaralism" }
			allowed = { always = no }
			picture = pmr_liberals
			modifier = {
				political_power_factor = 0.10
				democratic_drift = 0.02
				social_cost_multiplier_modifier = -0.05
			}
		}
		PMR_people_militia = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_people_militia" }
			allowed = { always = no }
			picture = pmr_people_militia
			cancel = {
				NOT = {
					emerging_communist_state_are_in_power = yes
				}
			}
			modifier = {
				conscription = 0.005
				war_support_factor = 0.05
				justify_war_goal_time = -0.1
				personnel_cost_multiplier_modifier = -0.10
			}
		}
		PMR_patriotic_act = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_people_militia" }
			allowed = { always = no }
			picture = pmr_patriotic
			cancel = {
				NOT = {
					emerging_anarchist_communism_are_in_power = yes
				}
			}
			modifier = {
				operative_slot = 1
				civilian_intel_factor = 0.10
				army_intel_factor = 0.10
				navy_intel_factor = 0.10
				airforce_intel_factor = 0.10
			}
		}
		PMR_deregulation = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_deregulation" }
			allowed = { always = no }
			picture = inflation
			modifier = {
				production_speed_buildings_factor = 0.10
				corporate_tax_income_multiplier_modifier = -0.05
				tax_rate_change_multiplier_modifier = 0.10
			}
		}
		PMR_family = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_family" }
			allowed = { always = no }
			picture = family_supports
			cancel = {
				NOT = {
					emerging_anarchist_communism_are_in_power = yes
				}
			}
			modifier = {
				MONTHLY_POPULATION = 0.20
				industrial_capacity_factory = -0.10
				consumer_goods_factor = -0.05
				stability_factor = 0.05
				social_cost_multiplier_modifier = 0.10
				health_cost_multiplier_modifier = 0.10
			}
		}
		PMR_commi_education = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_commi_education" }
			allowed = { always = no }
			picture = communist
			cancel = {
				NOT = {
					emerging_communist_state_are_in_power = yes
				}
			}
			modifier = {
				research_speed_factor = 0.05
				education_cost_multiplier_modifier = 0.05
				projects_cost_modifier = -0.10
			}
		}
		PMR_father_nation_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_father_nation_idea" }
			allowed = { always = no }
			picture = pmr_father_nation
			cancel = {
				NOT = {
					OR = {
						emerging_autocracy_are_in_power = yes
						emerging_reactionaries_are_in_power = yes
					}
				}
			}
			modifier = {
				communism_drift = 0.01
				stability_factor = -0.1
				drift_defence_factor = 0.10
				political_power_gain = 0.03
			}
		}
		PMR_father_nation_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_father_nation_idea1" }
			allowed = { always = no }
			picture = pmr_father_nation
			name = PMR_father_nation_idea
			cancel = {
				NOT = {
					OR = {
						emerging_autocracy_are_in_power = yes
						emerging_reactionaries_are_in_power = yes
					}
				}
			}
			modifier = {
				communism_drift = 0.01
				stability_factor = -0.12
				drift_defence_factor = 0.10
				political_power_gain = 0.03
				industrial_capacity_factory = -0.25
			}
		}
		PMR_father_nation_idea2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_father_nation_idea2" }
			allowed = { always = no }
			picture = pmr_father_nation
			name = PMR_father_nation_idea
			cancel = {
				NOT = {
					OR = {
						emerging_autocracy_are_in_power = yes
						emerging_reactionaries_are_in_power = yes
					}
				}
			}
			modifier = {
				communism_drift = 0.02
				democratic_drift = -0.01
				nationalist_drift = -0.01
				stability_factor = -0.14
				drift_defence_factor = 0.15
				political_power_gain = 0.04
				industrial_capacity_factory = -0.25
			}
		}
		PMR_father_nation_idea3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_father_nation_idea3" }
			allowed = { always = no }
			picture = pmr_father_nation
			name = PMR_father_nation_idea
			cancel = {
				NOT = {
					OR = {
						emerging_autocracy_are_in_power = yes
						emerging_reactionaries_are_in_power = yes
					}
				}
			}
			modifier = {
				communism_drift = 0.03
				democratic_drift = -0.02
				nationalist_drift = -0.02
				stability_factor = -0.18
				drift_defence_factor = 0.25
				political_power_gain = 0.06
				industrial_capacity_factory = -0.30
			}
		}
		PMR_father_nation_idea4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_father_nation_idea4" }
			allowed = { always = no }
			picture = pmr_father_nation
			name = PMR_father_nation_idea
			cancel = {
				NOT = {
					OR = {
						emerging_autocracy_are_in_power = yes
						emerging_reactionaries_are_in_power = yes
					}
				}
			}
			modifier = {
				communism_drift = 0.07
				democratic_drift = -0.04
				nationalist_drift = -0.04
				stability_factor = -0.22
				drift_defence_factor = 0.40
				political_power_gain = 0.12
				industrial_capacity_factory = -0.30
			}
		}
		PMR_internet_cencor = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_internet_cencor" }
			allowed = { always = no }
			picture = blr_internet_cencorship_economic
			modifier = {
				stability_factor = -0.03
				drift_defence_factor = 0.08
			}
		}
		PMR_denomination_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_denomination_idea" }
			allowed = { always = no }
			picture = pmr_valute
			modifier = {
				stability_factor = -0.3
				tax_gain_multiplier_modifier = -0.2
				economic_cycles_cost_factor = 0.5
			}
		}
		PMR_ruble_efffect_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_ruble_efffect_idea" }
			allowed = { always = no }
			picture = sov_ruble_collapse_idea
			modifier = {
				stability_factor = -0.3
				tax_gain_multiplier_modifier = -0.2
				economic_cycles_cost_factor = 0.5
			}
		}
		PMR_annex_efffect_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_annex_efffect_idea" }
			allowed = { always = no }
			picture = pmr_annex_effect
			modifier = {
				stability_factor = -0.1
				tax_gain_multiplier_modifier = -0.1
				economic_cycles_cost_factor = 0.3
			}
		}
		PMR_commi_education_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_commi_education_idea" }
			allowed = { always = no }
			picture = communism9
			cancel = {
				NOT = {
					emerging_communist_state_are_in_power = yes
				}
			}
			modifier = {
				research_speed_factor = 0.05
				education_cost_multiplier_modifier = 0.05
				projects_cost_modifier = -0.10
			}
		}
		PMR_agro_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_agro_idea" }
			allowed = { always = no }
			picture = blr_agro
			modifier = {
				monthly_population = 0.05
				agricolture_productivity_modifier = 0.15
				agriculture_tax_modifier = 0.08
			}
		}
		PMR_agro_idea1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_agro_idea1" }
			allowed = { always = no }
			name = PMR_agro_idea
			picture = blr_agro
			modifier = {
				monthly_population = 0.09
				agricolture_productivity_modifier = 0.20
				agriculture_tax_modifier = 0.12
			}
		}
		PMR_agro_idea2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_agro_idea2" }
			allowed = { always = no }
			name = PMR_agro_idea
			picture = blr_agro
			modifier = {
				monthly_population = 0.11
				agricolture_productivity_modifier = 0.25
				agriculture_tax_modifier = 0.16
			}
		}
		PMR_wind_farms = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_wind_farms" }
			allowed = { always = no }
			picture = Windturbine_State_Modifier
			modifier = {
				renewable_energy_gain_multiplier = 0.15
				production_speed_synthetic_refinery_factor = 0.10
			}
		}
		PMR_industry = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_industry" }
			allowed = { always = no }
			picture = industrial_focus
			modifier = {
				production_speed_buildings_factor = 0.05
			}
		}
		PMR_academy_air = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_academy_air" }
			allowed = { always = no }
			picture = air_bonus
			modifier = {
				experience_gain_air = 0.05
				air_accidents_factor = -0.05
			}
		}
		PMR_new_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_new_army" }
			allowed = { always = no }
			picture = pmr_new_army
			modifier = {
				army_defence_factor = 0.04
				experience_gain_army = 0.05
				land_reinforce_rate = 0.04
				conscription = 0.03
			}
		}
		PMR_new_army1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_new_army1" }
			allowed = { always = no }
			picture = pmr_new_army
			name = PMR_new_army
			modifier = {
				army_defence_factor = 0.04
				experience_gain_army = 0.05
				land_reinforce_rate = 0.04
				conscription = 0.03
			}
			equipment_bonus = {
				Kamikaze_drone_equipment = {
					build_cost_ic = -0.20
					instant = yes
				}
			}
		}
		PMR_new_army2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_new_army1" }
			allowed = { always = no }
			picture = Provisional_Council_of_the_Russian_Republic
			modifier = {
				army_attack_factor = 0.06
				army_defence_factor = 0.08
				experience_gain_army = 0.05
				land_reinforce_rate = 0.04
				conscription = 0.06
				send_volunteer_size = 1
			}
			equipment_bonus = {
				Kamikaze_drone_equipment = {
					build_cost_ic = -0.20
					instant = yes
				}
			}
		}
		PMR_highway_focus = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_highway_focus" }
			allowed = { always = no }
			picture = production_bonus
			modifier = {
				production_speed_infrastructure_factor = 0.15
			}
		}
		PMR_inter_rao = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_inter_rao" }
			allowed = { always = no }
			picture = pmr_interrao
			cancel = {
				has_war_with = SOV
			}
			modifier = {
				energy_use_multiplier = -0.05
				energy_gain_multiplier = 0.15
			}
		}
		PMR_civil_war = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_civil_war" }
			allowed = { always = no }
			picture = pmr_civil_war2
			cancel = {
				has_war = no
			}
			modifier = {
				political_power_factor = 0.02
				democratic_drift = -0.01
				nationalist_drift = 0.01
				communism_drift = 0.01
			}
		}
		PMR_forts_buildings = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_forts_buildings" }
			allowed = { always = no }
			picture = production_bonus
			modifier = {
				production_speed_bunker_factor = 0.2
			}
		}
		PMR_suvorov = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_suvorov" }
			allowed = { always = no }
			picture = army_war_college
			modifier = {
				experience_gain_army = 0.05
				army_org = 2
			}
		}
		PMR_spetznaz = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_spetznaz" }
			allowed_civil_war = {
				always = yes
			}
			picture = pmr_specnaz
			modifier = {
				special_forces_training_time_factor = -0.05
				special_forces_attack_factor = 0.05
			}
		}
		PMR_disorganized_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_disorganized_army" }
			allowed = { always = no }
			picture = pmr_bad_army
			modifier = {
				army_attack_factor = -0.50
				army_defence_factor = -0.50
				experience_gain_army_factor = -0.75
				dig_in_speed_factor = -0.25
			}
		}
		PMR_disorganized_army_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_disorganized_army_2" }
			allowed = { always = no }
			name = PMR_disorganized_army
			picture = pmr_bad_army
			modifier = {
				army_attack_factor = -0.35
				army_defence_factor = -0.35
				experience_gain_army_factor = -0.55
				dig_in_speed_factor = -0.15
			}
		}
		PMR_disorganized_army_3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_disorganized_army_3" }
			allowed = { always = no }
			name = PMR_disorganized_army
			picture = pmr_bad_army
			modifier = {
				army_attack_factor = -0.28
				army_defence_factor = -0.28
				experience_gain_army_factor = -0.45
				dig_in_speed_factor = -0.13
			}
		}
		PMR_reformed_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_reformed_army" }
			allowed = { always = no }
			picture = pmr_reform_army
			modifier = {
				personnel_cost_multiplier_modifier = 0.25
				army_defence_factor = 0.05
				army_org_factor = 0.05
				army_attack_factor = 0.03
			}
		}
		PMR_russian_legacy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_russian_legacy" }
			allowed = { always = no }
			picture = SOV_russian_legacy
			modifier = {
				drift_defence_factor = 0.50
				democratic_drift = -0.05
			}
		}
		PMR_russian_legacy1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_russian_legacy1" }
			allowed = { always = no }
			picture = SOV_russian_legacy
			name = PMR_russian_legacy
			modifier = {
				drift_defence_factor = 0.50
				democratic_drift = -0.05
				nationalist_drift = 0.02
			}
		}
		PMR_russian_legacy2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_russian_legacy2" }
			allowed = { always = no }
			picture = SOV_russian_legacy
			name = PMR_russian_legacy
			modifier = {
				drift_defence_factor = 0.50
				democratic_drift = -0.05
				nationalist_drift = 0.04
				communism_drift = -0.02
			}
		}
		PMR_reform_debuff = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_reform_debuff" }
			allowed = { always = no }
			picture = political_drain
			modifier = {
				bureaucracy_cost_multiplier_modifier = 0.25
				political_power_gain = -0.03
			}
		}
		PMR_reform_debuff1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_reform_debuff1" }
			allowed = { always = no }
			picture = political_drain
			modifier = {
				bureaucracy_cost_multiplier_modifier = 0.15
				political_power_gain = -0.02
			}
		}
		PMR_anti_gays = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_anti_gays" }
			allowed = { always = no }
			picture = anti_lgbt
			cancel = {
				has_government = democratic
			}
			modifier = {
				political_power_factor = 0.02
				democratic_drift = -0.01
				nationalist_drift = 0.01
				communism_drift = 0.01
				monthly_population = 0.04
			}
		}
		PMR_chinese_economic = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_chinese_economic" }
			allowed = { always = no }
			picture = well_balanced
			cancel = {
				NOT = { emerging_communist_state_are_in_power = yes }
			}
			modifier = {
				communism_drift = 0.02
				research_speed_factor = -0.02
				production_factory_efficiency_gain_factor = -0.02
				production_speed_industrial_complex_factor = -0.02
				trade_opinion_factor = -0.02
				consumer_goods_factor = -0.02
				production_speed_arms_factory_factor = 0.02
				corruption_cost_factor = -0.02
			}
		}
		PMR_decommunization = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_decommunization" }
			allowed = { always = no }
			picture = sov_comm_angry
			cancel = {
				emerging_communist_state_are_in_power = yes
			}
			modifier = {
				communism_drift = -0.03
			}
		}
		PMR_rule_romanov = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_rule_romanov" }
			allowed = { always = no }
			picture = pmr_romanovs
			cancel = {
				NOT = {
					nationalist_monarchists_are_in_power = yes
				}
			}
			modifier = {
				nationalist_drift = 0.02
				stability_factor = 0.03
			}
		}
		PMR_rule_romanov1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_rule_romanov1" }
			allowed = { always = no }
			picture = pmr_romanovs
			name = PMR_rule_romanov
			cancel = {
				NOT = {
					nationalist_monarchists_are_in_power = yes
				}
			}
			modifier = {
				nationalist_drift = 0.03
				stability_factor = 0.01
				political_power_factor = 0.05
			}
		}
		PMR_rule_romanov2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_rule_romanov2" }
			allowed = { always = no }
			picture = pmr_romanovs
			name = PMR_rule_romanov
			cancel = {
				NOT = {
					nationalist_monarchists_are_in_power = yes
				}
			}
			modifier = {
				war_support_factor = 0.05
				democratic_drift = -0.02
				nationalist_drift = 0.05
				stability_factor = 0.01
				political_power_factor = 0.05
			}
		}
		PMR_rule_romanov3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_rule_romanov3" }
			allowed = { always = no }
			picture = pmr_romanovs
			name = PMR_rule_romanov
			cancel = {
				NOT = {
					nationalist_monarchists_are_in_power = yes
				}
			}
			modifier = {
				war_support_factor = 0.05
				democratic_drift = -0.02
				nationalist_drift = 0.05
				stability_factor = 0.01
				political_power_factor = 0.05
				army_morale_factor = 0.03
				army_org_factor = 0.01
				army_attack_factor = 0.05
			}
		}
		PMR_aggresive_foreign_policy_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SOV_aggresive_foriegn_policy_putin_idea" }
			picture = SOV_mytishchy_machine_building_plant
			allowed = {
				original_tag = PMR
			}
			cancel = {
				is_subject = yes
			}
			modifier = {
				justify_war_goal_time = -0.5
				generate_wargoal_tension = -0.25
			}
		}
		PMR_free_weapons = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_free_weapons" }
			allowed = { always = no }
			picture = anti_lgbt
			modifier = {
				production_speed_arms_factory_factor = 0.15
				bureaucracy_cost_multiplier_modifier = 0.10
				police_cost_multiplier_modifier = 0.10
			}
		}
		PMR_null_airforce = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_null_airforce" }
			allowed = { always = no }
			name = PMR_null_airforce
			picture = pmr_null_airforce
			modifier = {
				air_training_xp_gain_factor = -0.25
				air_mission_xp_gain_factor = -0.25
				airforce_personnel_cost_multiplier_modifier = 0.25
			}
		}
		PMR_null_airforce_2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_null_airforce" }
			allowed = { always = no }
			name = PMR_null_airforce
			picture = pmr_null_airforce
			modifier = {
				air_training_xp_gain_factor = -0.10
				air_mission_xp_gain_factor = -0.10
				airforce_personnel_cost_multiplier_modifier = 0.15
			}
		}
		PMR_exercises = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_exercises" }
			allowed = { always = no }
			picture = sov_tos_idea
			modifier = {
				experience_gain_army_factor = 0.25
				army_org_factor = 0.1
				war_support_factor = 0.05
				war_support_weekly = 0.01
			}
		}
		PMR_14_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_14_army" }
			allowed = {	original_tag = PMR }
			picture = pmr_14_army
			modifier = {
				political_power_gain = -0.10
				stability_factor = 0.15
				required_garrison_factor = -0.2
				resistance_activity = -0.2
				civilian_intel_to_others = 50
				hidden_modifier = {
				ai_focus_peaceful_factor = 0.20
				}
			}
		}
		PMR_start_fight = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_start_fight" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			modifier = {
				corruption_cost_factor = -0.15
			}
		}
		PMR_anti_corruption_fight = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_anti_corruption_fight" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			modifier = {
				corruption_cost_factor = -0.15
			}
		}
		PMR_Ministry_of_internal_affairs_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_Ministry_of_internal_affairs_idea" }
			picture = police_badge
			modifier = {
				conscription = 0.02
				foreign_influence_defense_modifier = 0.10
				political_power_gain = 0.05
			}
		}
		PMR_five_year_plans_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add PMR_five_year_plans_idea" }
			picture = blr_communist_economy_org
			allowed_civil_war = { always = yes }
			modifier = {
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = 0.25
				civilian_factories_productivity = 0.12
			}
		}
		PMR_investors_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_investors_idea" }
			allowed = { always = no }
			picture = foreign_capital
			modifier = {
				tax_gain_multiplier_modifier = 0.10
				research_speed_factor = 0.05
			}
		}
		PMR_western_market_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_western_market_idea" }
			allowed = { always = no }
			picture = economic_increase
			modifier = {
				faction_trade_opinion_factor = 0.2
				opinion_gain_monthly_same_ideology_factor = 0.1
			}
		}
		PMR_sheriff = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_sheriff" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			modifier = {
				political_power_gain = 0.06
				stability_factor = -0.10
				foreign_influence_defense_modifier = 0.27
				corruption_cost_factor = 0.25
				tax_gain_multiplier_modifier = -0.15
			}
		}
		PMR_sheriff1 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_sheriff1" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			name = PMR_sheriff
			modifier = {
				political_power_gain = 0.05
				stability_factor = -0.11
				foreign_influence_defense_modifier = 0.22
				corruption_cost_factor = 0.23
				tax_gain_multiplier_modifier = -0.14
			}
		}
		PMR_sheriff2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_sheriff2" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			name = PMR_sheriff
			modifier = {
				political_power_gain = 0.04
				stability_factor = -0.08
				foreign_influence_defense_modifier = 0.18
				corruption_cost_factor = 0.20
				tax_gain_multiplier_modifier = -0.10
			}
		}
		PMR_sheriff3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_sheriff3" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			name = PMR_sheriff
			modifier = {
				political_power_gain = 0.02
				stability_factor = -0.05
				foreign_influence_defense_modifier = 0.10
				corruption_cost_factor = 0.12
				tax_gain_multiplier_modifier = -0.04
			}
		}
		PMR_sheriff4 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_sheriff4" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			name = PMR_sheriff
			modifier = {
				political_power_gain = 0.09
				stability_factor = -0.14
				foreign_influence_defense_modifier = 0.32
				corruption_cost_factor = 0.33
				tax_gain_multiplier_modifier = -0.20
			}
		}
		PMR_sheriff5 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_sheriff5" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			name = PMR_sheriff
			modifier = {
				political_power_gain = 0.12
				stability_factor = -0.18
				foreign_influence_defense_modifier = 0.38
				corruption_cost_factor = 0.38
				tax_gain_multiplier_modifier = -0.25
			}
		}
		PMR_sheriff6 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_sheriff6" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			name = PMR_sheriff
			modifier = {
				political_power_gain = 0.16
				stability_factor = -0.22
				foreign_influence_defense_modifier = 0.41
				corruption_cost_factor = 0.45
				tax_gain_multiplier_modifier = -0.32
			}
		}
		PMR_sheriff7 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_sheriff7" }
			allowed = {	original_tag = PMR }
			picture = pmr_sheriff
			name = PMR_sheriff
			modifier = {
				political_power_gain = 0.20
				stability_factor = -0.26
				foreign_influence_defense_modifier = 0.47
				corruption_cost_factor = 0.51
				tax_gain_multiplier_modifier = -0.38
			}
		}
		PMR_great = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_great" }
			allowed = {	original_tag = PMR }
			picture = transnistrian_spirit
			cancel = {
				NOT = {
					emerging_anarchist_communism_are_in_power = yes
				}
			}
			modifier = {
				political_power_factor = 0.10
				stability_factor = 0.05
				industrial_capacity_factory = 0.10
				foreign_influence_defense_modifier = 0.05
				foreign_influence_modifier = 0.05
				consumer_goods_factor = 0.05
			}
		}
		PMR_fragment_ussr = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_fragment_ussr" }
			allowed = {	original_tag = PMR }
			picture = pmr_ussr_fragment
			modifier = {
				communism_drift = 0.02
			}
		}
		PMR_fragment_rus_empire = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_fragment_rus_empire" }
			allowed = {	original_tag = PMR }
			picture = pmr_ros_empire
			modifier = {
				nationalist_drift = 0.04
			}
		}
		PMR_victory_moldova = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_victory_moldova" }
			allowed = {	original_tag = PMR }
			picture = pmr_defeat_moldova
			modifier = {
				stability_factor = 0.1
				nationalist_drift = 0.01
				war_support_factor = 0.04
			}
			targeted_modifier = {
				tag = MLV
				attack_bonus_against = 0.05
				defense_bonus_against = 0.1
			}
		}
		PMR_doctrine_ukraine = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_doctrine_ukraine" }
			allowed = {	original_tag = PMR }
			picture = pmr_doctrine_ukr
			targeted_modifier = {
				tag = UKR
				attack_bonus_against = 0.05
				defense_bonus_against = 0.05
			}
		}
		PMR_subject_doctrine_ukraine = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_subject_doctrine_ukraine" }
			allowed = {	original_tag = PMR }
			picture = pmr_doctrine_ukr
			name = PMR_doctrine_ukraine
			cancel = {
				NOT = {
					has_autonomy_state = autonomy_moldovian_federation
				}
			}
			targeted_modifier = {
				tag = UKR
				attack_bonus_against = 0.05
				defense_bonus_against = 0.05
			}
		}
		PMR_doctrine_romania = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_doctrine_romania" }
			allowed = {	original_tag = PMR }
			picture = pmr_doctrine_rom
			targeted_modifier = {
				tag = ROM
				attack_bonus_against = 0.05
				defense_bonus_against = 0.05
			}
		}
		PMR_doctrine_subject_romania = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea PMR_doctrine_subject_romania" }
			picture = pmr_doctrine_rom
			name = PMR_doctrine_romania
			cancel = {
				NOT = {
					has_autonomy_state = autonomy_moldovian_federation
				}
			}
			targeted_modifier = {
				tag = ROM
				attack_bonus_against = 0.05
				defense_bonus_against = 0.05
			}
		}
	}
}