ideas = {
	hidden_ideas = {
		BOS_attack_upon_nato = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			targeted_modifier = {
				tag = BOS
				defense_bonus_against = -0.15
			}
			targeted_modifier = {
				tag = BOS
				attack_bonus_against = -0.15
			}
		}
		##
		bos_bosnian_manpower_friendly = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_serb_manpower_bad = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_cards_program = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_bosnian_manpower_bad = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_bosnian_manpower_mid = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_bosnian_manpower_loyal = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_serb_manpower_loyal = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_serb_manpower_friendly = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_serb_manpower_mid = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_croat_manpower_friendly = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_croat_manpower_bad = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_croat_manpower_mid = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_croat_manpower_loyal = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

		}
		bos_sfor_vol_troops = {
			picture = disorganized_army
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { BOS = { 	has_country_flag = bosnian_civil_war_over } }
			modifier = {
				send_volunteers_tension = -0.98
				send_volunteer_divisions_required = -0.90
			}
		}
		bos_SER_vol_troops = {
			picture = disorganized_army
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { BOS = { 	has_country_flag = bosnian_civil_war_over } }
			modifier = {
				send_volunteer_divisions_required = -9
				send_volunteer_size = 5
				send_volunteers_tension = -1
				air_volunteer_cap = 1
			}
		}
		bos_tal_and_other_salifist = {
			picture = disorganized_army
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { BOS = { 	has_country_flag = bosnian_civil_war_over } }
			modifier = {
				send_volunteer_divisions_required = -1
				send_volunteer_size = 1
				send_volunteers_tension = -1
				air_volunteer_cap = 1
			}
		}
	}

	country = {
		bos_international_funding = {
			picture = foreign_capital
			allowed = {
			always = no
			}
			allowed_civil_war = { always = yes }
			cancel = { NOT = { has_idea = BOS_SFOR } }
			modifier = {
				custom_modifier_tooltip = Bosnia_united_nations_aid
				production_speed_buildings_factor = 0.15
				consumer_goods_factor = 0.20
			}
		}
		BOS_EUFOR = {
			picture = EU_Roundel
			allowed = {
				always = no
			}
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
				custom_modifier_tooltip = bos_save_by_peacekeepersEUFOR
				custom_modifier_tooltip = BOS_EUFOR_aid_money
				required_garrison_factor = -0.2
				resistance_activity = -0.2
				civilian_intel_to_others = 50
				hidden_modifier = {
				ai_focus_peaceful_factor = 0.20
				}
				consumer_goods_factor = 0.15
			}
		}
		BOS_SFOR = {
			picture = MNNA
			allowed = { always = no }
			allowed_civil_war = { always = no }
			cancel = { always = no }

			modifier = {
				custom_modifier_tooltip = bos_save_by_peacekeepers
				political_power_gain = -0.10
				stability_factor = 0.15
				required_garrison_factor = -0.2
				resistance_activity = -0.2
				civilian_intel_to_others = 50
				hidden_modifier = {
				ai_focus_peaceful_factor = 0.20
				}
			}
		}
		BOS_politicised_Army_idea = {
			picture = bosnia_officer_core
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
				surrender_limit = -0.3
				political_power_cost = -0.08
				war_support_factor = -0.02
				hidden_modifier = {
					ai_focus_military_advancements_factor = -0.2
					ai_focus_war_production_factor = -0.2
				}
				command_power_gain = -0.3
				personnel_cost_multiplier_modifier = 0.15
			}
		}
		BOS_Muslim_Nationalism = {
			picture = muslim1
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				war_support_factor = 0.15
				drift_defence_factor = 0.50
				conscription = 0.02
				enemy_justify_war_goal_time = -0.4
				justify_war_goal_time = -0.4
				hidden_modifier = {
				required_garrison_factor = -0.4
				compliance_gain = 0.2
				resistance_activity = -0.4
				}
			}
		}
		BOS_religious_identityone = {
			picture = muslim3
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				stability_factor = 0.05
				fascism_acceptance = 50
			}
		}
		BOS_low_police_buget = {
			picture = roit_police_idea

			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				crime_fighting_cost_factor = 0.25
				police_cost_multiplier_modifier = -0.15
			}
		}
		BOS_bosniak_balkan_aspiration = {
			picture = bosnia_civil_war_icon
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = {
				has_idea = BOS_bosniak_balkan_aspiration2
			}
			modifier = {
				compliance_gain = -0.25
				surrender_limit = -0.100
				hidden_modifier = {
				attrition = 0.3
				no_supply_grace = -15
				supply_consumption_factor = 0.3
				ai_badass_factor = 1.0
				ai_focus_aggressive_factor = 1.0
				ai_focus_defense_factor = -1.0
				ai_focus_war_production_factor = 1.0
				ai_join_ally_desire_factor = -1.0
				}
			}
			targeted_modifier = {
				tag = RSK
				attack_bonus_against = 1.0
			}
			targeted_modifier = {
				tag = HZG
				attack_bonus_against = 1.0
			}
		}
		BOS_bosniak_balkan_aspiration2 = {
			picture = bosniaks_civil_war_icon
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				compliance_gain = -0.25
				hidden_modifier = {
				attrition = 0.3
				no_supply_grace = -15
				supply_consumption_factor = 0.3
				ai_badass_factor = 1.0
				ai_focus_aggressive_factor = 1.0
				ai_focus_defense_factor = -1.0
				ai_focus_war_production_factor = 1.0
				ai_join_ally_desire_factor = -1.0
				}
			}
			targeted_modifier = {
				tag = RSK
				attack_bonus_against = 0.3
				defense_bonus_against = 0.5
			}
			targeted_modifier = {
				tag = SER
				defense_bonus_against = 0.5
			}
			targeted_modifier = {
				tag = SER
				hidden_modifier = {
				defense_bonus_against = 0.2
				}
			}
			targeted_modifier = {
				tag = HZG
				attack_bonus_against = 0.3
				defense_bonus_against = 0.5
			}
		}
		BOS_srpska_balkan_aspiration = {
			picture = srpska_civil_war_icon
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				hidden_modifier = {
					ai_badass_factor = 1.0
					ai_focus_aggressive_factor = 1.0
					ai_focus_defense_factor = -1.0
					ai_focus_war_production_factor = 1.0
					ai_join_ally_desire_factor = -1.0
				}
			}

			targeted_modifier = {
				tag = HZG
				attack_bonus_against = 1.0
			}
			targeted_modifier = {
				tag = BOS
				attack_bonus_against = 1.0
			}
		}
		BOS_croat_balkan_aspiration = {
			picture = croats_civil_war_icon
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				hidden_modifier = {
					ai_badass_factor = 1.0
					ai_focus_aggressive_factor = 1.0
					ai_focus_defense_factor = -1.0
					ai_focus_war_production_factor = 1.0
					ai_join_ally_desire_factor = -1.0
				}
			}

			targeted_modifier = {
				tag = RSK
				attack_bonus_against = 1.0
			}
			targeted_modifier = {
				tag = BOS
				attack_bonus_against = 1.0
			}
		}
		BOS_post_yugoslav_war_state = {
			picture = the_bosnian_people
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				political_power_gain = -0.10
				bureaucracy_cost_multiplier_modifier = 0.10
				health_cost_multiplier_modifier = 0.10
				civil_war_involvement_tension = -0.15
			}
		}
		BOS_police_high = {
			picture = roit_police_idea
			allowed = { always = no }

			allowed_civil_war = {
				always = yes
			}
			cancel = { always = no }

			modifier = {
				political_power_gain = 0.10
				crime_fighting_cost_factor = 0.60
				police_cost_multiplier_modifier = 0.05
			}
		}
		BOS_politicised_Army_idea2 = {
			picture = army_solider_head
			allowed = { always = no }

			allowed_civil_war = {
				always = yes
			}
			cancel = { always = no }

			modifier = {
				surrender_limit = -0.15
				political_power_cost = -0.10
				war_support_factor = -0.02
				hidden_modifier = {
					ai_focus_military_advancements_factor = -0.1
					ai_focus_war_production_factor = -0.1
				}
				command_power_gain = -0.15
				personnel_cost_multiplier_modifier = 0.13
			}
		}
		BOS_politicised_Army_idea3 = {
			picture = army_solider_head
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				surrender_limit = -0.06
				political_power_cost = -0.05
				war_support_factor = -0.01
				hidden_modifier = {
					ai_focus_military_advancements_factor = 0.1
					ai_focus_war_production_factor = 0.1
				}
				command_power_gain = -0.05
				personnel_cost_multiplier_modifier = 0.08
			}
		}
		BOS_politicised_Army_idea4 = {
			picture = army_solider_head
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				surrender_limit = -0.05
				war_support_factor = 0.01
				hidden_modifier = {
					ai_focus_military_advancements_factor = 0.3
					ai_focus_war_production_factor = 0.3
				}
				command_power_gain = -0.01
				personnel_cost_multiplier_modifier = 0.02
			}
		}
		BOS_politicised_Army_idea5 = {

			picture = idea_to_many_generals

			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}

			cancel = { always = no }

			modifier = {
				surrender_limit = -0.12
				war_support_factor = -0.01
				hidden_modifier = {
					ai_focus_military_advancements_factor = 0.3
					ai_focus_war_production_factor = 0.3
				}
				command_power_gain = 0.15
				personnel_cost_multiplier_modifier = 0.02
			}
		}
		BOS_Land_doctrine_review = {
			picture = incompetent_officiers
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }

			modifier = {
				land_doctrine_cost_factor = -0.25
			}
		}
		BOS_Air_doctrine_review = {
			picture = incompetent_officiers
			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}
			cancel = { always = no }


			modifier = {
				air_doctrine_cost_factor = -0.25
			}
		}
		BOS_mechanization_of_the_army = {

			picture = armor2
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			army_org = 2
			army_morale = 10
			breakthrough_factor = 0.12
			dig_in_speed = -2
			max_dig_in = -5
			max_planning = 0.05
			planning_speed = 0.2
			offence = 0.08
			defence = 0.05
			personnel_cost_multiplier_modifier = 0.10

			}
		}
		BOS_transportation_methods = {

			picture = planning_bonus
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				util_vehicle_equipment = {
					build_cost_ic = -0.15
				}
			}

			modifier = {
				personnel_cost_multiplier_modifier = 0.02
			}
		}
		BOS_transportation_methods2 = {

			picture = planning_bonus
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				util_vehicle_equipment = {
					build_cost_ic = -0.15
				}
				transport_helicopter_equipment = {
					build_cost_ic = -0.15
				}
			}

			modifier = {
				personnel_cost_multiplier_modifier = 0.02
			}
		}
		BOS_the_land_fleet = {

			picture = armor
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				apc_hull = {
					build_cost_ic = -0.20
				}
				ifv_hull = {
					build_cost_ic = -0.18
				}
				util_vehicle_equipment = {
					build_cost_ic = -0.15
				}
				transport_helicopter_equipment = {
					build_cost_ic = -0.15
				}
			}

			modifier = {
			army_org = 2
			army_morale = 10
			breakthrough_factor = 0.14
			dig_in_speed = -2
			max_dig_in = -5
			max_planning = 0.05
			planning_speed = 0.2
			offence = 0.012
			defence = 0.05
			personnel_cost_multiplier_modifier = 0.32

			}
		}
		BOS_aerial_dominance = {

			picture = air_research
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				medium_plane_fighter_airframe = {
					build_cost_ic = -0.15
				}
				medium_plane_airframe = {
					build_cost_ic = -0.15
				}
			}

			modifier = {
			air_ace_bonuses_factor = 0.15
			air_agility_factor = 0.05
			air_attack_factor = 0.08
			air_defence_factor = 0.08
			ground_attack_factor = 0.05
			}
		}
		BOS_internal_investment = {

			picture = foreign_capital
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			production_speed_industrial_complex_factor = 0.25
			consumer_goods_factor = -0.15
			}
		}
		BOS_crime_increase = {

			picture = great_depression
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			political_power_cost = 0.05
			consumer_goods_factor = 0.05
			}
		}
		BOS_nationalise_compnies = {

			picture = puppet_industry
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			production_speed_industrial_complex_factor = 0.2
			consumer_goods_factor = -0.05
			}
		}
		BOS_Air_Defence_initiative = {

			picture = air_support
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				AA_Equipment = {
					build_cost_ic = -0.15
				}
				medium_plane_airframe = {
					build_cost_ic = -0.15
				}
			}

			modifier = {
				static_anti_air_damage_factor = 0.1
				static_anti_air_hit_chance_factor = 0.1
			}
		}
		BOS_Air_Defence_initiative2 = {

			picture = air_support
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				AA_Equipment = {
					build_cost_ic = -0.15
				}
				medium_plane_airframe = {
					build_cost_ic = -0.15
				}
			}

			modifier = {
				static_anti_air_damage_factor = 0.25
				static_anti_air_hit_chance_factor = 0.25
				navy_anti_air_attack = 5
				navy_anti_air_attack_factor = 0.3
			}
		}
		BOS_aerial_dominance2 = {

			picture = bomber_production_increase
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				medium_plane_fighter_airframe = {
					build_cost_ic = -0.08
				}
				medium_plane_airframe = {
					build_cost_ic = -0.08
				}
			}

			modifier = {
			air_ace_bonuses_factor = 0.4
			air_agility_factor = 0.10
			air_attack_factor = 0.04
			air_defence_factor = 0.04
			ground_attack_factor = 0.07
			static_anti_air_damage_factor = 0.1
			static_anti_air_hit_chance_factor = 0.05
			}
		}
		BOS_APC_armament = {

			picture = apc_star_idea
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				apc_hull = {
					build_cost_ic = -0.20
				}
			}

			modifier = {
				army_org = 2
			army_morale = 10
			breakthrough_factor = 0.12
			dig_in_speed = -2
			max_dig_in = -5
			max_planning = 0.05
			planning_speed = 0.2
			offence = 0.08
			defence = 0.05
			personnel_cost_multiplier_modifier = 0.16

			}
		}
		BOS_APC_armament1 = {

			picture = apc_star_idea
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				apc_hull = {
					build_cost_ic = -0.20
				}
				ifv_hull = {
					build_cost_ic = -0.18
				}
			}

			modifier = {
			army_org = 2
			army_morale = 10
			breakthrough_factor = 0.12
			dig_in_speed = -2
			max_dig_in = -5
			max_planning = 0.05
			planning_speed = 0.2
			offence = 0.08
			defence = 0.05
			personnel_cost_multiplier_modifier = 0.20

			}
		}
		BOS_main_battle_line = {

			picture = apc_star_idea
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }
			equipment_bonus = {
				light_tank_hull = {
					build_cost_ic = -0.08
				}
			}

			modifier = {

			army_morale = 10
			breakthrough_factor = 0.04

			personnel_cost_multiplier_modifier = 0.05

			}
		}

		BOS_boots_on_the_ground = {

			picture = iran_fighter_for_nation
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			army_org = 10
			breakthrough_factor = 0.2
			dig_in_speed = 2
			max_dig_in = 20
			max_planning = 0.20
			planning_speed = -0.2
			offence = -0.1
			defence = 0.1
			}
		}
		BOS_boots_on_the_ground1 = {

			picture = iran_fighter_for_nation
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			army_org = 10
			breakthrough_factor = 0.3
			dig_in_speed = 3
			max_dig_in = 20
			max_planning = 0.20
			planning_speed = -0.1
			offence = -0.05
			defence = 0.05
			conscription = 0.02
			}
		}
		BOS_detain_Ratko_Mladic = {
			picture = un_intervention
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			stability_factor = 0.05
			western_outlook_campaign_cost_modifier = -0.12
			}
		}
		BOS_financial_crisis = {
			picture = crush_bosnia
			allowed = { always = no }

			allowed_civil_war = { always = yes }
			cancel = { always = no }



			modifier = {
			stability_factor = -0.15
			interest_rate_multiplier_modifier = 0.03
			education_cost_multiplier_modifier = 0.12
			econ_cycle_upg_cost_multiplier_modifier = -0.35
			tax_rate_change_multiplier_modifier = 0.25
			tax_gain_multiplier_modifier = -0.12
			}
		}
		BOS_financial_crisis2 = {

			picture = crush_bosnia
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			interest_rate_multiplier_modifier = 0.03
			econ_cycle_upg_cost_multiplier_modifier = -0.35
			tax_rate_change_multiplier_modifier = 0.25
			tax_gain_multiplier_modifier = 0.01
			}
		}
		BOS_financial_crisis3 = {

			picture = crush_bosnia
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			stability_factor = -0.10
			interest_rate_multiplier_modifier = 0.03
			education_cost_multiplier_modifier = 0.14
			econ_cycle_upg_cost_multiplier_modifier = -0.12
			tax_rate_change_multiplier_modifier = 0.12
			tax_gain_multiplier_modifier = 0.06
			}
		}
		BOS_boots_on_the_ground2 = {

			picture = reserve_divisions
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			army_org = 10
			breakthrough_factor = 0.3
			dig_in_speed = 3
			max_dig_in = 20
			max_planning = 0.20
			planning_speed = -0.2
			offence = -0.05
			defence = 0.05
			conscription = 0.03
			production_factory_start_efficiency_factor = 0.15
			production_factory_max_efficiency_factor = 0.05
			production_factory_efficiency_gain_factor = 0.08
			}
		}
		BOS_civilian_training = {

			picture = army_corruption2
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			dig_in_speed = 7
			max_dig_in = 30
			max_planning = 0.20
			offence = -0.05
			defence = 0.05
			conscription = 0.01
			}
		}
		BOS_rifle_advancements = {

			picture = politicised_army2
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
				production_factory_start_efficiency_factor = 0.15

			}
		}
		bos_rotating_presidency = {

				picture = bosnia_goverment_change

				allowed = {
					always = no
				}

				allowed_civil_war = {
				always = yes
				}

				cancel = {
					always = no
				}



				modifier = {
					bureaucracy_cost_multiplier_modifier = 0.15
					stability_factor = 0.15
					democratic_drift = 0.3
					neutrality_drift = 0.3
					foreign_influence_modifier = -0.50
					foreign_influence_defense_modifier = -0.50
					custom_modifier_tooltip = bos_no_Wars_tt
					#custom_modifier_tooltip = bos_rotating_presidency_info_tt
					tax_gain_multiplier_modifier = 0.08
					drift_defence_factor = 0.15

				}
		}
		bos_rotating_presidency2 = {

			picture = bosnia_goverment_change
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
				bureaucracy_cost_multiplier_modifier = 0.12
				stability_factor = 0.15
				democratic_drift = 0.3
				neutrality_drift = 0.3
				foreign_influence_modifier = -0.25
				foreign_influence_defense_modifier = -0.25
				tax_gain_multiplier_modifier = 0.08
				custom_modifier_tooltip = bos_no_Wars_tt
				#custom_modifier_tooltip = bos_rotating_presidency_info_tt

				drift_defence_factor = 0.15

			}
		}
		bos_rotating_presidency3 = {

			picture = bosnia_goverment_change

			allowed = {
				always = no
			}

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				always = no
			}



			modifier = {
				bureaucracy_cost_multiplier_modifier = 0.12
				stability_factor = 0.15
				democratic_drift = 0.3
				neutrality_drift = 0.3
				tax_gain_multiplier_modifier = 0.08
				foreign_influence_modifier = 0.10
				#foreign_influence_defense_modifier = 0.10
				#custom_modifier_tooltip = bos_rotating_presidency_info_tt
			}
		}
		bos_formed_yugo = {

			picture = defense

			allowed = {
				always = no
			}

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				always = no
			}



			modifier = {
				communism_acceptance = 12
				production_speed_buildings_factor = 0.08
				#custom_modifier_tooltip = bos_no_Wars_yugo_tt
			}
		}
		BOS_encourage_traditions = {

			picture = bosnia_goverment_change

			allowed = {
				always = no
			}

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				always = no
			}



			modifier = {
				bureaucracy_cost_multiplier_modifier = 0.35
				education_cost_multiplier_modifier = -0.15
			}
		}
		bos_PRO_bosnia = {
			picture = political_freedom

			allowed = {
				always = no
			}

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				always = no
			}



			modifier = {
				#receiving_investment_duration_modifier = 0.15
				bureaucracy_cost_multiplier_modifier = -0.10
			}
		}
		bos_PRO_srpska = {

			picture = political_freedom

			allowed = {
				always = no
			}

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				always = no
			}



			modifier = {
				police_cost_multiplier_modifier = -0.2
			}
		}
		bos_civil_war_recovery = {

			picture = bosnia_first
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			production_factory_efficiency_gain_factor = -0.05
			production_factory_start_efficiency_factor = -0.05
			political_power_gain = -0.05
			war_support_factor = -0.3
			join_faction_tension = 0.80
			}
		}
		bos_post_war_efforts = {

			picture = positive_gold
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			production_speed_buildings_factor = 0.2
			production_factory_efficiency_gain_factor = 0.20
			interest_rate_multiplier_modifier = -2
			population_tax_income_multiplier_modifier = 0.03
			}
		}
		####srpska factions
		bos_Milorad_Dodik = {

			picture = democracy

			allowed = { always = no }

			allowed_civil_war = { always = yes }

			cancel = { always = no }



			modifier = {
			political_power_gain = -0.25

			}
		}
		####### up srpska factions
			bos_farming_improvements = {

			picture = agriculture
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			monthly_population = 0.05
			stability_factor = 0.05
			projects_cost_modifier = 0.12
			}
		}
			bos_farming_powerhouse = {

			picture = agricultural_reforms
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			monthly_population = 0.15
			stability_factor = 0.15
			population_tax_income_multiplier_modifier = 0.05
			}
		}
			bos_mining_development = {

			picture = resource_production
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			local_resources_factor = 0.05
			population_tax_income_multiplier_modifier = 0.05
			}
		}
		bos_mining_industry = {

			picture = resource_production
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			local_resources_factor = 0.08
			population_tax_income_multiplier_modifier = 0.03
			}
		}
		bos_lumber_harvesting_idea = {

			picture = resource_production
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			local_resources_factor = 0.03
			population_tax_income_multiplier_modifier = 0.03
			}
		}
		BOS_Srpska_Supremacy = {

			picture = resource_production
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			local_resources_factor = 0.03
			population_tax_income_multiplier_modifier = 0.03
			}
		}
		bos_failing_mining_industy = {

			picture = great_depression
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			local_resources_factor = -0.20
			population_tax_income_multiplier_modifier = -0.03
			}
		}
		bos_unproductive_farming = {

			picture = great_depression
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			monthly_population = -0.17
			stability_factor = -0.15
			population_tax_income_multiplier_modifier = -0.03


			}
		}
		bos_bosnian_lead_army = {

			picture = international_treaty
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			surrender_limit = -0.20
			offence = 0.15
			defence = -0.05
			personnel_cost_multiplier_modifier = 0.05
			hidden_modifier = {
			population_tax_income_multiplier_modifier = 2
			}

			}
		}
			BOS_post_war_clean_up = {

			picture = foreign_capital
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			production_speed_buildings_factor = 0.1
			production_speed_industrial_complex_factor = 0.1
			interest_rate_multiplier_modifier = -6
			population_tax_income_multiplier_modifier = 0.04
			}
		}
		#
		BOS_prepping_an_offensive = {

			picture = planning_bonus

			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				has_war = no
			}



			modifier = {
			max_planning = 0.2
			planning_speed = 0.03
			}
		}
		BOS_commence_the_offensives = {

			picture = planning_bonus

			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				has_war = no
			}



			modifier = {
			offence = 0.15
			}
		}
		BOS_rally_the_troops = {

			picture = politicised_army

			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				has_war = no
			}



			modifier = {
			war_support_weekly = 0.005
			}
		}
		BOS_mobilize_the_nation = {

			picture = volunteer_defenders2

			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				has_war = no
			}



			modifier = {
			army_attack_speed_factor = 0.2
			license_infantry_eq_production_speed_factor = 0.3
			}
		}
		BOS_dig_in = {

			picture = strong_outposts_1

			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				has_war = no
			}



			modifier = {
			dig_in_speed = 3
			}
		}
		BOS_fight_or_Perish = {

			picture = manpower_bonus

			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				has_war = no
			}



			modifier = {
			conscription_factor = 0.03
			}
		}
		BOS_develop_national_sentiment = {


			picture = popular_front

			allowed = { always = no }

			allowed_civil_war = {
			always = yes
			}

			cancel = {
				has_war = no
			}



			modifier = {

			nationalist_drift = 0.03
			}
		}
		#
			BOS_ruined_nation = {

			picture = bosnia_down
			#bosnian_hat			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			production_speed_buildings_factor = -0.1
			production_speed_industrial_complex_factor = -0.1
			personnel_cost_multiplier_modifier = 0.05
			bureaucracy_cost_multiplier_modifier = 0.3
			}
		}
		BOS_land_mines = {

			picture = bos_land_mines_1
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			health_cost_multiplier_modifier = 0.10
			recruitable_population = -0.01
			monthly_population = -0.1
			}
		}
		BOS_officer_core = {

			picture = un_intervention2
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			hidden_modifier = {
				population_tax_income_multiplier_modifier = 2
			}
			command_power_gain = -0.05
			army_org = -10
			promote_cost_factor = 0.5
			assign_army_leader_cp_cost = 15
			personnel_cost_multiplier_modifier = -0.02
			}
		}
			BOS_dayton_accords = {

			picture = international_treaty2

			allowed = { always = no }

			allowed_civil_war = {
			always = no
			}

			cancel = {
				has_war = yes
			}


			targeted_modifier = {
				tag = SER
				attack_bonus_against = -0.1
				defense_bonus_against = -0.1
			}
			targeted_modifier = {
				tag = CRO
				attack_bonus_against = -0.1
				defense_bonus_against = -0.1
			}
			modifier = {
			annex_cost_factor = -1.0
			}
		}
		BOS_land_mine_removal_efforts = {

			picture = bos_land_mines_3
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			health_cost_multiplier_modifier = 0.05
			population_tax_income_multiplier_modifier = -0.02
			}
		}
		BOS_combined_army = {

			picture = bosnian_hat
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
				hidden_modifier = {
				population_tax_income_multiplier_modifier = 2
			}
			conscription_factor = 0.05
			personnel_cost_multiplier_modifier = 0.05
			}
		}
		bos_trade_deal_with_chnia = {

			picture = poor_economy
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			consumer_goods_factor = -0.05
			communism_drift = 0.01
			interest_rate_multiplier_modifier = -0.02

			}
		}
		bos_trade_deal_with_russia = {

			picture = flexible_foreign_policy2
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			production_speed_buildings_factor = 0.05
			consumer_goods_factor = -0.05
			communism_drift = 0.01
			interest_rate_multiplier_modifier = -0.02
			}
		}
		bos_trade_deal_with_russia_expanded = {

			picture = flexible_foreign_policy2
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			production_speed_buildings_factor = 0.10
			consumer_goods_factor = -0.10
			communism_drift = 0.02
			interest_rate_multiplier_modifier = -0.03
			}
		}
		bos_failures_of_protection = {

			picture = un_intervention2
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			war_support_factor = 0.05
			democratic_drift = -0.1
			army_core_defence_factor = 0.08
			}
		}
		bos_trade_deal_with_eu = {

			picture = trade
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			consumer_goods_factor = -0.05
			democratic_drift = 0.01
			interest_rate_multiplier_modifier = -0.02
			}
		}
		bos_trade_deal_with_eu_expanded = {

			picture = trade
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			consumer_goods_factor = -0.10
			democratic_drift = 0.02
			interest_rate_multiplier_modifier = -0.03
			}
		}
		bos_trade_with_nato = {

			picture = flexible_foreign_policy2
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			consumer_goods_factor = -0.05
			democratic_drift = 0.01
			interest_rate_multiplier_modifier = -0.01
			}
		}
		bos_trade_with_usa = {

			picture = flexible_foreign_policy2
			allowed = { always = no }
			allowed_civil_war = { always = yes }
			cancel = { always = no }

			modifier = {
			production_oil_factor = 0.05
			consumer_goods_factor = -0.10
			democratic_drift = 0.02
			interest_rate_multiplier_modifier = -0.02
			}
		}
	}
}
