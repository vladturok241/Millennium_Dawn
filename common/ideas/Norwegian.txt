ideas = {
	country = {

		# Economic Spirits

		NOR_ministry_of_agri_and_food = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_oil_investments2" }
			picture = agriculture

			modifier = {
				monthly_population = 0.05
				agricolture_productivity_modifier = 0.05
			}
		}

		NOR_ministry_of_agri_and_food2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_oil_investments2" }
			picture = agriculture
			name = NOR_ministry_of_agri_and_food
			modifier = {
				monthly_population = 0.10
				agricolture_productivity_modifier = 0.15
			}
		}

		NOR_norsk_industri_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_norsk_industri_idea" }
			picture = agriculture
			modifier = {
				production_speed_industrial_complex_factor = 0.10
				industrial_capacity_factory = 0.10
			}
		}

		NOR_bergen_engines = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_bergen_engines" }
			picture = sustainable_development
			modifier = {
				custom_modifier_tooltip = NOR_bergen_engines_TT
			}
		}

		NOR_aker_yards_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_aker_yards_idea" }
			picture = navy_bonus
			modifier = {
				dockyard_productivity = 0.05
				industrial_capacity_dockyard = 0.10
				production_speed_dockyard_factor = 0.10
			}
		}

		NOR_equinor_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_equinor_idea" }
			picture = foreign_capital
			modifier = {
				oil_export_multiplier_modifier = 0.10
			}
		}

		NOR_equinor_idea2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_equinor_idea2" }
			picture = foreign_capital
			name = NOR_equinor_idea
			modifier = {
				oil_export_multiplier_modifier = 0.15
				return_on_investment_modifier = 0.02
				local_resources_factor = 0.05
			}
		}

		NOR_the_green_shift = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_the_green_shift" }
			picture = solar_energy
			modifier = {
				production_speed_synthetic_refinery_factor = 0.10
			}
		}

		NOR_the_green_shift2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_the_green_shift2" }
			picture = solar_energy
			name = NOR_the_green_shift
			modifier = {
				production_speed_synthetic_refinery_factor = 0.15
				renewable_energy_gain_multiplier = 0.10
				fossil_pp_fuel_consumption_modifier = -0.05
			}
		}

		NOR_ministry_of_transport = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_ministry_of_transport" }
			picture = motorized_focus
			modifier = {
				bureaucracy_cost_multiplier_modifier = -0.10
				production_speed_infrastructure_factor = 0.10
			}
		}

		NOR_ministry_of_transport_baneservice = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_ministry_of_transport_baneservice" }
			picture = motorized_focus
			modifier = {
				bureaucracy_cost_multiplier_modifier = -0.10
				production_speed_infrastructure_factor = 0.10
				production_speed_rail_way_factor = 0.10
				production_speed_supply_node_factor = 0.20
				supply_node_range = 0.10
			}
		}

		NOR_avinor_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_avinor_idea" }
			picture = air_bonus
			modifier = {
				production_speed_radar_station_factor = 0.10
				production_speed_air_base_factor = 0.15
			}
		}

		NOR_standardization_of_infrastructure = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_standardization_of_infrastructure" }
			picture = construction
			modifier = {
				industry_repair_factor = 0.15
				production_speed_offices_factor = 0.10
			}
		}
		# Replaces idea above
		NOR_bilfinger_contract = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_bilfinger_contract" }
			picture = international_treaty2
			modifier = {
				industry_repair_factor = 0.30
				production_speed_offices_factor = 0.15
				production_speed_fuel_silo_factor = 0.10
				production_speed_fossil_powerplant_factor = 0.10
			}
		}

		# Political Spirits

		# Labour Party
		NOR_youth_wing_workers = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_youth_wing_workers" }
			picture = social_democracy
			modifier = {
				party_popularity_stability_factor = 0.15
				mobilization_speed = 0.05
				propaganda_campaign_cost_modifier = -0.35
			}
		}

		NOR_the_modern_welfare_state = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_the_modern_welfare_state" }
			picture = production_bonus
			modifier = {
				social_cost_multiplier_modifier = -0.10
				expected_welfare_modifier = 1.00
			}
		}

		NOR_the_modern_welfare_state2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_the_modern_welfare_state" }
			picture = production_bonus
			name = NOR_the_modern_welfare_state
			modifier = {
				social_cost_multiplier_modifier = -0.10
				expected_welfare_modifier = 1.00
				health_cost_multiplier_modifier = 0.05
				expected_healthcare_modifier = -1.00
			}
		}

		# Christian Peoples Party

		NOR_restrictive_abortions = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_the_modern_welfare_state" }
			picture = no_women_in_military
			modifier = {
				monthly_population = 0.05
				population_tax_income_multiplier_modifier = 0.10
			}
		}

		NOR_restrictive_abortions2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_the_modern_welfare_state" }
			picture = no_women_in_military
			name = NOR_restrictive_abortions
			modifier = {
				monthly_population = 0.05
				population_tax_income_multiplier_modifier = 0.10
				health_cost_multiplier_modifier = -0.10
				consumer_goods_factor = -0.05
			}
		}

		NOR_nato_drilling_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_nato_drilling_idea" }
			picture = ser_nato_bombing_idea
			modifier = {
				personnel_cost_multiplier_modifier = -0.10
				military_leader_cost_factor = -0.15
				army_leader_start_planning_level = 1
			}
		}

		NOR_fair_refugee_distribution_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_nato_drilling_idea" }
			picture = migrant_crisis_europe
			modifier = {
				production_factory_start_efficiency_factor = 0.15
				production_factory_max_efficiency_factor = 0.10
				foreign_influence_defense_modifier = -0.02
			}
		}

		# Nationalists
		NOR_abolish_inheritence_tax = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = international_treaty
			modifier = {
				tax_rate_change_multiplier_modifier = -0.15
				econ_cycle_upg_cost_multiplier_modifier = -0.15
				trade_laws_cost_factor = -0.15
			}
		}

		NOR_abolish_inheritence_tax2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = international_treaty
			name = NOR_abolish_inheritence_tax
			modifier = {
				tax_rate_change_multiplier_modifier = -0.15
				econ_cycle_upg_cost_multiplier_modifier = -0.15
				trade_laws_cost_factor = -0.15
				expected_healthcare_modifier = -2.00
				health_cost_multiplier_modifier = -0.05
			}
		}

		NOR_free_society = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = social_liberalism
			modifier = {
				bureaucracy_cost_multiplier_modifier = -0.20
				corruption_cost_factor = -0.30
			}
		}

		NOR_free_society2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = social_liberalism
			name = NOR_free_society
			modifier = {
				bureaucracy_cost_multiplier_modifier = -0.20
				corruption_cost_factor = -0.30
				research_speed_factor = 0.10
			}
		}

		NOR_lawful_society = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = police_05
			modifier = {
				police_cost_multiplier_modifier = -0.10
				drift_defence_factor = 0.10
			}
		}

		NOR_lawful_society2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = police_05
			name = NOR_lawful_society
			modifier = {
				police_cost_multiplier_modifier = -0.10
				drift_defence_factor = 0.15
				education_cost_multiplier_modifier = -0.10
				nationalist_drift = 0.03
			}
		}

		# Military ideas

		NOR_navy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = coastal_defense_ships
			modifier = {
				navy_personnel_cost_multiplier_modifier = -0.15
			}
		}

		NOR_navy2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = coastal_defense_ships
			name = NOR_navy
			modifier = {
				navy_personnel_cost_multiplier_modifier = -0.15
				naval_accidents_chance = -0.10
				naval_mine_hit_chance = -0.10
				navy_max_range_factor = 0.10
			}
		}

		NOR_cyber_defense = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_cyber_defense" }
			picture = internet_icon
			modifier = {
				production_speed_internet_station_factor = 0.15
				research_speed_factor = 0.10
			}
		}

		NOR_army = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = national_defenders
			modifier = {
				army_personnel_cost_multiplier_modifier = -0.15
			}
		}

		NOR_army2 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = national_defenders
			name = NOR_army
			modifier = {
				army_personnel_cost_multiplier_modifier = -0.15
				recruitable_population_factor = 0.10
				land_reinforce_rate = 0.10
				army_org_factor = -0.03
			}
		}

		NOR_army3 = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = national_defenders
			name = NOR_army
			modifier = {
				recruitable_population_factor = -0.10
				army_org_factor = 0.05
				army_core_defence_factor = 0.20
				army_core_attack_factor	= 0.15
			}
		}

		NOR_airforce = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_abolish_inheritence_tax" }
			picture = fighter_production_increase
			modifier = {
				airforce_personnel_cost_multiplier_modifier = -0.15
			}
		}

		# Migration

		NOR_pro_migration = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_pro_migration" }
			picture = migrant_crisis_europe
			modifier = {
				monthly_population = 0.15
				civ_facs_worker_requirement_modifier = -0.25
				mil_facs_worker_requirement_modifier = -0.20
				offices_worker_requirement_modifier = -0.15
				stability_factor = -0.10
				police_cost_multiplier_modifier = 0.15
			}
		}

		NOR_anti_migration = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add NOR_pro_migration" }
			picture = migrant_crisis_europe
			modifier = {
				police_cost_multiplier_modifier = -0.15
				research_speed_factor = 0.10
				education_cost_multiplier_modifier = -0.15
				foreign_influence_defense_modifier = 0.15
				corporate_tax_income_multiplier_modifier = 0.10
				civ_facs_worker_requirement_modifier = 0.10
				mil_facs_worker_requirement_modifier = 0.05
			}
		}
	}
}