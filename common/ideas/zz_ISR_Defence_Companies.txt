ideas = {

	materiel_manufacturer = {

		designer = yes

		ISR_rafael_materiel_manufacturer = {
			allowed = { original_tag = ISR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_rafael_materiel_manufacturer" }

			picture = Rafael_ISR
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_aa = 0.217
			}

			traits = {
				CAT_aa_7

			}
			ai_will_do = {
				factor = 1
			}
		}
		ISR_israel_weapon_industries_materiel_manufacturer = {
			allowed = { original_tag = ISR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_israel_weapon_industries_materiel_manufacturer" }

			picture = Israel_Weapon_Industries_ISR

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.248
			}

			traits = {
				CAT_inf_wep_8

			}
			ai_will_do = {
				factor = 1
			}
		}

		ISR_israel_military_industries_materiel_manufacturer = {
			allowed = { original_tag = ISR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_israel_military_industries_materiel_manufacturer" }

			picture = Israel_Military_Industries_ISR
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf = 0.217
			}

			traits = {
				CAT_inf_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		ISR_soltam_systems_tank_manufacturer = {
			allowed = { original_tag = ISR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_soltam_systems_tank_manufacturer" }

			picture = Soltam_Systems_ISR

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_artillery = 0.155
			}

			traits = {
				CAT_artillery_5

			}
			ai_will_do = {
				factor = 1
			}
		}
		ISR_plasan_tank_manufacturer = {
			allowed = { original_tag = ISR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_plasan_tank_manufacturer" }

			picture = Plasan_ISR
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.186
			}

			traits = {
				CAT_afv_6

			}
			ai_will_do = {
				factor = 1
			}
		}
		ISR_mantak_tank_manufacturer = {
			allowed = { original_tag = ISR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_mantak_tank_manufacturer" }

			picture = MANTAK_ISR
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_tanks = 0.155
			}

			traits = {
				CAT_tanks_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		ISR_israel_aerospace_industries_aircraft_manufacturer = {
			allowed = { original_tag = ISR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea ISR_israel_aerospace_industries_aircraft_manufacturer" }

			picture = Israel_Aerospace_Industries_ISR
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.217
			}

			traits = {
				CAT_fixed_wing_7

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
