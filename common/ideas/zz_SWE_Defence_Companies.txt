ideas = {

	tank_manufacturer = {

		designer = yes

		SWE_bae_systems_hagglunds_tank_manufacturer = {
			allowed = { original_tag = SWE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWE_bae_systems_hagglunds_tank_manufacturer" }

			picture = BAE_Hagglunds
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_afv = 0.217
			}

			traits = { CAT_armor_7 }
			ai_will_do = {
				factor = 1
			}
		}
		SWE_bae_systems_bofors_tank_manufacturer = {
			allowed = { original_tag = SWE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWE_bae_systems_bofors_tank_manufacturer" }
			picture = BAE_Bofors
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.217
			}

			traits = { CAT_artillery_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		SWE_saab_bofors_dynamics_materiel_manufacturer = {
			allowed = { original_tag = SWE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWE_saab_bofors_dynamics_materiel_manufacturer" }
			picture = Bofors
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf = 0.217
			}
			traits = { CAT_inf_7 }
			ai_will_do = {
				factor = 1
			}
		}
		SWE_saab_ab_materiel_manufacturer = {
			allowed = { original_tag = SWE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWE_saab_ab_materiel_manufacturer" }
			picture = SAAB_tech
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf = 0.217
			}
			traits = { CAT_inf_7 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {
		designer = yes
		SWE_saab_ab_aircraft_manufacturer = {
			allowed = { original_tag = SWE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWE_saab_ab_aircraft_manufacturer" }
			picture = SAAB_tech
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_fighter = 0.248
			}

			traits = { CAT_fighter_8 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {
		designer = yes
		SWE_kockums_ab_naval_manufacturer = {
			allowed = { original_tag = SWE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWE_kockums_ab_naval_manufacturer" }
			picture = Kockums_AB
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_naval_eqp = 0.186
			}

			traits = { CAT_naval_eqp_6 }
			ai_will_do = {
				factor = 1
			}
		}

		SWE_kockums_ab_naval_manufacturer2 = {
			allowed = { original_tag = SWE }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SWE_kockums_ab_naval_manufacturer" }
			picture = Kockums_AB
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_d_sub = 0.186
			}

			traits = {
				CAT_d_sub_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}