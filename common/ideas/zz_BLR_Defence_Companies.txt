ideas = {
	tank_manufacturer = {
		designer = yes
		BLR_MZKT_tank_manufacturer = {
			allowed = { original_tag = BLR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BLR_MZKT_tank_manufacturer" }
			picture = MZKT_BLR
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.150
			}

			traits = {
				CAT_afv_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {

		designer = yes

		BLR_BELSPETSVNESHTECHNIKA_materiel_manufacturer = {
			allowed = { original_tag = BLR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BLR_BELSPETSVNESHTECHNIKA_materiel_manufacturer" }
			picture = BLR_BELSPETSVNESHTECHNIKA
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.100
			}

			traits = {
				CAT_inf_wep_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		BLR_558_aircraft_repair_plant_aircraft_manufacturer = {
			allowed = { original_tag = BLR }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BLR_558_aircraft_repair_plant_aircraft_manufacturer" }

			picture = BLR_558_aircraft_repair_plant
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.100
			}

			traits = {
				CAT_fixed_wing_6

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
