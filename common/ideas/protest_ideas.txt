ideas = {
	country = {
		protest_construction_easy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea protest_construction_easy" }
			picture = industrial_concern
			allowed = { always = no }
			modifier = {
				production_speed_buildings_factor = -0.3
			}
		}

		protest_production_easy = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea protest_production_easy" }
			picture = factory_strikes
			allowed = { always = no }
			modifier = {
				industrial_capacity_factory = -0.15
				industrial_capacity_dockyard = -0.15
			}
		}

		protest_construction_hard = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea protest_construction_hard" }
			picture = industrial_concern
			allowed = { always = no }
			modifier = {
				production_speed_buildings_factor = -0.5
			}
		}

		protest_production_hard = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea protest_production_hard" }
			picture = factory_strikes
			allowed = { always = no }
			modifier = {
				industrial_capacity_factory = -0.25
				industrial_capacity_dockyard = -0.25
			}
		}
	}
}