ideas = {

	materiel_manufacturer = {

		designer = yes

		SER_zastava_arms_materiel_manufacturer = {
			allowed = { original_tag = SER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SER_zastava_arms_materiel_manufacturer" }

			picture = Zastava
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf_wep = 0.155
			}

			traits = {
				CAT_inf_wep_5

			}
			ai_will_do = {
				factor = 1
			}
		}
		SER_yugoimport_spdr_materiel_manufacturer = {
			allowed = { original_tag = SER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SER_yugoimport_spdr_materiel_manufacturer" }
			picture = Yugoimport
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_at = 0.155
			}

			traits = { CAT_at_5 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {

		designer = yes

		SER_utva_aircraft_manufacturer = {
			allowed = { original_tag = SER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SER_utva_aircraft_manufacturer" }

			picture = utva
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_fixed_wing = 0.146
			}

			traits = {
				CAT_fixed_wing_4
			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		SER_yugoimport_spdr_tank_manufacturer = {
			allowed = { original_tag = SER }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea SER_yugoimport_spdr_tank_manufacturer" }
			picture = Yugoimport
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.155
			}

			traits = {
				CAT_armor_5

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
