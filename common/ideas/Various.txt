ideas = {
	hidden_ideas = {
		blue_water_expert_xp_gain_factor_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = global_naval_ambitions
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea blue_water_expert_xp_gain_factor_hidden_spirit" }
			modifier = {
				trait_blue_water_expert_xp_gain_factor = 1
			}
		}
		green_water_expert_xp_gain_factor_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = the_waters_that_surrounds_us
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea green_water_expert_xp_gain_factor_hidden_spirit" }
			modifier = {
				trait_green_water_expert_xp_gain_factor = 1
				trait_inshore_fighter_xp_gain_factor = 1
			}
		}
		securing_the_coast_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = securing_the_coast
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea securing_the_coast_hidden_spirit" }
			modifier = {
				trait_superior_tactician_xp_gain_factor = 1
				trait_fleet_protector_xp_gain_factor = 1
			}
		}
		green_water_navy_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = green_water_navy
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea green_water_navy_hidden_spirit" }
			equipment_bonus = {
				corvette = {
					build_cost_ic = -0.1
					instant = yes
				}
				frigate = {
					build_cost_ic = -0.1
					instant = yes
				}
			}
		}
		jeune_ecole_2_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = jeune_ecole_two
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea green_water_navy_hidden_spirit" }
			equipment_bonus = {
				corvette = {
					build_cost_ic = -0.05
					instant = yes
				}
				frigate = {
					build_cost_ic = -0.05
					instant = yes
				}
				destroyer = {
					build_cost_ic = -0.05
					instant = yes
				}
				stealth_destroyer = {
					build_cost_ic = 5
					instant = yes
				}
				cruiser = {
					build_cost_ic = 5
					instant = yes
				}
				battle_cruiser = {
					build_cost_ic = 7
					instant = yes
				}
				battleship_hull_0 = {
					build_cost_ic = 7
					instant = yes
				}
				helicopter_operator = {
					build_cost_ic = 8
					instant = yes
				}
				carrier = {
					build_cost_ic = 8
					instant = yes
				}
			}
		}
		juryrigged_submarines_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = juryrigged_submarines
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea juryrigged_submarines_hidden_spirit" }
			equipment_bonus = {
				attack_submarine = {
					build_cost_ic = -0.25
					instant = yes
				}
				missile_submarine = {
					build_cost_ic = 3
					instant = yes
				}
			}
		}
		conscript_navy_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = conscript_navy
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea conscript_navy_hidden_spirit" }
			equipment_bonus = {
				corvette = {
					build_cost_ic = -0.05
					instant = yes
				}
				frigate = {
					build_cost_ic = -0.05
					instant = yes
				}
				patrol_boat = {
					build_cost_ic = -0.05
					instant = yes
				}
			}
		}
		concentrated_aircraft_design_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = concentrated_aircraft_design
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea concentrated_aircraft_design_hidden_spirit" }
			equipment_bonus = {
				medium_plane_airframe = {
					build_cost_ic = -0.10
					instant = yes
				}
				cv_medium_plane_airframe = {
					build_cost_ic = -0.10
					instant = yes
				}
				small_plane_strike_airframe = {
					build_cost_ic = -0.10
					instant = yes
				}
				cv_small_plane_strike_airframe = {
					build_cost_ic = -0.10
					instant = yes
				}
			}
		}
		dispersed_aircraft_design_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = dispersed_aircraft_design
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea dispersed_aircraft_design_hidden_spirit" }
			equipment_bonus = {
				medium_plane_fighter_airframe = {
					build_cost_ic = -0.15
					instant = yes
				}
				cv_medium_plane_fighter_airframe = {
					build_cost_ic = -0.15
					instant = yes
				}
				small_plane_airframe = {
					build_cost_ic = -0.15
					instant = yes
				}
				cv_small_plane_airframe = {
					build_cost_ic = -0.15
					instant = yes
				}
				medium_plane_cas_airframe = {
					build_cost_ic = -0.15
					instant = yes
				}
				cv_medium_plane_cas_airframe = {
					build_cost_ic = -0.15
					instant = yes
				}
				small_plane_cas_airframe = {
					build_cost_ic = -0.15
					instant = yes
				}
				cv_small_plane_cas_airframe = {
					build_cost_ic = -0.15
					instant = yes
				}
			}
		}
		dedicated_air_force_manufacturing_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = dedicated_air_force_manufacturing
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea dedicated_air_force_manufacturing_hidden_spirit" }
			equipment_bonus = {
				small_plane_airframe = {
					build_cost_ic = -0.2
					instant = yes
				}
				small_plane_strike_airframe = {
					build_cost_ic = -0.2
					instant = yes
				}
				small_plane_cas_airframe = {
					build_cost_ic = -0.2
					instant = yes
				}
				small_plane_naval_bomber_airframe = {
					build_cost_ic = -0.2
					instant = yes
				}
				small_plane_suicide_airframe = {
					build_cost_ic = -0.2
					instant = yes
				}
			}
		}
		light_vehicle_production_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = increased_vehicle_maintinance
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea light_vehicle_production_hidden_spirit" }
			equipment_bonus = {
				util_vehicle_equipment = {
					build_cost_ic = -0.2
					maximum_speed = 0.1
					instant = yes
				}
			}
		}
		multirole_chassis_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = one_chassis_many_jobs
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea multirole_chassis_hidden_spirit" }
			equipment_bonus = {
				util_vehicle_equipment = {
					build_cost_ic = -0.05
					hard_attack = 0.05
					defense = 0.05
					breakthrough = 0.05
					reliability = -0.10
					instant = yes
				}
			}
		}
		next_gen_aav_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = next_generation_aavs
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea next_gen_aav_hidden_spirit" }
			equipment_bonus = {
				util_vehicle_equipment = {
					build_cost_ic = 0.025
					hard_attack = 0.05
					breakthrough = 0.05
					reliability = -0.05
					instant = yes
				}
			}
		}
		atk_helo_crew_prioritization_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = prioritized_gunship_crews
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea atk_helo_crew_prioritization_hidden_spirit" }
			equipment_bonus = {
				attack_helicopter_hull = {
					build_cost_ic = -0.05
					reliability = 0.10
					instant = yes
				}
			}
		}
		gunship_escorts_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = gunship_escorts
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea gunship_escorts_hidden_spirit" }
			equipment_bonus = {
				attack_helicopter_hull = {
					build_cost_ic = -0.05
					soft_attack = 0.05
					breakthrough = 0.10
					instant = yes
				}
			}
		}
		transport_helicopter_defensive_weapons_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = transport_helicopter_defensive_weapons
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea transport_helicopter_defensive_weapons_hidden_spirit" }
			equipment_bonus = {
				transport_helicopter_equipment = {
					soft_attack = 0.10
					instant = yes
				}
			}
		}
		airmobile_maintainers_hidden_spirit = {
			cancel = {
				NOT = {
					has_tech = airmobile_maintainers
				}
			}
			allowed_civil_war = { always = yes }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea airmobile_maintainers_hidden_spirit" }
			equipment_bonus = {
				transport_helicopter_equipment = {
					reliability = 0.10
					instant = yes
				}
				attack_helicopter_hull = {
					reliability = 0.10
					instant = yes
				}
			}
		}
	}
	country = {
		multi_ethnic_state_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea multi_ethnic_state_idea" }
			available = {
				Multi_Ethnic_State = yes
			}
			modifier = {
				stability_factor = -0.05
			}
		}
		warlords_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea warlords_idea" }
			modifier = {
				stability_factor = -0.15
				political_power_factor = -0.15
				corruption_cost_factor = 0.5
				army_morale_factor = -0.10
				army_org_factor = -0.15
			}
		}
		nuclear_power = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea nuclear_power" }
			picture = GFX_idea_nuclear_power
			allowed_civil_war = { always = yes }
			modifier = {
				political_power_factor = 0.05
			}
		}
		international_sanctions = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea international_sanctions" }
			picture = international_treaty
			modifier = {
				political_power_factor = -0.10
				consumer_goods_factor = 0.10
				stability_factor = -0.10
				production_speed_buildings_factor = -0.10
				trade_opinion_factor = -0.50
			}
		}

		asian_tigers_legacy = {
			picture = tiger_economy
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea asian_tigers_legacy" }
			modifier = {
				production_speed_buildings_factor = 0.075
				trade_opinion_factor = 0.05
				consumer_goods_factor = -0.025
				tax_gain_multiplier_modifier = 0.05
			}
		}
		gateway_to_asia = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea gateway_to_asia" }
			picture = chinese_gateway
			modifier = {
				trade_opinion_factor = 0.20
				consumer_goods_factor = -0.03
				political_power_factor = 0.15
				production_speed_buildings_factor = 0.1
			}
		}
		mp_optimization_idea = {
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea mp_optimization_idea" }
			picture = GFX_idea_nuclear_power
			modifier = {
				conversion_cost_civ_to_mil_factor = 1.0
				lend_lease_tension = 1.00
				send_volunteers_tension = 1.00
				guarantee_tension = 1.00
				join_faction_tension = 1.00
				war_support_factor = -0.25
				industrial_capacity_factory = -0.50
				industrial_capacity_dockyard = -0.50
				enemy_justify_war_goal_time = 10
				research_speed_factor = -0.9
				production_speed_buildings_factor = -0.5
				consumer_goods_factor = 999
				agency_upgrade_time = 999
				ai_focus_aggressive_factor = -999
				ai_get_ally_desire_factor = -999
				ai_join_ally_desire_factor = -999
				ai_license_acceptance = -999
				conscription = -999
				training_time_factor = 999
			}
		}
	}
}