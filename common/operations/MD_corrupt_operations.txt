increase_corrutiption = {
	icon = GFX_increase_corruption
	map_icon = GFX_increase_corruption_map
	name = increase_corrutiption
	desc = increase_corrutiption_desc
	priority = 1

	days = 90
	network_strength = 50
	operatives = 2

	visible = {
		num_of_operatives > 0
		network_national_coverage = {
			target = FROM
			value > 0
		}
		has_operation_token = {
			tag = FROM
			token = token_civilian
		}
	}

	required_tokens = { token_civilian }
	awarded_tokens = {
		token_civilian
	}

	risk_chance = 0.1
	experience = 2
	outcome_extra_chance = 0.25
	risk_modifiers = {  operation_risk }
	outcome_modifiers = {  operation_outcome }
	cost_modifiers = {  operation_cost }
	outcome_execute = {
		ROOT = {
			add_operation_token = {
				tag = FROM
				token = token_civilian
			}
		}
		FROM = {
			increase_corruption = yes
		}
	}

	outcome_potential = {
		ROOT = {
			add_operation_token = {
				tag = FROM
				token = token_civilian
			}
		}
		FROM = {
			increase_corruption = yes
		}
	}

	outcome_extra_execute = {
		FROM = {
			increase_corruption = yes
		}
	}

	phases = { #infiltration
		infiltration_border = { base = 25 }
		infiltration_submarine = { base = 25 }
		infiltration_paradrop = {
			base = 25
			modifier = {
				ROOT = {
					AND = {
						has_equipment = {
							transport_plane_equipment < 1
						}
						has_equipment = {
							large_plane_air_transport_airframe < 1
						}
						has_equipment = {
							cv_medium_plane_air_transport_airframe < 1
						}
					}
				}
				factor = 0.1 #less likely if we have no transports
			}
		}
		infiltration_diplomatic = {
			base = 25
			modifier = {
				factor = 20
				ROOT = { has_war = no }
				FROM = {
					has_war = no
				}
			}
		}
	}
	phases = { #infiltrate military
		infiltrate_military_bribe = { base = 25 }
		infiltrate_military_seduction = { base = 25 }
		infiltrate_military_ideological_supporter = {
			base = 25
			modifier = {
				FROM = { has_government = ROOT }
				factor = 0
			}
		}
	}
	phases = { #exfiltration
		exfiltration_air_pickup = {
			base = 15
			modifier = {
				ROOT = {
					AND = {
						has_equipment = {
							transport_plane_equipment < 1
						}
						has_equipment = {
							large_plane_air_transport_airframe < 1
						}
						has_equipment = {
							cv_medium_plane_air_transport_airframe < 1
						}
					}
				}
				factor = 0.1 #less likely if we have no transports
			}
		}
		exfiltration_border = { base = 30 }
		exfiltration_go_to_ground = { base = 30 }
		exfiltration_submarine_pickup = { base = 25 }
	}

}