israel_has_less_than_1_influence_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = ISR }
			check_variable = { influence_array_val^0 < 1 }
		}
		AND = {
			check_variable = { influence_array^1 = ISR }
			check_variable = { influence_array_val^1 < 1 }
		}
		AND = {
			check_variable = { influence_array^2 = ISR }
			check_variable = { influence_array_val^2 < 1 }
		}
		AND = {
			check_variable = { influence_array^3 = ISR }
			check_variable = { influence_array_val^3 < 1 }
		}
		AND = {
			check_variable = { influence_array^4 = ISR }
			check_variable = { influence_array_val^4 < 1 }
		}
		AND = {
			check_variable = { influence_array^5 = ISR }
			check_variable = { influence_array_val^5 < 1 }
		}
		AND = {
			check_variable = { influence_array^6 = ISR }
			check_variable = { influence_array_val^6 < 1 }
		}
	}
}
israel_has_more_than_1_influence_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = ISR }
			check_variable = { influence_array_val^0 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^1 = ISR }
			check_variable = { influence_array_val^1 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^2 = ISR }
			check_variable = { influence_array_val^2 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^3 = ISR }
			check_variable = { influence_array_val^3 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^4 = ISR }
			check_variable = { influence_array_val^4 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^5 = ISR }
			check_variable = { influence_array_val^5 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^6 = ISR }
			check_variable = { influence_array_val^6 > 0.999 }
		}
	}
}
israel_has_more_than_10_influence_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = ISR }
			check_variable = { influence_array_val^0 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^1 = ISR }
			check_variable = { influence_array_val^1 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^2 = ISR }
			check_variable = { influence_array_val^2 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^3 = ISR }
			check_variable = { influence_array_val^3 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^4 = ISR }
			check_variable = { influence_array_val^4 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^5 = ISR }
			check_variable = { influence_array_val^5 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^6 = ISR }
			check_variable = { influence_array_val^6 > 9.999 }
		}
	}
}
israel_has_more_than_20_influece_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = ISR }
			check_variable = { influence_array_val^0 > 19.999 }
		}
		AND = {
			check_variable = { influence_array^1 = ISR }
			check_variable = { influence_array_val^1 > 19.999 }
		}
		AND = {
			check_variable = { influence_array^2 = ISR }
			check_variable = { influence_array_val^2 > 19.999 }
		}
		AND = {
			check_variable = { influence_array^3 = ISR }
			check_variable = { influence_array_val^3 > 19.999 }
		}
	}
}
#####################
saudi_has_less_than_1_influence_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = SAU }
			check_variable = { influence_array_val^0 < 1 }
		}
		AND = {
			check_variable = { influence_array^1 = SAU }
			check_variable = { influence_array_val^1 < 1 }
		}
		AND = {
			check_variable = { influence_array^2 = SAU }
			check_variable = { influence_array_val^2 < 1 }
		}
		AND = {
			check_variable = { influence_array^3 = SAU }
			check_variable = { influence_array_val^3 < 1 }
		}
		AND = {
			check_variable = { influence_array^4 = SAU }
			check_variable = { influence_array_val^4 < 1 }
		}
		AND = {
			check_variable = { influence_array^5 = SAU }
			check_variable = { influence_array_val^5 < 1 }
		}
		AND = {
			check_variable = { influence_array^6 = SAU }
			check_variable = { influence_array_val^6 < 1 }
		}
	}
}
saudi_has_more_than_1_influence_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = SAU }
			check_variable = { influence_array_val^0 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^1 = SAU }
			check_variable = { influence_array_val^1 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^2 = SAU }
			check_variable = { influence_array_val^2 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^3 = SAU }
			check_variable = { influence_array_val^3 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^4 = SAU }
			check_variable = { influence_array_val^4 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^5 = SAU }
			check_variable = { influence_array_val^5 > 0.999 }
		}
		AND = {
			check_variable = { influence_array^6 = SAU }
			check_variable = { influence_array_val^6 > 0.999 }
		}
	}
}
saudi_has_more_than_10_influence_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = SAU }
			check_variable = { influence_array_val^0 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^1 = SAU }
			check_variable = { influence_array_val^1 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^2 = SAU }
			check_variable = { influence_array_val^2 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^3 = SAU }
			check_variable = { influence_array_val^3 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^4 = SAU }
			check_variable = { influence_array_val^4 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^5 = SAU }
			check_variable = { influence_array_val^5 > 9.999 }
		}
		AND = {
			check_variable = { influence_array^6 = SAU }
			check_variable = { influence_array_val^6 > 9.999 }
		}
	}
}
saudi_has_more_than_20_influece_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = SAU }
			check_variable = { influence_array_val^0 > 19.999 }
		}
		AND = {
			check_variable = { influence_array^1 = SAU }
			check_variable = { influence_array_val^1 > 19.999 }
		}
		AND = {
			check_variable = { influence_array^2 = SAU }
			check_variable = { influence_array_val^2 > 19.999 }
		}
		AND = {
			check_variable = { influence_array^3 = SAU }
			check_variable = { influence_array_val^3 > 19.999 }
		}
	}
}
iran_has_more_than_15_influence_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = PER }
			check_variable = { influence_array_val^0 > 14.999 }
		}
		AND = {
			check_variable = { influence_array^1 = PER }
			check_variable = { influence_array_val^1 > 14.999 }
		}
		AND = {
			check_variable = { influence_array^2 = PER }
			check_variable = { influence_array_val^2 > 14.999 }
		}
		AND = {
			check_variable = { influence_array^3 = PER }
			check_variable = { influence_array_val^3 > 14.999 }
		}
		AND = {
			check_variable = { influence_array^4 = PER }
			check_variable = { influence_array_val^4 > 14.999 }
		}
		AND = {
			check_variable = { influence_array^5 = PER }
			check_variable = { influence_array_val^5 > 14.999 }
		}
		AND = {
			check_variable = { influence_array^6 = PER }
			check_variable = { influence_array_val^6 > 14.999 }
		}
	}
}
iran_has_more_than_40_influence_trigger = {
	OR = {
		AND = {
			check_variable = { influence_array^0 = PER }
			check_variable = { influence_array_val^0 > 39.999 }
		}
		AND = {
			check_variable = { influence_array^1 = PER }
			check_variable = { influence_array_val^1 > 39.999 }
		}
		AND = {
			check_variable = { influence_array^2 = PER }
			check_variable = { influence_array_val^2 > 39.999 }
		}
		AND = {
			check_variable = { influence_array^3 = PER }
			check_variable = { influence_array_val^3 > 39.999 }
		}
		AND = {
			check_variable = { influence_array^4 = PER }
			check_variable = { influence_array_val^4 > 39.999 }
		}
		AND = {
			check_variable = { influence_array^5 = PER }
			check_variable = { influence_array_val^5 > 39.999 }
		}
		AND = {
			check_variable = { influence_array^6 = PER }
			check_variable = { influence_array_val^6 > 39.999 }
		}
	}
}