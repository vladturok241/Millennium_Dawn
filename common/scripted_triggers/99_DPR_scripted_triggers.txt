DPR_targeted_country_has_more_than_55_percent_influence = {
	check_variable = { influence_array^0 = DPR }
	check_variable = { influence_array_val^0 > 54.999 }
}
