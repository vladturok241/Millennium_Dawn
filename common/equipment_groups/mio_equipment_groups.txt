########  ##          ###    ##    ## ########     ######   ########   #######  ##     ## ########   ######
##     ## ##         ## ##   ###   ## ##          ##    ##  ##     ## ##     ## ##     ## ##     ## ##    ##
##     ## ##        ##   ##  ####  ## ##          ##        ##     ## ##     ## ##     ## ##     ## ##
########  ##       ##     ## ## ## ## ######      ##   #### ########  ##     ## ##     ## ########   ######
##        ##       ######### ##  #### ##          ##    ##  ##   ##   ##     ## ##     ## ##              ##
##        ##       ##     ## ##   ### ##          ##    ##  ##    ##  ##     ## ##     ## ##        ##    ##
##        ######## ##     ## ##    ## ########     ######   ##     ##  #######   #######  ##         ######


mio_cat_eq_only_light_aircraft = { #Includes Small Fighter and CV Fighter (no CAS, no Nav...) --- Needed because plane designer is a gift to us all
	equipment_type = {
		small_plane_airframe
		small_plane_cas_airframe
		small_plane_strike_airframe
		small_plane_naval_bomber_airframe
	}
}

mio_cat_eq_only_mr_fighters = {
	equipment_type = {
		small_plane_strike_airframe
		medium_plane_airframe
		cv_medium_plane_airframe
	}
}

mio_cat_eq_only_as_fighters = {
	equipment_type = {
		small_plane_airframe
		cv_small_plane_airframe
		medium_plane_fighter_airframe
		cv_medium_plane_fighter_airframe
	}
}

mio_cat_eq_only_cas_fighters = {
	equipment_type = {
		small_plane_cas_airframe
		cv_small_plane_cas_airframe
		medium_plane_cas_airframe
		cv_medium_plane_cas_airframe
		large_plane_cas_airframe
	}
}

mio_cat_eq_only_uav = {
	equipment_type = {
		small_plane_suicide_airframe
		cv_small_plane_suicide_airframe
		medium_plane_suicide_airframe
	}
}

mio_cat_eq_only_awacs = {
	equipment_type = {
		cv_medium_plane_scout_airframe
		large_plane_awacs_airframe
	}
}

mio_cat_eq_only_nav = {
	equipment_type = {
		small_plane_naval_bomber_airframe
		cv_small_plane_naval_bomber_airframe
		medium_plane_maritime_patrol_airframe
		cv_medium_plane_maritime_patrol_airframe
		large_plane_maritime_patrol_airframe
	}
}

mio_cat_eq_only_transport = {
	equipment_type = {
		cv_medium_plane_air_transport_airframe
		large_plane_air_transport_airframe
	}
}

mio_cat_eq_only_light_cv_aircraft = { #Includes only Small Fighter (No CV, no CAS, no Nav...) --- Needed because plane designer is a gift to us all
	equipment_type = {
		cv_small_plane_airframe
		cv_small_plane_cas_airframe
		cv_small_plane_strike_airframe
		cv_small_plane_naval_bomber_airframe
	}
}

mio_cat_eq_only_medium_aircraft = { #Includes only Tactical Bomber (Heavy Fighter, No Scout...) --- Needed because plane designer is a gift to us all
	equipment_type = {
		medium_plane_airframe
		medium_plane_fighter_airframe
		medium_plane_cas_airframe
		medium_plane_suicide_airframe
		medium_plane_maritime_patrol_airframe
	}
}

mio_cat_eq_only_cv_medium_aircraft = { #Includes only Strategic Bomber (No Maritime Patrol) --- Needed because plane designer is a gift to us all
	equipment_type = {
		cv_medium_plane_airframe
		cv_medium_plane_fighter_airframe
		cv_medium_plane_cas_airframe
		cv_medium_plane_maritime_patrol_airframe
		cv_medium_plane_scout_airframe
		cv_medium_plane_air_transport_airframe
	}
}

mio_cat_eq_only_large_aircraft = { #Includes only Strategic Bomber (No Maritime Patrol) --- Needed because plane designer is a gift to us all
	equipment_type = {
		large_plane_airframe
		large_plane_maritime_patrol_airframe
		large_plane_awacs_airframe
		large_plane_cas_airframe
		large_plane_air_transport_airframe
	}
}