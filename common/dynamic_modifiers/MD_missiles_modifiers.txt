#example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}

#############################
### Air Defense Modifiers ###
#############################

### Fleet Air Defense System ###
air_defense_modifiers_system = {
	enable = { always = yes }
	static_anti_air_damage_factor = var_SAM_AD_damage
	static_anti_air_hit_chance_factor = var_SAM_AD_intercept_chance_total
	navy_anti_air_attack_factor = var_fleet_air_defense_system
	navy_anti_air_attack = var_NSAM_AD_damage
}

##################################
### Satellite System Modifiers ###
##################################

### GNSS System ###

GNSS_system = {
	icon = GFX_idea_GNSS_system
	enable = { always = yes }
	### GNSS_mil_system
	army_speed_factor = var_GNSS_mil_army_speed_factor
	air_cas_efficiency = var_GNSS_mil_air_cas_efficiency
	air_nav_efficiency = var_GNSS_mil_air_nav_efficiency
	air_strategic_bomber_bombing_factor = var_GNSS_mil_air_strategic_bomber_bombing_factor
	positioning = var_GNSS_mil_positioning
	naval_hit_chance = var_GNSS_mil_naval_hit_chance
	### GNSS_civ_system
	production_speed_buildings_factor = var_GNSS_civ_production_speed_buildings_factor
	production_speed_infrastructure_factor = var_GNSS_civ_production_speed_infrastructure_factor
	local_resources_factor = var_GNSS_civ_local_resources_factor
}

COMSAT_system = {
	icon = GFX_idea_COMSAT_system
	enable = { always = yes }
	### COM_mil_system
	max_command_power = var_COM_mil_max_command_power
	army_org_factor = var_COM_mil_army_org_factor
	planning_speed = var_COM_mil_planning_speed
	air_escort_efficiency = var_COM_mil_air_escort_efficiency
	air_intercept_efficiency = var_COM_mil_air_intercept_efficiency
	naval_coordination = var_COM_mil_naval_coordination
	### COM_civ_system
	# var_COM_civ_income
	### political_power_factor was to heavy, needed a nerf
	political_power_factor = var_COM_civ_political_power_factor
	#political_power_gain = var_COM_civ_political_power_factor
	decryption_factor = var_COM_civ_decryption_factor
	encryption_factor = var_COM_civ_encryption_factor
	intel_network_gain_factor = var_COM_civ_intel_network_gain_factor
	operation_outcome = var_COM_civ_operation_outcome
}

SPYSAT_system = {
	icon = GFX_idea_SPYSAT_system
	enable = { always = yes }
	### SPY_mil_system
	max_planning_factor = var_SPY_mil_max_planning_factor
	recon_factor = var_SPY_mil_recon_factor
	air_weather_penalty = var_SPY_mil_air_weather_penalty
	spotting_chance = var_SPY_mil_spotting_chance
	convoy_raiding_efficiency_factor = var_SPY_mil_convoy_raiding_efficiency_factor
	convoy_escort_efficiency = var_SPY_mil_convoy_escort_efficiency
	### SPY_civ_system
	research_speed_factor = var_SPY_civ_research_speed_factor
	civilian_intel_factor = var_SPY_civ_civilian_intel_factor
	army_intel_factor = var_SPY_civ_army_intel_factor
	navy_intel_factor = var_SPY_civ_navy_intel_factor
	airforce_intel_factor = var_SPY_civ_airforce_intel_factor
	root_out_resistance_effectiveness_factor = var_SPY_civ_root_out_resistance_effectiveness_factor
}

#########################
### nuclear modifiers ###
#########################

### nuclear country
nuclear_state_modifier = {
	icon = GFX_idea_nuclear_power
	enable = { always = yes }
	#political_power_gain = var_nuclear_deterrence_PP_factor
	foreign_influence_defense_modifier = var_nuclear_deterrence_foreign_influence_defense
	political_power_factor = var_nuclear_deterrence_PP_factor
	stability_factor = var_nuclear_stability_factor
	civilian_factory_use = var_CIC_use_nuclear
	energy_gain = var_nuke_energy_prod_hour
}

radiation_modifier = {
	icon = GFX_idea_Radioactive
	enable = { always = yes }
	local_manpower = var_nuke_local_manpower
	local_non_core_manpower = var_nuke_local_non_core_manpower
	non_core_manpower = var_nuke_non_core_manpower
	recruitable_population = var_nuke_recruitable_population
	recruitable_population_factor = var_nuke_recruitable_population_factor
}