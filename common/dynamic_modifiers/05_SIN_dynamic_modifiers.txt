#Author(s): AngriestBird

SIN_singaporean_trade_strength = {
	enable = { always = yes }
	icon = "GFX_idea_trade"

	foreign_influence_modifier = SIN_foreign_influence_strength
	bureaucracy_cost_multiplier_modifier = SIN_bureaucracy_cost_multiplier_modifier_strength
	custom_modifier_tooltip = SIN_tariff_income_tt
}