FRA_idea_nepad_modifier = {
	icon = "GFX_idea_AU_member"
	enable = { always = yes }

	political_power_factor = 0.15
	custom_modifier_tooltip = FRA_idea_nepad_modifier_cost_tt
}

AFRICA_idea_nepad_modifier = {
	icon = "GFX_idea_AU_member"
	enable = { always = yes }

	health_cost_multiplier_modifier = global.africa_nepad_health_cost_modifier
	social_cost_multiplier_modifier = global.africa_nepad_social_care_modifier
	education_cost_multiplier_modifier = global.africa_nepad_education_modifier
	production_speed_buildings_factor = global.africa_nepad_building_construction_modifier
	custom_modifier_tooltip = AFRICA_idea_nepad_modifier_tt
}