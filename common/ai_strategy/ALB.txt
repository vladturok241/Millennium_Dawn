ALB_KOS_War = {
	allowed = { original_tag = ALB }
	enable = {
		KOS = {
			has_war_with = SER
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = send_volunteers_desire id = "KOS" value = 1000 }
	ai_strategy = { type = send_lend_lease_desire id = "KOS" value = 1000 }
	ai_strategy = { type = support id = "KOS" value = 1000 }
}
ALB_Friend_KOS = {
	allowed = { original_tag = ALB }
	enable = {
		country_exists = KOS
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = befriend id = "KOS" value = 1000 }
}