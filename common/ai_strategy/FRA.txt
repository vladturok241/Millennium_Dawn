# Author(s): AngriestBird
FRA_befriend_the_francosphere = {
	allowed = { original_tag = FRA }
	enable = {
		original_tag = FRA
		NOT = { has_completed_focus = FRA_aggressive_expansionism } #France going this route should prevent all from befriending
	}
	abort_when_not_enabled = yes

	# Befriend List
	ai_strategy = { type = befriend id = "ALG" value = 50 }
	ai_strategy = { type = befriend id = "MOR" value = 50 }
	ai_strategy = { type = befriend id = "TUN" value = 50 }
	ai_strategy = { type = befriend id = "SEN" value = 50 }
	ai_strategy = { type = befriend id = "CDI" value = 50 }
	ai_strategy = { type = befriend id = "MAD" value = 50 }
	ai_strategy = { type = befriend id = "LEB" value = 50 }
	ai_strategy = { type = befriend id = "SYR" value = 50 }
	ai_strategy = { type = befriend id = "VIE" value = 50 }
	ai_strategy = { type = befriend id = "LAO" value = 50 }
	ai_strategy = { type = befriend id = "CBD" value = 50 }
	ai_strategy = { type = befriend id = "CAR" value = 50 }
	ai_strategy = { type = befriend id = "DJI" value = 50 }
	ai_strategy = { type = befriend id = "CHA" value = 50 }
	ai_strategy = { type = befriend id = "NGR" value = 50 }
	ai_strategy = { type = befriend id = "MAL" value = 50 }
	ai_strategy = { type = befriend id = "BFA" value = 50 }
	ai_strategy = { type = befriend id = "GAB" value = 50 }
	ai_strategy = { type = befriend id = "CNG" value = 50 }
	ai_strategy = { type = befriend id = "MRT" value = 50 }
	ai_strategy = { type = befriend id = "HAI" value = 50 }
	ai_strategy = { type = befriend id = "DMI" value = 50 }
	ai_strategy = { type = befriend id = "FIJ" value = 50 }
	# Support List
	ai_strategy = { type = support id = "ALG" value = 50 }
	ai_strategy = { type = support id = "MOR" value = 50 }
	ai_strategy = { type = support id = "TUN" value = 50 }
	ai_strategy = { type = support id = "SEN" value = 50 }
	ai_strategy = { type = support id = "CDI" value = 50 }
	ai_strategy = { type = support id = "MAD" value = 50 }
	ai_strategy = { type = support id = "LEB" value = 50 }
	ai_strategy = { type = support id = "SYR" value = 50 }
	ai_strategy = { type = support id = "VIE" value = 50 }
	ai_strategy = { type = support id = "LAO" value = 50 }
	ai_strategy = { type = support id = "CBD" value = 50 }
	ai_strategy = { type = support id = "CAR" value = 50 }
	ai_strategy = { type = support id = "DJI" value = 50 }
	ai_strategy = { type = support id = "CHA" value = 50 }
	ai_strategy = { type = support id = "NGR" value = 50 }
	ai_strategy = { type = support id = "MAL" value = 50 }
	ai_strategy = { type = support id = "BFA" value = 50 }
	ai_strategy = { type = support id = "GAB" value = 50 }
	ai_strategy = { type = support id = "CNG" value = 50 }
	ai_strategy = { type = support id = "MRT" value = 50 }
	ai_strategy = { type = support id = "HAI" value = 50 }
	ai_strategy = { type = support id = "DMI" value = 50 }
	ai_strategy = { type = support id = "FIJ" value = 50 }
	# Protect
	ai_strategy = { type = protect id = "ALG" value = 50 }
	ai_strategy = { type = protect id = "MOR" value = 50 }
	ai_strategy = { type = protect id = "TUN" value = 50 }
	ai_strategy = { type = protect id = "SEN" value = 50 }
	ai_strategy = { type = protect id = "CDI" value = 50 }
	ai_strategy = { type = protect id = "MAD" value = 50 }
	ai_strategy = { type = protect id = "LEB" value = 50 }
	ai_strategy = { type = protect id = "SYR" value = 50 }
	ai_strategy = { type = protect id = "VIE" value = 50 }
	ai_strategy = { type = protect id = "LAO" value = 50 }
	ai_strategy = { type = protect id = "CBD" value = 50 }
	ai_strategy = { type = protect id = "CAR" value = 50 }
	ai_strategy = { type = protect id = "DJI" value = 50 }
	ai_strategy = { type = protect id = "CHA" value = 50 }
	ai_strategy = { type = protect id = "NGR" value = 50 }
	ai_strategy = { type = protect id = "MAL" value = 50 }
	ai_strategy = { type = protect id = "BFA" value = 50 }
	ai_strategy = { type = protect id = "GAB" value = 50 }
	ai_strategy = { type = protect id = "CNG" value = 50 }
	ai_strategy = { type = protect id = "MRT" value = 50 }
	ai_strategy = { type = protect id = "HAI" value = 50 }
	ai_strategy = { type = protect id = "DMI" value = 50 }
	ai_strategy = { type = protect id = "FIJ" value = 50 }
}

FRA_support_rojava = {
	allowed = { original_tag = FRA }
	enable = {
		country_exists = ROJ
		NOT = { has_government = nationalist }
		ROJ = {
			OR = {
				has_war_with = SYR
				has_war_with = ISI
			}
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = protect id = "ROJ" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "ROJ" value = 150 }
}

FRA_intervene_in_central_african_republic = {
	allowed = { original_tag = FRA }
	enable = {
		country_exists = CAR
		NOT = { has_war_with = CAR }
		NOT = { has_government = nationalist }
		CAR = {
			OR = {
				has_civil_war = yes
				has_war_with = SEL
				has_war_with = BAL
				has_war_with = LOG
			}
		}
		has_completed_focus = FRA_CAR_intervention
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "CAR" value = 50 }
	ai_strategy = { type = protect id = "CAR" value = 50 }
	ai_strategy = { type = influence id = "CAR" value = 50 }
	ai_strategy = { type = support id = "CAR" value = 50 }
	ai_strategy = { type = send_volunteers_desire id = "CAR" value = 50 }
}

FRA_support_house_of_reps = {
	allowed = { original_tag = FRA }
	enable = {
		country_exists = HOR
		NOT = { has_war_with = HOR }
		NOT = { has_government = nationalist }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "HOR" value = 50 }
	ai_strategy = { type = protect id = "HOR" value = 50 }
	ai_strategy = { type = influence id = "HOR" value = 50 }
}

FRA_support_pakistan_against_jihadists = {
	allowed = { original_tag = FRA }
	enable = {
		country_exists = PAK
		NOT = { has_war_with = PAK }
		PAK = {
			has_civil_war = yes
			NOT = { has_government = fascism }
			has_country_flag = fighting_jihadis
		}
		NOT = { has_government = nationalist }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = send_volunteers_desire id = "PAK" value = 100 }
	ai_strategy = { type = support id = "PAK" value = 100 }
}

FRA_support_egypt_against_jihadists = {
	allowed = { original_tag = FRA }
	enable = {
		EGY = {
			has_civil_war = yes
			NOT = { has_government = fascism }
			has_country_flag = fighting_jihadis
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = send_volunteers_desire id = "EGY" value = 100 }
	ai_strategy = { type = support id = "EGY" value = 100 }
}

FRA_factory_target = {
	allowed = { tag = FRA }
	enable = {
		original_tag = FRA
		num_of_civilian_factories < 130
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = building_target id = industrial_complex value = 130 }
}

FRA_office_target = {
	allowed = { tag = FRA }
	enable = {
		original_tag = FRA
		num_of_available_civilian_factories > 29
		check_variable = { office_park_total < 30 }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = building_target id = offices value = 30 }
}

# France unique AI for combat units
FRA_default_naval_breakdown = {
	allowed = { original_tag = FRA }
	enable = { original_tag = FRA }
	abort_when_not_enabled = yes

	ai_strategy = { type = role_ratio id = naval_corvettes value = 10 }
	ai_strategy = { type = role_ratio id = naval_frigate value = 15 }
	ai_strategy = { type = role_ratio id = naval_destroyer value = 23 }
	ai_strategy = { type = role_ratio id = naval_stealth_destroyer value = 4 }
	ai_strategy = { type = role_ratio id = naval_attack_submarine value = 24 }
	ai_strategy = { type = role_ratio id = naval_missile_submarine value = 8 }
	ai_strategy = { type = role_ratio id = naval_helicopter_operator value = 12 }
	ai_strategy = { type = role_ratio id = naval_carrier value = 3 }
	ai_strategy = { type = role_ratio id = naval_cruiser value = 1 }
}

FRA_protect_andorra_monaco = {
	allowed = {
		original_tag = FRA
		NOT = { has_global_flag = GAME_RULE_microstates_removed }
	}
	enable = {
		FRA = { has_government = democratic }
		OR = {
			country_exists = ADO
			country_exists = MNC
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = protect id = "ADO" value = 125 }
	ai_strategy = { type = support id = "ADO" value = 125 }
	ai_strategy = { type = protect id = "MNC" value = 125 }
	ai_strategy = { type = support id = "MNC" value = 125 }
}

FRA_united_kingdom_french_relations = {
	allowed = { original_tag = FRA }
	enable = {
		NOT = {
			has_government = nationalist
			has_government = fascism
		}
		country_exists = ENG
		NOT = { ENG = { has_government = nationalist } }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "ENG" value = 125 }
	ai_strategy = { type = support id = "ENG" value = 125 }
}

FRA_support_senegal_in_civil_war = {
	allowed = { original_tag = FRA }
	enable = {
		original_tag = FRA
		SEN = {
			OR = {
				has_civil_war = yes
				has_war_with = CSM
			}
		}
		is_historical_focus_on = yes
		has_completed_focus = FRA_senegalese_civil_war
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "SEN" value = 50 }
	ai_strategy = { type = support id = "SEN" value = 50 }
	ai_strategy = { type = send_volunteers_desire id = "SEN" value = 25 }
}

FRA_intervene_in_mali = {
	allowed = { original_tag = FRA }
	enable = {
		original_tag = FRA
		MAL = {
			OR = {
				has_civil_war = yes
				has_war_with = TUA
			}
		}
		is_historical_focus_on = yes
		has_completed_focus = FRA_operation_serval
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "MAL" value = 50 }
	ai_strategy = { type = support id = "MAL" value = 50 }
	ai_strategy = { type = send_volunteers_desire id = "MAL" value = 25 }
}

FRA_intervene_in_drc = {
	allowed = { original_tag = FRA }
	enable = {
		original_tag = FRA
		UGA = {
			OR = {
				has_civil_war = yes
				has_war_with = RWA
				has_war_with = DRC
			}
		}
		is_historical_focus_on = yes
		has_completed_focus = FRA_operation_artemis
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "UGA" value = 50 }
	ai_strategy = { type = support id = "UGA" value = 50 }
	ai_strategy = { type = send_volunteers_desire id = "UGA" value = 25 }
}

FRA_support_the_european_union = {
	allowed = { original_tag = FRA }
	enable = {
		original_tag = FRA
		OR = {
			has_government = democratic
			is_historical_focus_on = yes
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "SPR" value = 50 }
	ai_strategy = { type = befriend id = "POR" value = 50 }
	ai_strategy = { type = befriend id = "ENG" value = 50 }
	ai_strategy = { type = befriend id = "IRE" value = 50 }
	ai_strategy = { type = befriend id = "ITA" value = 50 }
	ai_strategy = { type = befriend id = "GER" value = 50 }
	ai_strategy = { type = befriend id = "BEL" value = 50 }
	ai_strategy = { type = befriend id = "HOL" value = 50 }
	ai_strategy = { type = befriend id = "DEN" value = 50 }
	ai_strategy = { type = befriend id = "CRO" value = 50 }
	ai_strategy = { type = befriend id = "CZE" value = 50 }
}

FRA_support_ukraine_against_russia = {
	allowed = { original_tag = FRA }
	enable = {
		original_tag = FRA
		OR = {
			has_government = democratic
			is_historical_focus_on = yes
		}
		UKR = {
			has_war_with = SOV
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = protect id = "UKR" value = 125 }
	ai_strategy = { type = support id = "UKR" value = 125 }
}

FRA_support_morocco = {
	allowed = { original_tag = FRA }
	enable = {
		country_exists = MOR
		NOT = { has_war_with = MOR }
		MOR = { has_government = democratic }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "MOR" value = 50 }
}

FRA_intervene_in_cdi = {
	allowed = { original_tag = FRA }
	enable = {
		CDI = {
			OR = {
				has_civil_war = yes
				has_war_with = FNC
			}
		}
		is_historical_focus_on = yes
		has_completed_focus = FRA_operation_licorne
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "CDI" value = 50 }
	ai_strategy = { type = support id = "CDI" value = 50 }
	ai_strategy = { type = send_volunteers_desire id = "CDI" value = 25 }
}

FRA_support_the_somalian_government = {
	allowed = { original_tag = FRA }
	enable = {
		SOM = {
			OR = {
				has_civil_war = yes
				has_war_with = SHB
			}
		}
		is_historical_focus_on = yes
		has_completed_focus = FRA_oef_hoa
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "SOM" value = 50 }
	ai_strategy = { type = support id = "SOM" value = 50 }
	ai_strategy = { type = send_volunteers_desire id = "SOM" value = 25 }
}

FRA_supporting_armenia_strategy = {
	allowed = { original_tag = FRA }
	enable = {
		country_exists = ARM
		has_completed_focus = FRA_supporting_armenia
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "ARM" value = 50 }
	ai_strategy = { type = support id = "ARM" value = 50 }
	ai_strategy = { type = send_volunteers_desire id = "ARM" value = 25 }
}

FRA_annoy_the_assads_syria = {
	allowed = { original_tag = FRA }
	enable = {
		country_exists = SYR
		SYR = {
			OR = {
				has_country_leader = { name = "Bashar al-Assad" ruling_only = yes }
				has_country_leader = { name = "Hafez al-Assad" ruling_only = yes }
				has_country_leader = { name = "Bushra al-Assad" ruling_only = yes }
				has_country_leader = { name = "Maher al-Assad" ruling_only = yes }
				has_country_leader = { name = "Rifaat al-Assad" ruling_only = yes }
			}
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = contain id = "SYR" value = 50 }
	ai_strategy = { type = antagonize id = "SYR" value = 50 }
}

FRA_support_lebanon_against_syria = {
	allowed = { original_tag = FRA }
	enable = {
		country_exists = LEB
		LEB = {
			OR = {
				is_subject_of = SYR
				has_war_with = SYR
			}
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "LEB" value = 50 }
	ai_strategy = { type = support id = "LEB" value = 50 }
	ai_strategy = { type = send_volunteers_desire id = "LEB" value = 25 }
}

FRA_support_afghanistan_against_taliban = {
	allowed = { original_tag = FRA }
	enable = {
		NOT = { has_war_with = AFG }
		AFG = { NOT = { has_government = fascism } }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "AFG" value = 75 }
	ai_strategy = { type = support id = "AFG" value = 100 }
	ai_strategy = { type = send_volunteers_desire id = "AFG" value = 75 }
}