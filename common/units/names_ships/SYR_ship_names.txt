﻿##### NEW ZEALAND NAME LISTS #####
### REGULAR DESTROYER NAMES###
SYR_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { SYR }

	type = ship
	ship_types = { destroyer }

	prefix = "SARS "
	fallback_name = "Destroyer %s"

	unique = {
		"Al-Qurayya" "Al-Shaheel" "Al-Suwaydah" "Al-Afra" "Al-Filah" "Al-Aminah" "Al-Saqqalibah" "Al-Shal" "Al-Suwaydiyah" "Al-Qardaha" "Al-Maklub" "Al-Talasah" "Al-Muayzil" "Al-Himma" "Al-Mahmudi" "Al-Khaznawi" "Al-Shahba" "Al-Tibah" "Al-Zuhrah" "Al-Ruwaq" "Al-Hayal" "Al-Sawma" "Al-Malah" "Al-Hawz" "Al-Niyl" 
	}
}

SYR_FRIGATES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATES
	for_countries = { SYR }
	type = ship
	ship_types = {
		frigate
	}
	prefix = "SARS "
	fallback_name = "FFG-%d"

	unique = {
		"Al-Sahel" "Al-Jazira" "Al-Sahra" "Al-Arman" "Al-Sharq" "Al-Gharb" "Al-Qasr" "Al-Mahatta" "Al-Musaytib" "Al-Qasmiya" "Al-Salihiya" "Al-Nabek" "Al-Yabrud" "Al-Tall" "Al-Rastan" "Al-Hawash" "Al-Nasiriya" "Al-Qalamoun" "Al-Tadmur" "Al-Suqalibiyah" "Al-Qurdaha" "Al-Haffah" "Al-Hums" "Al-Aqirabat" "Al-Jablah" "Al-Quatna" "Al-Abrash" "Al-Karnak" "Al-Bandora" "Al-Maara" "Al-Abil" "Al-Qatana" "Al-Tabbah" "Al-Tuffah" "Al-Sawadi" "Al-Siryan" "Al-Tadmurah" "Al-Qitar"   
	}
}

### CORVETTE NAMES ###
SYR_CORVETTE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTE

	for_countries = { SYR }
	prefix = "SARS "
	type = ship
	ship_types = { corvette }
	fallback_name = "KKG-%d"

	unique = {
		"Al-Rasheed" "Al-Majd" "Al-Mustafa" "Al-Wahid" "Al-Yaman" "Al-Bakr" "Al-Rahman" "Al-Furat" "Al-Isra" "Al-Nasr" "Al-Khalil" "Al-Hayat" "Al-Fajr" "Al-Azhar" "Al-Maali" "Al-Din" "Al-Amin" "Al-Saif" "Al-Madina" "Al-Takbir" "Al-Jaheem" "Al-Hoor" "Al-Nahar" "Al-Farouq" "Al-Jazeera" "Al-Arabi" "Al-Tayyib" "Al-Malik" "Al-Nasiri" "Al-Badi" "Al-Wajid" "Al-Wardi" "Al-Zahra" "Al-Khansa" "Al-Zahid" "Al-Dhakir" "Al-Asir" "Al-Qamar" "Al-Suhail" "Al-Murjan" "Al-Abyad" "Al-Ahmar" "Al-Azraq" "Al-Aswad" "Al-Yasmin" "Al-Rumman" "Al-Siddiq" "Al-Saadi" "Al-Najm" "Al-Burak" "Al-Zabib" "Al-Falah" "Al-Hilal" "Al-Baath" "Al-Jihad" "Al-Tahrir" "Al-Intisar" "Al-Rafiq" "Al-Nusayr" "Al-Halabi" "Al-Samsam" "Al-Zaytoon" "Al-Zafaran" "Al-Tamr" "Al-Kharroub" "Al-Kanadil" "Al-Salib" "Al-Sanbous" "Al-Bustan" "Al-Dumayr" "Al-Sumra" "Al-Tufah" "Al-Kumquat" "Al-Karam" "Al-Jauz" "Al-Safarjal" "Al-Razq" "Al-Banafsaj" "Al-Rihan" "Al-Yasmine" "Al-Abqari" 
	}
}

### LIGHT CRUISER NAMES###
SYR_CL_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CL

	for_countries = { SYR }

	type = ship
	ship_types = { cruiser }

	prefix = "SARS "
	fallback_name = "Light Cruiser %s"

	unique = {
		"Al-Qusayr" "Al-Misfirah" "Al-Mansourah" "Al-Houla" "Al-Hasya" "Al-Maama" "Al-Fukhari" "Al-Qunaytirah" "Al-Masht" "Al-Nisr" "Al-Daiyq" "Al-Quwaisib" "Al-Turayf" "Al-Tahira" "Al-Suhba" "Al-Muhajir" "Al-Raml" "Al-Bayda" "Al-Tahona" "Al-Rahwa" "Al-Sifah" 
	}
}

### LHA NAMES ###
SYR_LHA_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_HELICOPTER_CARRIERS

	for_countries = { SYR }

	type = ship
	ship_types = { helicopter_operator }

	prefix = "SARS "
	fallback_name = "LHA %s"

	unique = {
		"Al-Khiyarah" "Al-Tanf" "Al-Samsam" "Al-Dahr" "Al-Sanawbar" "Al-Amanah" "Al-Dalham" "Al-Sibaa" "Al-Shuhadah" "Al-Furat" "Al-Hud" "Al-Jazirah" "Al-Sakhra"
 	}
}

### SUBMARINES ###
SYR_SS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES

	for_countries = { SYR }

	type = ship
	ship_types = { attack_submarine missile_submarine }

	prefix = "SARS "
	fallback_name = "Submarine %s"

	unique = {
		"Al-Mutawakkil" "Al-Fatih" "Al-Mukhlis" "Al-Qudwah" "Al-Nazir" "Al-Hakeem" "Al-Haider" "Al-Azam" "Al-Mutayyib" "Al-Taliya" "Al-Tareeq" "Al-Khaleej" 
	}
}
