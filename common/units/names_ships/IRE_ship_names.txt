﻿
IRE_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { IRE }

	type = ship
	ship_types = { stealth_destroyer destroyer }

	prefix = "LÉ "
	fallback_name = "DDG-%d"

	unique = {
		"Seabreeze" "Emerald" "Leprechaun" "Shamrock" "Harmony" "Banshee" "Whisper" "Celtic" "Tir na nÓg" "Mistral" "Aurora" "Serenity" "Claddagh" "Tír Eoghain" "Leinster" "Connacht" "Munster" "Ulster" "Lough Derg" "Lough Neagh" "Lough Corrib" "Lough Mask" "Lough Erne" "Lough Ree" "Derryveagh" "Benbulbin" "Croagh Patrick"
	}
}

IRE_FRIGATES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATES
	for_countries = { IRE }
	type = ship
	ship_types = {
		frigate
	}
	prefix = "LÉ "
	fallback_name = "FFG-%d"

	unique = {
		"Muckish" "Errigal" "Slieve League" "Lough Tay" "Lough Melvin" "Lough Eske" "Lough Swilly" "Lough Allen" "Lough Key" "Lough Gill" "Lough Arrow" "Lough Carra" "Lough Conn" "Lough Currane" "Lough Gillaroo" "Lough Glore" "Lough Gur" "Lough Hyne" "Lough Ine" "Lough Kinale" "Lough Leane" "Lough Mask" "Lough Muck" "Lough Nafooey" "Lough Neagh" "Lough Oughter" "Lough Owel" "Lough Ramor" "Lough Ree" "Lough Rinn" "Lough Scannal" "Lough Sheelin" "Lough Sillan" "Lough Slievee" "Lough Strangford" "Lough Swilly" "Lough Talt" "Lough Tonnalee" "Lough Tor" "Lough Veagh" "Lough Walla" "Lough Mask" "Lough Melvin" "Lough Mohill"
	}
}

### CORVETTE NAMES ###
IRE_CORVETTE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTE

	for_countries = { IRE }
	prefix = "LÉ "
	type = ship
	ship_types = { corvette }
	fallback_name = "KKG-%d"

	unique = {
		"Lough Moneen" "Lough Muckanaghederdauhaulia" "Lough Mullauna" "Lough Murne" "Lough Nabrackboy" "Lough Nacung" "Lough Nadirkmore" "Lough Naganoo" "Lough Napanand" "Lough Naweelaun" "Lough Nellistown" "Lough Nenagh" "Lough Newhall" "Lough Ogrann" "Lough Olgetta" "Lough Oonagh" "Lough Oppyaleen" "Lough Oranboy" "Lough Orril" "Lough Oselmar" "Lough Osprey" "Lough Oughter" "Lough Ouughterard" "Lough Owen" "Lough Pallas" "Lough Partry" "Lough Peirce" "Lough Pollacalluna" "Lough Pollapeasta" "Lough Pollelenhaun" "Lough Pollglass" "Lough Polltully" "Lough Rahir" "Lough Rahaninny" "Lough Raloo" "Lough Randalstown" "Lough Randrew" "Lough Rane" "Lough Ranoo"
	}
}

IRE_SUBMARINES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES
	for_countries = { IRE }
	prefix = "LÉ "
	type = ship
	ship_types = {
		attack_submarine missile_submarine
	}
	fallback_name = "SS-%d"
}