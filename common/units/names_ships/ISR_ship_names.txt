##### ISRAEL NAME LISTS #####

### HELICOPTER OPERATOR NAMES ###
ISR_LHA_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_HELICOPTER_CARRIERS

	for_countries = { ISR }

	type = ship
		ship_types = { helicopter_operator }

	prefix = "INS "
	fallback_name = "Helicopter carrier-%d"

	unique = {
		"David Ben-Gurion" "Golda Meir" "Yitzhak Rabin" "Menachem Begin" "Shimon Peres" "Ariel Sharon" "Levi Eshkol" "Moshe Dayan" "Ehud Barak" "Yitzhak Shamir"
	}
}

### CRUISER NAMES###
ISR_CRUISERS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CRUISER

	for_countries = { ISR }

	type = ship
	ship_types = { battle_cruiser cruiser }

	prefix = "INS "
	fallback_name = "Cruiser-%d"

	unique = {
		"Chaim Weizmann" "Moshe Sharett" "Ezer Weizman" "Yigal Allon" "Abba Eban" "Ze'ev Jabotinsky" "Reuven Rivlin" "Moshe Katsav" "Yitzhak Navon" "Shaul Mofaz"
	}
}

### REGULAR DESTROYER NAMES###
ISR_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS

	for_countries = { ISR }

	type = ship
	ship_types = { stealth_destroyer destroyer }

	prefix = "INS "
	fallback_name = "Destroyer-%d"

	unique = {
		"Dan Halutz" "Amnon Lipkin-Shahak" "Yonatan Netanyahu" "Mordechai Gur" "Israel Tal" "Emmanuel Shaked" "Ehud Yatom" "Eliezer Shkedi" "Ron Arad" "Asher Levy"
	}
}

### FRIGATE NAMES###
ISR_FRIGATE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATE

	for_countries = { ISR }

	type = ship
	ship_types = { frigate }

	prefix = "INS "
	fallback_name = "Frigate-%d"

	unique = {
		"Rafael Eitan" "David Elazar" "Amos Lapidot" "Yitzhak Hofi" "Avigdor Kahalani" "David Ivry" "Meir Dagan" "Amiram Levin" "Ze'ev Almog" "Yossi Peled" "Yiftach Spector" "Ze'ev Raz" "Haim Bar-Lev" "Zvika Greengold" "Elazar Stern" "Giora Epstein" "Amos Yadlin" "Rehavam Zeevi" "Gabi Ashkenazi" "Matan Vilnai"
	}
}

### CORVETTE NAMES ###
ISR_CORVETTE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTE

	for_countries = { ISR }

	type = ship
	ship_types = { corvette }
	prefix = "INS "
	fallback_name = "Corvette-%d"

	unique = {
		"Ometz" "Emunah" "Tikvah" "Gevurah" "Tzedek" "Chesed" "Da'at" "Yashar" "Shalom" "Nitzachon" "Tiferet" "Chochmah" "Binah" "Hoda'ah" "Savlanut" "Ahavah" "Anavah" "Emun" "Ma'amin" "Shalem" "Kavod" "Netzach"
	}
}

### MISSILE SUBMARINES NAMES ###
ISR_MISSILE_SUBMARINE_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_MISSILE_SUBMARINES

	for_countries = { ISR }

	type = ship
	ship_types = { missile_submarine }

	prefix = "INS "
	
	fallback_name = "Tz-%d"
}

### ATTACK SUBMARINES ###
ISR_SS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES

	for_countries = { ISR }

	type = ship
	ship_types = { attack_submarine }

	prefix = "INS "
	fallback_name = "Tz-%d"
}
