BRM_DESTROYERS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS
	for_countries = { BRM }
	type = ship
	ship_types = {
		destroyer
	}
	prefix = "UMS "
	fallback_name = "DDG-%d"

	unique = {
		"Manawyadana" "Anawrahta" "Bayinnaung" "Narapati" "Kyanzittha" "Sawlu" "Dhammazedi" "Minye Kyawswa" "Hsinbyushin" "Bagyidaw"
		"Thalun" "Mindon" "Thibaw" "Nandabayin" "Mingyi Swa" "Shwenankyawshin" "Aung San" "Ne Win" "Tin Maung" "Yaza Thingyan"
		"Pagan Min" "Mingalar Min" "Pyin U Lwin" "Tay Za" "Thado Dhamma Yaza" "Yazathu" "Min Hla U" "Zaw Gyi" "Thee Lay Thee"
		"Kyansittha" "Thamoddarit" "Wethandaya" "Dhammika" "Kawliya" "Yuzana" "Sulamani" "Pagan" "U Bein" "Maha Bandula" "Thilawuntha"
		"Min Khaung" "Min Bya" "Min Phyu" "Min Pyan" "Min Yaman" "Min Nanda" "Min Htin" "Min Saw" "Min Phalaung" "Min Hti"
		"Manthiha" "Manthiriha" "Minlann" "Minye Kyawswar" "Minye Thihathu" "Minye Uzana" "Minye Kyawhtin" "Yay Le Kyawswa" "Yay Le Uzana" "Yay Le Kyawtin"
		"King Mindon" "King Pagan" "King Narathu" "King Alaungsithu" "King Anawrahta" "King Kyanzittha"
	}
}

BRM_FRIGATES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_FRIGATES
	for_countries = { BRM }
	type = ship
	ship_types = {
		frigate_1
		frigate
	}
	prefix = "UMS "
	fallback_name = "FFG-%d"

	unique = {
		"Tharrawaddy" "Mindon" "Thibaw" "Nandabayin" "Mingyi Swa" "Shwenankyawshin" "Aung San" "Ne Win" "Tin Maung" "Yaza Thingyan"
		"Mingalar Min" "Pyin U Lwin" "Tay Za" "Thado Dhamma Yaza" "Yazathu" "Min Hla U" "Zaw Gyi" "Thee Lay Thee" "Kyansittha" "Thamoddarit"
		"Manawyadana" "Anawrahta" "Bayinnaung" "Narapati" "Kyanzittha" "Sawlu" "Dhammazedi" "Minye Kyawswa" "Hsinbyushin" "Bagyidaw"
		"Thalun"
	}
}

BRM_CORVETTES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CORVETTES
	for_countries = { BRM }
	type = ship
	ship_types = {
		corvette_1
		corvette
	}
	prefix = "UMS "
	fallback_name = "KKG-%d"

	unique = {
		"Manawyadana" "Anawrahta" "Bayinnaung" "Narapati" "Kyanzittha" "Sawlu" "Dhammazedi" "Minye Kyawswa" "Hsinbyushin" "Bagyidaw"
		"Thalun" "Mindon" "Thibaw" "Nandabayin" "Mingyi Swa" "Shwenankyawshin" "Aung San" "Ne Win" "Tin Maung"
		"Yaza Thingyan" "Pagan Min" "Mingalar Min" "Pyin U Lwin" "Tay Za" "Thado Dhamma Yaza" "Yazathu" "Min Hla U" "Zaw Gyi" "Thee Lay Thee"
		"Kyansittha" "Thamoddarit" "Wethandaya" "Dhammika" "Kawliya" "Yuzana" "Sulamani" "Pagan" "U Bein" "Maha Bandula" "Thilawuntha"
		"Min Khaung" "Min Bya" "Min Phyu" "Min Pyan" "Min Yaman" "Min Nanda" "Min Htin" "Min Saw" "Min Phalaung" "Min Hti"
		"Manthiha" "Manthiriha" "Minlann" "Minye Kyawswar" "Minye Thihathu" "Minye Uzana" "Minye Kyawhtin" "Yay Le Kyawswa" "Yay Le Uzana" "Yay Le Kyawtin"
		"Yadanabon" "Nandawyun" "U Nu" "Than Shwe" "Htin Kyaw" "Win Myint" "Thein Sein" "Suu Kyi" "Soe Win" "Min Aung Hlaing"
		"Thinga" "Kyawswar" "Thihathu" "Uzana" "Kyawtin" "Le Kyawswa" "Le Uzana" "Le Kyawtin"
	}
}

BRM_SUBMARINES_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES
	for_countries = { BRM }
	type = ship
	ship_types = {
		attack_submarine missile_submarine
	}
	prefix = "UMS "
	fallback_name = "SS-%d"

	unique = {
		"Manawyadana" "Anawrahta" "Bayinnaung" "Narapati" "Kyanzittha" "Sawlu" "Dhammazedi" "Minye Kyawswa" "Hsinbyushin" "Bagyidaw"
		"Thalun" "Mindon" "Thibaw" "Nandabayin" "Mingyi Swa" "Shwenankyawshin" "Aung San" "Ne Win" "Tin Maung"
		"Yaza Thingyan" "Pagan Min" "Mingalar Min" "Pyin U Lwin" "Tay Za" "Thado Dhamma Yaza" "Yazathu" "Min Hla U" "Zaw Gyi" "Thee Lay Thee"
		"Kyansittha" "Thamoddarit" "Wethandaya" "Dhammika" "Kawliya" "Yuzana" "Sulamani" "Pagan" "U Bein" "Maha Bandula" "Thilawuntha"
		"Min Khaung" "Min Bya" "Min Phyu" "Min Pyan" "Min Yaman" "Min Nanda" "Min Htin" "Min Saw" "Min Phalaung" "Min Hti"
		"Manthiha" "Manthiriha" "Minlann" "Minye Kyawswar" "Minye Thihathu" "Minye Uzana" "Minye Kyawhtin" "Yay Le Kyawswa" "Yay Le Uzana" "Yay Le Kyawtin"
		"Yadanabon" "Nandawyun" "U Nu" "Than Shwe" "Htin Kyaw" "Win Myint" "Thein Sein" "Suu Kyi" "Soe Win" "Min Aung Hlaing"
		"Thinga" "Kyawswar" "Thihathu" "Uzana" "Kyawtin" "Le Kyawswa" "Le Uzana" "Le Kyawtin" "U Thu Kha" "U Razak" "Bo Let Ya" "Pandit Nehru"
	}
}

