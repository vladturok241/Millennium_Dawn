﻿

SOV_INF_01 =
{
	name = "Motostrelk. Brigada"

	for_countries = { SOV }



	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SOV_INF_01 }

	fallback_name = "%dya Motostrelk. Brigada"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		6 = { "%dya Gvds. 'Tatsinsk' Motostrelk. Brigada" }
		7 = { "%dya Gvds. 'Moscow-Minsk' Motostrelk. Brigada" }
		8 = { "%dya Gvds. 'Kramatorsk' Motostrelk. Brigada" }
		15 = { "%dya Otdel. Gvds. 'Berlinskaya' Motostrelk. Brigada" }
		17 = { "%dya Otdel. Gvds. Motostrelk. Brigada" }
		18 = { "%dya Otdel. Gvds. 'Yevpatoriya' Motostrelk. Brigada" }
		19 = { "%dya Otdel. 'Voronezh-Shumlinskaya' Motostrelk. Brigada" }
		20 = { "%dya Gvds. 'Prikarpat-Berlin' Motostrelk. Brigada" }
		21 = { "%dya Otdel. Gvds. 'Omsk-Novoburg' Motostrelk. Brigada" }
		23 = { "%dya Otdel. Gvds. 'Petrakuvskaya' Motostrelk. Brigada" }
		25 = { "%dya Otdel. Gvds. 'Sevastopol' Motostrelk. Brigada" }
		27 = { "%dya Otdel. Gvds. 'Sevastopol' Motostrelk. Brigada" }
		32 = { "%dya Motostrelk. Brigada" }
		35 = { "%dya Otdel. Gvds. Motostrelk. Brigada" }
		36 = { "%dya Otdel. Gvds. 'Lozovskaya' Motostrelk. Brigada" }
		37 = { "%dya Otdel. Gvds. 'Donskaya-Budapestskaya' Motostrelk. Brigada" }
		38 = { "%dya Otdel. Gvds. Motostrelk. Brigada" }
		57 = { "%dya Otdel. Gvds. Motostrelk. Brigada" }
		59 = { "%dya Otdel. Motostrelk. Brigada" }
		60 = { "%dya Otdel. Motostrelk. Brigada" }
		70 = { "%dya Otdel. Gvds. Motostrelk. Brigada" }
		74 = { "%dya Otdel. Gvds. Motostrelk. Brigada" }
		79 = { "%dya Gvds. Motostrelk. Brigada" }
		136 = { "%dya Gvds. 'Umansko-Berlinskaya' Motostrelk. Brigada" }
		138 = { "%dya Otdel. Gvds. 'Krasnosel' Motostrelk. Brigada" }
		200 = { "%dya Otdel. 'Pechenga' Motostrelk. Brigada" }
		82 = { "%dya Gvds. Motostrelk. Brigada" }
		131 = { "%dya Otdel. 'Krasnodar' Motostrelk. Brigada" }
		693 = { "%dya Gvds. Motostrelk. Brigada" }
		90 = { "%dya Gvds. Motostrelk. Brigada" }
		405 = { "%dya Gvds. Motostrelk. Brigada" }
		409 = { "%dya Gvds. Motostrelk. Brigada" }
		412 = { "%dya Gvds. Motostrelk. Brigada" }
		127 = { "%dya Gvds. Motostrelk. Brigada" }
		92 = { "%dya Gvds. Motostrelk. Brigada" }
		149 = { "%dya Gvds. Motostrelk. Brigada" }
		191 = { "%dya Gvds. Motostrelk. Brigada" }
		64 = { "%dya Otdel. Motostrelk. Brigada" }
		80 = { "%dya Otdel. Motostrelk. Brigada (Arkticheskaya)" }
		39 = { "%dya Otdel. Motostrelk. Brigada" }
		205 = { "%dya Otdel. Motostrelk. Brigada" }
		76 = { "%dya Motostrelk. Brigada" }
		73 = { "%dya Motostrelk. Brigada" }
		28 = { "%dya Otdel. 'Simferopol' Motostrelk. Brigada" }
	}
}

SOV_INF_02 =
{
	name = "Motostrelk. Regiment"

	for_countries = { SOV }



	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat"  }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SOV_INF_01 }

	fallback_name = "%dya Motostrelk. Polk"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dya Gvds. 'Sevastopolsky' Motostrelk. Polk" }
		228 = { "%dya Gvds. 'Sevastopolsky' Motostrelk. Polk" }
		283 = { "%dya Gvds. 'Berlinsky' Motostrelk. Polk" }
		311 = { "%dya Gvds. Motostrelk. Polk" }
		423 = { "%dya Gvds. 'Yampolsky' Motostrelk. Polk" }
		245 = { "%dya Gvds. 'Gniezno' Motostrelk. Polk" }
		252 = { "%dya Gvds. 'Stalingrad-Korsun' Motostrelk. Polk" }
		752 = { "%dya Motostrelk. Polk" }
		102 = { "%dya Motostrelk. Polk" }
		103 = { "%dya Motostrelk. Polk" }
		92 = { "%dya Motostrelk. Polk" }
		149 = { "%dya Gvds. Motostrelk. Polk" }
		191 = { "%dya Motostrelk. Polk" }
	}
}




SOV_ARM_01 =
{
	name = "Armored Brigada"

	for_countries = { SOV }



	division_types = { "armor_Bat" "Arm_Inf_Bat"  }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SOV_INF_01 }

	fallback_name = "%dya Gvds. Tankovaya Brigada"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dya Otdel. Gvds. 'Ural-Lvovsky' Tankovaya Brigada" }
		5 = { "%dya Gvds. 'Tatsinskaya' Tankovaya Brigada" }
	}
}

SOV_ARM_02 =
{
	name = "Tankovaya Regiment"

	for_countries = { SOV }



	division_types = { "armor_Bat" "Arm_Inf_Bat" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SOV_INF_01 }

	fallback_name = "%dya Gvds. Tankovaya Polk"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dya Gvds. 'Chertkov' Tankovaya Polk" }
		12 = { "%dya Gvds. Tankovaya Polk" }
		13 = { "%dya Gvds. Tankovaya Polk" }
		14 = { "%dya Gvds. Tankovaya Polk" }
		15 = { "%dya Gvds. Tankovaya Polk" }
		16 = { "%dya Gvds. 'Lvovskaya' Tankovaya Polk" }
		80 = { "%dya Gvds. Tankovaya Polk" }
		140 = { "%dya Gvds. Tankovaya Polk" }
		160 = { "%dya Gvds. Tankovaya Polk" }
		237 = { "%dya Gvds. Tankovaya Polk" }
		239 = { "%dya Gvds. Tankovaya Polk" }
	}
}

SOV_PAR_01 =
{
	name = "Airborne Units"

	for_countries = { SOV }



	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" "Mech_Air_Inf_Bat" "Arm_Air_Inf_Bat" "L_Air_assault_Bat" "Arm_Air_assault_Bat"  }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SOV_PAR_01 }

	fallback_name = "%dya Gvds. Desantno-Shturmovaya Brigada"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		217 = { "%dya Gvds. Parashyutno-Desantnyy Polk" }
		331 = { "%dya Gvds. Parashyutno-Desantnyy Polk" }
		51 = { "%dya Gvds. Parashyutno-Desantnyy Polk" }
		137 = { "%dya Gvds. Parashyutno-Desantnyy Polk" }
		108 = { "%dya Gvds. 'Kuban' Desantno-Shturmovaya Polk" }
		247 = { "%dya Gvds. 'Kavkaz' Desantno-Shturmovaya Polk" }
		23 = { "%dya Gvds. Desantno-Shturmovaya Polk" }
		104 = { "%dya Gvds. Desantno-Shturmovaya Polk" }
		234 = { "%dya Gvds. Desantno-Shturmovaya Polk" }
		11 = { "%dya Gvds. Desantno-Shturmovaya Brigada" }
		31 = { "%dya Gvds. Desantno-Shturmovaya Brigada" }
		56 = { "%dya Gvds. Desantno-Shturmovaya Brigada" }
		83 = { "%dya Gvds. Desantno-Shturmovaya Brigada" }

	}
}
SOV_MAR_01 =
{
	name = "Morskoi Pekhoty"

	for_countries = { SOV }



	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SOV_INF_01 }

	fallback_name = "%dya Morskoi Pekhoty"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		155 = { "%dya Morskoi Pekhoty" }
		3 = { "%dya Gvds. 'Krasnodar-Harbin' Morskoi Pekhoty" }
		336 = { "%dya Gvds. 'Bialystok' Morskoi Pekhoty" }
		61 = { "%dya 'Kirkines' Morskoi Pekhoty" }
		810 = { "%dya Morskoi Pekhoty" }
	}
}

SOV_MNT_01 =
{
	name = "Gorno. Motostrelk. Brigada"

	for_countries = { SOV }



	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SOV_INF_01 }

	fallback_name = "%dya Gvds. Gorno. Motostrelk. Brigada"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		8 = { "%dya Gvds. 'Chertkovsky' Gorno. Motostrelk. Brigada" }
		55 = { "%dya Gvds. Gorno. Motostrelk. Brigada" }
		33 = { "%dya Gvds. Gorno. Motostrelk. Brigada" }
		34 = { "%dya Gvds. Gorno. Motostrelk. Brigada" }
	}
}
SOV_REC_01 =
{
	name = "Reconnaissance Brigada"

	for_countries = { SOV }



	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SOV_INF_01 }

	fallback_name = "%dya Otdel. Razvedyvatelnaya Brigada"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		100 = { "%dya Otdel. Razvedyvatelnaya Brigada" }
		127 = { "%dya Otdel. Razvedyvatelnaya Brigada" }
	}
}


SOV_SOF_01 =
{
	name = "Special Operations Forces"

	for_countries = { SOV }



	division_types = { "Special_Forces" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { SOV_SOF_01 }

	fallback_name = "%dya Spetsnaz Brigada"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		45 = { "%dya Gvds. Otdel. Spetsnaz Brigada" }
		604 = { "%dya Spetsnaz Centr 'Dzerzhinsky'" }
		900 = { "Spetsnaz Centr 'Senezh'" }
		901 = { "Spetsnaz Centr 'Kubinka-2'" }
		2 = { "%dya Spetsnaz Brigada" }
		3 = { "%dya Gvds. 'Varshavsko-Berlinskaya' Spetsnaz Brigada" }
		10 = { "%dya Spetsnaz Brigada" }
		14 = { "%dya Spetsnaz Brigada" }
		16 = { "%dya Spetsnaz Brigada" }
		22 = { "%dya Gvds. Spetsnaz Brigada" }
		24 = { "%dya Spetsnaz Brigada" }
		346 = { "%dya Spetsnaz Brigada" }
		25 = { "%dya Spetsnaz Polk" }
	}
}


SOV_BRI_01 =
{
	name = "USSR Motor Brigade"

	for_countries = { SOV }

	can_use = { 
		OR = {
			emerging_communist_state_are_in_power = yes
			neutrality_neutral_communism_are_in_power = yes
		}
		has_completed_focus = SOV_communist_party_dominance
	}

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%dya Motostrelk. Brigada Kadra"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dya Otdel. 'Order of Red Star' Strelk. Brigada" }
		2 = { "%dya Gvds. 'Brest' Inzhenerno-Sapernaya Brigada" }
		3 = { "%dya Dorozhno-Komendantskaya Brigada" }
		4 = { "%dya Zheleznodorozhnaya Brigada" }
		5 = { "%dya 'Poznan' Zheleznodorozhnaya Brigada" }
		6 = { "%dya Otdel. Gvds. 'Berlinskaya' Motostrelk. Brigada" }
		7 = { "%dya Otdel. Motostrelk. Brigada" }
		8 = { "%dya Zheleznodorozhnaya Brigada" }
		9 = { "%dya Dorozhno-Komendantskaya Brigada" }
		10 = { "%dya Inzhenerno-Sapernaya Brigada" }
		12 = { "%dya Zheleznodorozhnaya Brigada" }
		13 = { "%dya Zheleznodorozhnaya Brigada" }
		14 = { "%dya Zheleznodorozhnaya Brigada" }
		15 = { "%dya Dorozhno-Komendantskaya Brigada" }
		16 = { "%dya Dorozhno-Komendantskaya Brigada" }
		17 = { "%dya 'MNR' Zheleznodorozhnaya Brigada" }
		18 = { "%dya Zheleznodorozhnaya Brigada" }
		19 = { "%dya Dorozhno-Komendantskaya Brigada" }
		20 = { "%dya Zheleznodorozhnaya Brigada" }
		21 = { "%dya Zheleznodorozhnaya Brigada" }
		22 = { "%dya Zheleznodorozhnaya Brigada" }
		23 = { "%dya Zheleznodorozhnaya Brigada" }
		24 = { "%dya 'Warsaw' Zheleznodorozhnaya Brigada" }
		25 = { "%dya Zheleznodorozhnaya Brigada" }
		26 = { "%dya 'Kaliningrad' Zheleznodorozhnaya Brigada" }
		27 = { "%dya Otdel. Gvds. 'Sevastopolskaya' Motostrelk. Brigada" }
		28 = { "%dya Zheleznodorozhnaya Brigada" }
		29 = { "%dya Zheleznodorozhnaya Brigada" }
		30 = { "%dya Zheleznodorozhnaya Brigada" }
		31 = { "%dya Zheleznodorozhnaya Brigada" }
		32 = { "%dya Zheleznodorozhnaya Brigada" }
		33 = { "%dya Zheleznodorozhnaya Brigada" }
		34 = { "%dya Zheleznodorozhnaya Brigada" }
		35 = { "%dya Zheleznodorozhnaya Brigada" }
		36 = { "%dya Dorozhno-Mostovaya Brigada" }
		37 = { "%dya Zheleznodorozhnaya Brigada" }
		38 = { "%dya Zheleznodorozhnaya Brigada" }
		39 = { "%dya Zheleznodorozhnaya Brigada" }
		40 = { "%dya Zheleznodorozhnaya Brigada" }
		42 = { "%dya Zheleznodorozhnaya Brigada" }
		43 = { "%dya Zheleznodorozhnaya Brigada" }
		44 = { "%dya Zheleznodorozhnaya Brigada" }
		45 = { "%dya Zheleznodorozhnaya Brigada" }
		46 = { "%dya Zheleznodorozhnaya Brigada" }
		47 = { "%dya Zheleznodorozhnaya Brigada" }
		48 = { "%dya Zheleznodorozhnaya Brigada" }
		50 = { "%dya Zheleznodorozhnaya Brigada" }
		68 = { "%dya Otdel. Gornaya Motostrelk. Brigada" }
		71 = { "%dya Inzhenerno-Sapernaya Brigada" }
		93 = { "%dya Uchebnaya 'Pskov' Inzhenerno-Sapernaya Brigada" }
		106 = { "%dya Uchebnaya Inzhenerno-Sapernaya Brigada" }
		111 = { "%dya Inzhenerno-Sapernaya Brigada" }
		114 = { "%dya Inzhenerno-Sapernaya Brigada" }
		116 = { "%dya Inzhenerno-Sapernaya Brigada" }
		119 = { "%dya Inzhenerno-Sapernaya Brigada" }
		129 = { "%dya Dorozhno-Komendantskaya Brigada" }
		151 = { "%dya Pontonno-Mostovaya Brigada" }
		188 = { "%dya Gvds. 'Novgorod' Inzhenerno-Sapernaya Brigada" }
		190 = { "%dya Pontonno-Mostovaya Brigada" }
		201 = { "%dya 'Dvinsk' Inzhenerno-Sapernaya Brigada" }
		208 = { "%dya 'Fokshany' Inzhenerno-Sapernaya Brigada" }
		230 = { "%dya Inzhenerno-Sapernaya Brigada" }
		237 = { "%dya Inzhenerno-Sapernaya Brigada" }
		263 = { "%dya Dorozhno-Komendantskaya Brigada" }
		273 = { "%dya Brigada Inzhenernyh Zagrazhdeniy" }
		278 = { "%dya Dorozhno-Komendantskaya Brigada" }
		313 = { "%dya Gvds. 'Berlin' Inzhenerno-Sapernaya Brigada" }
		315 = { "%dya 'Uman' Inzhenerno-Sapernaya Brigada" }
	}
}

SOV_BRI_02 =
{
	name = "USSR Art. Brigade"

	for_countries = { SOV }

	can_use = { 
		OR = {
			emerging_communist_state_are_in_power = yes
			neutrality_neutral_communism_are_in_power = yes
		}
		has_completed_focus = SOV_communist_party_dominance
	}

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%dya Art. Brigada Kadra"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		13 = { "%dya Art. Brigada Bolsh. Mosch." }
		14 = { "%dya Art. Brigada Bolsh. Mosch." }
		45 = { "%dya Art. Brigada" }
		122 = { "%dya Protivotank. Art. Brigada" }
		135 = { "%dya Protivotank. Art. Brigada" }
		165 = { "%dya 'Praga' Pushechnaya Art. Brigada" }
		166 = { "%dya Pushechnaya Art. Brigada" }
		167 = { "%dya Pushechnaya Art. Brigada" }
		170 = { "%dya Gaubichnaya Art. Brigada" }
		171 = { "%dya Gaubichnaya Art. Brigada" }
		178 = { "%dya Pushechnaya Art. Brigada" }
		182 = { "%dya Protivotank. Art. Brigada" }
		184 = { "%dya 'Kutuzov-Suvorov' Gaubichnaya Art. Brigada" }
		188 = { "%dya Art. Brigada Bolsh. Mosch." }
		190 = { "%dya Art. Brigada Bolsh. Mosch." }
		192 = { "%dya 'Yasskaya' Gaubichnaya Art. Brigada" }
		197 = { "%dya Art. Brigada Bolsh. Mosch." }
		200 = { "%dya Art. Brigada Bolsh. Mosch." }
		201 = { "%dya Samohodn. Art. Brigada" }
		209 = { "%dya Pushechnaya Art. Brigada" }
		211 = { "%dya Gvds. 'Sandomir' Pushechnaya Art. Brigada" }
		216 = { "%dya Pushechnaya Art. Brigada" }
		217 = { "%dya Pushechnaya Art. Brigada" }
		219 = { "%dya Gaubichnaya Art. Brigada" }
		222 = { "%dya Pushechnaya Art. Brigada" }
		227 = { "%dya Art. Brigada Bolsh. Mosch." }
		228 = { "%dya Art. Brigada Bolsh. Mosch." }
		229 = { "%dya Art. Brigada Bolsh. Mosch." }
		231 = { "%dya Pushechnaya Art. Brigada" }
		232 = { "%dya 'Praga' Samohodn. Art. Brigada" }
		235 = { "%dya Gvds. Pushechnaya Art. Brigada" }
		236 = { "%dya Pushechnaya Art. Brigada" }
		238 = { "%dya Gaubichnaya Art. Brigada" }
		239 = { "%dya Pushechnaya Art. Brigada" }
		251 = { "%dya Protivotank. Art. Brigada" }
		256 = { "%dya Art. Brigada" }
		264 = { "%dya Pushechnaya Art. Brigada" }
		265 = { "%dya Art. Brigada" }
		268 = { "%dya Gvds. 'Sevastopol' Pushechnaya Art. Brigada" }
		273 = { "%dya 'Kirkenes' Pushechnaya Art. Brigada" }
		279 = { "%dya Pushechnaya Art. Brigada" }
		285 = { "%dya Gaubichnaya Art. Brigada" }
		286 = { "%dya Gvds. 'Praga' Gaubichnaya Art. Brigada" }
		287 = { "%dya Gvds. 'Sevastopol' Gaubichnaya Art. Brigada" }
		288 = { "%dya 'Warsaw' Gaubichnaya Art. Brigada" }
		289 = { "%dya Art. Brigada Bolsh. Mosch." }
		290 = { "%dya 'Lodsz' Pushechnaya Art. Brigada" }
		300 = { "%dya Protivotank. Art. Brigada" }
		301 = { "%dya Pushechnaya Art. Brigada" }
		303 = { "%dya Gvds. 'Kalinkovichi' Gaubichnaya Art. Brigada" }
		304 = { "%dya Art. Brigada Bolsh. Mosch." }
		305 = { "%dya 'Gumbinnen' Gaubichnaya Art. Brigada" }
		306 = { "%dya Pushechnaya Art. Brigada" }
		307 = { "%dya 'Brandenburg' Samohodn. Art. Brigada" }
		308 = { "%dya 'Lodsz' Pushechnaya Art. Brigada" }
		336 = { "%dya Gvds. Samohodn. Art. Brigada" }
		337 = { "%dya Gvds. 'Kiev' Samohodn. Art. Brigada" }
		338 = { "%dya Gvds. 'Dvinsk' Samohodn. Art. Brigada" }
		345 = { "%dya Art. Brigada Bolsh. Mosch." }
		346 = { "%dya Pushechnaya Art. Brigada" }
		349 = { "%dya Protivotank. Art. Brigada" }
		351 = { "%dya Art. Brigada Bolsh. Mosch." }
		353 = { "%dya Gvds. 'Mogilev' Pushechnaya Art. Brigada" }
		380 = { "%dya Gvds. 'Krivoy Rog' Samohodn. Art. Brigada" }
		384 = { "%dya Art. Brigada Bolsh. Mosch." }
		385 = { "%dya Gvds. 'Odessa' Art. Brigada" }
		387 = { "%dya Gvds. 'Berlin' Pushechnaya Art. Brigada" }
		390 = { "%dya Gvds. 'Zaporozhye' Pushechnaya Art. Brigada" }
		400 = { "%dya Pushechnaya Art. Brigada" }
		401 = { "%dya Protivotank. Art. Brigada" }
		404 = { "%dya Art. Brigada" }
		430 = { "%dya Gaubichnaya Art. Brigada" }
		485 = { "%dya Pushechnaya Art. Brigada" }
		501 = { "%dya Protivotank. Art. Brigada" }
		502 = { "%dya Protivotank. Art. Brigada" }
		520 = { "%dya Pushechnaya Art. Brigada" }
		530 = { "%dya Pushechnaya Art. Brigada" }
	}
}

SOV_BRI_03 =
{
	name = "USSR AA Brigade"

	for_countries = { SOV }

	can_use = { 
		OR = {
			emerging_communist_state_are_in_power = yes
			neutrality_neutral_communism_are_in_power = yes
		}
		has_completed_focus = SOV_communist_party_dominance
	}

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%dya Zenitno-Raketnaya Brigada Kadra"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		2 = { "%dya 'Dresden' Zenitno-Raketnaya Brigada" }
		5 = { "%dya Zenitno-Raketnaya Brigada" }
		7 = { "%dya Zenitno-Raketnaya Brigada" }
		8 = { "%dya 'Shavlin' Zenitno-Raketnaya Brigada" }
		18 = { "%dya Zenitno-Raketnaya Brigada" }
		25 = { "%dya Zenitno-Raketnaya Brigada" }
		29 = { "%dya Zenitno-Raketnaya Brigada" }
		31 = { "%dya Zenitno-Raketnaya Brigada" }
		40 = { "%dya Radiotehnicheskaya Brigada" }
		43 = { "%dya Zenitno-Raketnaya Brigada" }
		45 = { "%dya Radiotehnicheskaya Brigada" }
		46 = { "%dya Zenitno-Raketnaya Brigada" }
		49 = { "%dya Zenitno-Raketnaya Brigada" }
		53 = { "%dya Zenitno-Raketnaya Brigada" }
		55 = { "%dya Zenitno-Raketnaya Brigada" }
		56 = { "%dya Zenitno-Raketnaya Brigada" }
		57 = { "%dya Radiotehnicheskaya Brigada" }
		59 = { "%dya Zenitno-Raketnaya Brigada" }
		61 = { "%dya Zenitno-Raketnaya Brigada" }
		62 = { "%dya Zenitno-Raketnaya Brigada" }
		67 = { "%dya Zenitno-Raketnaya Brigada" }
		69 = { "%dya Radiotehnicheskaya Brigada" }
		70 = { "%dya Zenitno-Raketnaya Brigada" }
		71 = { "%dya Zenitno-Raketnaya Brigada" }
		72 = { "%dya Radiotehnicheskaya Brigada" }
		73 = { "%dya Radiotehnicheskaya Brigada" }
		74 = { "%dya Radiotehnicheskaya Brigada" }
		75 = { "%dya Radiotehnicheskaya Brigada" }
		76 = { "%dya Radiotehnicheskaya Brigada" }
		77 = { "%dya Radiotehnicheskaya Brigada" }
		101 = { "%dya Radiotehnicheskaya Brigada" }
		102 = { "%dya Zenitno-Raketnaya Brigada" }
		108 = { "%dya 'Zaporozhye' Zenitno-Raketnaya Brigada" }
		117 = { "%dya Zenitno-Raketnaya Brigada" }
		120 = { "%dya 'Yaroslavl' Zenitno-Raketnaya Brigada" }
		133 = { "%dya Gvds.'Dvinsk' Zenitno-Raketnaya Brigada" }
		137 = { "%dya Zenitno-Raketnaya Brigada" }
		138 = { "%dya 'Gdansk' Zenitno-Raketnaya Brigada" }
		140 = { "%dya 'Borisov' Zenitno-Raketnaya Brigada" }
		141 = { "%dya Zenitno-Raketnaya Brigada" }
		147 = { "%dya Zenitno-Raketnaya Brigada" }
		151 = { "%dya Zenitno-Raketnaya Brigada" }
		156 = { "%dya Zenitno-Raketnaya Brigada" }
		157 = { "%dya Zenitno-Raketnaya Brigada" }
		163 = { "%dya Zenitno-Raketnaya Brigada" }
		179 = { "%dya Zenitno-Raketnaya Brigada" }
		180 = { "%dya 'Orsha' Zenitno-Raketnaya Brigada" }
		200 = { "%dya Zenitno-Raketnaya Brigada" }
		202 = { "%dya Zenitno-Raketnaya Brigada" }
		203 = { "%dya Zenitno-Raketnaya Brigada" }
		220 = { "%dya Zenitno-Raketnaya Brigada" }
		223 = { "%dya Zenitno-Raketnaya Brigada" }
		240 = { "%dya Zenitno-Raketnaya Brigada" }
		252 = { "%dya Zenitno-Raketnaya Brigada" }
		269 = { "%dya Zenitno-Raketnaya Brigada" }
		271 = { "%dya Gvds.'Dvinsk' Zenitno-Raketnaya Brigada" }
		272 = { "%dya Gvds.'Rogachev' Zenitno-Raketnaya Brigada" }
		273 = { "%dya Zenitno-Raketnaya Brigada" }
		295 = { "%dya Zenitno-Raketnaya Brigada" }
		296 = { "%dya Zenitno-Raketnaya Brigada" }
		297 = { "%dya Zenitno-Raketnaya Brigada" }
		381 = { "%dya Zenitno-Raketnaya Brigada" }
		447 = { "%dya Zenitno-Raketnaya Brigada" }
	}
}

SOV_BRI_04 =
{
	name = "USSR Air Assault Brigade"

	for_countries = { SOV }

	can_use = { 
		OR = {
			emerging_communist_state_are_in_power = yes
			neutrality_neutral_communism_are_in_power = yes
		}
		has_completed_focus = SOV_communist_party_dominance
	}

	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" "Mech_Air_Inf_Bat" "Arm_Air_Inf_Bat" "L_Air_assault_Bat" "Arm_Air_assault_Bat"  }

	fallback_name = "%dya Desantno-Shturmovaya Brigada"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		7 = { "%dya Gvds. Desantno-Shturmovaya Brigada" }
		11 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		13 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		21 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		23 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		35 = { "%dya Gvds. Otdel. Desantno-Shturmovaya Brigada" }
		36 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		37 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		38 = { "%dya Gvds. 'Vena' Otdel. Desantno-Shturmovaya Brigada" }
		39 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		40 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		44 = { "%dya Gvds. Uchebnaya Desantno-Shturmovaya Brigada" }
		56 = { "%dya Gvds. Otdel. Desantno-Shturmovaya Brigada" }
		57 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		76 = { "%dya Gvds. 'Chernigov' Desantno-Shturmovaya Brigada" }
		83 = { "%dya Otdel. Desantno-Shturmovaya Brigada" }
		98 = { "%dya Gvds. 'Svirsk' Desantno-Shturmovaya Brigada" }
		103 = { "%dya Gvds. '60 let SSSR' Desantno-Shturmovaya Brigada" }
		104 = { "%dya Gvds. Desantno-Shturmovaya Brigada" }
		105 = { "%dya Gvds. 'Vena' Desantno-Shturmovaya Brigada" }
		106 = { "%dya Gvds. Desantno-Shturmovaya Brigada" }
	}
}

SOV_BRI_05 =
{
	name = "USSR Spetsnaz Brigade"

	for_countries = { SOV }

	can_use = { 
		OR = {
			emerging_communist_state_are_in_power = yes
			neutrality_neutral_communism_are_in_power = yes
		}
		has_completed_focus = SOV_communist_party_dominance
	}

	division_types = { "Special_Forces" }

	fallback_name = "%dya Brigada Spetsnaza GRU"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		2 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		3 = { "%dya Otdel. Gvds. 'Berlin' Brigada Spetsnaza GRU" }
		4 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		5 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		8 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		9 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		10 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		12 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		14 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		15 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		16 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		17 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		20 = { "%dya Otdel. Razvedyvatelnaya Brigada GRU" }
		22 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		24 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		25 = { "%dya Otdel. Razvedyvatelnaya Brigada GRU" }
		67 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		126 = { "%dya Otdel. Brigada Spetsnaza GRU" }
		901 = { "Centr Spetsnaza GRU 'Kubinka-1'" }
		902 = { "Centr Spetsnaza GRU 'Kubinka-2'" }
		903 = { "Centr Spetsnaza GRU 'Kubinka-3'" }
		904 = { "Centr Spetsnaza GRU 'Lenin'" }
		905 = { "Centr Spetsnaza GRU 'Ural'" }
		906 = { "Centr Spetsnaza GRU 'Amur'" }
		907 = { "Centr Spetsnaza GRU 'Dzerzhinsky'" }
		908 = { "Centr Spetsnaza GRU 'Senezh'" }
		909 = { "Centr Spetsnaza GRU 'Volga'" }
		910 = { "Centr Spetsnaza GRU 'Planeta'" }
		911 = { "Centr Spetsnaza GRU 'Albatros'" }
		912 = { "Centr Spetsnaza GRU 'Voin'" }
		913 = { "Osobaya Gruppa GRU 'Sever'" }
	}
}

SOV_BRI_06 =
{
	name = "USSR Marine Brigade"

	for_countries = { SOV }

	can_use = { 
		OR = {
			emerging_communist_state_are_in_power = yes
			neutrality_neutral_communism_are_in_power = yes
		}
		has_completed_focus = SOV_communist_party_dominance
	}

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	fallback_name = "%dya Brigada Morskoy Pekhoty"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dya Osobaya 'Leningradskaya' Brigada Morskoy Pekhoty" }
		3 = { "%dya 'Krasnodar-Harbin' Brigada Morskoy Pekhoty" }
		7 = { "%dya 'Baltiyskaya' Brigada Morskoy Pekhoty" }
		14 = { "%dya Brigada Morskoy Pekhoty" }
		61 = { "%dya 'Kirkenes' Otdel. Brigada Morskoy Pekhoty" }
		55 = { "%dya Brigada Morskoy Pekhoty" }
		155 = { "%dya Brigada Morskoy Pekhoty" }
		175 = { "%dya Otdel. Brigada Morskoy Pekhoty" }
		336 = { "%dya Gvds. 'Belostok' Otdel. Brigada Morskoy Pekhoty" }
		810 = { "%dya '60 let SSSR' Otdel. Brigada Morskoy Pekhoty" }
	}
}

SOV_DIV_01 =
{
	name = "USSR Motor Division"

	for_countries = { SOV }

	can_use = { 
		OR = {
			emerging_communist_state_are_in_power = yes
			neutrality_neutral_communism_are_in_power = yes
		}
		has_completed_focus = SOV_communist_party_dominance
	}

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%dya Motostrelk. Diviziya"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dya Gvds. 'Moskva-Minsk' Motostrelk. Diviziya" }
		2 = { "%dya Gvds. 'Tamanskaya' Motostrelk. Diviziya" }
		3 = { "%dya Gvds. 'Volnovaha' Motostrelk. Diviziya" }
		4 = { "%dya Gvds. 'Stalingrad' Motostrelk. Diviziya" }
		5 = { "%dya Gvds. '60 let SSSR' Motostrelk. Diviziya" }
		6 = { "%dya Gvds. 'Vitebsk-Novgorod' Motostrelk. Diviziya" }
		7 = { "%dya Gvds. 'Rezhick' Motostrelk. Diviziya" }
		8 = { "%dya Gvds. 'Panfilov' Motostrelk. Diviziya" }
		9 = { "%dya 'Krasnodar' Motostrelk. Diviziya" }
		10 = { "%dya Gvds. 'Pechenga' Motostrelk. Diviziya" }
		11 = { "%dya Gvds. 'Kuzbass' Motostrelk. Diviziya" }
		12 = { "%dya Motostrelk. Diviziya" }
		13 = { "%dya Motostrelk. Diviziya" }
		14 = { "%dya Gvds. 'Pechenga' Motostrelk. Diviziya" }
		15 = { "%dya Gvds. 'Stettin' Motostrelk. Diviziya" }
		16 = { "%dya 'Klaipeda' Motostrelk. Diviziya" }
		17 = { "%dya Gvds. 'Dunai' Motostrelk. Diviziya" }
		18 = { "%dya Gvds. 'Instenburg' Motostrelk. Diviziya" }
		19 = { "%dya 'Voronezh' Motostrelk. Diviziya" }
		20 = { "%dya Gvds. 'Berlin' Motostrelk. Diviziya" }
		21 = { "%dya 'Taganrog' Motostrelk. Diviziya" }
		22 = { "%dya 'Krasnodar-Harbin' Motostrelk. Diviziya" }
		23 = { "%dya Gvds. 'Brandenburg' Motostrelk. Diviziya" }
		24 = { "%dya 'Ulyanovsk' Motostrelk. Diviziya" }
		25 = { "%dya Gvds. 'Budapest' Motostrelk. Diviziya" }
		26 = { "%dya Gvds. 'Sibirskaya' Motostrelk. Diviziya" }
		27 = { "%dya Gvds. 'Omsk' Motostrelk. Diviziya" }
		28 = { "%dya Gvds. 'Harkov' Motostrelk. Diviziya" }
		29 = { "%dya 'Polotsk' Motostrelk. Diviziya" }
		30 = { "%dya Gvds. 'Irkutsk' Motostrelk. Diviziya" }
		31 = { "%dya Gvds. 'Vitebsk' Motostrelk. Diviziya" }
		32 = { "%dya Gvds. 'Tamanskaya' Motostrelk. Diviziya" }
		33 = { "%dya Motostrelk. Diviziya" }
		34 = { "%dya 'Simferopol' Motostrelk. Diviziya" }
		35 = { "%dya 'Krasnograd' Motostrelk. Diviziya" }
		36 = { "%dya Motostrelk. Diviziya" }
		37 = { "%dya Gvds. 'Rechitsk' Motostrelk. Diviziya" }
		38 = { "%dya Gvds. 'Lozov' Motostrelk. Diviziya" }
		39 = { "%dya Gvds. 'Barvenkov' Motostrelk. Diviziya" }
		40 = { "%dya 'Ordzhonikidze' Motostrelk. Diviziya" }
		41 = { "%dya Motostrelk. Diviziya" }
		42 = { "%dya Gvds. Uchebnaya Motostrelk. Diviziya" }
		43 = { "%dya Uchebnaya 'Tartur' Motostrelk. Diviziya" }
		44 = { "%dya 'Chudovskaya' Motostrelk. Diviziya" }
		45 = { "%dya Gvds. 'Kransnoselskaya' Motostrelk. Diviziya" }
		46 = { "%dya Motostrelk. Diviziya" }
		47 = { "%dya Motostrelk. Diviziya" }
		48 = { "%dya 'Kalinin' Motostrelk. Diviziya" }
		49 = { "%dya 'Herson' Motostrelk. Diviziya" }
		50 = { "%dya Gvds. 'Donetsk' Motostrelk. Diviziya" }
		51 = { "%dya Gvds. 'Perekop' Motostrelk. Diviziya" }
		52 = { "%dya 'Melitopol' Motostrelk. Diviziya" }
		53 = { "%dya 'Engels' Motostrelk. Diviziya" }
		54 = { "%dya Motostrelk. Diviziya" }
		55 = { "%dya 'Molot' Motostrelk. Diviziya" }
		56 = { "%dya Uchebnaya 'Pushkin' Motostrelk. Diviziya" }
		57 = { "%dya Gvds. 'Novobug' Motostrelk. Diviziya" }
		58 = { "%dya 'Roslavl' Motostrelk. Diviziya" }
		59 = { "%dya Gvds. 'Kramatorsk' Motostrelk. Diviziya" }
		60 = { "%dya 'Tolbuhin' Motostrelk. Diviziya" }
		61 = { "%dya Uchebnaya Motostrelk. Diviziya" }
		62 = { "%dya Motostrelk. Diviziya" }
		63 = { "%dya Gvds. Uchebnaya Motostrelk. Diviziya" }
		64 = { "%dya Gvds. 'Krasnoselskaya' Motostrelk. Diviziya" }
		65 = { "%dya 'Rechitsk' Motostrelk. Diviziya" }
		66 = { "%dya Uchebnaya 'Poltava' Motostrelk. Diviziya" }
		67 = { "%dya Motostrelk. Diviziya" }
		68 = { "%dya 'Novgorod' Motostrelk. Diviziya" }
		69 = { "%dya 'Sevsk' Motostrelk. Diviziya" }
		70 = { "%dya Gvds. 'Gluhovsk' Motostrelk. Diviziya" }
		71 = { "%dya Motostrelk. Diviziya" }
		72 = { "%dya Gvds. 'Krasnograd' Motostrelk. Diviziya" }
		73 = { "%dya 'Novozybkov' Motostrelk. Diviziya" }
		74 = { "%dya 'Temruk' Motostrelk. Diviziya" }
		75 = { "%dya 'Lev Rohlin' Motostrelk. Diviziya" }
		76 = { "%dya 'Warsaw' Motostrelk. Diviziya" }
		77 = { "%dya Gvds. 'Moskva' Motostrelk. Diviziya" }
		78 = { "%dya Uchebnaya 'Sivash' Motostrelk. Diviziya" }
		79 = { "%dya 'Sakhalin' Motostrelk. Diviziya" }
		80 = { "%dya Gvds. 'Uman' Motostrelk. Diviziya" }
		81 = { "%dya Gvds. 'Krasnodar' Motostrelk. Diviziya" }
		82 = { "%dya Motostrelk. Diviziya" }
		83 = { "%dya Gvds. 'Orenburg' Motostrelk. Diviziya" }
		84 = { "%dya 'Tulskaya' Motostrelk. Diviziya" }
		85 = { "%dya 'Leningrad' Motostrelk. Diviziya" }
		86 = { "%dya Gvds. 'Nikolaevsk' Motostrelk. Diviziya" }
		87 = { "%dya Motostrelk. Diviziya" }
		88 = { "%dya Motostrelk. Diviziya" }
		89 = { "%dya 'Tamanskaya' Motostrelk. Diviziya" }
		90 = { "%dya 'Stralsund' Motostrelk. Diviziya" }
		91 = { "%dya Motostrelk. Diviziya" }
		92 = { "%dya Uchebnaya 'Krivoy Rog' Motostrelk. Diviziya" }
		93 = { "%dya Gvds. 'Harkov' Motostrelk. Diviziya" }
		94 = { "%dya Gvds. 'Zvenigorod' Motostrelk. Diviziya" }
		96 = { "%dya 'Gomel' Motostrelk. Diviziya" }
		97 = { "%dya Gvds. 'Poltava' Motostrelk. Diviziya" }
		99 = { "%dya Motostrelk. Diviziya" }
		100 = { "%dya Gvds. 'Vena' Motostrelk. Diviziya" }
		101 = { "%dya Uchebnaya Motostrelk. Diviziya" }
		107 = { "%dya Motostrelk. Diviziya" }
		108 = { "%dya 'Nevel' Motostrelk. Diviziya" }
		111 = { "%dya Motostrelk. Diviziya" }
		114 = { "%dya Motostrelk. Diviziya" }
		118 = { "%dya Motostrelk. Diviziya" }
		120 = { "%dya Gvds. 'Rogachev' Motostrelk. Diviziya" }
		121 = { "%dya Uchebnaya Motostrelk. Diviziya" }
		122 = { "%dya Gvds. 'Stalingrad' Motostrelk. Diviziya" }
		123 = { "%dya Gvds. 'Khingan' Motostrelk. Diviziya" }
		126 = { "%dya 'Gorlovka' Motostrelk. Diviziya" }
		127 = { "%dya Motostrelk. Diviziya" }
		128 = { "%dya Gvds. 'Turkestan' Motostrelk. Diviziya" }
		129 = { "%dya Uchebnaya 'Tihookeanskaya' Motostrelk. Diviziya" }
		131 = { "%dya 'Pechenga' Motostrelk. Diviziya" }
		134 = { "%dya Motostrelk. Diviziya" }
		135 = { "%dya Motostrelk. Diviziya" }
		144 = { "%dya Gvds. 'Yelnya' Motostrelk. Diviziya" }
		145 = { "%dya Motostrelk. Diviziya" }
		147 = { "%dya Motostrelk. Diviziya" }
		150 = { "%dya Uchebnaya Motostrelk. Diviziya" }
		152 = { "%dya Motostrelk. Diviziya" }
		155 = { "%dya Motostrelk. Diviziya" }
		157 = { "%dya Motostrelk. Diviziya" }
		161 = { "%dya 'Stanislavsk' Motostrelk. Diviziya" }
		164 = { "%dya 'Vitebsk' Motostrelk. Diviziya" }
		167 = { "%dya Motostrelk. Diviziya" }
		180 = { "%dya 'Kiev' Motostrelk. Diviziya" }
		192 = { "%dya Motostrelk. Diviziya" }
		194 = { "%dya Motostrelk. Diviziya" }
		199 = { "%dya Motostrelk. Diviziya" }
		200 = { "%dya Udarnaya Motostrelk. Diviziya" }
		201 = { "%dya 'Gatchina' Motostrelk. Diviziya" }
		203 = { "%dya 'Hingansk' Motostrelk. Diviziya" }
		206 = { "%dya Motostrelk. Diviziya" }
		207 = { "%dya 'Pomeranskaya' Motostrelk. Diviziya" }
		213 = { "%dya Motostrelk. Diviziya" }
		218 = { "%dya Motostrelk. Diviziya" }
		219 = { "%dya Motostrelk. Diviziya" }
		242 = { "%dya Motostrelk. Diviziya" }
		245 = { "%dya Motostrelk. Diviziya" }
		254 = { "%dya 'Cherkassy' Motostrelk. Diviziya" }
		265 = { "%dya 'Vyborg' Motostrelk. Diviziya" }
		266 = { "%dya 'Artemovsk' Motostrelk. Diviziya" }
		270 = { "%dya Motostrelk. Diviziya" }
		272 = { "%dya 'Svirsk' Motostrelk. Diviziya" }
		277 = { "%dya 'Kutuzov' Motostrelk. Diviziya" }
		295 = { "%dya 'Herson' Motostrelk. Diviziya" }
	}
}

SOV_DIV_02 =
{
	name = "USSR Tank Division"

	for_countries = { SOV }

	can_use = { 
		OR = {
			emerging_communist_state_are_in_power = yes
			neutrality_neutral_communism_are_in_power = yes
		}
		has_completed_focus = SOV_communist_party_dominance
	}

	division_types = { "armor_Bat" "Arm_Inf_Bat" }

	fallback_name = "%dya Tankovaya Diviziya"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		1 = { "%dya 'Insteburg' Tankovaya Diviziya" }
		2 = { "%dya Gvds. 'Tatsinsk' Tankovaya Diviziya" }
		3 = { "%dya Gvds. 'Kotelniki' Tankovaya Diviziya" }
		4 = { "%dya Gvds. 'Kantemirovskaya' Tankovaya Diviziya" }
		5 = { "%dya Gvds. 'Donskaya' Tankovaya Diviziya" }
		6 = { "%dya Gvds. 'Berlin' Tankovaya Diviziya" }
		7 = { "%dya Gvds. 'Kiev' Tankovaya Diviziya" }
		8 = { "%dya Gvds. Tankovaya Diviziya" }
		9 = { "%dya Gvds. 'Bobruysk' Tankovaya Diviziya" }
		10 = { "%dya Gvds. 'Ural-Lvov' Tankovaya Diviziya" }
		11 = { "%dya Gvds. 'Karpaty' Tankovaya Diviziya" }
		12 = { "%dya Gvds. 'Uman' Tankovaya Diviziya" }
		13 = { "%dya Gvds. 'Poltava' Tankovaya Diviziya" }
		14 = { "%dya Tankovaya Diviziya" }
		15 = { "%dya Gvds. 'Mozyr' Tankovaya Diviziya" }
		16 = { "%dya Gvds. 'Uman' Tankovaya Diviziya" }
		17 = { "%dya Gvds. 'Krivoy Rog' Tankovaya Diviziya" }
		18 = { "%dya Tankovaya Diviziya" }
		19 = { "%dya Gvds. 'Budapest' Tankovaya Diviziya" }
		20 = { "%dya 'Zvenigorod' Tankovaya Diviziya" }
		21 = { "%dya Gvds. 'Vitebsk' Tankovaya Diviziya" }
		23 = { "%dya Uchebnaya 'Budapest' Tankovaya Diviziya" }
		24 = { "%dya Uchebnaya Tankovaya Diviziya" }
		25 = { "%dya Tankovaya Diviziya" }
		26 = { "%dya Gvds. 'Moskva' Tankovaya Diviziya" }
		27 = { "%dya Uchebnaya Tankovaya Diviziya" }
		28 = { "%dya 'Aleksandr' Tankovaya Diviziya" }
		29 = { "%dya 'Znamensk' Tankovaya Diviziya" }
		30 = { "%dya Gvds. 'Rovno' Tankovaya Diviziya" }
		31 = { "%dya 'Visla' Tankovaya Diviziya" }
		32 = { "%dya Gvds. 'Poltava' Tankovaya Diviziya" }
		34 = { "%dya 'Dnepr' Tankovaya Diviziya" }
		37 = { "%dya Gvds. 'Rechitsk' Tankovaya Diviziya" }
		40 = { "%dya Gvds. 'Pomeraniya' Tankovaya Diviziya" }
		41 = { "%dya Gvds. 'Korsun' Tankovaya Diviziya" }
		42 = { "%dya Gvds. 'Priluki' Tankovaya Diviziya" }
		44 = { "%dya Uchebnaya 'Lisichansk' Tankovaya Diviziya" }
		45 = { "%dya Gvds. 'Rovno' Tankovaya Diviziya" }
		47 = { "%dya Gvds. 'Dnepr' Tankovaya Diviziya" }
		48 = { "%dya Gvds. 'Zvenigorod' Tankovaya Diviziya" }
		49 = { "%dya Uchebnaya Tankovaya Diviziya" }
		51 = { "%dya Tankovaya Diviziya" }
		57 = { "%dya Uchebnaya Tankovaya Diviziya" }
		60 = { "%dya 'Sevsk' Tankovaya Diviziya" }
		75 = { "%dya Gvds. 'Bahmach' Tankovaya Diviziya" }
		76 = { "%dya Tankovaya Diviziya" }
		77 = { "%dya Uchebnaya Tankovaya Diviziya" }
		78 = { "%dya 'Nevel' Tankovaya Diviziya" }
		79 = { "%dya Gvds. 'Zaporozhye' Tankovaya Diviziya" }
		90 = { "%dya Gvds. 'Lvov' Tankovaya Diviziya" }
		117 = { "%dya Gvds. 'Berdichev' Tankovaya Diviziya" }
		193 = { "%dya 'Dnepr' Tankovaya Diviziya" }
	}
}

SOV_DIV_03 =
{
	name = "USSR Art. Division"

	for_countries = { SOV }

	can_use = { 
		OR = {
			emerging_communist_state_are_in_power = yes
			neutrality_neutral_communism_are_in_power = yes
		}
		has_completed_focus = SOV_communist_party_dominance
	}

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%dya Art. Diviziya"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered =
	{
		2 = { "%dya Gvds. 'Perekop' Art. Diviziya" }
		12 = { "%dya 'Praga' Art. Diviziya" }
		15 = { "%dya Gvds. 'Neman' Art. Diviziya" }
		18 = { "%dya Pulem. Art. Diviziya" }
		20 = { "%dya Uchebnaya Art. Diviziya" }
		26 = { "%dya 'Sivash' Art. Diviziya" }
		34 = { "%dya Art. Diviziya" }
		51 = { "%dya Gvds. 'Rogachev' Art. Diviziya" }
		55 = { "%dya 'Budapest' Art. Diviziya" }
		81 = { "%dya Art. Diviziya" }
		110 = { "%dya Gvds. 'Perekop' Art. Diviziya" }
		122 = { "%dya Gvds. Pulem. Art. Diviziya" }
		126 = { "%dya Pulem. Art. Diviziya" }
		127 = { "%dya Pulem. Art. Diviziya" }
		128 = { "%dya 'Svirsk' Pulem. Art. Diviziya" }
		129 = { "%dya Gvds. 'Khingansk' Pulem. Art. Diviziya" }
		130 = { "%dya Pulem. Art. Diviziya" }
		131 = { "%dya Gvds. 'Lozovsk' Pulem. Art. Diviziya" }
		149 = { "%dya Art. Diviziya" }
	}
}