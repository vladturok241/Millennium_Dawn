﻿POL_ARM_01 = {
	name = "Dywizja Zmechanizowana"

	for_countries = { POL }

	division_types = { "Arm_Inf_Bat" "armor_Bat" "Mech_Inf_Bat" "SP_Arty_Bat" "SP_AA_Bat" }

	fallback_name = "%d Dywizja Zmechanizowana"

	ordered = {
		1 = { "%d Warszawska Dywizja Zmechanizowana" }
		2 = { "%d Warszawska Dywizja Zmechanizowana" }
		3 = { "2 Pomorska Dywizja Zmechanizowana" }
		4 = { "3 Pomorska Dywizja Zmechanizowana" }
		5 = { "4 Pomorska Dywizja Zmechanizowana im. Jana Kilińskiego" }
		6 = { "4 Lubuska Dywizja Zmechanizowana" }
		7 = { "5 Kresowa Dywizja Zmechanizowana" }
		8 = { "8 Drezdeńska Dywizja Zmechanizowana" }
		9 = { "%d Drezdeńska Dywizja Zmechanizowana" }
		10 = { "8 Bałtycka Dywizja Obrony Wybrzeża" }
		11 = { "10 Sudecka Dywizja Zmechanizowana" }
		12 = { "11 Dywizja Zmechanizowana" }
		13 = { "12 Dywizja Zmechanizowana" }
		14 = { "12 Szczecińska Dywizja Zmechanizowana" }
		15 = { "%d Dywizja Zmechanizowana" }
		16 = { "15 Warmińsko-Mazurska Dywizja Zmechanizowana" }
		17 = { "16 Pomorska Dywizja Zmechanizowana" }
		18 = { "19 Dywizja Zmechanizowana" }
		19 = { "20 Dywizja Zmechanizowana" }
		20 = { "26 Zapasowa Dywizja Zmechanizowana" }
		21 = { "26 Rezerwowa Dywizja Zmechanizowana" }
		22 = { "28 Zapasowa Dywizja Zmechanizowana" }
		23 = { "28 Rezerwowa Dywizja Zmechanizowana" }
		24 = { "30 Rezerwowa Dywizja Zmechanizowana" }
		25 = { "31 Rezerwowa Dywizja Zmechanizowana" }
		26 = { "32 Rezerwowa Dywizja Zmechanizowana" }
	}
}

POL_ARM_02 = {
	name = "Piechota"

	for_countries = { POL }

	division_types = { "L_Inf_Bat" }

	fallback_name = "%d Dywizja Piechoty"

	ordered = {
		1 = { "1 Warszawska Dywizja Piechoty im.Tadeusza Kościuszki" }
		2 = { "1 Szkolna Dywizja Piechoty" }
		3 = { "2 Warszawska Dywizja Piechoty im. Henryka Dąbrowskiego" }
		4 = { "3 Pomorska Dywizja Piechoty im. Romualda Traugutta" }
		5 = { "4 Pomorska Dywizja Piechoty im. Jana Kilińskiego" }
		6 = { "5 Saska Dywizja Piechoty" }
		7 = { "6 Pomorska Dywizja Piechoty" }
		8 = { "7 Łużycka Dywizja Piechoty" }
		9 = { "8 Drezdeńska Dywizja Piechoty" }
		10 = { "9 Drezdeńska Dywizja Piechoty" }
		11 = { "10 Sudecka Dywizja Piechoty" }
		12 = { "11 Dywizja Piechoty" }
		13 = { "12 Dywizja Piechoty" }
		14 = { "13 Dywizja Piechoty" }
		15 = { "14 Dywizja Piechoty" }
		16 = { "15 Dywizja Piechoty" }
		17 = { "16 Kaszubska Dywizja Piechoty" }
		18 = { "18 Dywizja Piechoty" }
		19 = { "21 Dywizja Piechoty" }
		20 = { "22 Dywizja Piechoty" }
		21 = { "23 Dywizja Piechoty" }
		22 = { "24 Dywizja Piechoty" }
		23 = { "25 Dywizja Piechoty" }
		24 = { "27 Dywizja Piechoty" }
		25 = { "29 Dywizja Piechoty" }
		26 = { "30 Dywizja Piechoty" }
		27 = { "34 Dywizja Piechoty czasu „W”" }
		28 = { "37 Dywizja Piechoty czasu „W”" }
		29 = { "40 Dywizja Piechoty czasu „W”" }
	}
}

POL_ARM_03 = {
	name = "Dywizja Kawalerii Pancernej"

	for_countries = { POL }

	division_types = { "armor_Bat" "Mech_Inf_Bat" "SP_Arty_Bat" "SP_AA_Bat" }

	fallback_name = "%d Dywizja Kawalerii Pancernej"

	ordered = {
		1 = { "4 Brygada Kawalerii Pancernej" }
		2 = { "6 Brygada Kawalerii Pancernej" }
		3 = { "9 Brygada Kawalerii Pancernej" }
		4 = { "10 Brygada Kawalerii Pancernej" }
		5 = { "12 Brygada Kawalerii Pancernej" }
		6 = { "15 Wielkopolska Brygada Kawalerii Pancernej" }
		7 = { "34 Brygada Kawalerii Pancernej" }
		8 = { "11 Lubuska Dywizja Kawalerii Pancernej" }
	}
}

POL_ARM_04 = {
	name = "Kawaleria Powietrzna"

	for_countries = { POL }

	division_types = { "L_Air_assault_Bat" }

	fallback_name = "%d Dywizja Kawalerii Powietrznej"

	ordered = {
		1 = { "1 Batalion Kawalerii Powietrznej" }
		2 = { "7 Batalion Kawalerii Powietrznej" }
		3 = { "25 Brygada Kawalerii Powietrznej" }
	}
}

POL_ARM_05 = {
	name = "Strzelcy Podhalanscy"

	for_countries = { POL }

	division_types = { "armor_Bat" "Arm_Inf_Bat" "SP_Arty_Bat" "SP_AA_Bat" }

	fallback_name = "%d Brygada Strzelców Podhalańskich"

	ordered = {
		1 = { "1 Pułk Strzelców Podhalańskich" }
		2 = { "2 Pułk Strzelców Podhalańskich" }
		3 = { "3 Pułk Strzelców Podhalańskich" }
		4 = { "4 Pułk Strzelców Podhalańskich" }
		5 = { "5 Pułk Strzelców Podhalańskich" }
		6 = { "6 Pułk Strzelców Podhalańskich" }
		7 = { "1 Samodzielna Brygada Strzelców Podhalańskich" }
		8 = { "1 Batalion Strzelców Podhalańskich" }
		9 = { "21 Brygada Strzelców Podhalańskich" }
	}
}

POL_ARM_06 = {
	name = "Airborne Brigade"

	for_countries = { POL }

	division_types = { "L_Air_Inf_Bat" }

	fallback_name = "%d Batalion Powietrznodesantowy"

	ordered = {
		1 = { "6 Batalion Powietrznodesantowa" }
		2 = { "18 Bielski Batalion Powietrznodesantowa" }
	}
}

POL_ARM_07 = {
	name = "Brygada P.Gorskiej"

	for_countries = { POL }

	division_types = { "L_Inf_Bat" }

	fallback_name = "%d Brygada P.Gorskiej"

	ordered = {
		1 = { "22 Karpacka Brygada Piechoty Gorskiej" }
	}
}

POL_ARM_08 = {
	name = "Komandosi"

	for_countries = { POL }

	division_types = { "Special_Forces" }

	fallback_name = "%d Pułk Specjalny Komandosów"

	ordered = {
		1 = { "1 Pułk Specjalny Komandosów" }
	}
}

POL_ARM_09 = {
	name = "Pulk Rozpoznawczy"

	for_countries = { POL }

	division_types = { "Mech_Inf_Bat" }

	fallback_name = "%d Pułk Rozpoznawczy"

	ordered = {
		1 = { "2 Hrubieszowski Pułk Rozpoznawczy" }
		2 = { "18 Białostocki Pułk Rozpoznawczy" }
		3 = { "34 Żagański Pułk Rozpoznawczy" }
	}
}

POL_ARM_10 = {
	name = "Coastal Brigade"

	for_countries = { POL }

	division_types = { "Arm_Marine_Bat" "SP_Arty_Bat" "SP_AA_Bat" }

	fallback_name = "%d Brygada Obrony Wybrzeża"

	ordered = {
		1 = { "1 Brygada Obrony Wybrzeża im. Króla Jana III Sobieskiego" }
		2 = { "3 Brygada Obrony Wybrzeża im. Króla Kazimierza Wielkiego" }
		3 = { "Formoza Gdańsk" }
	}
}