﻿AFG_ARMY_DIVISIONS = {
	name = "Army Divisions"
	for_countries = { AFG }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { "AFG_ARMY_BRIGADES" }

	fallback_name = "%d. Jalaväebrigaad"
}

AFG_ARMY_BRIGADES = {
	name = "Army Brigades"
	for_countries = { AFG }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { "AFG_ARMY_DIVISIONS" }

	fallback_name = "%d. Firqa-e Lashkar"
}

AFG_MEC_BRIGADES = {
	name = "Mechanized Division"
	for_countries = { AFG }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat" }

	fallback_name = "%d. Firqa-e Topchi"
}

AFG_ARM_BRIGADES = {
	name = "Armoured Brigades"
	for_countries = { AFG }

	division_types = { "armor_Bat" }

	fallback_name = "%d. Firqa-e Zerehi"
}

AFG_MIL_BRIGADES = {
	name = "Mujahideen"
	for_countries = { AFG }

	division_types = { "Militia_Bat" "Mot_Militia_Bat" }

	fallback_name = "%d. Mujahideen-e Jamiat-e Islami"
}