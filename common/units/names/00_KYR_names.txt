KYR = {
	air_wing_names_template = AIR_WING_NAME_KYR_FALLBACK

	#Air wings can only be named through archetype
	small_plane_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC
	}
	small_plane_strike_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	small_plane_cas_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	small_plane_naval_bomber_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	small_plane_suicide_airframe = {
		prefix = ""
		generic = { "Pilotsuz soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	medium_plane_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC
		unique = {
			"3 Soguştuk uçak" "7 Soguştuk uçak" "8 Soguştuk uçak"
		}
	}
	medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	medium_plane_suicide_airframe = {
		prefix = ""
		generic = { "Pilotsuz soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	cv_medium_plane_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC
	}
	cv_medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	cv_medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	cv_medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	cv_medium_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Transporttuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	large_plane_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	large_plane_cas_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	large_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	large_plane_awacs_airframe = {
		prefix = ""
		generic = { "Soguştuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	large_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Transporttuk uçak" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
	attack_helicopter_equipment = {
		prefix = ""
		generic = { "Çabuul vertoletu" }
		generic_pattern = AIR_WING_NAME_KYR_GENERIC

	}
}