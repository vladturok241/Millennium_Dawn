#Written by Hiddengearz
#Balance Changes done by Didi, Simone

equipments = {
	###Light Anti-Tank
	L_AT_Equipment = {
		is_archetype = yes
		type = {
			infantry
			anti_tank
		}
		group_by = archetype

		interface_category = interface_category_land

		upgrades = {
			L_AT_Fire_Control
			L_AT_Warhead
			L_AT_Reliability
		}

		#Misc Abilities
		reliability = 0.9

		#Offensive Abilities
		soft_attack = 2.5
		hard_attack = 2.25
		ap_attack = 10
		air_attack = 0

		#Defensive Abilities
		defense = 2
		breakthrough = 3
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 0.6

		resources = {
			steel = 1
		}
	}

	#1965
	Anti_tank_0 = {
		year = 1965

		archetype = L_AT_Equipment
		priority = 5
		visual_level = 0

		#Misc Abilities
		reliability = 0.9

		#Offensive Abilities
		soft_attack = 3.75
		hard_attack = 3.75
		ap_attack = 12
		air_attack = 0

		#Defensive Abilities
		defense = 3
		breakthrough = 2
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 0.6
		resources = {
			steel = 1
		}

	}
	#1985
	Anti_tank_1 = {
		year = 1985

		archetype = L_AT_Equipment
		parent = Anti_tank_0
		priority = 5
		visual_level = 1

		#Misc Abilities
		reliability = 0.9

		#Offensive Abilities
		soft_attack = 5
		hard_attack = 4.5
		ap_attack = 15
		air_attack = 0

		#Defensive Abilities
		defense = 4
		breakthrough = 3
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 0.84
		resources = {
			steel = 1
			tungsten = 1
		}
	}
	#2005
	Anti_tank_2 = {
		year = 2005

		archetype = L_AT_Equipment
		parent = Anti_tank_1
		priority = 5
		visual_level = 2

		#Misc Abilities
		reliability = 0.9

		#Offensive Abilities
		soft_attack = 6.25
		hard_attack = 5.25
		ap_attack = 18
		air_attack = 0

		#Defensive Abilities
		defense = 5
		breakthrough = 4
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 1.57
		resources = {
			steel = 2
			tungsten = 1

		}
	}
	#2025
	Anti_tank_3 = {
		year = 2025

		archetype = L_AT_Equipment
		parent = Anti_tank_2
		priority = 5
		visual_level = 2

		#Misc Abilities
		reliability = 0.9

		#Offensive Abilities
		soft_attack = 7.5
		hard_attack = 6
		ap_attack = 21
		air_attack = 0

		#Defensive Abilities
		defense = 6
		breakthrough = 5
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 1.65
		resources = {
			steel = 2
			chromium = 1
			tungsten = 1
		}
	}
	#2035
	Anti_tank_4 = {
		year = 2035

		archetype = L_AT_Equipment
		parent = Anti_tank_3
		priority = 5
		visual_level = 2

		#Misc Abilities
		reliability = 0.9

		#Offensive Abilities
		soft_attack = 8.75
		hard_attack = 6.75
		ap_attack = 24
		air_attack = 0

		#Defensive Abilities
		defense = 7
		breakthrough = 6
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 2.14
		resources = {
			steel = 2
			chromium = 2
			tungsten = 1
		}
	}

	###Heavy Anti-Tank###

	H_AT_Equipment = {
		is_archetype = yes
		type = {
			infantry
			anti_tank
		}
		group_by = archetype

		interface_category = interface_category_land

		upgrades = {
			H_AT_Fire_Control
			H_AT_Warhead
			H_AT_Reliability
		}

		#Misc Abilities
		reliability = 0.9
		maximum_speed = 8.5

		#Offensive Abilities
		soft_attack = 2
		hard_attack = 6
		ap_attack = 20
		air_attack = 0

		#Defensive Abilities
		defense = 2
		breakthrough = 0
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 4.5
		resources = {
			tungsten = 1
			steel = 1
		}
	}

	#1965
	Heavy_Anti_tank_0 = {
		year = 1965

		archetype = H_AT_Equipment
		priority = 5
		visual_level = 0

		#Misc Abilities
		reliability = 0.825
		maximum_speed = 8.5

		#Offensive Abilities
		soft_attack = 2
		hard_attack = 6
		ap_attack = 20
		air_attack = 0

		#Defensive Abilities
		defense = 2
		breakthrough = 0
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 4.5
		resources = {
			steel = 1
		}

	}
	#1985
	Heavy_Anti_tank_1 = {
		year = 1985

		archetype = H_AT_Equipment
		parent = Heavy_Anti_tank_0
		priority = 5
		visual_level = 1

		#Misc Abilities
		reliability = 0.85
		maximum_speed = 9.5

		#Offensive Abilities
		soft_attack = 3
		hard_attack = 6.75 #+1
		ap_attack = 24
		air_attack = 0

		#Defensive Abilities
		defense = 3
		breakthrough = 0
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 4.75
		resources = {
			steel = 2
		}
	}
	#2005
	Heavy_Anti_tank_2 = {
		year = 2005

		archetype = H_AT_Equipment
		parent = Heavy_Anti_tank_1
		priority = 5
		visual_level = 2

		#Misc Abilities
		reliability = 0.875
		maximum_speed = 10.5

		#Offensive Abilities
		soft_attack = 4
		hard_attack = 7.5 #+1
		ap_attack = 28
		air_attack = 0

		#Defensive Abilities
		defense = 4
		breakthrough = 0
		hardness = 0
		armor_value = 0

		#Space taken in convoy
		lend_lease_cost = 1

		build_cost_ic = 5
		resources = {
			steel = 2
			chromium = 1
		}
	}
	#2025
	Heavy_Anti_tank_3 = {
		year = 2025

		archetype = H_AT_Equipment
		parent = Heavy_Anti_tank_2
		priority = 5
		visual_level = 2

		#Misc Abilities
		reliability = 0.9
		maximum_speed = 11.5

		#Offensive Abilities
		soft_attack = 5
		hard_attack = 8.25 #+1
		ap_attack = 32
		air_attack = 0

		#Defensive Abilities
		defense = 5
		breakthrough = 0
		hardness = 0
		armor_value = 0

		build_cost_ic = 5.25

		resources = {
			steel = 3
			chromium = 1
		}
	}
	#2035
	Heavy_Anti_tank_4 = {
		year = 2035

		archetype = H_AT_Equipment
		parent = Heavy_Anti_tank_3
		priority = 5
		visual_level = 2

		#Misc Abilities
		reliability = 0.9
		maximum_speed = 12

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 9 #+1
		ap_attack = 36
		air_attack = 0

		#Defensive Abilities
		defense = 6
		breakthrough = 0
		hardness = 0
		armor_value = 0

		build_cost_ic = 5.5
		resources = {
			steel = 3
			chromium = 1
		}
	}
}
