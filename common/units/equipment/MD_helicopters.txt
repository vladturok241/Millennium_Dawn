#Written by Hiddengearz, balanced by bird, re-balanced by dc
##Rebalanced as of 11/27/2020 due to full re-balance of aircraft
equipments = {
	attack_helicopter_equipment = {
		archetype = attack_helicopter_hull
		is_buildable = no
		type = {
			infantry
			motorized
		}
		group_by = archetype
		interface_category = interface_category_land

		maximum_speed = 20
		build_cost_ic = 20
		fuel_consumption = 0.1
		resources = {
			aluminium = 3
		}
	}

	#1965
	attack_helicopter1 = {
		year = 1965

		archetype = attack_helicopter_hull
		priority = 5

		#Misc Abilities
		maximum_speed = 40
		reliability = 0.825

		#Offensive Abilities
		soft_attack = 10
		hard_attack = 7.5
		ap_attack = 20
		air_attack = 0.5

		#Defensive Abilities
		defense = 12
		breakthrough = 12
		hardness = 0.3
		armor_value = 0

		build_cost_ic = 20

		#Space taken in convoy
		lend_lease_cost = 5
		fuel_consumption = 3.5
		resources = {
			aluminium = 3
		}
	}
	#1995
	attack_helicopter2 = {
		year = 1995
		archetype = attack_helicopter_hull
		parent = attack_helicopter1
		priority = 5

		#Misc Abilities
		maximum_speed = 48
		reliability = 0.85

		#Offensive Abilities
		soft_attack = 12
		hard_attack = 9.75
		ap_attack = 25
		air_attack = 1

		#Defensive Abilities
		defense = 15
		breakthrough = 15
		hardness = 0.3
		armor_value = 0

		build_cost_ic = 24

		#Space taken in convoy
		lend_lease_cost = 5
		fuel_consumption = 3.5
		resources = {
			aluminium = 2
			tungsten = 1
		}
	}
	#2005
	attack_helicopter3 = {
		year = 2005

		archetype = attack_helicopter_hull
		parent = attack_helicopter2
		priority = 5

		#Misc Abilities
		maximum_speed = 52
		reliability = 0.875

		#Offensive Abilities
		soft_attack = 15
		hard_attack = 12
		ap_attack = 30
		air_attack = 1.5

		#Defensive Abilities
		defense = 18
		breakthrough = 18
		hardness = 0.3
		armor_value = 0

		build_cost_ic = 28

		#Space taken in convoy
		lend_lease_cost = 5
		fuel_consumption = 5.5
		resources = {
			aluminium = 2
			tungsten = 1
			chromium = 1
		}
	}
	#2015
	attack_helicopter4 = {
		year = 2015

		archetype = attack_helicopter_hull
		parent = attack_helicopter3
		priority = 5

		#Misc Abilities
		maximum_speed = 56
		reliability = 0.9

		#Offensive Abilities
		soft_attack = 17.5
		hard_attack = 14.25
		ap_attack = 35
		air_attack = 2

		build_cost_ic = 32

		#Defensive Abilities
		defense = 21
		breakthrough = 21
		hardness = 0.3
		armor_value = 0
		fuel_consumption = 7.0
		resources = {
			aluminium = 3
			chromium = 1
			tungsten = 1
		}
	}
	#2035
	attack_helicopter5 = {
		year = 2035
		archetype = attack_helicopter_hull
		parent = attack_helicopter4
		priority = 5

		#Misc Abilities
		maximum_speed = 60
		reliability = 0.915

		#Offensive Abilities
		soft_attack = 20
		hard_attack = 16.5
		ap_attack = 40
		air_attack = 2.5

		#Defensive Abilities
		defense = 24
		breakthrough = 24
		hardness = 0.3
		armor_value = 0

		build_cost_ic = 36

		#Space taken in convoy
		lend_lease_cost = 5
		fuel_consumption = 9.0
		resources = {
			aluminium = 3
			chromium = 1
			tungsten = 1
		}
	}
}
