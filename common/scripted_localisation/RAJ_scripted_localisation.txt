#STEP 3
defined_text = {
	name = 1063_maoist_influence

	text = {
		trigger = {
			1063 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_1063_high_influence
	}

	text = {
		trigger = {
			1063 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_1063_medium_influence
	}

	text = {
		trigger = {
			1063 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_1063_low_influence
	}

	text = {
		trigger = {
			1063 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_1063_no_influence
	}
}
defined_text = {
	name = 991_maoist_influence

	text = {
		trigger = {
			991 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_991_high_influence
	}

	text = {
		trigger = {
			991 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_991_medium_influence
	}

	text = {
		trigger = {
			991 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_991_low_influence
	}

	text = {
		trigger = {
			991 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_991_no_influence
	}
}
defined_text = {
	name = 470_maoist_influence

	text = {
		trigger = {
			470 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_470_high_influence
	}

	text = {
		trigger = {
			470 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_470_medium_influence
	}

	text = {
		trigger = {
			470 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_470_low_influence
	}

	text = {
		trigger = {
			470 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_470_no_influence
	}
}
defined_text = {
	name = 455_maoist_influence

	text = {
		trigger = {
			455 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_455_high_influence
	}

	text = {
		trigger = {
			455 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_455_medium_influence
	}

	text = {
		trigger = {
			455 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_455_low_influence
	}

	text = {
		trigger = {
			455 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_455_no_influence
	}
}
defined_text = {
	name = 447_maoist_influence

	text = {
		trigger = {
			447 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_447_high_influence
	}

	text = {
		trigger = {
			447 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_447_medium_influence
	}

	text = {
		trigger = {
			447 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_447_low_influence
	}

	text = {
		trigger = {
			447 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_447_no_influence
	}
}
defined_text = {
	name = 455_maoist_influence

	text = {
		trigger = {
			455 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_455_high_influence
	}

	text = {
		trigger = {
			455 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_455_medium_influence
	}

	text = {
		trigger = {
			455 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_455_low_influence
	}

	text = {
		trigger = {
			455 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_455_no_influence
	}
}
defined_text = {
	name = 452_maoist_influence

	text = {
		trigger = {
			452 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_452_high_influence
	}

	text = {
		trigger = {
			452 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_452_medium_influence
	}

	text = {
		trigger = {
			452 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_452_low_influence
	}

	text = {
		trigger = {
			452 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_452_no_influence
	}
}
defined_text = {
	name = 456_maoist_influence

	text = {
		trigger = {
			456 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_456_high_influence
	}

	text = {
		trigger = {
			456 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_456_medium_influence
	}

	text = {
		trigger = {
			456 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_456_low_influence
	}

	text = {
		trigger = {
			456 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_456_no_influence
	}
}
defined_text = {
	name = 436_maoist_influence

	text = {
		trigger = {
			436 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_436_high_influence
	}

	text = {
		trigger = {
			436 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_436_medium_influence
	}

	text = {
		trigger = {
			436 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_436_low_influence
	}

	text = {
		trigger = {
			436 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_436_no_influence
	}
}
defined_text = {
	name = 458_maoist_influence

	text = {
		trigger = {
			458 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_458_high_influence
	}

	text = {
		trigger = {
			458 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_458_medium_influence
	}

	text = {
		trigger = {
			458 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_458_low_influence
	}

	text = {
		trigger = {
			458 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_458_no_influence
	}
}

defined_text = {
	name = 459_maoist_influence #These are tied to the .gui GFX file

	text = {
		trigger = {
			459 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_459_high_influence
	}

	text = {
		trigger = {
			459 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_459_medium_influence
	}

	text = {
		trigger = {
			459 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_459_low_influence
	}

	text = {
		trigger = {
			459 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_459_no_influence
	}
}

defined_text = {
	name = 988_maoist_influence #These are tied to the .gui GFX file

	text = {
		trigger = {
			988 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_988_high_influence
	}

	text = {
		trigger = {
			988 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_988_medium_influence
	}

	text = {
		trigger = {
			988 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_988_low_influence
	}

	text = {
		trigger = {
			988 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_988_no_influence
	}
}

defined_text = {
	name = 989_maoist_influence #These are tied to the .gui GFX file

	text = {
		trigger = {
			989 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_989_high_influence
	}

	text = {
		trigger = {
			989 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_989_medium_influence
	}

	text = {
		trigger = {
			989 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_989_low_influence
	}

	text = {
		trigger = {
			989 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_989_no_influence
	}
}

defined_text = {
	name = 451_maoist_influence #These are tied to the .gui GFX file

	text = {
		trigger = {
			451 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
				}
			}
		}
		localization_key = GFX_451_high_influence
	}

	text = {
		trigger = {
			451 = {
				has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
			}
		}
		localization_key = GFX_451_medium_influence
	}

	text = {
		trigger = {
			451 = {
				OR = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_451_low_influence
	}

	text = {
		trigger = {
			451 = {
				NOT = {
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel1 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel2 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel3 }
					has_dynamic_modifier = { modifier = RAJ_hoxaists_rebel4 }
				}
			}
		}
		localization_key = GFX_451_no_influence
	}
}




