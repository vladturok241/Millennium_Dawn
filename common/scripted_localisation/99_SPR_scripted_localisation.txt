defined_text = {
	name = selected_culture_index

	text = {
		trigger = { check_variable = { culture_index = 0 } }
		localization_key = catalonians_loc
	}
	text = {
		trigger = { check_variable = { culture_index = 1 } }
		localization_key = basques_loc
	}
	text = {
		trigger = { check_variable = { culture_index = 2 } }
		localization_key = galicians_loc
	}
	text = {
		trigger = { check_variable = { culture_index = 3 } }
		localization_key = andalucians_loc
	}
	text = {
		trigger = { check_variable = { culture_index = 4 } }
		localization_key = canarians_loc
	}
}
defined_text = {
	name = selected_culture_index_cur_opin

	text = {
		trigger = { check_variable = { culture_index = 0 } }
		localization_key = "[?SPR_culture_opinion^0|Y0]"
	}
	text = {
		trigger = { check_variable = { culture_index = 1 } }
		localization_key = "[?SPR_culture_opinion^1|Y0]"
	}
	text = {
		trigger = { check_variable = { culture_index = 2 } }
		localization_key = "[?SPR_culture_opinion^2|Y0]"
	}
	text = {
		trigger = { check_variable = { culture_index = 3 } }
		localization_key = "[?SPR_culture_opinion^3|Y0]"
	}
	text = {
		trigger = { check_variable = { culture_index = 4 } }
		localization_key = "[?SPR_culture_opinion^4|Y0]"
	}
}