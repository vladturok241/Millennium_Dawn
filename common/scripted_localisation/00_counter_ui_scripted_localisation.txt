# Author(s): Stjern from Total War Mod
# Adapted by Mahhouse, Bird
defined_text = {
	name = GetCasualtiesAmount
	text = {
		trigger = {
			casualties < 100000
		}
		localization_key = "[?casualties]"
	}
	text = {
		trigger = {
			casualties > 100000
		}
		localization_key = "[?casualties_k|0]K"
	}
	text = {
		trigger = {
			casualties > 1000000
			set_temp_variable = { casualties_k_k = casualties_k }
			divide_temp_variable = { casualties_k_k = 1000 }
		}
		localization_key = "[?casualties_k_k|2]M"
	}
}

defined_text = {
	name = GetSelectedTopbarInfoText
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 1 } }
		localization_key = TOPBAR_CASUALTIES_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 2 } }
		localization_key = TOPBAR_SURRENDER_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 3 } }
		localization_key = TOPBAR_STRENGTH_RATIO_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 4 } }
		localization_key = TOPBAR_NAVAL_STRENGTH_RATIO_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 5 } }
		localization_key = TOPBAR_RESOURCE_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 6 } }
		localization_key = TOPBAR_POPULARITY_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 9 } }
		localization_key = TOPBAR_ARMY_STRENGTH_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 10 } }
		localization_key = TOPBAR_ENERGY_BALANCE_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 11 } }
		localization_key = TOPBAR_WEEKLY_BALANCE_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 12 } }
		localization_key = TOPBAR_UNEMPLOYMENT_RATE_FORCES_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 13 } }
		localization_key = TOPBAR_NAVY_STRENGTH_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 14 } }
		localization_key = TOPBAR_SHIPS_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 15 } }
		localization_key = TOPBAR_CONS_GOODS_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 19 } }
		localization_key = TOPBAR_FACTORIES_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 20 } }
		localization_key = TOPBAR_PROTESTS_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 21 } }
		localization_key = TOPBAR_FOREIGN_INFLUENCING_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 22 } }
		localization_key = TOPBAR_EUROSCEPTICISM_TEXT
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 0 } }
		localization_key = "Unassigned "
	}
}

defined_text = {
	name = GetTopbarIcon
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 1 }
		}
		localization_key = "GFX_topbar_casualties"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 2 }
		}
		localization_key = "GFX_topbar_surrender_progress"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 3 }
		}
		localization_key = "GFX_topbar_strength_ratio"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 4 }
		}
		localization_key = "GFX_topbar_naval_strength_ratio"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
		}
		localization_key = "GFX_topbar_oil"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
		}
		localization_key = "GFX_topbar_rubber"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
		}
		localization_key = "GFX_topbar_aluminium"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
		}
		localization_key = "GFX_topbar_steel"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 4 }
		}
		localization_key = "GFX_topbar_technology_metals"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 5 }
		}
		localization_key = "GFX_topbar_precious_metals"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
		}
		localization_key = "GFX_topbar_ruling_coalition"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
		}
		localization_key = "GFX_democratic_group_small"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
		}

		localization_key = "GFX_communism_group_small"

	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
		}
		localization_key = "GFX_fascism_group_small"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 4 }
		}
		localization_key = "GFX_neutrality_group_small"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 5 }
		}
		localization_key = "GFX_nationalist_group_small"
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 9 } }
		localization_key = "GFX_topbar_army_strength"
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 10 } }
		localization_key = "GFX_topbar_energy_balance"
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 11 } }
		localization_key = "GFX_topbar_weekly_balance"
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 12 } }
		localization_key = "GFX_topbar_unemployment_rate"
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 13 } }
		localization_key = "GFX_topbar_navy_strength"
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 14 } }
		localization_key = "GFX_topbar_ships"
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 15 } }
		localization_key = "GFX_topbar_consumer"
	}

	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 19 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
		}
		localization_key = "GFX_topbar_civilian_factory"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 19 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
		}
		localization_key = "GFX_topbar_military_factory"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 19 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
		}
		localization_key = "GFX_topbar_dockyard"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 19 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
		}
		localization_key = "GFX_topbar_office"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 0 }
		}
		localization_key = "GFX_topbar_blank"
	}

	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 20 } }
		localization_key = "GFX_topbar_protests"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 21 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
		}
		localization_key = "GFX_foreign_influence_modifier_texticon"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 21 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
		}
		localization_key = "GFX_foreign_influence_modifier_texticon"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 21 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
		}
		localization_key = "GFX_foreign_influence_modifier_texticon"
	}
	# Euroscepticism
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 22 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
		}
		localization_key = "GFX_decision_europe"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 22 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
		}
		localization_key = "GFX_decision_europe"
	}
}

defined_text = {
	name = GetTopbarListText
	text = { trigger = { check_variable = { topbar_menu^i = 1 } }
		localization_key = TOPBAR_CASUALTIES_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 2 } }
		localization_key = TOPBAR_SURRENDER_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 3 } }
		localization_key = TOPBAR_STRENGTH_RATIO_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 4 } }
		localization_key = TOPBAR_NAVAL_STRENGTH_RATIO_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 5 } }
		localization_key = TOPBAR_RESOURCE_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 6 } }
		localization_key = TOPBAR_POPULARITY_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 9 } }
		localization_key = TOPBAR_ARMY_STRENGTH_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 10 } }
		localization_key = TOPBAR_ENERGY_BALANCE_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 11 } }
		localization_key = TOPBAR_WEEKLY_BALANCE_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 12 } }
		localization_key = TOPBAR_UNEMPLOYMENT_RATE_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 13 } }
		localization_key = TOPBAR_NAVY_STRENGTH_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 14 } }
		localization_key = TOPBAR_SHIPS_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 15 } }
		localization_key = TOPBAR_CONS_GOODS_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 19 } }
		localization_key = TOPBAR_FACTORIES_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 20 } }
		localization_key = TOPBAR_PROTESTS_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 21 } }
		localization_key = TOPBAR_FOREIGN_INFLUENCING_TITLE
	}
	text = { trigger = { check_variable = { topbar_menu^i = 22 } }
		localization_key = TOPBAR_EUROSCEPTICISM_TITLE
	}
	text = {
		localization_key = "NO INFO SELECTED"
	}
}
defined_text = {
	name = GetTopbarListTooltip
	text = { trigger = { check_variable = { topbar_menu^i = 1 } }
		localization_key = TOPBAR_LIST_CASUALTIES_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 2 } }
		localization_key = TOPBAR_LIST_SURRENDER_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 3 } }
		localization_key = TOPBAR_LIST_STRENGTH_RATIO_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 4 } }
		localization_key = TOPBAR_LIST_NAVAL_STRENGTH_RATIO_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 5 } }
		localization_key = TOPBAR_LIST_RESOURCE_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 6 } }
		localization_key = TOPBAR_LIST_POPULARITY_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 9 } }
		localization_key = TOPBAR_LIST_ARMY_STRENGTH_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 10 } }
		localization_key = TOPBAR_LIST_ENERGY_BALANCE_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 11 } }
		localization_key = TOPBAR_LIST_WEEKLY_BALANCE_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 12 } }
		localization_key = TOPBAR_LIST_UNEMPLOYMENT_RATE_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 13 } }
		localization_key = TOPBAR_LIST_NAVY_STRENGTH_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 14 } }
		localization_key = TOPBAR_LIST_SHIPS_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 15 } }
		localization_key = TOPBAR_LIST_CONS_GOODS_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 19 } }
		localization_key = TOPBAR_LIST_FACTORIES_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 20 } }
		localization_key = TOPBAR_LIST_PROTESTS_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 21 } }
		localization_key = TOPBAR_LIST_FOREIGN_INFLUENCING_TOOLTIP
	}
	text = { trigger = { check_variable = { topbar_menu^i = 22 } }
		localization_key = TOPBAR_LIST_EUROSCEPTICSM_TOOLTIP
	}
	text = {
		localization_key = "NO INFO SELECTED"
	}
}

defined_text = {
	name = GetTopbarAllianceStrengthRatio
	text = {
		trigger = {
			has_war = yes
			check_variable = { alliance_strength_ratio > 1 }
		}
		localization_key = "[?alliance_strength_ratio|G2]"
	}
	text = {
		trigger = {
			has_war = yes
			check_variable = { alliance_strength_ratio < 1 }
			check_variable = { alliance_strength_ratio > 0 }
		}
		localization_key = "[?alliance_strength_ratio|Y2]"
	}
	text = {
		trigger = {
			has_war = yes
			check_variable = { alliance_strength_ratio < 0 }
		}
		localization_key = "[?alliance_strength_ratio|R2]"
	}
	text = {
		trigger = {
			has_war = no
		}
		localization_key = "-"
	}
}
defined_text = {
	name = GetTopbarAllianceNavalStrengthRatio
	text = {
		trigger = {
			has_war = yes
			check_variable = { alliance_naval_strength_ratio > 1 }
		}
		localization_key = "[?alliance_naval_strength_ratio|G2]"
	}
	text = {
		trigger = {
			has_war = yes
			check_variable = { alliance_naval_strength_ratio < 1 }
			check_variable = { alliance_naval_strength_ratio > 0 }
		}
		localization_key = "[?alliance_naval_strength_ratio|Y2]"
	}
	text = {
		trigger = {
			has_war = yes
			check_variable = { alliance_naval_strength_ratio < 0 }
		}
		localization_key = "[?alliance_naval_strength_ratio|R2]"
	}
	text = {
		trigger = {
			has_war = no
		}
		localization_key = "-"
	}
}
# 5
defined_text = {
	name = GetTopbarSelectedResourceVariable
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^i = 0 }
		}
		localization_key = "resource"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^i = 1 }
		}
		localization_key = "resource_consumed"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^i = 2 }
		}
		localization_key = "resource_exported"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^i = 3 }
		}
		localization_key = "resource_imported"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^i = 4 }
		}
		localization_key = "resource_produced"
	}
}
# 5
defined_text = {
	name = GetTopbarSelectedResourceVariableTooltip
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^topbar_container_index = 0 }
		}
		localization_key = "Net Resource" #was Total Resource
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^topbar_container_index = 1 }
		}
		localization_key = "Consumed Resource"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^topbar_container_index = 2 }
		}
		localization_key = "Exported Resource"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^topbar_container_index = 3 }
		}
		localization_key = "Imported Resource"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_two^topbar_container_index = 4 }
		}
		localization_key = "Produced Resource"
	}
}
# 5
defined_text = {
	name = GetTopbarSelectedResource
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_one^i = 0 }
			meta_trigger = {
				text = {
					set_temp_variable = { resource_temp = Root.[VARIABLE]@oil }
				}
				VARIABLE = "[This.GetTopbarSelectedResourceVariable]"
			}
		}
		localization_key = "[?resource_temp|+0]"
	}

	#rubber
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_one^i = 1 }
			meta_trigger = {
				text = {
					set_temp_variable = { resource_temp = Root.[VARIABLE]@rubber }
				}
				VARIABLE = "[This.GetTopbarSelectedResourceVariable]"
			}
		}
		localization_key = "[?resource_temp|+0]"
	}
	#aluminium
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_one^i = 2 }
			meta_trigger = {
				text = {
					set_temp_variable = { resource_temp = Root.[VARIABLE]@aluminium }
				}
				VARIABLE = "[This.GetTopbarSelectedResourceVariable]"
			}
		}
		localization_key = "[?resource_temp|+0]"
	}
	#steel
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_one^i = 3 }
			meta_trigger = {
				text = {
					set_temp_variable = { resource_temp = Root.[VARIABLE]@steel }
				}
				VARIABLE = "[This.GetTopbarSelectedResourceVariable]"
			}
		}
		localization_key = "[?resource_temp|+0]"
	}
	#tungsten
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_one^i = 4 }
			meta_trigger = {
				text = {
					set_temp_variable = { resource_temp = Root.[VARIABLE]@tungsten }
				}
				VARIABLE = "[This.GetTopbarSelectedResourceVariable]"
			}
		}
		localization_key = "[?resource_temp|+0]"
	}
	#chromium
	text = {
		trigger = {
			check_variable = { ROOT.topbar_option_one^i = 5 }
			meta_trigger = {
				text = {
					set_temp_variable = { resource_temp = Root.[VARIABLE]@chromium }
				}
				VARIABLE = "[This.GetTopbarSelectedResourceVariable]"
			}
		}
		localization_key = "[?resource_temp|+0]"
	}
	text = {
		localization_key = "test test "
	}
}
# 5
defined_text = {
	name = GetTopbarSelectedResourceTooltip
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 0 } }
		localization_key = "Fossil Fuels"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 1 } }
		localization_key = "Rubber"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 2 } }
		localization_key = "Light Metals" #was Aluminum
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 3 } }
		localization_key = "Steel"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 4 } }
		localization_key = "Technology Metals"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 5 } }
		localization_key = "Precious Metals"
	}
	text = {
		localization_key = "No Trigger"
	}
}


# 6
defined_text = {
	name = GetSelectedTopbarPopularityText
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 0 } }
		localization_key = "[?Root.government_coalition_strength|2%]" #replaced the following with what I have [?Root.party_popularity@ruling_party|2]%. I believe everything below related to popularity, and potentially in this whole file, can be deleted unless I want to try and get the cycling to work so you can specify it by ideology too.
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 1 } }
		localization_key = "[?Root.party_popularity@democratic|2%]"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 2 } }
		localization_key = "[?Root.party_popularity@communism|2%]"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 3 } }
		localization_key = "[?Root.party_popularity@fascism|2%]"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 4 } }
		localization_key = "[?Root.party_popularity@neutrality|2%]"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 5 } }
		localization_key = "[?ROOT.party_popularity@nationalist|2%]"
	}
}

# 6
defined_text = {
	name = GetTopbarSelectedPopularityTooltip
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 0 } }
		localization_key = "Ruling Coalition"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 1 } }
		localization_key = "Pro-Western Outlook"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 2 } }
		localization_key = "Emerging Outlook"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 3 } }
		localization_key = "Salafist Outlook"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 4 } }
		localization_key = "Non-Aligned Outlook"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 5 } }
		localization_key = "Nationalist Outlook"
	}
}

# 19
defined_text = {
	name = GetTopbarSelectedBuilding
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 0 } }
		localization_key = "[?Root.num_of_civilian_factories]"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 1 } }
		localization_key = "[?Root.num_of_military_factories]"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 2 } }
		localization_key = "[?Root.num_of_naval_factories]"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 3 } }
		localization_key = "[?Root.office_park_total]"
	}
}

defined_text = {
	name = GetTopbarSelectedBuildingTooltip
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 0 } }
		localization_key = "Civilian Factories"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 1 } }
		localization_key = "Military Factories"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 2 } }
		localization_key = "Naval Dockyards"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 3 } }
		localization_key = "Offices"
	}
}

# 21
defined_text = {
	name = GetTopbarSelectedInfluence
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 0 } }
		localization_key = "[?ROOT.auto_influence_users|0]/[?ROOT.auto_influence_cap|0Y]" #need to change this out with foreign influencing current/cap
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 1 } }
		localization_key = "[?ROOT.modifier@foreign_influence_modifier|%+1]"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 2 } }
		localization_key = "[?ROOT.modifier@foreign_influence_defense_modifier|%+1]"
	}
}

# 21
defined_text = {
	name = GetTopbarSelectedInfluenceTooltip
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 0 } }
		localization_key = "Foreign Influencing Current/Cap"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 1 } }
		localization_key = "Foreign Influence Modifier"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 2 } }
		localization_key = "Foreign Influence Defense Modifier"
	}
}

# 22
defined_text = {
	name = GetTopbarSelectedEuroscepticsm
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 0 } }
		localization_key = "[?ROOT.eurosceptic|§%R1]"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^i = 1 } }
		localization_key = "[?global.var_europeanism|§%G1]"
	}
}

# 22
defined_text = {
	name = GetTopbarSelectedEuroscepticsmTooltip
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 0 } }
		localization_key = "[ROOT.GetNameWithFlag] Euroscepticism"
	}
	text = { trigger = { check_variable = { ROOT.topbar_option_one^topbar_container_index = 1 } }
		localization_key = "[EUU.GetNameWithFlag] Europeanism"
	}
}

defined_text = {
	name = GetDynamicTooltip
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 1 } }
		localization_key = TOPBAR_CASUALTIES_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 2 } }
		localization_key = TOPBAR_SURRENDER_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 3 } }
		localization_key = TOPBAR_STRENGTH_RATIO_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 4 } }
		localization_key = TOPBAR_NAVAL_STRENGTH_RATIO_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 9 } }
		localization_key = TOPBAR_ARMY_STRENGTH_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 10 } }
		localization_key = TOPBAR_ENERGY_BALANCE_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 11 } }
		localization_key = TOPBAR_WEEKLY_BALANCE_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 12 } }
		localization_key = TOPBAR_LIST_UNEMPLOYMENT_RATE_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 13 } }
		localization_key = TOPBAR_NAVY_STRENGTH_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 14 } }
		localization_key = Total_Number_of_Ships
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 15 } }
		localization_key = TOPBAR_CONS_GOODS_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 17 } }
		localization_key = TOPBAR_RESEARCH_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 18 } }
		localization_key = TOPBAR_CONSCRIPTION_TOOLTIP
	}
	text = { trigger = { check_variable = { ROOT.topbar_selected_container^i = 20 } }
		localization_key = TOPBAR_PROTESTS_TOOLTIP
	}
	# Resources Section
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
			check_variable = { ROOT.topbar_option_two^i = 0 }
		}
		localization_key = "Net Fossil Fuels" #was Total_Oil
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
			check_variable = { ROOT.topbar_option_two^i = 1 }
		}
		localization_key = "Consumed Fossil Fuels" #was Consumed_Oil
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
			check_variable = { ROOT.topbar_option_two^i = 2 }
		}
		localization_key = "Exported Fossil Fuels" #was Exported_Oil
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
			check_variable = { ROOT.topbar_option_two^i = 3 }
		}
		localization_key = "Imported Fossil Fuels" #was Imported_Oil
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
			check_variable = { ROOT.topbar_option_two^i = 4 }
		}
		localization_key = "Produced Fossil Fuels" #was Produced_Oil
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
			check_variable = { ROOT.topbar_option_two^i = 0 }
		}
		localization_key = "Net Rubber" #was Total_Rubber
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
			check_variable = { ROOT.topbar_option_two^i = 1 }
		}
		localization_key = "Consumed Rubber" #was Consumed_Rubber
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
			check_variable = { ROOT.topbar_option_two^i = 2 }
		}
		localization_key = "Exported Rubber" #was Exported_Rubber
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
			check_variable = { ROOT.topbar_option_two^i = 3 }
		}
		localization_key = "Imported Rubber" #was Imported_Rubber
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
			check_variable = { ROOT.topbar_option_two^i = 4 }
		}
		localization_key = "Produced Rubber" #was Produced_Rubber
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
			check_variable = { ROOT.topbar_option_two^i = 0 }
		}
		localization_key = "Net Light Metals" #was Aluminium_Total
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
			check_variable = { ROOT.topbar_option_two^i = 1 }
		}
		localization_key = "Consumed Light Metals" #was Aluminium_Consumed
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
			check_variable = { ROOT.topbar_option_two^i = 2 }
		}
		localization_key = "Exported Light Metals" #was Exported_Aluminum
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
			check_variable = { ROOT.topbar_option_two^i = 3 }
		}
		localization_key = "Imported Light Metals" #was Imported_Aluminum
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
			check_variable = { ROOT.topbar_option_two^i = 4 }
		}
		localization_key = "Produced Light Metals" #was Produced_Aluminum
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
			check_variable = { ROOT.topbar_option_two^i = 0	 }
		}
		localization_key = "Net Steel" #was Total_Steel
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
			check_variable = { ROOT.topbar_option_two^i = 1 }
		}
		localization_key = "Consumed Steel" #was Consumed_Steel
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
			check_variable = { ROOT.topbar_option_two^i = 2 }
		}
		localization_key = "Exported Steel" #was Exported_Steel
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
			check_variable = { ROOT.topbar_option_two^i = 3 }
		}
		localization_key = "Imported Steel" #was Imported_Steel
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
			check_variable = { ROOT.topbar_option_two^i = 4 }
		}
		localization_key = "Produced Steel" #was Produced_Steel
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 4 }
			check_variable = { ROOT.topbar_option_two^i = 0 }
		}
		localization_key = "Net Technology Metals" #was Total_Tungsten
	}

	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 4 }
			check_variable = { ROOT.topbar_option_two^i = 1 }
		}
		localization_key = "Consumed Technology Metals" #was Consumed_Tungsten
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 4 }
			check_variable = { ROOT.topbar_option_two^i = 2 }
		}
		localization_key = "Exported Technology Metals" #was Exported_Tungsten
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 4 }
			check_variable = { ROOT.topbar_option_two^i = 3 }
		}
		localization_key = "Imported Technology Metals" #was Imported_Tungsten
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 4 }
			check_variable = { ROOT.topbar_option_two^i = 4 }
		}
		localization_key = "Produced Technology Metals" #was Produced_Tungsten
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 5 }
			check_variable = { ROOT.topbar_option_two^i = 0 }
		}
		localization_key = "Net Precious Metals" #was Total_Raw_Materials
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 5 }
			check_variable = { ROOT.topbar_option_two^i = 1 }
		}
		localization_key = "Consumed Precious Metals" #was Consumed_Raw_Materials
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 5 }
			check_variable = { ROOT.topbar_option_two^i = 2 }
		}
		localization_key = "Exported Precious Metals" #was Exported_Raw_Materials
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 5 }
			check_variable = { ROOT.topbar_option_two^i = 3 }
		}
		localization_key = "Imported Precious Metals" #was Imported_Raw_Materials
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 5 }
			check_variable = { ROOT.topbar_option_one^i = 5 }
			check_variable = { ROOT.topbar_option_two^i = 4 }
		}
		localization_key = "Produced Precious Metals" #was Produced_Raw_Materials
	}
	# Govt. Popularity
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
		}
		localization_key = "Ruling Coalition"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
		}
		localization_key = "Pro-Western Outlook"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
		}

		localization_key = "Emerging Outlook"

	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
		}
		localization_key = "Salafist Outlook"

	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 4 }
		}
		localization_key = "Non-Aligned Outlook"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 6 }
			check_variable = { ROOT.topbar_option_one^i = 5 }
		}
		localization_key = "Nationalist Outlook"
	}
	text = {
		localization_key = "NO_INFO_SELECTED"
	}
	# Factories
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 19 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
		}
		localization_key = "Total Civilian Factories"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 19 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
		}
		localization_key = "Total Military Factories"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 19 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
		}
		localization_key = "Total Naval Dockyards"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 19 }
			check_variable = { ROOT.topbar_option_one^i = 3 }
		}
		localization_key = "Total Offices"
	}
	# Foreign Influencing
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 21 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
		}
		localization_key = "Auto Influencing Current/Cap"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 21 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
		}
		localization_key = "Foreign Influence"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 21 }
			check_variable = { ROOT.topbar_option_one^i = 2 }
		}
		localization_key = "Foreign Influence Defense"
	}
	# Euroscepticism
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 22 }
			check_variable = { ROOT.topbar_option_one^i = 0 }
		}
		localization_key = "[ROOT.GetNameWithFlag] Euroscepticism"
	}
	text = {
		trigger = {
			check_variable = { ROOT.topbar_selected_container^i = 22 }
			check_variable = { ROOT.topbar_option_one^i = 1 }
		}
		localization_key = "[EUU.GetNameWithFlag] Europeanism"
	}
}

defined_text = {
	name = GetTopbarConsumerGoodsPercent
	text = {
		trigger = {
			always = yes

			set_temp_variable = { actual_consumer_goods_percent = modifier@consumer_goods_expected_value }
			set_temp_variable = { consumer_goods_factor_full = 1 }
			add_to_temp_variable = { consumer_goods_factor_full = modifier@consumer_goods_factor }
			multiply_temp_variable = { actual_consumer_goods_percent = consumer_goods_factor_full }
		}

		localization_key = "[?actual_consumer_goods_percent|%1]"
	}
}
