##############################
### EU voting majority Loc ###
##############################
defined_text = {
	name = EU_voting_majority_Loc
	text = { trigger = { has_global_flag = EU_unanimity_voting }
		localization_key = EU_unanimity_voting
	}
	text = { trigger = { has_global_flag = EU_qualified_majority_voting }
		localization_key = EU_qualified_majority_voting
	}
	text = { trigger = { has_global_flag = EU_majority_voting }
		localization_key = EU_majority_voting
	}
}
defined_text = {
	name = EU_voting_result_Loc
	text = { trigger = { has_global_flag = EU_unanimity_voting_YES }
		localization_key = EU_unanimity_voting_YES
	}
	text = { trigger = { has_global_flag = EU_qualified_majority_voting_YES }
		localization_key = EU_qualified_majority_voting_YES
	}
	text = { trigger = { has_global_flag = EU_majority_voting_YES }
		localization_key = EU_majority_voting_YES
	}
	text = { trigger = { has_global_flag = EU_unanimity_voting_NO }
		localization_key = EU_unanimity_voting_NO
	}
	text = { trigger = { has_global_flag = EU_qualified_majority_voting_NO }
		localization_key = EU_qualified_majority_voting_NO
	}
	text = { trigger = { has_global_flag = EU_majority_voting_NO }
		localization_key = EU_majority_voting_NO
	}
}
###############################
### EU Focus Tree Names Loc ###
###############################

defined_text = {
	name = EU_focus_name_Loc
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU002_voted
				has_global_flag = focus_EU002_QMV_voted
				has_global_flag = focus_EU002_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU002
					has_country_flag = focus_EU002_QMV
					has_country_flag = focus_EU002_maj
				}
			}
		}
		localization_key = EU002
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU101_voted
				has_global_flag = focus_EU101_QMV_voted
				has_global_flag = focus_EU101_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU101
					has_country_flag = focus_EU101_QMV
					has_country_flag = focus_EU101_maj
				}
			}
		}
		localization_key = EU101
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU102_voted
				has_global_flag = focus_EU102_QMV_voted
				has_global_flag = focus_EU102_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU102
					has_country_flag = focus_EU102_QMV
					has_country_flag = focus_EU102_maj
				}
			}
		}
		localization_key = EU102
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU103_voted
				has_global_flag = focus_EU103_QMV_voted
				has_global_flag = focus_EU103_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU103
					has_country_flag = focus_EU103_QMV
					has_country_flag = focus_EU103_maj
				}
			}
		}
		localization_key = EU103
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU104_voted
				has_global_flag = focus_EU104_QMV_voted
				has_global_flag = focus_EU104_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU104
					has_country_flag = focus_EU104_QMV
					has_country_flag = focus_EU104_maj
				}
			}
		}
		localization_key = EU104
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU105_voted
				has_global_flag = focus_EU105_QMV_voted
				has_global_flag = focus_EU105_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU105
					has_country_flag = focus_EU105_QMV
					has_country_flag = focus_EU105_maj
				}
			}
		}
		localization_key = EU105
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU201_voted
				has_global_flag = focus_EU201_QMV_voted
				has_global_flag = focus_EU201_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU201
					has_country_flag = focus_EU201_QMV
					has_country_flag = focus_EU201_maj
				}
			}
		}
		localization_key = EU201
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU202_voted
				has_global_flag = focus_EU202_QMV_voted
				has_global_flag = focus_EU202_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU202
					has_country_flag = focus_EU202_QMV
					has_country_flag = focus_EU202_maj
				}
			}
		}
		localization_key = EU202
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU203_voted
				has_global_flag = focus_EU203_QMV_voted
				has_global_flag = focus_EU203_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU203
					has_country_flag = focus_EU203_QMV
					has_country_flag = focus_EU203_maj
				}
			}
		}
		localization_key = EU203
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU204_voted
				has_global_flag = focus_EU204_QMV_voted
				has_global_flag = focus_EU204_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU204
					has_country_flag = focus_EU204_QMV
					has_country_flag = focus_EU204_maj
				}
			}
		}
		localization_key = EU204
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU205_voted
				has_global_flag = focus_EU205_QMV_voted
				has_global_flag = focus_EU205_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU205
					has_country_flag = focus_EU205_QMV
					has_country_flag = focus_EU205_maj
				}
			}
		}
		localization_key = EU205
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU206_voted
				has_global_flag = focus_EU206_QMV_voted
				has_global_flag = focus_EU206_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU206
					has_country_flag = focus_EU206_QMV
					has_country_flag = focus_EU206_maj
				}
			}
		}
		localization_key = EU206
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU207_voted
				has_global_flag = focus_EU207_QMV_voted
				has_global_flag = focus_EU207_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU207
					has_country_flag = focus_EU207_QMV
					has_country_flag = focus_EU207_maj
				}
			}
		}
		localization_key = EU207
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU208_voted
				has_global_flag = focus_EU208_QMV_voted
				has_global_flag = focus_EU208_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU208
					has_country_flag = focus_EU208_QMV
					has_country_flag = focus_EU208_maj
				}
			}
		}
		localization_key = EU208
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU301_voted
				has_global_flag = focus_EU301_QMV_voted
				has_global_flag = focus_EU301_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU301
					has_country_flag = focus_EU301_QMV
					has_country_flag = focus_EU301_maj
				}
			}
		}
		localization_key = EU301
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU302_voted
				has_global_flag = focus_EU302_QMV_voted
				has_global_flag = focus_EU302_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU302
					has_country_flag = focus_EU302_QMV
					has_country_flag = focus_EU302_maj
				}
			}
		}
		localization_key = EU302
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU303_voted
				has_global_flag = focus_EU303_QMV_voted
				has_global_flag = focus_EU303_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU303
					has_country_flag = focus_EU303_QMV
					has_country_flag = focus_EU303_maj
				}
			}
		}
		localization_key = EU303
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU304_voted
				has_global_flag = focus_EU304_QMV_voted
				has_global_flag = focus_EU304_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU304
					has_country_flag = focus_EU304_QMV
					has_country_flag = focus_EU304_maj
				}
			}
		}
		localization_key = EU304
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU305_voted
				has_global_flag = focus_EU305_QMV_voted
				has_global_flag = focus_EU305_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU305
					has_country_flag = focus_EU305_QMV
					has_country_flag = focus_EU305_maj
				}
			}
		}
		localization_key = EU305
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU306_voted
				has_global_flag = focus_EU306_QMV_voted
				has_global_flag = focus_EU306_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU306
					has_country_flag = focus_EU306_QMV
					has_country_flag = focus_EU306_maj
				}
			}
		}
		localization_key = EU306
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU307_voted
				has_global_flag = focus_EU307_QMV_voted
				has_global_flag = focus_EU307_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU307
					has_country_flag = focus_EU307_QMV
					has_country_flag = focus_EU307_maj
				}
			}
		}
		localization_key = EU307
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU308_voted
				has_global_flag = focus_EU308_QMV_voted
				has_global_flag = focus_EU308_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU308
					has_country_flag = focus_EU308_QMV
					has_country_flag = focus_EU308_maj
				}
			}
		}
		localization_key = EU308
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU401_voted
				has_global_flag = focus_EU401_QMV_voted
				has_global_flag = focus_EU401_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU401
					has_country_flag = focus_EU401_QMV
					has_country_flag = focus_EU401_maj
				}
			}
		}
		localization_key = EU401
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU402_voted
				has_global_flag = focus_EU402_QMV_voted
				has_global_flag = focus_EU402_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU402
					has_country_flag = focus_EU402_QMV
					has_country_flag = focus_EU402_maj
				}
			}
		}
		localization_key = EU402
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU403_voted
				has_global_flag = focus_EU403_QMV_voted
				has_global_flag = focus_EU403_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU403
					has_country_flag = focus_EU403_QMV
					has_country_flag = focus_EU403_maj
				}
			}
		}
		localization_key = EU403
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU404_voted
				has_global_flag = focus_EU404_QMV_voted
				has_global_flag = focus_EU404_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU404
					has_country_flag = focus_EU404_QMV
					has_country_flag = focus_EU404_maj
				}
			}
		}
		localization_key = EU404
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU405_voted
				has_global_flag = focus_EU405_QMV_voted
				has_global_flag = focus_EU405_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU405
					has_country_flag = focus_EU405_QMV
					has_country_flag = focus_EU405_maj
				}
			}
		}
		localization_key = EU405
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU406_voted
				has_global_flag = focus_EU406_QMV_voted
				has_global_flag = focus_EU406_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU406
					has_country_flag = focus_EU406_QMV
					has_country_flag = focus_EU406_maj
				}
			}
		}
		localization_key = EU406
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU407_voted
				has_global_flag = focus_EU407_QMV_voted
				has_global_flag = focus_EU407_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU407
					has_country_flag = focus_EU407_QMV
					has_country_flag = focus_EU407_maj
				}
			}
		}
		localization_key = EU407
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU408_voted
				has_global_flag = focus_EU408_QMV_voted
				has_global_flag = focus_EU408_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU408
					has_country_flag = focus_EU408_QMV
					has_country_flag = focus_EU408_maj
				}
			}
		}
		localization_key = EU408
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU601_voted
				has_global_flag = focus_EU601_QMV_voted
				has_global_flag = focus_EU601_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU601
					has_country_flag = focus_EU601_QMV
					has_country_flag = focus_EU601_maj
				}
			}
		}
		localization_key = EU601
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU602_voted
				has_global_flag = focus_EU602_QMV_voted
				has_global_flag = focus_EU602_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU602
					has_country_flag = focus_EU602_QMV
					has_country_flag = focus_EU602_maj
				}
			}
		}
		localization_key = EU602
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU603_voted
				has_global_flag = focus_EU603_QMV_voted
				has_global_flag = focus_EU603_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU603
					has_country_flag = focus_EU603_QMV
					has_country_flag = focus_EU603_maj
				}
			}
		}
		localization_key = EU603
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU703_voted
				has_global_flag = focus_EU703_QMV_voted
				has_global_flag = focus_EU703_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU703
					has_country_flag = focus_EU703_QMV
					has_country_flag = focus_EU703_maj
				}
			}
		}
		localization_key = EU703
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU110_voted
				has_global_flag = focus_EU110_QMV_voted
				has_global_flag = focus_EU110_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU110
					has_country_flag = focus_EU110_QMV
					has_country_flag = focus_EU110_maj
				}
			}
		}
		localization_key = EU110
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU111_voted
				has_global_flag = focus_EU111_QMV_voted
				has_global_flag = focus_EU111_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU111
					has_country_flag = focus_EU111_QMV
					has_country_flag = focus_EU111_maj
				}
			}
		}
		localization_key = EU111
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU112_voted
				has_global_flag = focus_EU112_QMV_voted
				has_global_flag = focus_EU112_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU112
					has_country_flag = focus_EU112_QMV
					has_country_flag = focus_EU112_maj
				}
			}
		}
		localization_key = EU112
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU605_voted
				has_global_flag = focus_EU605_QMV_voted
				has_global_flag = focus_EU605_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU605
					has_country_flag = focus_EU605_QMV
					has_country_flag = focus_EU605_maj
				}
			}
		}
		localization_key = EU605
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU606_voted
				has_global_flag = focus_EU606_QMV_voted
				has_global_flag = focus_EU606_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU606
					has_country_flag = focus_EU606_QMV
					has_country_flag = focus_EU606_maj
				}
			}
		}
		localization_key = EU606
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU607_voted
				has_global_flag = focus_EU607_QMV_voted
				has_global_flag = focus_EU607_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU607
					has_country_flag = focus_EU607_QMV
					has_country_flag = focus_EU607_maj
				}
			}
		}
		localization_key = EU607
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU608_voted
				has_global_flag = focus_EU608_QMV_voted
				has_global_flag = focus_EU608_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU608
					has_country_flag = focus_EU608_QMV
					has_country_flag = focus_EU608_maj
				}
			}
		}
		localization_key = EU608
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU609_voted
				has_global_flag = focus_EU609_QMV_voted
				has_global_flag = focus_EU609_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU609
					has_country_flag = focus_EU609_QMV
					has_country_flag = focus_EU609_maj
				}
			}
		}
		localization_key = EU609
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU610_voted
				has_global_flag = focus_EU610_QMV_voted
				has_global_flag = focus_EU610_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU610
					has_country_flag = focus_EU610_QMV
					has_country_flag = focus_EU610_maj
				}
			}
		}
		localization_key = EU610
	}
	text = {
		trigger = {
			OR = {
				has_global_flag = focus_EU611_voted
				has_global_flag = focus_EU611_QMV_voted
				has_global_flag = focus_EU611_maj_voted
			}
			any_of_scopes = {
				array = global.EU_member
				OR = {
					has_country_flag = focus_EU611
					has_country_flag = focus_EU611_QMV
					has_country_flag = focus_EU611_maj
				}
			}
		}
		localization_key = EU611
	}


}

##########################################
### [Loc for EU Voting Events Focuses] ###
##########################################

### EU Exit Loc ###

defined_text = {
	name = ALB_EU_exit_Loc
	text = { trigger = { ALB = { has_country_flag = Article_50 } }
		localization_key = ALB_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ARM_EU_exit_Loc
	text = { trigger = { ARM = { has_country_flag = Article_50 } }
		localization_key = ARM_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = AUS_EU_exit_Loc
	text = { trigger = { AUS = { has_country_flag = Article_50 } }
		localization_key = AUS_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = AZE_EU_exit_Loc
	text = { trigger = { AZE = { has_country_flag = Article_50 } }
		localization_key = AZE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = BEL_EU_exit_Loc
	text = { trigger = { BEL = { has_country_flag = Article_50 } }
		localization_key = BEL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = BLR_EU_exit_Loc
	text = { trigger = { BLR = { has_country_flag = Article_50 } }
		localization_key = BLR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = BOS_EU_exit_Loc
	text = { trigger = { BOS = { has_country_flag = Article_50 } }
		localization_key = BOS_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = BUL_EU_exit_Loc
	text = { trigger = { BUL = { has_country_flag = Article_50 } }
		localization_key = BUL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CAT_EU_exit_Loc
	text = { trigger = { CAT = { has_country_flag = Article_50 } }
		localization_key = CAT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CRE_EU_exit_Loc
	text = { trigger = { CRE = { has_country_flag = Article_50 } }
		localization_key = CRE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CRO_EU_exit_Loc
	text = { trigger = { CRO = { has_country_flag = Article_50 } }
		localization_key = CRO_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CYP_EU_exit_Loc
	text = { trigger = { CYP = { has_country_flag = Article_50 } }
		localization_key = CYP_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = CZE_EU_exit_Loc
	text = { trigger = { CZE = { has_country_flag = Article_50 } }
		localization_key = CZE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = DEN_EU_exit_Loc
	text = { trigger = { DEN = { has_country_flag = Article_50 } }
		localization_key = DEN_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ENG_EU_exit_Loc
	text = { trigger = { ENG = { has_country_flag = Article_50 } }
		localization_key = ENG_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = EST_EU_exit_Loc
	text = { trigger = { EST = { has_country_flag = Article_50 } }
		localization_key = EST_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = FIN_EU_exit_Loc
	text = { trigger = { FIN = { has_country_flag = Article_50 } }
		localization_key = FIN_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = FRA_EU_exit_Loc
	text = { trigger = { FRA = { has_country_flag = Article_50 } }
		localization_key = FRA_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = FYR_EU_exit_Loc
	text = { trigger = { FYR = { has_country_flag = Article_50 } }
		localization_key = FYR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = GEO_EU_exit_Loc
	text = { trigger = { GEO = { has_country_flag = Article_50 } }
		localization_key = GEO_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = GER_EU_exit_Loc
	text = { trigger = { GER = { has_country_flag = Article_50 } }
		localization_key = GER_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = GRE_EU_exit_Loc
	text = { trigger = { GRE = { has_country_flag = Article_50 } }
		localization_key = GRE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = HOL_EU_exit_Loc
	text = { trigger = { HOL = { has_country_flag = Article_50 } }
		localization_key = HOL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = HUN_EU_exit_Loc
	text = { trigger = { HUN = { has_country_flag = Article_50 } }
		localization_key = HUN_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ICE_EU_exit_Loc
	text = { trigger = { ICE = { has_country_flag = Article_50 } }
		localization_key = ICE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = IRE_EU_exit_Loc
	text = { trigger = { IRE = { has_country_flag = Article_50 } }
		localization_key = IRE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ITA_EU_exit_Loc
	text = { trigger = { ITA = { has_country_flag = Article_50 } }
		localization_key = ITA_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = KOS_EU_exit_Loc
	text = { trigger = { KOS = { has_country_flag = Article_50 } }
		localization_key = KOS_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = LAT_EU_exit_Loc
	text = { trigger = { LAT = { has_country_flag = Article_50 } }
		localization_key = LAT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = LIT_EU_exit_Loc
	text = { trigger = { LIT = { has_country_flag = Article_50 } }
		localization_key = LIT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = LUX_EU_exit_Loc
	text = { trigger = { LUX = { has_country_flag = Article_50 } }
		localization_key = LUX_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = MLT_EU_exit_Loc
	text = { trigger = { MLT = { has_country_flag = Article_50 } }
		localization_key = MLT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = MLV_EU_exit_Loc
	text = { trigger = { MLV = { has_country_flag = Article_50 } }
		localization_key = MLV_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = MNT_EU_exit_Loc
	text = { trigger = { MNT = { has_country_flag = Article_50 } }
		localization_key = MNT_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = NOR_EU_exit_Loc
	text = { trigger = { NOR = { has_country_flag = Article_50 } }
		localization_key = NOR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = POL_EU_exit_Loc
	text = { trigger = { POL = { has_country_flag = Article_50 } }
		localization_key = POL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = POR_EU_exit_Loc
	text = { trigger = { POR = { has_country_flag = Article_50 } }
		localization_key = POR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = ROM_EU_exit_Loc
	text = { trigger = { ROM = { has_country_flag = Article_50 } }
		localization_key = ROM_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SCL_EU_exit_Loc
	text = { trigger = { SCL = { has_country_flag = Article_50 } }
		localization_key = SCL_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SCO_EU_exit_Loc
	text = { trigger = { SCO = { has_country_flag = Article_50 } }
		localization_key = SCO_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SER_EU_exit_Loc
	text = { trigger = { SER = { has_country_flag = Article_50 } }
		localization_key = SER_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SLO_EU_exit_Loc
	text = { trigger = { SLO = { has_country_flag = Article_50 } }
		localization_key = SLO_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SLV_EU_exit_Loc
	text = { trigger = { SLV = { has_country_flag = Article_50 } }
		localization_key = SLV_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SPR_EU_exit_Loc
	text = { trigger = { SPR = { has_country_flag = Article_50 } }
		localization_key = SPR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SWE_EU_exit_Loc
	text = { trigger = { SWE = { has_country_flag = Article_50 } }
		localization_key = SWE_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = SWI_EU_exit_Loc
	text = { trigger = { SWI = { has_country_flag = Article_50 } }
		localization_key = SWI_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = TUR_EU_exit_Loc
	text = { trigger = { TUR = { has_country_flag = Article_50 } }
		localization_key = TUR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = UKR_EU_exit_Loc
	text = { trigger = { UKR = { has_country_flag = Article_50 } }
		localization_key = UKR_Article_50_YES
	}
	text = { localization_key = "" }
}
defined_text = {
	name = WAS_EU_exit_Loc
	text = { trigger = { WAS = { has_country_flag = Article_50 } }
		localization_key = WAS_Article_50_YES
	}
	text = { localization_key = "" }
}
