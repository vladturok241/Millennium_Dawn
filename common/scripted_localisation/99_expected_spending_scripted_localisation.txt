defined_text = {
	name = GET_WELFARE_SPENDING_LEVEL_1_COLOR
	text = {
		trigger = {
			check_variable = { THIS.social_budget > 0 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_WELFARE_SPENDING_LEVEL_2_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 1 }
			check_variable = { THIS.social_budget > 1 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 1 }
			check_variable = { THIS.social_budget > 1 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 1 }
			check_variable = { THIS.social_budget = 1 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_WELFARE_SPENDING_LEVEL_3_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 3 }
			check_variable = { THIS.social_budget > 2 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 2 }
			check_variable = { THIS.social_budget > 2 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 2 }
			check_variable = { THIS.social_budget < 3 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_WELFARE_SPENDING_LEVEL_4_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 4 }
			check_variable = { THIS.social_budget > 3 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 3 }
			check_variable = { THIS.social_budget > 3 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 3 }
			check_variable = { THIS.social_budget < 4 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_WELFARE_SPENDING_LEVEL_5_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 5 }
			check_variable = { THIS.social_budget > 4 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 4 }
			check_variable = { THIS.social_budget > 4 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending > 4 }
			check_variable = { THIS.social_budget < 5 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_WELFARE_SPENDING_LEVEL_6_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending < 6 }
			check_variable = { THIS.social_budget > 5 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 6 }
			check_variable = { THIS.social_budget = 6 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_welfare_spending = 6 }
			check_variable = { THIS.social_budget < 6 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}

defined_text = {
	name = GET_HEALTH_SPENDING_LEVEL_1_COLOR
	text = {
		trigger = {
			check_variable = { THIS.health_budget > 0 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_HEALTH_SPENDING_LEVEL_2_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 1 }
			check_variable = { THIS.health_budget > 1 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 1 }
			check_variable = { THIS.health_budget > 1 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 1 }
			check_variable = { THIS.health_budget = 1 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_HEALTH_SPENDING_LEVEL_3_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 3 }
			check_variable = { THIS.health_budget > 2 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 2 }
			check_variable = { THIS.health_budget > 2 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 2 }
			check_variable = { THIS.health_budget < 3 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_HEALTH_SPENDING_LEVEL_4_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 4 }
			check_variable = { THIS.health_budget > 3 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 3 }
			check_variable = { THIS.health_budget > 3 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 3 }
			check_variable = { THIS.health_budget < 4 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_HEALTH_SPENDING_LEVEL_5_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 5 }
			check_variable = { THIS.health_budget > 4 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 4 }
			check_variable = { THIS.health_budget > 4 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending > 4 }
			check_variable = { THIS.health_budget < 5 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_HEALTH_SPENDING_LEVEL_6_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending < 6 }
			check_variable = { THIS.health_budget > 5 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 6 }
			check_variable = { THIS.health_budget = 6 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_health_spending = 6 }
			check_variable = { THIS.health_budget < 6 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}

defined_text = {
	name = GET_ADM_SPENDING_LEVEL_1_COLOR
	text = {
		trigger = {
			check_variable = { THIS.bureaucracy > 0 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_ADM_SPENDING_LEVEL_2_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 1 }
			check_variable = { THIS.bureaucracy > 1 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 1 }
			check_variable = { THIS.bureaucracy > 1 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 1 }
			check_variable = { THIS.bureaucracy = 1 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_ADM_SPENDING_LEVEL_3_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending < 3 }
			check_variable = { THIS.bureaucracy > 2 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 2 }
			check_variable = { THIS.bureaucracy > 2 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 2 }
			check_variable = { THIS.bureaucracy < 3 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_ADM_SPENDING_LEVEL_4_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending < 4 }
			check_variable = { THIS.bureaucracy > 3 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 3 }
			check_variable = { THIS.bureaucracy > 3 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending > 3 }
			check_variable = { THIS.bureaucracy < 4 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_ADM_SPENDING_LEVEL_5_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending < 5 }
			check_variable = { THIS.bureaucracy > 4 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 5 }
			check_variable = { THIS.bureaucracy = 5 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_adm_spending = 5 }
			check_variable = { THIS.bureaucracy < 5 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}

defined_text = {
	name = GET_EDU_SPENDING_LEVEL_1_COLOR
	text = {
		trigger = {
			check_variable = { THIS.education_budget > 0 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_EDU_SPENDING_LEVEL_2_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 1 }
			check_variable = { THIS.education_budget > 1 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 1 }
			check_variable = { THIS.education_budget > 1 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 1 }
			check_variable = { THIS.education_budget = 1 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_EDU_SPENDING_LEVEL_3_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending < 3 }
			check_variable = { THIS.education_budget > 2 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 2 }
			check_variable = { THIS.education_budget > 2 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 2 }
			check_variable = { THIS.education_budget < 3 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_EDU_SPENDING_LEVEL_4_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending < 4 }
			check_variable = { THIS.education_budget > 3 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 3 }
			check_variable = { THIS.education_budget > 3 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending > 3 }
			check_variable = { THIS.education_budget < 4 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_EDU_SPENDING_LEVEL_5_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending < 5 }
			check_variable = { THIS.education_budget > 4 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 5 }
			check_variable = { THIS.education_budget = 5 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_edu_spending = 5 }
			check_variable = { THIS.education_budget < 5 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}

defined_text = {
	name = GET_POLICE_SPENDING_LEVEL_1_COLOR
	text = {
		trigger = {
			check_variable = { THIS.crime_fighting > 0 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_POLICE_SPENDING_LEVEL_2_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 1 }
			check_variable = { THIS.crime_fighting > 1 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 1 }
			check_variable = { THIS.crime_fighting > 1 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 1 }
			check_variable = { THIS.crime_fighting = 1 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_POLICE_SPENDING_LEVEL_3_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending < 3 }
			check_variable = { THIS.crime_fighting > 2 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 2 }
			check_variable = { THIS.crime_fighting > 2 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 2 }
			check_variable = { THIS.crime_fighting < 3 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_POLICE_SPENDING_LEVEL_4_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending < 4 }
			check_variable = { THIS.crime_fighting > 3 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 3 }
			check_variable = { THIS.crime_fighting > 3 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending > 3 }
			check_variable = { THIS.crime_fighting < 4 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_POLICE_SPENDING_LEVEL_5_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending < 5 }
			check_variable = { THIS.crime_fighting > 4 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 5 }
			check_variable = { THIS.crime_fighting = 5 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_police_spending = 5 }
			check_variable = { THIS.crime_fighting < 5 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}

defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_1_COLOR
	text = {
		trigger = {
			check_variable = { THIS.Military_Spending > 0 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_2_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 1 }
			check_variable = { THIS.Military_Spending > 1 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 1 }
			check_variable = { THIS.Military_Spending > 1 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 1 }
			check_variable = { THIS.Military_Spending = 1 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_3_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 3 }
			check_variable = { THIS.Military_Spending > 2 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 2 }
			check_variable = { THIS.Military_Spending > 2 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 2 }
			check_variable = { THIS.Military_Spending < 3 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_4_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 4 }
			check_variable = { THIS.Military_Spending > 3 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 3 }
			check_variable = { THIS.Military_Spending > 3 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 3 }
			check_variable = { THIS.Military_Spending < 4 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_5_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 5 }
			check_variable = { THIS.Military_Spending > 4 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 4 }
			check_variable = { THIS.Military_Spending > 4 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 4 }
			check_variable = { THIS.Military_Spending < 5 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_6_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 6 }
			check_variable = { THIS.Military_Spending > 5 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 5 }
			check_variable = { THIS.Military_Spending > 5 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 5 }
			check_variable = { THIS.Military_Spending < 6 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_7_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 7 }
			check_variable = { THIS.Military_Spending > 6 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 6 }
			check_variable = { THIS.Military_Spending > 6 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 6 }
			check_variable = { THIS.Military_Spending < 7 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_8_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 8 }
			check_variable = { THIS.Military_Spending > 7 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 7 }
			check_variable = { THIS.Military_Spending > 7 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp > 7 }
			check_variable = { THIS.Military_Spending < 8 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_9_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 9 }
			check_variable = { THIS.Military_Spending > 8 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 9 }
			check_variable = { THIS.Military_Spending = 9 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 9 }
			check_variable = { THIS.Military_Spending < 9 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}
defined_text = {
	name = GET_DEFENCE_SPENDING_LEVEL_10_COLOR
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp < 10 }
			check_variable = { THIS.Military_Spending > 9 }
		}
		localization_key = "GFX_expected_spending_blue"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 10 }
			check_variable = { THIS.Military_Spending = 10 }
		}
		localization_key = "GFX_expected_spending_green"
	}
	text = {
		trigger = {
			check_variable = { THIS.expected_military_sp = 10 }
			check_variable = { THIS.Military_Spending < 10 }
		}
		localization_key = "GFX_expected_spending_red"
	}
	text = {
		localization_key = "GFX_expected_spending_white"
	}
}