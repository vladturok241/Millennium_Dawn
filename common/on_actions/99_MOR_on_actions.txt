on_actions = {
	on_daily_MOR = {
		effect = {
			if = {
				limit = { 
					MOR = {
						has_war_with = SHA
						surrender_progress > 0.09
						NOT = { has_country_flag = MOR_sahrawi_go }
					} 
				}
				MOR = { country_event = egypt_sahar.2 }
			}
		}
	}
}