KOR_daewoo_firearms_materiel_manufacturer = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Daewoo_logo
	name = KOR_daewoo_firearms_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

KOR_hanwha_defense_systems_tank_manufacturer = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Hanwha_defence_systems
	name = KOR_hanwha_defense_systems_tank_manufacturer
	include = generic_specialized_tank_organization
}

KOR_hyundai_rotem_tank_manufacturer = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Hyundai_Rotem
	name = KOR_hyundai_rotem_tank_manufacturer
	include = generic_tank_equipment_organization
}

KOR_hanwha_techwin_tank_manufacturer = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Hanwha_techwin
	name = KOR_hanwha_techwin_tank_manufacturer
	# TODO: Replace with artillery 
	include = generic_tank_equipment_organization
}

KOR_korea_aerospace_industries_tank_manufacturer = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Korea_Aerospace_Industries
	name = KOR_korea_aerospace_industries_tank_manufacturer
	include = generic_specialized_helicopter_organization
}

KOR_korea_aerospace_industries_aircraft_manufacturer = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Korea_Aerospace_Industries
	name = KOR_korea_aerospace_industries_aircraft_manufacturer
	include = generic_air_equipment_organization
}

KOR_hanjin_heavy_industries_naval_manufacturer = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Hanjin_Heavy_Industries
	name = KOR_hanjin_heavy_industries_naval_manufacturer
	include = generic_naval_light_equipment_organization
}

KOR_hyundai_heavy_industries_naval_manufacturer = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Hyundai_Heavy_Industries
	name = KOR_hyundai_heavy_industries_naval_manufacturer
	include = generic_naval_light_equipment_organization
}

KOR_daewoo_shipbuilding_naval_manufacturer = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Daewoo_Shipbuilding
	name = KOR_daewoo_shipbuilding_naval_manufacturer
	include = generic_naval_equipment_organization
}

KOR_hyundai_heavy_industries_naval_manufacturer2 = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Hyundai_Heavy_Industries
	name = KOR_hyundai_heavy_industries_naval_manufacturer2
	include = generic_sub_equipment_organization
}

KOR_daewoo_shipbuilding_naval_manufacturer2 = {
	allowed = { original_tag = KOR }
	icon = GFX_idea_Daewoo_Shipbuilding
	name = KOR_daewoo_shipbuilding_naval_manufacturer2
	include = generic_sub_equipment_organization
}