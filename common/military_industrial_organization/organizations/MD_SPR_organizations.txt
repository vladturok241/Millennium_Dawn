SPR_cetme_materiel_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_CETME
	name = SPR_cetme_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

SPR_gmv_materiel_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_gmv
	name = SPR_gmv_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

SPR_instalaza_materiel_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_istalaza
	name = SPR_instalaza_materiel_manufacturer
	include = generic_aa_at_equipment_organization
}

SPR_indra_materiel_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_indra
	name = SPR_indra_materiel_manufacturer
	include = generic_infantry_equipment_organization
}

SPR_santa_barbara_sistemas_tank_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_indra
	name = SPR_santa_barbara_sistemas_tank_manufacturer
	include = generic_tank_equipment_organization
}

SPR_urovesa_tank_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_uro
	name = SPR_urovesa_tank_manufacturer
	include = generic_tank_equipment_organization
}

SPR_enasa_tank_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_pegaso
	name = SPR_enasa_tank_manufacturer
	include = generic_tank_equipment_organization
}

SPR_expal_tank_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_expal
	name = SPR_expal_tank_manufacturer
	include = generic_tank_equipment_organization
}

SPR_navantia_naval_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_Navantia
	name = SPR_navantia_naval_manufacturer
	include = generic_naval_equipment_organization
}

SPR_escribano_naval_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_escribano
	name = SPR_escribano_naval_manufacturer
	include = generic_naval_equipment_organization
}

SPR_navantia_naval_manufacturer2 = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_escribano
	name = SPR_navantia_naval_manufacturer2
	include = generic_naval_equipment_organization
}

SPR_airbus_defence_aircraft_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_Airbus_Defence
	name = SPR_airbus_defence_aircraft_manufacturer
	include = generic_air_equipment_organization
}

SPR_casa_aircraft_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_casa
	name = SPR_casa_aircraft_manufacturer
	include = generic_air_equipment_organization
}

SPR_airbus_helicopters_tank_manufacturer = {
	allowed = { original_tag = SPR }
	icon = GFX_idea_Airbus_helicopters
	name = SPR_airbus_helicopters_tank_manufacturer
	include = generic_air_equipment_organization
}