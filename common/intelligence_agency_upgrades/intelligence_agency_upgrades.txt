# Author(s): AngriestBird
# List of intelligence agency upgrades, and their effects
# For documentation, there is a file _documentation.info in the same folder.
branch_intelligence = {
	upgrade_economy_civilian = {
		picture = GFX_agency_economic_agency
		frame = GFX_upgrade_frame_economy
		sound = Spy_Agency_Intel_Upgrades_Confirm
		ai_will_do = {
			factor = 5
			modifier = {
				has_war = no
				factor = 2
			}
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		##Capped to 50% boost
		level = {
			modifier = {
				civilian_intel_factor = 0.10
			}

			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_economy_civilian"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				civilian_intel_factor = 0.10
			}

			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_economy_civilian"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				civilian_intel_factor = 0.10
			}

			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_economy_civilian"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				civilian_intel_factor = 0.10
			}

			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_economy_civilian"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				civilian_intel_factor = 0.10
			}

			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_economy_civilian"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}
	upgrade_army_department = {
		picture = GFX_agency_army_department
		frame = GFX_upgrade_frame_army
		sound = Spy_Agency_Intel_Upgrades_Confirm
		ai_will_do = {
			factor = 0
			modifier = {
				has_war = yes
				add = 4
			}

			modifier = {
				any_neighbor_country = {
					has_added_tension_amount > 0
				}
				add = 1
			}
			modifier = {
				threat > 0.20
				add = 1
			}
			modifier = {
				threat > 0.30
				add = 1
			}
			modifier = {
				threat > 0.40
				add = 1
			}
			modifier = {
				threat > 0.50
				add = 1
			}
			modifier = {
				threat > 0.60
				add = 1
			}
			modifier = {
				threat > 0.70
				add = 1
			}
			modifier = {
				threat > 0.80
				add = 1
			}
			modifier = {
				threat > 0.90
				add = 1
			}
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				army_intel_factor = 0.10
				navy_intel_factor = 0.10
				airforce_intel_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_army_department"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				army_intel_factor = 0.10
				navy_intel_factor = 0.10
				airforce_intel_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_army_department"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				army_intel_factor = 0.10
				navy_intel_factor = 0.10
				airforce_intel_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_army_department"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				army_intel_factor = 0.10
				navy_intel_factor = 0.10
				airforce_intel_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_army_department"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				army_intel_factor = 0.10
				navy_intel_factor = 0.10
				airforce_intel_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_army_department"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}
	upgrade_computer_servers = {
		picture = GFX_agency_computer_server
		frame = GFX_upgrade_frame_economy
		sound = Spy_Agency_Intel_Upgrades_Confirm
		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		available = {
			has_tech = computing3
			custom_trigger_tooltip = {
				tooltip = upgrade_computer_servers_TT
				has_done_agency_upgrade = upgrade_form_department
			}
		}
		level = {
			modifier = {
				civilian_intel_decryption_bonus = 1
				army_intel_decryption_bonus = 1
				navy_intel_decryption_bonus = 1
				airforce_intel_decryption_bonus = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_computer_servers"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				civilian_intel_decryption_bonus = 2
				army_intel_decryption_bonus = 2
				navy_intel_decryption_bonus = 2
				airforce_intel_decryption_bonus = 2
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_computer_servers"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				civilian_intel_decryption_bonus = 2
				army_intel_decryption_bonus = 2
				navy_intel_decryption_bonus = 2
				airforce_intel_decryption_bonus = 2
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_computer_servers"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
	}
}

branch_defense = {
	upgrade_passive_defense = {
		picture = GFX_agency_defense_department
		frame = GFX_upgrade_frame_defense
		sound = Spy_Agency_Defense_Upgrades_Confirm
		ai_will_do = {
			factor = 2
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				intelligence_agency_defense = 1.5
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_passive_defense"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				intelligence_agency_defense = 1.5
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_passive_defense"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				intelligence_agency_defense = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_passive_defense"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				intelligence_agency_defense = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_passive_defense"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}

	upgrade_anti_partisan = {
		picture = GFX_agency_counter_insurgency
		sound = Spy_Agency_Defense_Upgrades_Confirm
		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				root_out_resistance_effectiveness_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_anti_partisan"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				root_out_resistance_effectiveness_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_anti_partisan"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				root_out_resistance_effectiveness_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_anti_partisan"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				root_out_resistance_effectiveness_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_anti_partisan"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				root_out_resistance_effectiveness_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_anti_partisan"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}
}

branch_operation = {
	upgrade_blueprint_stealing = {
		picture = GFX_agency_steal_blueprints
		sound = Spy_Agency_Operations_Upgrades_Confirm
		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				operation_steal_tech_outcome = 0.15
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_blueprint_stealing"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				operation_steal_tech_outcome = 0.15
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_blueprint_stealing"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				operation_steal_tech_outcome = 0.15
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_blueprint_stealing"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}
	upgrade_portable_radios = {
		picture = GFX_agency_agency_secured_communication
		sound = Spy_Agency_Operations_Upgrades_Confirm
		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}
		available = {
			has_tech = internet3
		}
		level = {
			modifier = {
				operation_coordinated_strike_outcome = 0.50
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_portable_radios"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				operation_coordinated_strike_outcome = 0.50
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_portable_radios"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				operation_coordinated_strike_outcome = 0.50
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_portable_radios"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
	}
	upgrade_invisible_ink = {
		picture = GFX_agency_agency_discrete_listening_devices
		sound = Spy_Agency_Operations_Upgrades_Confirm
		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				intel_from_operatives_factor = 0.15
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_invisible_ink"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				intel_from_operatives_factor = 0.15
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_invisible_ink"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				intel_from_operatives_factor = 0.15
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_invisible_ink"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}

	}

	upgrade_plastic_explosives = {
		picture = GFX_agency_plastic_explosives
		sound = Spy_Agency_Operations_Upgrades_Confirm

		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				target_sabotage_factor = 0.25
				boost_resistance_factor = 0.25
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_plastic_explosives"
				set_temp_variable = { treasury_change = -25 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				target_sabotage_factor = 0.25
				boost_resistance_factor = 0.25
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_plastic_explosives"
				set_temp_variable = { treasury_change = -25 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				target_sabotage_factor = 0.25
				boost_resistance_factor = 0.25
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_plastic_explosives"
				set_temp_variable = { treasury_change = -25 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				target_sabotage_factor = 0.25
				boost_resistance_factor = 0.25
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_plastic_explosives"
				set_temp_variable = { treasury_change = -25 }
				modify_treasury_effect = yes
			}
		}
	}

	upgrade_suicide_pills = {
		picture = GFX_agency_suicide_pills
		sound = Spy_Agency_Operations_Upgrades_Confirm
		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				operative_death_on_capture_chance = 0.05
				own_operative_detection_chance = -0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_suicide_pills"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				operative_death_on_capture_chance = 0.05
				own_operative_detection_chance = -0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_suicide_pills"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				operative_death_on_capture_chance = 0.05
				own_operative_detection_chance = -0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_suicide_pills"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				operative_death_on_capture_chance = 0.05
				own_operative_detection_chance = -0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_suicide_pills"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				operative_death_on_capture_chance = 0.05
				own_operative_detection_chance = -0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_suicide_pills"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}
}

branch_operative = {
	upgrade_training_centers = {
		picture = GFX_agency_localized_training_center
		Sound = Spy_Agency_Training_Facilities_Upgrades_Confirm

		ai_will_do = {
			factor = 2
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				enemy_operative_recruitment_chance = 0.05
				occupied_operative_recruitment_chance = 0.05
				operative_slot = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_training_centers"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
				unlock_decision_category_tooltip = lar_local_recruitment
			}
		}
		level = {
			modifier = {
				enemy_operative_recruitment_chance = 0.05
				occupied_operative_recruitment_chance = 0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_training_centers"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				enemy_operative_recruitment_chance = 0.05
				occupied_operative_recruitment_chance = 0.05
				operative_slot = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_training_centers"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				enemy_operative_recruitment_chance = 0.05
				occupied_operative_recruitment_chance = 0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_training_centers"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				enemy_operative_recruitment_chance = 0.05
				occupied_operative_recruitment_chance = 0.05
				operative_slot = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_training_centers"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}

	}

	upgrade_commando_training = {
		picture = GFX_agency_commando_training
		sound = Spy_Agency_Training_Facilities_Upgrades_Confirm

		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				commando_trait_chance_factor = 0.50 # 50%
				special_forces_attack_factor = 0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_commando_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				commando_trait_chance_factor = 0.50 #50%
				special_forces_no_supply_grace = 24
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_commando_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				special_forces_out_of_supply_factor = -0.1
				special_forces_defence_factor = 0.05
				experience_loss_factor = -0.01
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_commando_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}

	upgrade_interrogation_techniques = {
		picture = GFX_agency_interrogation_techniques
		sound = Spy_Agency_Training_Facilities_Upgrades_Confirm

		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				enemy_operative_capture_chance_factor = 0.10
				enemy_operative_intel_extraction_rate = 0.05 # +25%
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_interrogation_techniques"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				enemy_operative_capture_chance_factor = 0.10
				enemy_operative_intel_extraction_rate = 0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_interrogation_techniques"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				enemy_operative_capture_chance_factor = 0.10
				enemy_operative_intel_extraction_rate = 0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_interrogation_techniques"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				enemy_operative_capture_chance_factor = 0.10
				enemy_operative_intel_extraction_rate = 0.05
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_interrogation_techniques"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				enemy_operative_intel_extraction_rate = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_interrogation_techniques"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}

	}

	upgrade_diplo_training = {
		picture = GFX_agency_diplomatic_training
		sound = Spy_Agency_Training_Facilities_Upgrades_Confirm

		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				control_trade_mission_factor = 0.10
				diplomatic_pressure_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				control_trade_mission_factor = 0.10
				diplomatic_pressure_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				control_trade_mission_factor = 0.10
				diplomatic_pressure_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				control_trade_mission_factor = 0.10
				diplomatic_pressure_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				control_trade_mission_factor = 0.10
				diplomatic_pressure_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}

	upgrade_psycho_warfare = {
		picture = GFX_agency_psychology_warfare
		sound = Spy_Agency_Training_Facilities_Upgrades_Confirm

		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				propaganda_mission_factor = 0.10
				boost_ideology_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				propaganda_mission_factor = 0.10
				boost_ideology_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				propaganda_mission_factor = 0.10
				boost_ideology_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				propaganda_mission_factor = 0.10
				boost_ideology_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				propaganda_mission_factor = 0.10
				boost_ideology_mission_factor = 0.10
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_diplo_training"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}
}

branch_crypto = {
	upgrade_form_department = {
		picture = GFX_agency_cyber_security
		frame = GFX_upgrade_frame_form
		sound = Spy_Agency_Cryptology_Upgrades_Confirm

		ai_will_do = {
			factor = 2
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}

		level = {
			modifier = {
				crypto_department_enabled = 1
				crypto_strength = 1
				decryption_power = 40
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_form_department"
				set_temp_variable = { treasury_change = -25 }
				modify_treasury_effect = yes
			}
		}

	}

	upgrade_decryption_boost = {
		picture = GFX_agency_cyber_warfare
		frame = GFX_upgrade_frame_economy
		sound = Spy_Agency_Cryptology_Upgrades_Confirm
		ai_will_do = {
			factor = 2
			modifier = {
				num_of_civilian_factories > 40
				add = 2
			}
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}
		available = {
			has_done_agency_upgrade = upgrade_form_department
		}
		level = {
			modifier = {
				decryption_power = 25
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_decryption_boost"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				decryption_power = 25
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_decryption_boost"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				decryption_power = 25
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_decryption_boost"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				decryption_power = 25
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_decryption_boost"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}

	upgrade_crypto_strength = {
		picture = GFX_agency_cyber_security
		frame = GFX_upgrade_frame_defense
		sound = Spy_Agency_Cryptology_Upgrades_Confirm
		ai_will_do = {
			factor = 2
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}
		available = {
			has_done_agency_upgrade = upgrade_form_department
		}
		level = {
			modifier = {
				crypto_strength = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_crypto_strength"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				crypto_strength = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_crypto_strength"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				crypto_strength = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_crypto_strength"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				crypto_strength = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_crypto_strength"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				crypto_strength = 1
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_crypto_strength"
				set_temp_variable = { treasury_change = -10 }
				modify_treasury_effect = yes
			}
		}
	}
	upgrade_crypto_strength_2 = {
		picture = GFX_agency_agency_machine_learning
		frame = GFX_upgrade_frame_defense
		sound = Spy_Agency_Cryptology_Upgrades_Confirm
		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0
				OR = {
					has_active_mission = bankruptcy_incoming_collapse
					check_variable = { interest_rate > 10.00 }
				}
			}
		}
		available = {
			has_done_agency_upgrade = upgrade_crypto_strength
			has_tech = sociable_machines
		}
		level = {
			modifier = {
				crypto_strength = 2
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_crypto_strength_2"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				crypto_strength = 2
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_crypto_strength_2"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
		level = {
			modifier = {
				crypto_strength = 2
			}
			complete_effect = {
				log = "[GetDateText]: [Root.GetName]: Intelligence Agency Upgrade upgrade_crypto_strength_2"
				set_temp_variable = { treasury_change = -30 }
				modify_treasury_effect = yes
			}
		}
	}
}
