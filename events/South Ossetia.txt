﻿add_namespace = Ossetia
add_namespace = OssetiaNews
country_event = {
	id = Ossetia.1
	title = Ossetia.1.t
	desc = Ossetia.1.d
	is_triggered_only = yes
	option = {
		name = Ossetia.1.a
		log = "[GetDateText]: [This.GetName]: Ossetia.1.a executed"
		SOO = {	
			set_country_flag = SOV_subject_agree
			set_country_flag = SUB_russ_polit
			update_party_name = yes
			country_event = Ossetia.2 
		}
		SOV = {
			set_autonomy = {
				target = SOO
				autonomy_state = autonomy_republic_rf
			}
			add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
			reverse_add_opinion_modifier = { target = ROOT modifier = has_expressed_loyalty }
		}
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = Ossetia.1.b
		log = "[GetDateText]: [This.GetName]: Ossetia.1.b executed"
		SOO = { country_event = Ossetia.3 }
		SOV = {
			add_opinion_modifier = { target = SOO modifier = major_breach_of_trust }
			reverse_add_opinion_modifier = { target = SOO modifier = major_breach_of_trust }
		}
		ai_chance = {
			base = 0
		}
	}
}
country_event = {
	id = Ossetia.2
	title = Ossetia.2.t
	desc = Ossetia.2.d
	is_triggered_only = yes
	option = {
		name = Ossetia.1.a
		log = "[GetDateText]: [This.GetName]: Ossetia.2.a executed"
	}
}
country_event = {
	id = Ossetia.3
	title = Ossetia.3.t
	desc = Ossetia.3.d
	is_triggered_only = yes
	option = {
		name = Ossetia.3.a
		log = "[GetDateText]: [This.GetName]: Ossetia.3.a executed"
	}
}
country_event = {
	id = Ossetia.4
	title = Ossetia.4.t
	desc = Ossetia.4.d
	is_triggered_only = yes
	option = {
		name = Ossetia.4.a
		log = "[GetDateText]: [This.GetName]: Ossetia.4.a executed"
		SOO = {
			transfer_state = 673
			country_event = Ossetia.5
		}
		ai_chance = {
			base = 50
		}
	}
	option = {
		name = Ossetia.4.b
		log = "[GetDateText]: [This.GetName]: Ossetia.4.a executed"
		SOO = { country_event = Ossetia.6 }
		ai_chance = {
			base = 50
		}
	}
}
country_event = {
	id = Ossetia.5
	title = Ossetia.5.t
	desc = Ossetia.5.d
	is_triggered_only = yes
	option = {
		name = Ossetia.5.a
		log = "[GetDateText]: [This.GetName]: Ossetia.5.a executed"
	}
}
country_event = {
	id = Ossetia.6
	title = Ossetia.6.t
	desc = Ossetia.6.d
	is_triggered_only = yes
	option = {
		name = Ossetia.6.a
		log = "[GetDateText]: [This.GetName]: Ossetia.6.a executed"
	}
}
country_event = {
	id = Ossetia.7
	title = Ossetia.7.t
	desc = Ossetia.7.d
	is_triggered_only = yes
	option = {
		name = Ossetia.7.a
		log = "[GetDateText]: [This.GetName]: Ossetia.7.a executed"
		SOO = {
			country_event = Ossetia.8
		}
		ai_chance = {
			base = 50
		}
	}
	option = {
		name = Ossetia.7.b
		log = "[GetDateText]: [This.GetName]: Ossetia.7.a executed"
		SOO = { country_event = Ossetia.9 }
		ai_chance = {
			base = 50
		}
	}
}
country_event = {
	id = Ossetia.8
	title = Ossetia.8.t
	desc = Ossetia.8.d
	is_triggered_only = yes
	option = {
		name = Ossetia.8.a
		log = "[GetDateText]: [This.GetName]: Ossetia.8.a executed"
		add_to_faction = FROM
		673 = { transfer_state_to = SOO }
	}
}
country_event = {
	id = Ossetia.9
	title = Ossetia.9.t
	desc = Ossetia.9.d
	is_triggered_only = yes
	option = {
		name = Ossetia.9.a
		log = "[GetDateText]: [This.GetName]: Ossetia.9.a executed"
	}
}

country_event = {
	id = Ossetia.10
	title = Ossetia.10.t
	desc = Ossetia.10.d
	picture = GFX_ssg_geo
	is_triggered_only = yes
	option = {
		name = Ossetia.10.a
		log = "[GetDateText]: [This.GetName]: Ossetia.10.a executed"
		add_stability = 0.09
	}
}

country_event = {
	id = Ossetia.11
	title = Ossetia.11.t
	desc = Ossetia.11.d
	picture = GFX_southern_options
	is_triggered_only = yes
	option = {
		name = Ossetia.11.a
		log = "[GetDateText]: [This.GetName]: Ossetia.11.a executed"
		retire_country_leader = yes
		add_stability = 0.05
		custom_effect_tooltip = SOO_kakoyti_tt
	}
}

country_event = {
	id = Ossetia.12
	title = Ossetia.12.t
	desc = Ossetia.12.d
#	picture = GFX_lego_store
	is_triggered_only = yes
	option = {
		name = Ossetia.12.a
		log = "[GetDateText]: [This.GetName]: Ossetia.12.a executed"
		hidden_effect = {
			clear_array = ruling_party
			clear_array = gov_coalition_array
			add_to_array = { ruling_party = 21 }
			update_government_coalition_strength = yes
			update_party_name = yes
			set_coalition_drift = yes
			meta_effect = {
				text = {
					set_country_flag = [META_SET_RULING_PARTY]
				}
				META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
			}
			set_leader = yes
			create_country_leader = {
				name = "Daurbek fyrt Khiutsau I"
				picture = "gfx/leaders/SOO/daurbek_makeev2.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism
					warmonger
					spirit_of_genghis
					inexperienced_imperialist
				}
			}
			add_popularity = {
				ideology = nationalist
				popularity = 0.05
			}
		}
	}
		option = {
			name = Ossetia.12.b
			log = "[GetDateText]: [This.GetName]: Ossetia.12.b executed"
			hidden_effect = {
				clear_array = ruling_party
				clear_array = gov_coalition_array
				add_to_array = { ruling_party = 23 }
				update_government_coalition_strength = yes
				update_party_name = yes
				set_coalition_drift = yes
				meta_effect = {
					text = {
						set_country_flag = [META_SET_RULING_PARTY]
					}
					META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
				}
				set_leader = yes
				create_country_leader = {
					name = "Marat Karabugaev"
					picture = "gfx/leaders/SOO/marat_karabugaev.dds"
					ideology = Monarchist
					traits = {
						nationalist_Monarchist
						spirit_of_genghis
						doctor
						ecological_economist
						king
						polished
					}
				}
				add_popularity = {
					ideology = nationalist
					popularity = 0.05
				}
			}
	}
	option = {
		name = Ossetia.12.c
		log = "[GetDateText]: [This.GetName]: Ossetia.12.c executed"
		hidden_effect = {
			set_politics = {
				ruling_party = nationalist
			}
			add_popularity = {
				ideology = nationalist
				popularity = 0.05
			}
		}
	}
}

country_event = {
	id = Ossetia.13
	title = Ossetia.13.t
	desc = Ossetia.13.d
	picture = GFX_SOV_generic
	is_triggered_only = yes
	option = {
		name = Ossetia.13.a
		log = "[GetDateText]: [This.GetName]: Ossetia.13.a executed"
		SOO = {
			add_ideas = SOO_help_ossetia
			country_event = Ossetia.14
		}
		set_temp_variable = { treasury_change = -10 }
		modify_treasury_effect = yes
		add_ideas = SOO_help_ossetia_SOV
		add_to_variable = { int_investments = 10 }
		set_temp_variable = { percent_change = 5 }
		set_temp_variable = { tag_index = SOV }
		set_temp_variable = { influence_target = SOO }
		change_influence_percentage = yes
		ai_chance = {
			base = 100
		}
	}
	option = {
		name = Ossetia.13.b
		log = "[GetDateText]: [This.GetName]: Ossetia.13.b executed"
		SOO = {
			country_event = Ossetia.15
		}
		ai_chance = {
			base = 0
		}
	}

}

country_event = {
	id = Ossetia.14
	title = Ossetia.14.t
	desc = Ossetia.14.d
	picture = GFX_SOV_generic
	is_triggered_only = yes
	option = {
		name = Ossetia.14.a
		log = "[GetDateText]: [This.GetName]: Ossetia.14.a executed"
		ai_chance = {
			base = 100
		}
		custom_effect_tooltip = SOO_russian_bank_tt_focus
		set_temp_variable = { treasury_change = 10.00 }
		modify_treasury_effect = yes
		add_country_leader_trait = pro_russia
	}
}

country_event = {
	id = Ossetia.15
	title = Ossetia.15.t
	desc = Ossetia.15.d
	picture = GFX_SOV_generic
	is_triggered_only = yes
	option = {
		name = Ossetia.15.a
		log = "[GetDateText]: [This.GetName]: Ossetia.15.a executed"
		ai_chance = {
			base = 00
		}
	}
}
country_event = {
	id = Ossetia.16
	title = Ossetia.16.t
	desc = Ossetia.16.d
	is_triggered_only = yes

	option = {
		name = Ossetia.16.a
		log = "[GetDateText]: [This.GetName]: Ossetia.16.a executed"
		add_ideas = CIS_two_member_state_idea
	}
}
