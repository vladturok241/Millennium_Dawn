﻿add_namespace = eritrea

# Appoint a Chief Executive
country_event = {
	id = eritrea.1
	title = eritrea.1.t
	desc = eritrea.1.d
	picture = GFX_court

	trigger = {
		focus_progress = {
			focus = ERI_eritrea_puppet_federation_appoint_chief_executive
		progress > 0.2
		}
	}

	fire_only_once = yes

	option = {
		name = eritrea.1.a	### Christian candidate
		log = "[GetDateText]: [This.GetName]: eritrea.1.a executed"
		create_country_leader = {
			name = "Ahmed Nasser"
			picture = "Portrait_Ahmed_Nasser.dds"
			ideology = Western_Autocracy
		}
		add_popularity = { ideology = democratic popularity = 0.3 }
		set_politics = { ruling_party = democratic elections_allowed = no }
		set_country_flag = ahmed_nasser_cf
	}

	option = {
		name = eritrea.1.b	### Muslim candidate
		log = "[GetDateText]: [This.GetName]: eritrea.1.b executed"
		create_country_leader = {
			name = "Hussein Kelifay"
			picture = "Portrait_Hussein_Kelifay.dds"
			ideology = Neutral_Muslim_Brotherhood
		}
		add_popularity = { ideology = neutrality popularity = 0.1 }
		set_politics = { ruling_party = neutrality elections_allowed = no }
		set_country_flag = hussein_kelifay_cf
	}
}

# AMHARIC LANGUAGE SCHOOLS OPEN
country_event = {
	id = eritrea.2
	title = eritrea.2.t
	desc = eritrea.2.d
	picture = GFX_african_school

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = eritrea.2.a
		log = "[GetDateText]: [This.GetName]: eritrea.2.a executed"
		add_autonomy_ratio = {
			value = -0.2
		}
		add_stability = 0.2
	}
}

# ERITREAN BRANCHES OF ETHIOPIAN PARTIES #
country_event = {
	id = eritrea.3
	title = eritrea.3.t
	desc = eritrea.3.d
	picture = GFX_political_deal

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = eritrea.3.a
		log = "[GetDateText]: [This.GetName]: eritrea.3.a executed"
		set_temp_variable = { party_index = 4 }
		set_temp_variable = { party_popularity_increase = 0.1 }
		add_relative_party_popularity = yes
		set_temp_variable = { party_index = 5 }
		set_temp_variable = { party_popularity_increase = 0.1 }
		add_relative_party_popularity = yes
		set_temp_variable = { party_index = 14 }
		set_temp_variable = { party_popularity_increase = 0.1 }
		add_relative_party_popularity = yes
	}
	option = {
		name = eritrea.3.b
		log = "[GetDateText]: [This.GetName]: eritrea.3.b executed"
		set_temp_variable = { party_index = 2 }
		set_temp_variable = { party_popularity_increase = 0.1 }
		add_relative_party_popularity = yes
		set_temp_variable = { party_index = 18 }
		set_temp_variable = { party_popularity_increase = 0.1 }
		add_relative_party_popularity = yes
	}
	option = {
		name = eritrea.3.c
		log = "[GetDateText]: [This.GetName]: eritrea.3.c executed"
		set_temp_variable = { party_index = 23 }
		set_temp_variable = { party_popularity_increase = 0.3 }
		add_relative_party_popularity = yes
		hidden_effect = {
			country_event = {
				id = eritrea.4
				days = 60
			}
		}
	}
}

# IT'S JIHADIN' TIME
country_event = {
	id = eritrea.4
	title = eritrea.4.t
	desc = eritrea.4.d
	picture = GFX_political_deal

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = eritrea.4.a
		log = "[GetDateText]: [This.GetName]: eritrea.4.a executed"
		set_temp_variable = { party_index = 11 }
		set_temp_variable = { party_popularity_increase = 0.5 }
		add_relative_party_popularity = yes
	}
	option = {
		name = eritrea.4.b
		log = "[GetDateText]: [This.GetName]: eritrea.4.b executed"
		set_temp_variable = { party_index = 23 }
		set_temp_variable = { party_popularity_increase = 0.1 }
		add_relative_party_popularity = yes
	}
}