add_namespace = hezbollah_md
add_namespace = hezbollah_border_war
add_namespace = hezbollah_events
add_namespace = international_hezbollah
add_namespace = message_from_iran

country_event = {
	id = hezbollah_events.1
	title = hezbollah_events.1.t
	desc = hezbollah_events.1.d
	picture = GFX_laf_defectors
	is_triggered_only = yes

	option = {
		name = hezbollah_events.1.o1
		log = "[GetDateText]: [This.GetName]:hezbollah_events.1.o1 executed"
		if = {
			limit = {
				LEB = {
					influence_higher_50 = yes
				}
			}
			add_manpower = 5000
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = 1000
				producer = LEB
			}
			add_equipment_to_stockpile = {
				type = cnc_equipment
				amount = 1000
				producer = LEB
			}
			add_equipment_to_stockpile = {
				type = APC_Equipment
				amount = 200
				producer = LEB
			}
			add_equipment_to_stockpile = {
				type = util_vehicle_equipment
				amount = 300
				producer = LEB
			}
			add_equipment_to_stockpile = {
				type = mbt_hull
				amount = 100
				producer = LEB
			}
		}
		else_if = {
			limit = {
				LEB = {
					influence_higher_30 = yes
				}
			}
			add_manpower = 3000
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = 1000
				producer = LEB
			}
			add_equipment_to_stockpile = {
				type = cnc_equipment
				amount = 1000
				producer = LEB
			}
			add_equipment_to_stockpile = {
				type = APC_Equipment
				amount = 100
				producer = LEB
			}
			add_equipment_to_stockpile = {
				type = util_vehicle_equipment
				amount = 100
				producer = LEB
			}
		}
		else_if = {
			limit = {
				LEB = {
					influence_higher_10 = yes
				}
			}
			add_manpower = 2000
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = 1000
				producer = LEB
			}
			add_equipment_to_stockpile = {
				type = cnc_equipment
				amount = 1000
				producer = LEB
			}
		}
		else_if = {
			limit = {
				LEB = {
					influence_higher_5 = yes
				}
			}
			add_manpower = 1000
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = 1000
				producer = LEB
			}
		}
	}
}
country_event = {
	id = hezbollah_events.2
	title = hezbollah_events.2.t
	desc = hezbollah_events.2.d
	picture = GFX_hezbollah_asks_iran
	is_triggered_only = yes

	option = {
		name = hezbollah_events.2.o1
		log = "[GetDateText]: [This.GetName]:hezbollah_events.1.o1 executed"
		HEZ = { add_political_power = 150 set_temp_variable = { treasury_change = 20 } modify_treasury_effect = yes } 
		add_political_power = -150 set_temp_variable = { treasury_change = -20 } modify_treasury_effect = yes
	} 
	option = {
		name = hezbollah_events.2.o2
		log = "[GetDateText]: [This.GetName]:hezbollah_events.1.o2 executed"
		HEZ = { add_political_power = 150 } 
		add_political_power = -150
	}
	option = {
		name = hezbollah_events.2.o3
		log = "[GetDateText]: [This.GetName]:hezbollah_events.1.o3 executed"
		HEZ = { set_temp_variable = { treasury_change = 20 } modify_treasury_effect = yes } 
		set_temp_variable = { treasury_change = -20 } modify_treasury_effect = yes
	}
	option = {
		name = hezbollah_events.2.o4
		log = "[GetDateText]: [This.GetName]:hezbollah_events.1.o4 executed"
		set_temp_variable = { percent_change = -15 }
		set_temp_variable = { tag_index = PER }
		set_temp_variable = { influence_target = HEZ }
		change_influence_percentage = yes
	}
}
country_event = {
	id = hezbollah_events.3 # Seeking Allies
	title = hezbollah_events.3.t
	desc = hezbollah_events.3.d
	picture = GFX_hezbollah_seeking_allies
	is_triggered_only = yes

	option = { # Deny
		name = hezbollah_events.3.o1
		log = "[GetDateText]: [This.GetName]:hezbollah_events.3.o1 executed"
		ai_chance = {
			factor = 3
			modifier = {
				factor = 0.2
				has_opinion = {
					target = HEZ
					value > 0
				}
			}
		}
	}
	option = { # Equipment
		name = hezbollah_events.3.o2
		log = "[GetDateText]: [This.GetName]:hezbollah_events.3.o2 executed"
		ai_chance = {
			factor = 2
			modifier = {
				factor = 0.3
				has_opinion = {
					target = HEZ
					value > 0
				}
			}
		}
		HEZ = {
			add_equipment_to_stockpile = {
				type = Inf_equipment
				amount = 1000
				producer = ROOT
			}
			add_equipment_to_stockpile = {
				type = cnc_equipment
				amount = 500
				producer = ROOT
			}
			add_equipment_to_stockpile = {
				type = AA_Equipment
				amount = 200
				producer = ROOT
			}
			add_equipment_to_stockpile = {
				type = L_AT_Equipment
				amount = 200
				producer = ROOT
			}
		}
		set_temp_variable = { treasury_change = -5 }
		modify_treasury_effect = yes
	}
	option = { # Financial Aid
		name = hezbollah_events.3.o3
		log = "[GetDateText]: [This.GetName]:hezbollah_events.3.o3 executed"
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0.4
				has_opinion = {
					target = HEZ
					value > 5
				}
			}
		}
		set_temp_variable = { treasury_change = gdp_total_display }
		multiply_temp_variable = { treasury_change = 0.01  }
		HEZ = {
			modify_treasury_effect = yes
		}
	}
	option = { # Investment
		name = hezbollah_events.3.o4
		log = "[GetDateText]: [This.GetName]:hezbollah_events.3.o4 executed"
		ai_chance = {
			factor = 1
			modifier = {
				factor = 0.5
				has_opinion = {
					target = HEZ
					value > 5
				}
				has_government = communism
			}
		}
		HEZ = {
			random_owned_controlled_state = {
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
		   		}
			}
		}
		set_temp_variable = { percent_change = 2 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = HEZ } 
		change_influence_percentage = yes
		set_temp_variable = { treasury_change = -7.5 }
		modify_treasury_effect = yes
	}
}
	news_event = {
		id = hezbollah_events.4
		title = hezbollah_events.4.t
		desc = hezbollah_events.4.d
		picture = GFX_news_SAU_YEM_Missile
		major = yes
		is_triggered_only = yes
		option = {
			name = hezbollah_events.4.a
			log = "[GetDateText]: [This.GetName]: hezbollah_events.4.a executed"
		}
	}
	country_event = {
		id = hezbollah_events.5 # lets fuck the americans
		title = hezbollah_events.5.t
		desc = hezbollah_events.5.d
		picture = GFX_crying_american_soldier
		is_triggered_only = yes
		option = { # ANOTHER 33 DAYS WAR.
			name = hezbollah_events.5.o1
			log = "[GetDateText]: [This.GetName]: hezbollah_events.5.o1 executed"
			USA = {
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = -500
			}
			add_equipment_to_stockpile = {
				type = cnc_equipment
				amount = -350
			}
			add_equipment_to_stockpile = {
				type = Anti_tank_2
				amount = -300
			}
			add_equipment_to_stockpile = {
				type = Anti_air_2
				amount = -100
			}
			add_manpower = -450
		}
			start_border_war = {
				change_state_after_war = no
				attacker = {
					state = 205
					num_provinces = 1
					on_win = axis_of_resistance_events.7
					on_lose = axis_of_resistance_events.7
					on_cancel = axis_of_resistance_events.7
					modifier = 0.1
					dig_in_factor = 0
					terrain_factor = 0
				}	
				defender = {
					state = 201
					num_provinces = 1
					on_win = axis_of_resistance_events.7
					on_lose = axis_of_resistance_events.7
					on_cancel = axis_of_resistance_events.7
				}
			}
			set_country_flag = HEZ_33_days
			news_event = hezbollah_events.27
		}
		option = { # US ATTACKS HEZBOLLAHI BASES AND INFLUENCE IN IRAQ.
			name = hezbollah_events.5.o2
			log = "[GetDateText]: [This.GetName]: hezbollah_events.5.o2 executed"
			USA = {
				add_equipment_to_stockpile = {
					type = infantry_weapons
					amount = -500
				}
				add_equipment_to_stockpile = {
					type = cnc_equipment
					amount = -350
				}
				add_equipment_to_stockpile = {
					type = Anti_tank_2
					amount = -300
				}
				add_equipment_to_stockpile = {
					type = Anti_air_2
					amount = -100
				}
				add_manpower = -450
			}
			HEZ = {
				add_manpower = -400
				add_equipment_to_stockpile = {
					type = infantry_weapons
					amount = -500
					producer = HEZ
				}
				add_equipment_to_stockpile = {
					type = command_control_equipment
					amount = -150
					producer = HEZ
				}
				add_equipment_to_stockpile = {
					type = Anti_tank_0
					amount = -100
					producer = HEZ
				}
				add_equipment_to_stockpile = {
					type = Anti_Air_0
					amount = -100
					producer = HEZ
				}
				set_temp_variable = { percent_change = -3 }
				set_temp_variable = { tag_index = HEZ }
				set_temp_variable = { influence_target = IRQ } 
				change_influence_percentage = yes
				add_stability = -0.05
				add_war_support = -0.05
			}
			set_country_flag = HEZ_attack
			news_event = hezbollah_events.27
		}
		option = { # US PRESSURES HEZBOLLAH.
			name = hezbollah_events.5.o3
 			log = "[GetDateText]: [This.GetName]: hezbollah_events.5.o3 executed"
			USA = {
				add_equipment_to_stockpile = {
					type = infantry_weapons
					amount = -500
				}
				add_equipment_to_stockpile = {
					type = cnc_equipment
					amount = -350
				}
				add_equipment_to_stockpile = {
					type = Anti_tank_2
					amount = -300
				}
				add_equipment_to_stockpile = {
					type = Anti_air_2
					amount = -100
				}
				add_manpower = -450
			}
			HEZ = {
			add_political_power = -150
			add_stability = -0.1
			}
			news_event = hezbollah_events.27
			set_country_flag = HEZ_pressure
		}
		option = { # US DOES NOTHING.
			name = hezbollah_events.5.o4
			log = "[GetDateText]: [This.GetName]: hezbollah_events.5.o4 executed"
			USA = {
				add_equipment_to_stockpile = {
					type = infantry_weapons
					amount = -500
				}
				add_equipment_to_stockpile = {
					type = cnc_equipment
					amount = -350
				}
				add_equipment_to_stockpile = {
					type = Anti_tank_2
					amount = -300
				}
				add_equipment_to_stockpile = {
					type = Anti_air_2
					amount = -100
				}
				add_manpower = -450
			}
			add_political_power = -150 
			add_war_support = -0.05
			set_country_flag = HEZ_nothing
			news_event = hezbollah_events.27
		}
	}


#EVENTS FOR TIMED EXECUTION OF EFFECTS OF INTERNATIONAL HEZBOLLAH MISSIONS
country_event = {
	id = international_hezbollah.1 # Recruit Militias
	title = international_hezbollah.1.t #needs loc
	desc = international_hezbollah.1.d
	picture = GFX_recruit_militia_soldiers
	is_triggered_only = yes

	immediate = {
		set_variable = { the_country_that_hezbollah_is_currently_doing_a_mission_in = 0 }
	}

	option = {
		name = international_hezbollah.1.o1
		log = "[GetDateText]: [This.GetName]: international_hezbollah.1.o1 executed"
		#Effects of Recruit Militia Mission
		add_manpower = 1000
	}
}
country_event = {
	id = international_hezbollah.2 # Sell Weapons
	title = international_hezbollah.2.t #needs loc
	desc = international_hezbollah.2.d
 	picture = GFX_sell_weapons_ak
	is_triggered_only = yes

	immediate = {
		set_variable = { the_country_that_hezbollah_is_currently_doing_a_mission_in = 0 }
	}

	option = {
		name = international_hezbollah.2.o1
		log = "[GetDateText]: [This.GetName]: international_hezbollah.2 executed"
		#Effects of Sell Weapons Mission
		custom_effect_tooltip = add_005b_weekly
		hidden_effect = {
			add_to_variable = { hezbollah_weekly_income_from_sold_weapons_variable = 0.05 }
		}
	}
}
country_event = {
	id = international_hezbollah.3 # Influence Nation
	title = international_hezbollah.3.t #needs loc
	desc = international_hezbollah.3.d
	picture = GFX_hezbollah_influence
	is_triggered_only = yes

	immediate = {
		set_variable = { the_country_that_hezbollah_is_currently_doing_a_mission_in = 0 }
	}

	option = {
		name = international_hezbollah.3.o1
		log = "[GetDateText]: [This.GetName]: international_hezbollah.3 executed"
		#Effects of Influence Nation Mission
		var:HEZ_country_to_influence_with_button = {
			set_temp_variable = { party_index = 9 }
			set_temp_variable = { party_popularity_increase = 0.03 }
			set_temp_variable = { temp_outlook_increase = 0.02 }
			add_relative_party_popularity = yes
		}
	}
}
country_event = {
	id = international_hezbollah.4 # Attack Zionists
	title = international_hezbollah.4.t #needs loc
	desc = international_hezbollah.4.d
	picture = GFX_fucked_up_israeli_tank
	is_triggered_only = yes

	immediate = {
		set_variable = { the_country_that_hezbollah_is_currently_doing_a_mission_in = 0 }
	}
	
	option = {
		name = international_hezbollah.4.o1
		log = "[GetDateText]: [This.GetName]: international_hezbollah.4 executed"
		#Effects of Attack Zionists Mission
		if = {
			limit = {
				var:hezbollah_selected_country_for_operation = {
					OR = {
						gives_military_access_to = USA
						is_subject_of = USA
					}
				}
			}
			#Target is America
 			USA = { country_event = { id = international_hezbollah.5 } }
		}
		else_if = {
			limit = {
				var:HEZ_country_to_influence_with_button = {
					OR = {
						gives_military_access_to = ISR
						is_subject_of = ISR
					}
				}
			}
			#Target is Israel
			ISR = { country_event = { id = international_hezbollah.5 } }
		}
	}
}
country_event = {
	id = international_hezbollah.5 # US/Israel receives this when Hezbollah attacks their base.
	title = international_hezbollah.5.t #needs loc
	desc = international_hezbollah.5.d
	picture = GFX_raid_american_base
	is_triggered_only = yes

	option = {
		name = international_hezbollah.5.o1
		log = "[GetDateText]: [This.GetName]: international_hezbollah.5.o1 executed"
		add_manpower = -300
		add_equipment_to_stockpile = {
			type = Inf_equipment
			amount = -400
		}
		add_equipment_to_stockpile = {
			type = cnc_equipment
			amount = -200
		}
		add_equipment_to_stockpile = {
			type = APC_Equipment
			amount = -50
		}
		add_equipment_to_stockpile = {
			type = IFV_Equipment
			amount = -30
		}
		add_equipment_to_stockpile = {
			type = util_vehicle_equipment
			amount = -50
		}
		set_temp_variable = { percent_change = -1 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = var:HEZ_country_to_attack_zionists_with_button }
		change_influence_percentage = yes
	}
}
country_event = {
	id = hezbollah_events.6
	title = hezbollah_events.6.t
	desc = hezbollah_events.6.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.6.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.6.o1 executed"
		HEZ = { #hezbollah winning the moderate border skrimish
		add_equipment_to_stockpile = {
					type = infantry_weapons
					amount = 100
				}
				add_equipment_to_stockpile = {
					type = cnc_equipment
					amount = 30
				}
				add_equipment_to_stockpile = {
					type = Anti_tank_0
					amount = 35
				}
				add_equipment_to_stockpile = {
					type = Anti_air_0
					amount = 35
				}
				army_experience = 15
		}
	}
}
country_event = {
	id = hezbollah_events.7
	title = hezbollah_events.7.t
	desc = hezbollah_events.7.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.7.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.7.o1 executed"
		HEZ = { #hezbollah losing the moderate border skrimish
				army_experience = 20
		}
	}
}
country_event = {
	id = hezbollah_events.8
	title = hezbollah_events.8.t
	desc = hezbollah_events.8.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.8.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.8.o1 executed"
		HEZ = { #hezbollah winning the huge border skrimish
		add_equipment_to_stockpile = {
					type = infantry_weapons
					amount = 150
				}
				add_equipment_to_stockpile = {
					type = cnc_equipment
					amount = 50
				}
				add_equipment_to_stockpile = {
					type = Anti_tank_0
					amount = 100
				}
				add_equipment_to_stockpile = {
					type = Anti_air_0
					amount = 100
				}
				army_experience = 30
		}
	}
}
country_event = {
	id = hezbollah_events.9
	title = hezbollah_events.9.t
	desc = hezbollah_events.9.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.9.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.9.o1 executed"
		HEZ = { #hezbollah losing the huge border skrimish
				army_experience = 40
		}
	}
}
country_event = { #Fuck you warner, I hate you so much
	id = hezbollah_events.10
	title = hezbollah_events.10.t
	desc = hezbollah_events.10.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: hezbollah_events.10.o1 executed"
		name = hezbollah_events.10.o1
		LEB = {
		 #event for lebanon winning the border war
			army_experience = 10
		}
	}
}
country_event = { #Fuck you warner, I hate you so much
	id = hezbollah_events.11
	title = hezbollah_events.11.t
	desc = hezbollah_events.11.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.11.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.11.o1 executed"
		LEB = {
			#event for lebanon losing the border war
			army_experience = 10
		}
	}
}
country_event = { #Fuck you warner, I hate you so much
	id = hezbollah_events.12
	title = hezbollah_events.12.t
	desc = hezbollah_events.12.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.12.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.12.o1 executed"
		ISR = {
			#event for israel winning the border war
			army_experience = 10
		}
	}
}
country_event = { #Fuck you warner, I hate you so much
	id = hezbollah_events.13
	title = hezbollah_events.13.t
	desc = hezbollah_events.13.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.13.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.13.o1 executed"
		ISR = {
			#event for israel losing the border war
			army_experience = 10
		}
	}
}
country_event = {
	id = hezbollah_events.14
	title = hezbollah_events.14.t
	desc = hezbollah_events.14.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.14.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.14.o1 executed"
		HEZ = { #hezbollah winning the small border skrimish leb
		add_equipment_to_stockpile = {
					type = infantry_weapons
					amount = 50
				}
				add_equipment_to_stockpile = {
					type = cnc_equipment
					amount = 10
				}
				add_equipment_to_stockpile = {
					type = Anti_tank_0
					amount = 20
				}
				add_equipment_to_stockpile = {
					type = Anti_air_0
					amount = 20
				}
				army_experience = 5
		}
	}
}
country_event = {
	id = hezbollah_events.15
	title = hezbollah_events.15.t
	desc = hezbollah_events.15.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.15.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.15.o1 executed"
		HEZ = { #hezbollah winning the small border skrimish isr
		add_equipment_to_stockpile = {
					type = infantry_weapons
					amount = 50
				}
				add_equipment_to_stockpile = {
					type = cnc_equipment
					amount = 10
				}
				add_equipment_to_stockpile = {
					type = Anti_tank_2
					amount = 20
				}
				add_equipment_to_stockpile = {
					type = Anti_air_2
					amount = 20
				}
				army_experience = 5
		}
	}
}
country_event = { #Fuck you warner, I hate you so much
	id = hezbollah_events.16
	title = hezbollah_events.16.t
	desc = hezbollah_events.16.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.16.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.16.o1 executed"
		HEZ = {
			#event for hez losing the small war leb
			army_experience = 10
		}
	}
}
country_event = {
	id = hezbollah_events.17
	title = hezbollah_events.17.t
	desc = hezbollah_events.17.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.17.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.17.o1 executed"
		HEZ = { #hezbollah winning the medium border skrimish isr
		add_equipment_to_stockpile = {
					type = infantry_weapons
					amount = 100
				}
				add_equipment_to_stockpile = {
					type = cnc_equipment
					amount = 20
				}
				add_equipment_to_stockpile = {
					type = Anti_tank_2
					amount = 35
				}
				add_equipment_to_stockpile = {
					type = Anti_air_2
					amount = 35
				}
				army_experience = 15
		}
	}
}
country_event = {
	id = hezbollah_events.18
	title = hezbollah_events.18.t
	desc = hezbollah_events.18.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.18.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.18.o1 executed"
		HEZ = { #hezbollah winning the huge border skrimish isr
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = 150
			}
			add_equipment_to_stockpile = {
				type = cnc_equipment
				amount = 50
			}
			add_equipment_to_stockpile = {
				type = Anti_tank_2
				amount = 100
			}
			add_equipment_to_stockpile = {
				type = Anti_air_2
				amount = 100
			}
			army_experience = 30
		}
	}
}
country_event = {
	id = hezbollah_events.19
	title = hezbollah_events.19.t
	desc = hezbollah_events.19.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.19.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.19.o1 executed"
		HEZ = { #hezbolla losing the huge border skrimish isr
			army_experience = 40
		}
	}
}
country_event = {
	id = hezbollah_events.20
	title = hezbollah_events.20.t
	desc = hezbollah_events.20.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		name = hezbollah_events.20.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.20.o1 executed"
		HEZ = { #hezbolla medium the huge border skrimish isr
			army_experience = 20
		}
	}
}
country_event = {
	id = hezbollah_events.21
	title = hezbollah_events.21.t
	desc = hezbollah_events.21.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: hezbollah_events.21.o1 executed"
		name = hezbollah_events.21.o1 #nobody wins isr ver
		HEZ = {
			army_experience = 10
		}
		ISR = {
			army_experience = 10
		}
	}
}
country_event = {
	id = hezbollah_events.22
	title = hezbollah_events.22.t
	desc = hezbollah_events.22.d
	picture = GFX_hezbollah_forces
	is_triggered_only = yes

	option = {
		log = "[GetDateText]: [This.GetName]: hezbollah_events.22.o1 executed"
		name = hezbollah_events.22.o1
		HEZ = {
			army_experience = 10
			  }
		LEB = {
			army_experience = 10
		}
	}
}

country_event = {
 	id = hezbollah_events.23 #Event about Hezbollah attacking jewish sectors in a country, this is used to notify them. (they can decrease chance of this happening by increaseing stability)
	title = hezbollah_events.23.t
	desc = hezbollah_events.23.d
	picture = GFX_sabotaged_amia
	is_triggered_only = yes

	option = {
		name = hezbollah_events.23.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.23.o1 executed"
		add_manpower = -50
		add_political_power = -25
		set_temp_variable = { treasury_change = -0.20 }
		modify_treasury_effect = yes
	}
}
country_event = {
	id = hezbollah_events.24 #Event about Hezbollah cooperating with the Drug Cartles and arming them, this is used to notify the country. (they can decrease chance of this happening by increaseing stability)
	title = hezbollah_events.24.t
	desc = hezbollah_events.24.d
	picture = GFX_soldiers_drug_dealing
	is_triggered_only = yes

	option = {
		name = hezbollah_events.24.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.24.o1 executed"
		add_political_power = -50
		set_temp_variable = { treasury_change = -1.50 }
		modify_treasury_effect = yes
	}
}
country_event = {
	id = hezbollah_events.25 #Event about Hezbollah destabilizing the US, this is used to notify the country. (they can combat Hezbollah's influence in south America to prevent this happening again)
	title = hezbollah_events.25.t
	desc = hezbollah_events.25.d
	picture = GFX_militiant_soldiers_in_america
	is_triggered_only = yes

	option = {
		name = hezbollah_events.25.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.25.o1 executed"
		add_stability = -0.05
		add_timed_idea = {
			idea = HEZ_unknown_cartel_operations
			days = 60
		}
	}
}
country_event = {
	id = hezbollah_events.26 #Event about Hezbollah raiding US bases in Iraq (for USA)
	title = hezbollah_events.26.t
	desc = hezbollah_events.26.d
	picture = GFX_raid_american_base
	is_triggered_only = yes

	option = {
		name = hezbollah_events.26.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.26.o1 executed"
		add_war_support = -0.02
		add_equipment_to_stockpile = {
			type = infantry_weapons
			amount = -450
		}
		add_equipment_to_stockpile = {
			type = cnc_equipment
			amount = -200
		}
		add_equipment_to_stockpile = {
			type = Anti_tank_2
			amount = -150
		}
		add_equipment_to_stockpile = {
			type = Anti_air_2
			amount = -150
		}
		add_manpower = -50
		HEZ = {
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = -300
			}
			add_equipment_to_stockpile = {
				type = cnc_equipment
				amount = -100
			}
			add_equipment_to_stockpile = {
				type = Anti_tank_2
				amount = -50
			}
			add_manpower = -100
		}
	}
	option = {
		name = hezbollah_events.26.o2
		log = "[GetDateText]: [This.GetName]: hezbollah_events.26.o2 executed"
		add_stability = -0.02

		add_equipment_to_stockpile = {
			type = infantry_weapons
			amount = -450
		}
		add_equipment_to_stockpile = {
			type = cnc_equipment
			amount = -200
		}
		add_equipment_to_stockpile = {
			type = Anti_tank_2
			amount = -150
		}
		add_equipment_to_stockpile = {
			type = Anti_air_2
			amount = -150
		}
		add_manpower = -50
	}
}
news_event = {
	id = hezbollah_events.27 #Event about Hezbollah raiding US bases in Iraq (World event)
	title = hezbollah_events.27.t
	desc = hezbollah_events.27.d
	picture = GFX_qasem_soleimani_iraq
	major = yes
	is_triggered_only = yes
	option = {
		name = hezbollah_events.27.a
		log = "[GetDateText]: [This.GetName]: hezbollah_events.27.a executed"
		trigger = {
			AND = {
				OR = { has_idea = shia has_idea = sunni }
				NOT = { OR = { is_ally_with = USA original_tag = USA } }
			}
		}
	}
	option = {
		name = hezbollah_events.27.b
		log = "[GetDateText]: [This.GetName]: hezbollah_events.27.b executed"
		trigger = {
			AND = {
				NOT = { OR = { has_idea = shia has_idea = sunni } }
				OR = { is_ally_with = USA original_tag = USA }
			}
		}
	}
}
country_event = {
	id = hezbollah_events.28 #Event about hezbollah having a chance to attack an Israeli patrol near the lebanese border
	title = hezbollah_events.28.t
	desc = hezbollah_events.28.d
	picture = GFX_israeli_soldier_lebanon
	is_triggered_only = yes

	option = {
		name = hezbollah_events.28.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.28.o1 executed"
		ISR = {
		country_event = hezbollah_events.29
		add_manpower = -5
		add_war_support = 0.1
		add_stability = -0.05
		}
	}
	option = {
		name = hezbollah_events.28.o2
		log = "[GetDateText]: [This.GetName]: hezbollah_events.28.o2 executed"
		add_war_support = -0.1
		add_stability = -0.1
		add_political_power = -100
	}
}
country_event = {
	id = hezbollah_events.29 #Event for Israel intiating a border war against hezbollah
	title = hezbollah_events.29.t
	desc = hezbollah_events.29.d
	picture = GFX_israeli_soldiers_injured
	is_triggered_only = yes
	option = {
		name = hezbollah_events.29.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.29.o1 executed"
		start_border_war = {
			change_state_after_war = no
			attacker = {
				state = 205
				num_provinces = 1
				on_win = hezbollah_events.31
				on_lose = hezbollah_events.30
				on_cancel = hezbollah_events.33
				modifier = 0.1
				dig_in_factor = 0
				terrain_factor = 0
			}
			defender = {
				state = 201
				num_provinces = 1
				on_win = hezbollah_events.30
				on_lose = hezbollah_events.31
				on_cancel = hezbollah_events.33
			}
		}
		add_war_support = 0.1
		add_stability = -0.05
		add_political_power = 50
		HEZ = {
			add_war_support = 0.1
			add_stability = -0.1
			}
			hidden_effect = { country_event = { id = hezbollah_events.34 days = 32 } }
			news_event = hezbollah_events.37
	}
	option = {
		name = hezbollah_events.29.o2
		log = "[GetDateText]: [This.GetName]: hezbollah_events.29.o2 executed"
		add_war_support = -0.1
		add_stability = 0.05
		add_political_power = -100
		HEZ = {
			add_war_support = 0.1
			add_stability = 0.1
			add_political_power = 200
			}
			ai_chance = {
				base = 0
			}
			trigger = { NOT = { is_ai = yes } }
	}
}
country_event = {
	id = hezbollah_events.30 #hezbollah wins the 33 days war.
	title = hezbollah_events.30.t
	desc = hezbollah_events.30.d
	picture = GFX_abumahdi_nasrollah_soleimani_meet
	is_triggered_only = yes

	option = {
		name = hezbollah_events.30.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.30.o1 executed"
		HEZ = {
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = 450
			}
			add_equipment_to_stockpile = {
				type = cnc_equipment
				amount = 200
			}
			add_equipment_to_stockpile = {
				type = Anti_tank_2
				amount = 150
			}
			add_equipment_to_stockpile = {
				type = Anti_air_2
				amount = 150
			}
		}
		army_experience = 50
		ISR = {
			add_war_support = -0.1
			add_political_power = -200
			add_stability = -0.05
		}
		army_experience = 25
		country_event = hezbollah_events.35
	}
}
country_event = {
	id = hezbollah_events.31 #hezbollah loses "game over for the player!", Iran can attack Israel
	title = hezbollah_events.31.t
	desc = hezbollah_events.31.d
	picture = GFX_hezbollahi_soldier_on_tank
	is_triggered_only = yes

	option = {
		name = hezbollah_events.31.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.31.o1 executed"
		201 = { transfer_state_to = ISR }
		ISR = {
			army_experience = 50
			add_war_support = 0.1
			add_political_power = 100
			add_stability = 0.15
			army_experience = 50
		}
		PER = {
			country_event = hezbollah_events.32
		}
	}
}
country_event = {
	id = hezbollah_events.32 #hezbollah loses "game over for the player!", Iran can attack Israel
	title = hezbollah_events.32.t
	desc = hezbollah_events.32.d
	picture = GFX_hezbollahi_soldier_on_tank
	is_triggered_only = yes

	option = {
		name = hezbollah_events.32.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.32.o1 executed"
		declare_war_on = {
			target = ISR
			type = liberate_wargoal
		}
		add_stability = -0.10
		add_war_support = 0.15
	}
	option = {
		name = hezbollah_events.32.o2
		log = "[GetDateText]: [This.GetName]: hezbollah_events.32.o2 executed"
			add_political_power = -100
			add_war_support = -0.05
		ai_chance = {
			base = 0
		}
	}
}
country_event = {
	id = hezbollah_events.33 #draw!
	title = hezbollah_events.33.t
	desc = hezbollah_events.33.d
	picture = GFX_un_soldier_lebanon
	is_triggered_only = yes

	option = {
		name = hezbollah_events.33.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.33.o1 executed"
		HEZ = {
			army_experience = 20
		}
		ISR = {
			army_experience = 20
		}
	}
}
country_event = {
	id = hezbollah_events.34 # hidden event for making the border war last in 33 days
	is_triggered_only = yes

	hidden = yes
	timeout_days = 1
	immediate = {
		log = "[GetDateText]: [This.GetName]: hezbollah_events.34.o1 executed"
		finalize_border_war = {
			defender_win = yes
			attacker = 205
			defender = 201
		}
	}
}
country_event = {
	id = hezbollah_events.35 #after hezbollah wins the border war, the player can declare war on Israel with the help of Iran and syria
	title = hezbollah_events.35.t
	desc = hezbollah_events.35.d
	picture = GFX_quds_lana
	is_triggered_only = yes

	option = {
		name = hezbollah_events.35.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.35.o1 executed"
		declare_war_on = {
			target = ISR
			type = liberate_wargoal
		}
		add_timed_idea = {
			idea = HEZ_the_last_push
			days = 730
		}
		PER = {
			country_event = hezbollah_events.36
		}
		SYR = {
			country_event = hezbollah_events.36
		}
		ai_chance = {
			base = 0
		}
	}
	option = {
		name = hezbollah_events.35.o2
		log = "[GetDateText]: [This.GetName]: hezbollah_events.35.o2 executed"
	}
}
country_event = {
	id = hezbollah_events.36 #iran and syria attacking israel
	title = hezbollah_events.36.t
	desc = hezbollah_events.36.d
	picture = GFX_retake_quds
	is_triggered_only = yes

	option = {
		name = hezbollah_events.36.o1
		log = "[GetDateText]: [This.GetName]: hezbollah_events.36.o1 executed"
		declare_war_on = {
			target = ISR
			type = liberate_wargoal
		}
		add_war_support = 0.05
		add_manpower = 20000
	}
	option = {
		name = hezbollah_events.36.o2
		log = "[GetDateText]: [This.GetName]: hezbollah_events.36.o2 executed"
		ai_chance = {
			base = 0
		}
	}
}
news_event = {
	id = hezbollah_events.37 #the 33 days war.
	title = hezbollah_events.37.t
	desc = hezbollah_events.37.d
	picture = GFX_israeli_lebanese_conflict
	major = yes
	is_triggered_only = yes
	option = {
		name = hezbollah_events.37.a
		log = "[GetDateText]: [This.GetName]: hezbollah_events.37.a executed"
		trigger = { OR = { original_tag = HEZ is_ally_with = PER is_ally_with = HEZ } }
	}
	option = {
		name = hezbollah_events.37.b
		trigger = { OR = { original_tag = ISR is_ally_with = USA } }
		log = "[GetDateText]: [This.GetName]: hezbollah_events.37.b executed"
	}
	option = {
		name = hezbollah_events.37.c
		trigger = { 
			NOT = {
				OR = {
					original_tag = HEZ
					original_tag = ISR
					is_ally_with = USA
					is_ally_with = PER
					is_ally_with = HEZ
				}
			}
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.37.c executed"
	}
}
news_event = {
	id = hezbollah_events.38 # The bastard survives
	title = hezbollah_events.38.t
	desc = hezbollah_events.38.d
	picture = GFX_rushdie_survives
	major = yes
	is_triggered_only = yes
	option = {
		name = hezbollah_events.38.a
		trigger = {
			NOT = { OR = { has_idea = shia has_idea = sunni } }
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.38.a executed"
	}
	option = {
		name = hezbollah_events.38.b
		every_country = {
			limit = {
				OR = {
					has_idea = shia
					has_idea = sufi_islam
					has_idea = sunni
					has_idea = ibadi
				}
			}
			custom_effect_tooltip = rushdie_survives_tooltip
		}
		trigger = {
			OR = { has_idea = shia has_idea = sunni  has_idea = ibadi has_idea = sufi_islam }
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.38.b executed"
		add_war_support = 0.01
	}
}
news_event = {
	id = hezbollah_events.39 #the death of rushdie
	title = hezbollah_events.39.t
	desc = hezbollah_events.39.d
	picture = GFX_rushdie_dies
	major = yes
	is_triggered_only = yes
	option = {
		name = hezbollah_events.39.a
		trigger = {
			NOT = { OR = { has_idea = shia has_idea = sunni } }
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.39.a executed"
	}
	option = {
		name = hezbollah_events.39.b
		every_country = {
			limit = {
				OR = {
					has_idea = shia
					has_idea = sufi_islam
					has_idea = sunni
					has_idea = ibadi
				}
			}
			custom_effect_tooltip = rushdie_dies2_tooltip
		}
		trigger = {
			OR = { has_idea = shia has_idea = sunni has_idea = ibadi has_idea = sufi_islam }
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.39.b executed"
		add_war_support = 0.02
		add_political_power = 50
	}
}
news_event = {
	id = hezbollah_events.40 #lebanese unification
	title = hezbollah_events.40.t
	desc = hezbollah_events.40.d
	picture = GFX_news_Hezbollah_Coup
	major = yes
	is_triggered_only = yes
	option = {
		name = hezbollah_events.40.a
		trigger = {
			AND = {
				OR = { has_idea = shia }
			}
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.40.a executed"
	}
	option = {
		name = hezbollah_events.40.b
		trigger = {
			OR = {
				NOT = { OR = { has_idea = shia } }
			}
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.40.b executed"
	}
}
news_event = {
	id = hezbollah_events.41 # second Lebanese Civil war
	title = hezbollah_events.41.t
	desc = hezbollah_events.41.d
	picture = GFX_lebanon_civil_war
	major = yes
	is_triggered_only = yes
	option = {
		name = hezbollah_events.41.a
		trigger = {
			NOT = { OR = { has_idea = shia  } }
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.41.a executed"
	}
	option = {
		name = hezbollah_events.41.b
		trigger = {
			OR = { has_idea = shia }
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.41.b executed"
	}
}
news_event = {
	id = hezbollah_events.42 # Hezbollah attacking Israel
	title = hezbollah_events.42.t
	desc = hezbollah_events.42.d
	picture = GFX_the_great_trial_israel
	major = yes
	is_triggered_only = yes
	option = {
		name = hezbollah_events.42.a
		trigger = {
			NOT = { OR = { has_idea = shia  } }
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.42.a executed"
	}
	option = {
		name = hezbollah_events.42.b
		trigger = {
			OR = { has_idea = shia }
		}
		log = "[GetDateText]: [This.GetName]: hezbollah_events.42.b executed"
		add_war_support = 0.05
	}
}
country_event = {
	id = message_from_iran.1 # Notifies Hezbollah that Iran is aiding them with weapons
	title = message_from_iran.1.t
	desc = message_from_iran.1.d
	picture = GFX_axis_of_resistance_country
	is_triggered_only = yes

	option = {
		name = message_from_iran.1.o1
		log = "[GetDateText]: [This.GetName]: message_from_iran.1.o1 executed"
		add_equipment_to_stockpile = {
			type = Inf_equipment
			amount = 500
			producer = PER
		}
		add_equipment_to_stockpile = {
			type = command_control_equipment1 #C3
			amount = 200
			producer = PER
		}
		add_equipment_to_stockpile = {
			type = Heavy_Anti_tank_1 #Toophan 5/Towsan-1
			amount = 30
			producer = PER
		}
		add_equipment_to_stockpile = {
			type = Anti_Air_1 #Stinger
			amount = 30
			producer = USA
		}
		add_equipment_to_stockpile = {
			type = artillery_0 #D-30
			amount = 10
			producer = SOV
		}
		add_equipment_to_stockpile = {
			type = util_vehicle_0 #Khawar
			amount = 10
			producer = PER
		}
	}
}
country_event = {
	id = message_from_iran.2 # Notifies Hezbollah that Iran is giving them moni
	title = message_from_iran.2.t
	desc = message_from_iran.2.d
	picture = GFX_axis_of_resistance_country
	is_triggered_only = yes

	option = {
		name = message_from_iran.2.o1
		log = "[GetDateText]: [This.GetName]: message_from_iran.2.o1 executed"
		set_temp_variable = { treasury_change = 2.5 }
		modify_treasury_effect = yes
	}
}
country_event = {
	id = message_from_iran.3 # Notifies Hezbollah that Iran is giving factories
	title = message_from_iran.3.t
	desc = message_from_iran.3.d
	picture = GFX_axis_of_resistance_country
	is_triggered_only = yes

	option = {
		name = message_from_iran.3.o1
		log = "[GetDateText]: [This.GetName]: message_from_iran.3.o1 executed"
		add_offsite_building = { type = arms_factory level = 1 }
	}
}