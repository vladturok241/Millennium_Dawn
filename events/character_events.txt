add_namespace = MD_character_army

# general died on battle lost
unit_leader_event = {
	id = MD_character_army.1
	title = MD_character_army.1.t
	desc = MD_character_army.1.d
	is_triggered_only = yes

	option = {
		name = MD_character_army.1.a
		log = "[GetDateText]: [This.GetName]: MD_character_army.1 - [FROM.GetName] has died"
		FROM = { retire = yes }
		add_war_support = -0.02
		ai_chance = { base = 10 }
	}
}

# general died on battle won
unit_leader_event = {
	id = MD_character_army.2
	title = MD_character_army.2.t
	desc = MD_character_army.2.d
	is_triggered_only = yes

	option = {
		name = MD_character_army.2.a
		log = "[GetDateText]: [This.GetName]: MD_character_army.2 - [FROM.GetName] has died"
		FROM = { retire = yes }
		add_war_support = -0.02
		ai_chance = { base = 10 }
	}
}