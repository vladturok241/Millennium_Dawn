﻿add_namespace = lar_operative_event

#operative event - operative captured
operative_leader_event = {
	id = lar_operative_event.1
	title = lar_operative_event.1.t
	desc = lar_operative_event.1.desc

	is_triggered_only = yes

	option = {
		name = lar_operative_event.1.a
		log = "[GetDateText]: [This.GetName]: lar_operative_event.1.a executed"
	}
}

#operative event - operative forced into hiding
operative_leader_event = {
	id = lar_operative_event.2
	title = lar_operative_event.2.t
	desc = lar_operative_event.2.desc

	is_triggered_only = yes

	option = {
		name = lar_operative_event.2.a
		log = "[GetDateText]: [This.GetName]: lar_operative_event.2.a executed"
	}
}

#operative event - operative killed
operative_leader_event = {
	id = lar_operative_event.3
	title = lar_operative_event.3.t
	desc = lar_operative_event.3.desc

	is_triggered_only = yes

	option = {
		name = lar_operative_event.3.a
		log = "[GetDateText]: [This.GetName]: lar_operative_event.3.a executed"
	}
}

#operative event - operative harmed
operative_leader_event = {
	id = lar_operative_event.4
	title = lar_operative_event.4.t
	desc = lar_operative_event.4.desc

	is_triggered_only = yes

	option = {
		name = lar_operative_event.4.a
		log = "[GetDateText]: [This.GetName]: lar_operative_event.4.a executed"
	}
}

#operative event - operative turned
operative_leader_event = {
	id = lar_operative_event.5
	title = lar_operative_event.5.t
	desc = lar_operative_event.5.desc

	is_triggered_only = yes

	option = {
		name = lar_operative_event.5.a
		log = "[GetDateText]: [This.GetName]: lar_operative_event.5.a executed"
		add_unit_leader_trait = operative_double_agent
	}
}