add_namespace = EU_POTEF
add_namespace = EU_POTEF_news

country_event = {
	id = EU_POTEF.1
	title = EU_POTEF.1.t
	desc = EU_POTEF.1.d

	#picture = GFX_eu

	is_triggered_only = yes

	option = { #a
		name = EU_POTEF.1.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF.1.a"
	}
}

news_event = {
	id = EU_POTEF_news.3
	title = EU_POTEF_news.3.t
	desc = EU_POTEF_news.3.d
	picture = GFX_afghanistan_war

	is_triggered_only = yes
	option = {
		name = EU_POTEF.news.3.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news"
	}
}

news_event = {
	id = EU_POTEF_news.4
	title = EU_POTEF_news.4.t
	desc = EU_POTEF_news.4.d
	picture = GFX_afghanistan_war

	is_triggered_only = yes
	option = {
		name = EU_POTEF_news.4.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news"
	}
}



#############################
### POTEF election events ###
#############################

news_event = {
	id = EU_POTEF_news.1
	title = EU_POTEF_news.1.t
	desc = EU_POTEF_news.1.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_POTEF_news.1.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news.1.a"

	}
}

news_event = {
	id = EU_POTEF_news.2
	title = EU_POTEF_news.2.t
	desc = EU_POTEF_news.2.d
	picture = GFX_eu
	major = yes
	is_triggered_only = yes
	option = {
		name = EU_POTEF_news.2.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news.2.a"

	}
}

news_event = {
	id = EU_POTEF_news.5
	title = EU_POTEF_news.5.t
	desc = EU_POTEF_news.5.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_POTEF_news.5.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news.5.a"
	}
}

news_event = {
	id = EU_POTEF_news.6
	title = EU_POTEF_news.6.t
	desc = EU_POTEF_news.6.d
	picture = GFX_eu

	is_triggered_only = yes
	option = {
		name = EU_POTEF_news.6.a
		log = "[GetDateText]: [Root.GetName]: event EU_POTEF_news.6.a"
	}
}
