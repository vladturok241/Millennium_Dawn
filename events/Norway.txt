﻿add_namespace = norway

# Åsta Train Collision
country_event = {
	id = norway.1
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.1" }
	title = norway.1.t
	desc = norway.1.d
	picture = GFX_train_collision
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = norway.1.o1
		log = "[GetDateText]: [This.GetName]: norway.1.o1 executed"
		add_political_power = -50
		set_temp_variable = { arg_popularity = -0.01 }
		add_ruling_outlook_popularity = yes

		ai_chance = {
			factor = 1
		}
	}


	option = {
		name = norway.1.o2
		log = "[GetDateText]: [This.GetName]: norway.1.o2 executed"
		add_stability = -0.03
		set_temp_variable = { arg_popularity = -0.02 }
		add_ruling_outlook_popularity = yes

		ai_chance = {
			factor = 0
		}
	}
}

# Joint Oil Drills
country_event = {
	id = norway.4
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.4" }
	title = norway.4.t
	desc = norway.4.d
	picture = GFX_norwegian_flag
	fire_only_once = yes
	is_triggered_only = yes
	trigger = {
		if = {
			limit = { original_tag = NOR }
			owns_state = 32
		}
		if = {
			limit = { original_tag = ENG }
			owns_state = 14
		}
	}

	option = {
		name = norway.4.o1
		log = "[GetDateText]: [This.GetName]: norway.4.o1 executed"
		if = {
			limit = { original_tag = NOR }
			32 = {
				add_resource = {
					type = oil
					amount = 25
				}
			}
		}
		if = {
			limit = { original_tag = ENG }

			14 = {
				add_resource = {
					type = oil
					amount = 15
				}
			}

		}
	}
}

# New Events

# Sends event to ENG, GER, and FRA about oil investments
country_event = {
	id = norway.5
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.5" }
	title = norway.5.t
	desc = norway.5.d
	#
	#fire_only_once = yes
	is_triggered_only = yes
	# Okay
	option = {
		name = norway.5.a
		log = "[GetDateText]: [This.GetName]: norway.5.a executed"
		random_list = {
			50 = {
				add_resource = {
					type = oil
					amount = 5
					state = 542
				}
			}
			50 = {
				add_resource = {
					type = oil
					amount = 6
					state = 30
				}
			}
		}
		newline = yes
		set_temp_variable = { treasury_change = -7.42 }
		modify_treasury_effect = yes
		newline = yes
		set_temp_variable = { percent_change = 5 }
		set_temp_variable = { tag_index = ROOT }
		set_temp_variable = { influence_target = NOR }
		change_influence_percentage = yes
		newline = yes
		NOR = {
			country_event = {
				id = norway.6
			}
		}
		ai_chance = {
			factor = 1
		}
	}
	# No
	option = {
		name = norway.5.b
		log = "[GetDateText]: [This.GetName]: norway.5.b executed"
		NOR = {
			country_event = {
				id = norway.7
			}
		}
		ai_chance = {
			factor = 0
		}
	}
}
# X Country Accepts!
country_event = {
	id = norway.6
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.6" }
	title = norway.6.t
	desc = norway.6.d
	#
	#fire_only_once = yes
	is_triggered_only = yes
	# Okay
	option = {
		name = norway.6.a
		log = "[GetDateText]: [This.GetName]: norway.6.a executed"
		ai_chance = {
			factor = 1
		}
	}
}
# X Country Rejects
country_event = {
	id = norway.7
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.7" }
	title = norway.7.t
	desc = norway.7.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Okay
	option = {
		name = norway.7.a
		log = "[GetDateText]: [This.GetName]: norway.7.a executed"
		ai_chance = {
			factor = 1
		}
	}
}

# Packages of Aid to the Levant
country_event = {
	id = norway.8
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.8" }
	title = norway.8.t
	desc = norway.8.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Minimal Amount of Aid
	option = {
		name = norway.8.a
		log = "[GetDateText]: [This.GetName]: norway.8.a executed"
		set_temp_variable = { treasury_change = -21.00 }
		modify_treasury_effect = yes
		newline = yes
		set_temp_variable = { percent_change = 4 }
		set_temp_variable = { tag_index = NOR }
		set_temp_variable = { influence_target = PAL }
		change_influence_percentage = yes
		newline = yes
		set_temp_variable = { percent_change = 4 }
		set_temp_variable = { tag_index = NOR }
		set_temp_variable = { influence_target = LEB }
		change_influence_percentage = yes
		newline = yes
		set_temp_variable = { percent_change = 4 }
		set_temp_variable = { tag_index = NOR }
		set_temp_variable = { influence_target = SYR }
		change_influence_percentage = yes
		ai_chance = {
			factor = 1
		}
	}
	# Reasonable Amount of Aid
	option = {
		name = norway.8.b
		log = "[GetDateText]: [This.GetName]: norway.8.a executed"
		set_temp_variable = { treasury_change = -36.00 }
		modify_treasury_effect = yes
		newline = yes
		set_temp_variable = { percent_change = 5 }
		set_temp_variable = { tag_index = NOR }
		set_temp_variable = { influence_target = PAL }
		change_influence_percentage = yes
		newline = yes
		set_temp_variable = { percent_change = 5 }
		set_temp_variable = { tag_index = NOR }
		set_temp_variable = { influence_target = LEB }
		change_influence_percentage = yes
		newline = yes
		set_temp_variable = { percent_change = 5 }
		set_temp_variable = { tag_index = NOR }
		set_temp_variable = { influence_target = SYR }
		change_influence_percentage = yes
		ai_chance = {
			factor = 1
		}
	}
	# Considerable Amount of Aid
	option = {
		name = norway.8.c
		log = "[GetDateText]: [This.GetName]: norway.8.a executed"
		set_temp_variable = { treasury_change = -50.00 }
		modify_treasury_effect = yes
		newline = yes
		set_temp_variable = { percent_change = 10 }
		set_temp_variable = { tag_index = NOR }
		set_temp_variable = { influence_target = PAL }
		change_influence_percentage = yes
		newline = yes
		set_temp_variable = { percent_change = 10 }
		set_temp_variable = { tag_index = NOR }
		set_temp_variable = { influence_target = LEB }
		change_influence_percentage = yes
		newline = yes
		set_temp_variable = { percent_change = 10 }
		set_temp_variable = { tag_index = NOR }
		set_temp_variable = { influence_target = SYR }
		change_influence_percentage = yes
		ai_chance = {
			factor = 1
		}
	}
}

# Norway would like to send us aid!
country_event = {
	id = norway.81
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.81" }
	title = norway.81.t
	desc = norway.81.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Sure
	option = {
		name = norway.81.a
		log = "[GetDateText]: [This.GetName]: norway.81.a executed"
		NOR = {
			country_event = {
				id = norway.8
				days = 1
			}
		}
		ai_chance = {
			base = 1
			modifier = {
				has_opinion = { target = NOR value > 44 }
				add = 10
			}
		}
	}
	# No
	option = {
		name = norway.81.b
		log = "[GetDateText]: [This.GetName]: norway.81.b executed"
		NOR = {
			country_event = {
				id = norway.82
				days = 1
			}
		}
		ai_chance = {
			factor = 1
		}
	}
}
# Aid request rejected
country_event = {
	id = norway.82
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.82" }
	title = norway.82.t
	desc = norway.82.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Ok
	option = {
		name = norway.82.a
		log = "[GetDateText]: [This.GetName]: norway.82.a executed"
		ai_chance = {
			factor = 1
		}
	}
}

#

# Weapons Sales

# Who do we get weapons from?
country_event = {
	id = norway.91
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.91" }
	title = norway.91.t
	desc = norway.91.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Germany
	option = {
		name = norway.91.a
		log = "[GetDateText]: [This.GetName]: norway.91.a executed"
		GER = {
			country_event = {
				id = norway.94
				days = 1
			}
		}
		custom_effect_tooltip = NOR_if_they_aceept_TT
		effect_tooltip = {
			if = {
				limit = { has_dlc = "No Step Back" }
				NOR = {
					set_temp_variable = { treasury_change = -45.29 }
					modify_treasury_effect = yes
				}
				GER = {
					set_temp_variable = { treasury_change = 45.29 }
					modify_treasury_effect = yes
				}
				NOR = {
					add_equipment_to_stockpile = {
						type = mbt_hull_2
						variant_name = "Leopard 2A5"
						amount = 45
						producer = GER
					}
				}
				GER = {
					add_equipment_to_stockpile = {
						type = mbt_hull_2
						variant_name = "Leopard 2A5"
						amount = -45
						producer = GER
					}
				}
			}
			else = {
				NOR = {
					set_temp_variable = { treasury_change = -45.29 }
					modify_treasury_effect = yes
				}
				GER = {
					set_temp_variable = { treasury_change = 45.29 }
					modify_treasury_effect = yes
				}
				NOR = {
					add_equipment_to_stockpile = {
						type = MBT_2
						amount = 45
						producer = GER
					}
				}
				GER = {
					add_equipment_to_stockpile = {
						type = MBT_2
						amount = -45
						producer = GER
					}
				}
			}
			newline = yes
			custom_effect_tooltip = NOR_higher_relations_more_likely_TT
		}
		ai_chance = {
			factor = 1
		}
	}
	# USA
	option = {
		name = norway.91.b
		log = "[GetDateText]: [This.GetName]: norway.91.b executed"
		USA = {
			country_event = {
				id = norway.94
				days = 1
			}
		}
		custom_effect_tooltip = NOR_if_they_aceept_TT
		effect_tooltip = {
			if = {
				limit = { has_dlc = "No Step Back" }
				NOR = {
					set_temp_variable = { treasury_change = -24.29 }
					modify_treasury_effect = yes
				}
				USA = {
					set_temp_variable = { treasury_change = 24.29 }
					modify_treasury_effect = yes
				}
				NOR = {
					add_equipment_to_stockpile = {
						type = ifv_hull_2
						variant_name = "M2 Bradley"
						amount = 125
						producer = USA
					}
				}
				USA = {
					add_equipment_to_stockpile = {
						type = ifv_hull_2
						variant_name = "M2 Bradley"
						amount = -125
						producer = USA
					}
				}
			}
			else = {
				NOR = {
					set_temp_variable = { treasury_change = -35.29 }
					modify_treasury_effect = yes
				}
				USA = {
					set_temp_variable = { treasury_change = 35.29 }
					modify_treasury_effect = yes
				}
				NOR = {
					add_equipment_to_stockpile = {
						type = IFV_2
						amount = 125
						producer = USA
					}
				}
				USA = {
					add_equipment_to_stockpile = {
						type = IFV_2
						amount = -125
						producer = USA
					}
				}
			}
		}
		newline = yes
		custom_effect_tooltip = NOR_higher_relations_more_likely_TT
		ai_chance = {
			factor = 1
		}
	}
}
# German Government approves arms sales!
country_event = {
	id = norway.92
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.92" }
	title = norway.92.t
	desc = norway.92.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Great
	option = {
		name = norway.92.a
		log = "[GetDateText]: [This.GetName]: norway.92.a executed"
		if = {
			limit = { has_dlc = "No Step Back" }
			set_temp_variable = { treasury_change = -45.29 }
			modify_treasury_effect = yes
			GER = {
				set_temp_variable = { treasury_change = 45.29 }
				modify_treasury_effect = yes
			}
			NOR = {
				add_equipment_to_stockpile = {
					type = mbt_hull_2
					variant_name = "Leopard 2A5"
					amount = 45
					producer = GER
				}
			}
			GER = {
				add_equipment_to_stockpile = {
					type = mbt_hull_2
					variant_name = "Leopard 2A5"
					amount = -45
					producer = GER
				}
			}
		}
		else = {
			set_temp_variable = { treasury_change = -45.29 }
			modify_treasury_effect = yes
			GER = {
				set_temp_variable = { treasury_change = 45.29 }
				modify_treasury_effect = yes
			}
			NOR = {
				add_equipment_to_stockpile = {
					type = MBT_2
					amount = 45
					producer = GER
				}
			}
			GER = {
				add_equipment_to_stockpile = {
					type = MBT_2
					amount = -45
					producer = GER
				}
			}
		}
		ai_chance = {
			factor = 1
		}
	}
	# Actually, These Cost Too Much...
	option = {
		name = norway.92.b
		log = "[GetDateText]: [This.GetName]: norway.92.b executed"
		ai_chance = {
			factor = 1
		}
	}
}

# US Government Approves Arms Sales
country_event = {
	id = norway.9
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.9" }
	title = norway.9.t
	desc = norway.9.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Minimal Amount of Aid
	# Reasonable Amount of Aid
	option = {
		name = norway.9.a
		log = "[GetDateText]: [This.GetName]: norway.9.a executed"
		if = {
			limit = { has_dlc = "No Step Back" }
			set_temp_variable = { treasury_change = -24.29 }
			modify_treasury_effect = yes
			USA = {
				set_temp_variable = { treasury_change = 24.29 }
				modify_treasury_effect = yes
			}
			NOR = {
				add_equipment_to_stockpile = {
					type = ifv_hull_2
					variant_name = "M2 Bradley"
					amount = 125
					producer = USA
				}
			}
			USA = {
				add_equipment_to_stockpile = {
					type = ifv_hull_2
					variant_name = "M2 Bradley"
					amount = -125
					producer = USA
				}
			}
		}
		else = {
			set_temp_variable = { treasury_change = -35.29 }
			modify_treasury_effect = yes
			USA = {
				set_temp_variable = { treasury_change = 35.29 }
				modify_treasury_effect = yes
			}
			NOR = {
				add_equipment_to_stockpile = {
					type = IFV_2
					amount = 125
					producer = USA
				}
			}
			USA = {
				add_equipment_to_stockpile = {
					type = IFV_2
					amount = -125
					producer = USA
				}
			}
		}
		ai_chance = {
			factor = 1
		}
	}
	option = {
		name = norway.9.b
		log = "[GetDateText]: [This.GetName]: norway.9.a executed"
		ai_chance = {
			factor = 1
		}
	}
}

# They Reject!
country_event = {
	id = norway.93
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.12" }
	title = norway.93.t
	desc = norway.93.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Okay
	option = {
		name = norway.93.a
		log = "[GetDateText]: [This.GetName]: norway.12.a executed"
		ai_chance = {
			factor = 1
		}
	}
}
# Norway Wants To Purchase Weapons
country_event = {
	id = norway.94
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.12" }
	title = norway.94.t
	desc = norway.94.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Okay
	option = {
		name = norway.94.a
		log = "[GetDateText]: [This.GetName]: norway.94.a executed"
		if = {
			limit = {
				original_tag = USA
			}
			NOR = {
				country_event = {
					id = norway.9
				}
			}
			custom_effect_tooltip = NOR_if_we_accept_TT
			effect_tooltip = {
				if = {
					limit = { has_dlc = "No Step Back" }
					NOR = {
						set_temp_variable = { treasury_change = -24.29 }
						modify_treasury_effect = yes
					}
					USA = {
						set_temp_variable = { treasury_change = 24.29 }
						modify_treasury_effect = yes
					}
					NOR = {
						add_equipment_to_stockpile = {
							type = ifv_hull_2
							variant_name = "M2 Bradley"
							amount = 125
							producer = USA
						}
					}
					USA = {
						add_equipment_to_stockpile = {
							type = ifv_hull_2
							variant_name = "M2 Bradley"
							amount = -125
							producer = USA
						}
					}
				}
				else = {
					set_temp_variable = { treasury_change = -35.29 }
					modify_treasury_effect = yes
					USA = {
						set_temp_variable = { treasury_change = 35.29 }
						modify_treasury_effect = yes
					}
					NOR = {
						add_equipment_to_stockpile = {
							type = IFV_2
							amount = 125
							producer = USA
						}
					}
					USA = {
						add_equipment_to_stockpile = {
							type = IFV_2
							amount = -125
							producer = USA
						}
					}
				}
			}
		}
		if = {
			limit = {
				original_tag = GER
			}
			custom_effect_tooltip = NOR_if_we_accept_TT
			effect_tooltip = {
				if = {
					limit = { has_dlc = "No Step Back" }
					NOR = {
						set_temp_variable = { treasury_change = -45.29 }
						modify_treasury_effect = yes
					}
					GER = {
						set_temp_variable = { treasury_change = 45.29 }
						modify_treasury_effect = yes
					}
					NOR = {
						add_equipment_to_stockpile = {
							type = mbt_hull_2
							variant_name = "Leopard 2A5"
							amount = 45
							producer = GER
						}
					}
					GER = {
						add_equipment_to_stockpile = {
							type = mbt_hull_2
							variant_name = "Leopard 2A5"
							amount = -45
							producer = GER
						}
					}
				}
				else = {
					set_temp_variable = { treasury_change = -45.29 }
					modify_treasury_effect = yes
					GER = {
						set_temp_variable = { treasury_change = 45.29 }
						modify_treasury_effect = yes
					}
					NOR = {
						add_equipment_to_stockpile = {
							type = MBT_2
							amount = 45
							producer = GER
						}
					}
					GER = {
						add_equipment_to_stockpile = {
							type = MBT_2
							amount = -45
							producer = GER
						}
					}
				}
			}
			NOR = {
				country_event = {
					id = norway.92
				}
			}
		}
		ai_chance = {
			base = 1
			modifier = {
				has_opinion = { target = NOR value > 44 }
				add = 10
			}
		}
	}
	# No
	option = {
		name = norway.94.b
		log = "[GetDateText]: [This.GetName]: norway.94.b executed"
		NOR = {
			country_event = {
				id = norway.93
			}
		}
		ai_chance = {
			base = 1
		}
	}
}

# Constructive Dialogue with Russia (Russian Event)
country_event = {
	id = norway.10
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.10" }
	title = norway.10.t
	desc = norway.10.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Okay
	option = {
		name = norway.10.a
		log = "[GetDateText]: [This.GetName]: norway.10.a executed"
		NOR = {
			country_event = {
				id = norway.11
				days = 2
			}
		}
		ai_chance = {
			base = 5
			modifier = {
				has_opinion = { target = NOR value > 20 }
				add = 20
			}
		}
	}

	# No
	option = {
		name = norway.10.b
		log = "[GetDateText]: [This.GetName]: norway.10.b executed"
		NOR = {
			country_event = {
				id = norway.12
				days = 2
			}
		}
		ai_chance = {
			factor = 1
		}
	}
}
# Accepts
country_event = {
	id = norway.11
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.11" }
	title = norway.11.t
	desc = norway.11.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Okay
	option = {
		name = norway.11.a
		log = "[GetDateText]: [This.GetName]: norway.11.a executed"
		set_temp_variable = { receiver_nation = NOR.id }
		set_temp_variable = { sender_nation = SOV.id }
		set_improved_trade_agreement = yes
		ROOT = {
			diplomatic_relation = {
				country = SOV
				relation = non_aggression_pact
			}
		}
		ai_chance = {
			factor = 1
		}
	}
}
# Rejects
country_event = {
	id = norway.12
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.12" }
	title = norway.12.t
	desc = norway.12.d

	#fire_only_once = yes
	is_triggered_only = yes
	# Okay
	option = {
		name = norway.12.a
		log = "[GetDateText]: [This.GetName]: norway.12.a executed"
		ai_chance = {
			factor = 1
		}
	}
}

# The Gays
country_event = {
	id = norway.14
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.14" }
	title = norway.14.t
	desc = norway.14.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Legalization
	option = {
		name = norway.14.a
		log = "[GetDateText]: [This.GetName]: norway.14.a executed"
		add_stability = -0.03
		newline = yes
		set_temp_variable = { temp_opinion = 10 }
		change_labour_unions_opinion = yes
		newline = yes
		add_popularity = {
			ideology = communism
			popularity = -0.10
		}
		newline = yes
		add_popularity = {
			ideology = nationalist
			popularity = -0.10
		}
		ai_chance = {
			factor = 1
		}
	}
	# No Legalization
	option = {
		name = norway.14.b
		log = "[GetDateText]: [This.GetName]: norway.14.b executed"
		add_stability = 0.03
		newline = yes
		set_party_index_to_ruling_party = yes
		set_temp_variable = { party_popularity_increase = 0.035 }
		set_temp_variable = { temp_outlook_increase = 0.02 }
		add_relative_party_popularity = yes
		newline = yes
		add_popularity = {
			ideology = nationalist
			popularity = 0.10
		}
		ai_chance = {
			factor = 1
		}
	}
}

country_event = {
	id = norway.16
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.16" }
	title = norway.16.t
	desc = norway.16.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Anti Immigration
	option = {
		name = norway.16.a
		log = "[GetDateText]: [This.GetName]: norway.16.a executed"
		add_ideas = NOR_anti_migration
		ai_chance = {
			factor = 1
		}
	}
	# Pro Immigration
	option = {
		name = norway.16.b
		log = "[GetDateText]: [This.GetName]: norway.16.b executed"
		add_ideas = NOR_pro_migration
		ai_chance = {
			factor = 1
		}
	}
}
# Focus Events
# Norway wants to increase cooperation economically
country_event = {
	id = norway.100
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.100" }
	title = norway.100.t
	desc = norway.100.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Ok
	option = {
		name = norway.100.a
		log = "[GetDateText]: [This.GetName]: norway.100.a executed"
		custom_effect_tooltip = NOR_if_they_aceept_TT
		NOR = {
			set_country_flag = NOR_UK_AGREED
		}
		effect_tooltip = {
			set_temp_variable = { receiver_nation = NOR.id }
			set_temp_variable = { sender_nation = ENG.id }
			set_improved_trade_agreement = yes
		}
		newline = yes
		effect_tooltip = {
			add_resource = {
				type = oil
				amount = 5
				state = 8
			}
			newline = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = NOR }
			set_temp_variable = { influence_target = ENG }
			change_influence_percentage = yes
			newline = yes
			set_temp_variable = { treasury_change = gdp_total }
			multiply_temp_variable = { treasury_change = -0.05 }
			modify_treasury_effect = yes
			set_temp_variable = { int_investment_change = treasury_change }
			multiply_temp_variable = { int_investment_change = -1.25 }
			modify_international_investment_effect = yes
			set_temp_variable = { debt_change = debt }
			multiply_temp_variable = { debt_change = -0.02 }
			modify_debt_effect = yes
		}
		ai_chance = {
			base = 1
			modifier = {
				has_opinion = { target = NOR value > 44 }
				add = 10
			}
		}
	}
	# No
	option = {
		name = norway.100.b
		log = "[GetDateText]: [This.GetName]: norway.100.b executed"
		ai_chance = {
			factor = 1
		}
	}
}

# They Accept
country_event = {
	id = norway.101
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.101" }
	title = norway.101.t
	desc = norway.101.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Ok
	option = {
		name = norway.101.a
		log = "[GetDateText]: [This.GetName]: norway.101.a executed"
		set_temp_variable = { receiver_nation = NOR.id }
		set_temp_variable = { sender_nation = ENG.id }
		set_improved_trade_agreement = yes
		ai_chance = {
			factor = 1
		}
	}
}
# They Reject
country_event = {
	id = norway.102
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.102" }
	title = norway.102.t
	desc = norway.102.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Ok
	option = {
		name = norway.102.a
		log = "[GetDateText]: [This.GetName]: norway.102.a executed"
		ai_chance = {
			factor = 1
		}
	}
}
# Norway Wants To Construct Infrastructure for us
country_event = {
	id = norway.103
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.103" }
	title = norway.103.t
	desc = norway.103.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Ok
	option = {
		name = norway.103.a
		log = "[GetDateText]: [This.GetName]: norway.103.a executed"
		custom_effect_tooltip = NOR_if_we_accept_TT
		newline = yes
		if = {
			limit = {
				original_tag = SWE
			}
			effect_tooltip = {
				881 = {
					add_building_construction = {
						type = infrastructure
						level = 1
					}
				}
				newline = yes
				34 = {
					add_building_construction = {
						type = infrastructure
						level = 1
					}
				}
				newline = yes
				set_temp_variable = { percent_change = 5 }
				set_temp_variable = { tag_index = NOR }
				set_temp_variable = { influence_target = SWE }
				change_influence_percentage = yes
			}
		}

		if = {
			limit = {
				original_tag = FIN
			}
			effect_tooltip = {
				104 = {
					add_building_construction = {
						type = infrastructure
						level = 1
					}
				}
				newline = yes
				102 = {
					add_building_construction = {
						type = infrastructure
						level = 1
					}
				}
				newline = yes
				set_temp_variable = { percent_change = 5 }
				set_temp_variable = { tag_index = NOR }
				set_temp_variable = { influence_target = FIN }
				change_influence_percentage = yes
			}
		}
		newline = yes
		NOR = {
			country_event = {
				id = norway.104
				days = 1
			}
		}
		ai_chance = {
			base = 1
			modifier = {
				has_opinion = { target = NOR value > 44 }
				add = 10
			}
		}
	}

	option = {
		name = norway.103.b
		log = "[GetDateText]: [This.GetName]: norway.103.b executed"
		NOR = {
			country_event = {
				id = norway.105
				days = 1
			}
		}
		ai_chance = {
			factor = 1
		}
	}
}
# They Accept!
country_event = {
	id = norway.104
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.104" }
	title = norway.104.t
	desc = norway.104.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Ok
	option = {
		name = norway.104.a
		log = "[GetDateText]: [This.GetName]: norway.104.a executed"
		if = {
			limit = {
				FROM = {
					original_tag = SWE
				}
			}
			881 = {
				add_building_construction = {
					type = infrastructure
					level = 1
				}
			}
			newline = yes
			34 = {
				add_building_construction = {
					type = infrastructure
					level = 1
				}
			}
			newline = yes
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = NOR }
			set_temp_variable = { influence_target = SWE }
			change_influence_percentage = yes
			newline = yes
			set_temp_variable = { treasury_change = -7.00 }
			modify_treasury_effect = yes
		}
		if = {
			limit = {
				FROM = {
					original_tag = FIN
				}
			}
			104 = {
				add_building_construction = {
					type = infrastructure
					level = 1
				}
			}
			newline = yes
			102 = {
				add_building_construction = {
					type = infrastructure
					level = 1
				}
			}
			newline = yes
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = NOR }
			set_temp_variable = { influence_target = FIN }
			change_influence_percentage = yes
			newline = yes
			set_temp_variable = { treasury_change = -7.00 }
			modify_treasury_effect = yes
		}
		ai_chance = {
			factor = 1
		}
	}
}

# They Reject!
country_event = {
	id = norway.105
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.105" }
	title = norway.105.t
	desc = norway.105.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Ok
	option = {
		name = norway.105.a
		log = "[GetDateText]: [This.GetName]: norway.105.a executed"
		ai_chance = {
			factor = 1
		}
	}
}
# Norway is influencing our politics
country_event = {
	id = norway.106
	immediate = { log = "[GetDateText]: [Root.GetName]: event norway.106" }
	title = norway.106.t
	desc = norway.106.d
	#fire_only_once = yes
	is_triggered_only = yes
	# Ok
	option = {
		name = norway.106.a
		log = "[GetDateText]: [This.GetName]: norway.106.a executed"
		ai_chance = {
			factor = 1
		}
	}
}